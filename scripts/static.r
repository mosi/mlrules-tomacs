#R methods to illustrate the results of the static experiment with the wnt model
#load all methods in R and follow the instructions in the readme.txt to create the plots properly


# argument = TRUE -> save plot as pdf; argument = FALSE -> show plot in R
wntPlotRuntime <- function(cond,ymax=NA) {
	#ifelse(cond,pdf("plot.pdf",width=8,height=5),dev.new(width=8,height=5))
	if (cond){
		pdf("wntRuntime.pdf",width=8,height=5)
	} else {
		dev.new(width=8,height=5)
	}
	simpleYesDG <- rowMeans(as.matrix(read.csv("./wntResults/staticWithDG/runtimes",header=FALSE)))/1000
	simpleNoDG <- rowMeans(as.matrix(read.csv("./wntResults/staticWithoutDG/runtimes",header=FALSE)))/1000
	standardYesDG <- rowMeans(as.matrix(read.csv("./wntResults/standardWithDG/runtimes",header=FALSE)))/1000
	standardNoDG <- rowMeans(as.matrix(read.csv("./wntResults/standardWithoutDG/runtimes",header=FALSE)))/1000
	
	if (is.na(ymax)) {
		ymax <- max(simpleYesDG, simpleNoDG, standardYesDG, standardNoDG)
	}
	plot(c(),xlim=c(1,10),ylim=c(0,ymax),ylab="Execution time in s",xlab="Number of cells",xaxt="n")
	axis(1,at=1:10)
	abline(h=seq(0,ymax,length=10),col="gray80")
	points(standardNoDG,col="black")
	points(standardYesDG,col="blue",pch=2)
	points(simpleNoDG, col="green",pch=3)
	points(simpleYesDG, col="red",pch=4)
	legend(0.8,ymax-ymax*0.05,horiz=FALSE,bg="white",c("Standard Simulator (A)","Standard Simulator (B)","Static Simulator (A)","Static Simulator (B)"),lty=c(NA,NA,NA,NA),lwd=c(1.4,1.4,1.4,1.4),pch=c(1,2,3,4),col=c("black","blue","green","red"))
	if(cond) {
		dev.off()
	}
}

read <- function(file) {
	as.matrix(read.csv(file,header=TRUE,sep=","))
	#as.matrix(read.csv(file,header=FALSE,sep=","))
}

getSpeciesForBoxplot <- function(files,timepoints,cols) {
	result <- matrix(0,ncol=timepoints,nrow=length(files))
	for (i in 1:length(files)) {
		if (length(cols) > 1) {
			result[i,] <- rowSums(read(files[i])[,cols])
		} else {
			result[i,] <- read(files[i])[,cols]
		}
	}
	return (result)
}

getFilesNew <- function(path,config,from,to) {
	paste(path,paste("result",config,from:to,"agg",sep="_"),sep="")
}

getYLabel <- function(y) {
	if (y == 1) {
		return("Wnt")
	}
	if (y == 2) {
		return("AxinP")
	}
	if (y == 3) {
		return("AxinU")
	}
	if (y == 4) {
		return(expression(paste("Amount of ", beta, "-catenin")))
	}
	return("")
}

wntInternal <- function(times,reps,x,config) {
	simpleYesDG <- getSpeciesForBoxplot(getFilesNew("./wntResults/staticWithDG/",config,0,reps),times,x)
	simpleNoDG <- getSpeciesForBoxplot(getFilesNew("./wntResults/staticWithoutDG/",config,0,reps),times,x)
	standardYesDG <- getSpeciesForBoxplot(getFilesNew("./wntResults/standardWithDG/",config,0,reps),times,x)
	standardNoDG <- getSpeciesForBoxplot(getFilesNew("./wntResults/standardWithoutDG/",config,0,reps),times,x)
	
	minY <- min(simpleYesDG,simpleNoDG,standardYesDG,standardNoDG)
	maxY <- max(simpleYesDG,simpleNoDG,standardYesDG,standardNoDG)

	ind <- as.integer(seq(1,times,length=min(times,10)))

	par(mar=c(5.1,4.1,4.1,14.1))
	plot(c(),main=config,xlim=c(1,times),ylim=c(minY,maxY),ylab=getYLabel(x),xlab="Simulation Time",cex.axis=0.75)
	abline(h=seq(minY,maxY,length=5),col="gray80")
	points(colMeans(standardNoDG),col="black",type="l")
	points(ind,colMeans(standardNoDG)[ind],col="black",pch=1)
	points(colMeans(standardYesDG),col="blue",type="l")
	points(ind,colMeans(standardYesDG)[ind],col="blue",pch=2)
	points(colMeans(simpleYesDG), col="green",type="l")
	points(ind,colMeans(simpleYesDG)[ind],col="green",pch=3)
	points(colMeans(simpleNoDG), col="red",type="l")
	points(ind,colMeans(simpleNoDG)[ind],col="red",pch=4)

	par(xpd=TRUE)
	legend(times*1.1,maxY,horiz=FALSE,bg="white",c("Standard Simulator (A)","Standard Simulator (B)","Static Simulator (A)","Static Simulator (B)"),lty=c(1,1,1,1),lwd=c(1,1,1,1),pch=c(1,2,3,4),col=c("black","blue","green","red"))	
	par(xpd=FALSE)
}

wntPlotSpecies <- function(reps) {
	pdf("wntPlots.pdf",width=8,height=4)
	times <- nrow(as.matrix(read.csv("./wntResults/standardWithDG/result_1_0_agg",header=TRUE)))
	
	lapply(1:10, function(y) lapply(1:4,function(x) wntInternal(times,reps-1,x,y)))

	dev.off()
}
