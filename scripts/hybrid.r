#R methods to illustrate the results of the hybrid experiment with the dicty model
#load all methods in R and follow the instructions in the readme.txt to create the plots properly

addPoints <- function(values,i,color) {
	index <- which(values[,1] == i)
	a <- values[which(values[,1] == i),2:3]
	if (length(index) == 1) {
		points(a[1],a[2],col=color,pch=19,cex=3)
	}
	if (length(index) > 1) {		
		points(a[,1],a[,2],col=color,pch=19,cex=3)
	}
}

draw <- function(config,rep,values,a,times,m,maxx,maxy) {
	x <- c(5,10,15,20,25,15,14,20)
	y <- c(10,10,10,10,10,20,25,20)

	tmp <- a
	par(oma=c(0,0,0,0),mar=c(3.5,4.1,2,1.1),mgp=c(2,1,0))
	plot(c(),xlim=c(1,maxx),ylim=c(1,maxy),xaxt="n",yaxt="n",xaxs = "i", yaxs = "i",xlab="",ylab="",main=paste("(",x[config],",",y[config],") rep =", rep, ", t =",times[a]))
	points(expand.grid(list(1:maxx,1:maxy)),col="white",pch=15,cex=3)
	colors <- gray(seq(1,0,length=m+1))
	lapply(0:m, function(x) addPoints(values[[a]],x,colors[x+1])) 
	par(mar=c(4.1,10.1,6.1,8.1),mgp=c(3,1,0))
	drawGray(m)
}

drawGray <- function(m) {
	color <- gray(seq(1.0,0,length=m+1))
	plot(c(),xlim=c(0,1),ylim=c(0,m+1),xlab="",ylab="",xaxs="i",yaxs="i",xaxt="n",yaxt="n")
	title("Number of Cells")
	lapply(0:(m), function(a) rect(0,a,1,a+1,col=color[a+1],border=NA))
	axis(side=4,at=seq(0,m+1,length=5),las=2)
	box()
}

#("dicty/8/4---CELL.csv",20,20,8,5.5)
animation <- function(config,rep,name,maxx,maxy,w,h) {
	#pdf("dictyCells.pdf",height=h,width=w)
	#dev.new(width=w,height=h)
	#par(mfrow=c(1,2))#,oma=c(0,0,0,0),mar=c(3.5,4.1,2,1.1),mgp=c(2,1,0))


	x <- as.matrix(read.csv(name,header=TRUE))
	times <- unique(x[,1])
	#times <- times[length(times)]
	values <- lapply(times,function(a) x[which(x[,1] == a),2:4])
	m <- max(x[,2])
	lapply(1:length(values),function(a) draw(config,rep,values,a,times,m,maxx,maxy))
	
	#par(mar=c(4.1,10.1,6.1,8.1),mgp=c(3,1,0))
	#drawGray(m)
	#dev.off()
}

dictyCells <- function(reps){
	x <- c(5,10,15,20,25,15,14,20)
	y <- c(10,10,10,10,10,20,25,20)

	pdf("dictyCells.pdf",height=4,width=8)
	#dev.new(width=w,height=h)
	par(mfrow=c(1,2))#,oma=c(0,0,0,0),mar=c(3.5,4.1,2,1.1),mgp=c(2,1,0))
	lapply(1:8,function(a) lapply(0:(reps-1),function(rep) animation(a,rep,paste("dictyResults/",a,"/",rep,"---CELL.csv",sep=""),x[a],y[a],8,4)))
	dev.off()
}

group <- function(campe) {
	values <- unique(campe[,1])
	unlist(lapply(values,function(x) sum(campe[which(campe[,1]==x),2])))
}

drawProteinPlot <- function(i,paths,rep=0,xmax) {
	path <- paths[i]
	aca <- as.matrix(read.csv(paste(path,rep,"---ACA.csv",sep=""),header=TRUE))
	campi <- as.matrix(read.csv(paste(path,rep,"---CAMPi.csv",sep=""),header=TRUE))	
	campe <- as.matrix(read.csv(paste(path,rep,"---CAMPe.csv",sep=""),header=TRUE))
	car1 <- as.matrix(read.csv(paste(path,rep,"---CAR1.csv",sep=""),header=TRUE))
	erk2 <- as.matrix(read.csv(paste(path,rep,"---ERK2.csv",sep=""),header=TRUE))
	pka <- as.matrix(read.csv(paste(path,rep,"---PKA.csv",sep=""),header=TRUE))
	regA <- as.matrix(read.csv(paste(path,rep,"---RegA.csv",sep=""),header=TRUE))

	#pdf("dictyPlot.pdf",height=3.5,width=8)
	#par(xpd=TRUE)
	groupedCampe <- group(campe)
	if (is.na(xmax)) {
		xmax <- nrow(aca)
	}
	ymax <- max(aca[1:xmax,2],campi[1:xmax,2],groupedCampe[1:xmax],car1[1:xmax,2],erk2[1:xmax,2],pka[1:xmax,2],regA[1:xmax,2])

	n <- seq(50,400,50)

	plot(c(),xlim=c(1,xmax),ylim=c(0,ymax),xlab="Observation",ylab="Species Amount",main=paste(path,rep,"(",n[i]," cells)",sep=""))
	#axis(1,at=seq(0,1000,200),labels=seq(0,100,20))
	points(aca[1:xmax,2],type="l",col="blue")
	points(campi[1:xmax,2],type="l",col="red")
	points(groupedCampe[1:xmax],type="l",col="green")
	points(car1[1:xmax,2],type="l",col="orange")
	points(erk2[1:xmax,2],type="l",col="yellow")
	points(pka[1:xmax,2],type="l",col="black")
	points(regA[1:xmax,2],type="l",col="violet")
	legend(0,ymax*1.2,c("ACA","CAMPi","CAMPe","CAR1","ERK2","PKA","RegA"),lty=c(1,1,1,1,1,1,1),col=c("blue","red","green","orange","yellow","black","violet"),horiz=TRUE,bty="n",cex=0.8)
	#dev.off()
}

drawProteinPlots <- function(reps,limit=NA) {
	pdf("dictyProteins.pdf",height=3.5,width=8)
	par(xpd=TRUE)

	paths <- unlist(lapply(1:8,function(a) paste("./dictyResults/",a,"/",sep="")))

	lapply(1:8,function(a) lapply(0:(reps-1), function(b) drawProteinPlot(a,paths,b,limit)))

	dev.off()
	
}

dictyPlotRuntime <- function(cond,ymax=NA) {
	if (cond) {
		pdf("dictyRuntime.pdf",width=8,height=3.5)
	} else {
		dev.new(width=8,height=3.5)
	}
	values <- as.matrix(read.csv("./dictyResults/runtimes",header=FALSE))
	
	rm <- rowMeans(values)/3600000
	if (is.na(ymax)) {
		ymax <- max(rm)
	}

	plot(c(),xlim=c(1,8),ylim=c(0,ymax),ylab="Execution time in h",xlab="Number of cells",xaxt="n",yaxt="n")
	axis(1,at=1:8,labels=seq(50,400,50))
	axis(2,at=seq(0,ymax,length=5))
	abline(h=seq(0,ymax,length=5),col="gray80")
	points(rm,col="black")

	if (cond) {
		dev.off()
	}
}