The mlrules-tomacs repository contains executables to reproduce the experiments described in the paper 
"Semantics and Efficient Simulation Algorithms of an Expressive Multi-Level Modeling Language" by Tobias Helms, Tom Warnke, Carsten Maus, and Adelinde M. Uhrmacher.
The source code of the mlrules project that has been used for these experiments is part of this repository. 
The current version of mlrules is maintained on https://git.informatik.uni-rostock.de/mosi/mlrules2.

- the model files are saved in the directory ./models
- the R scripts to create the plots and result pdfs are saved in the directory ./scripts
- all libraries are saved in the directory ./libs
- the documentation.pdf contains a documentation of ML-Rules including an explanation of the editor
- Java SE Runtime Environment 8 has to be installed to execute the programs
- the experiments generate lots of data, at least 2 GB should be free for the experiment results
- when starting a <name>Experiment.jar all existing data from previous experiments in the directory '<name>Results' are removed, i.e., make a backup if necessary before starting a new experiment
- the source code in form of a Maven project can be found in ./src

#### Introduction:
- before you can start any experiment, the repository has to be downloaded ([here](https://git.informatik.uni-rostock.de/mosi/mlrules-tomacs/repository/archive.zip?ref=master)) and extracted to a directory --- all results are saved in this directory

#### Wnt Experiment (Section 7.1):

- staticEditor.jar: the editor can be used to run the Wnt/Beta-catenin pathway model with different simulators to illustrate their runtime behavior
	- call java -jar staticEditor.jar to start the editor
	- it automatically loads the ./models/wnt.mlrj model
- staticExperiment.jar: start the experiment used to produce the runtime data
	- call java -jar staticExperiment.jar to start the experiment with default parameters
	- parameters:
		- reps: number of replications to be executed for the same simulator and the same model configuration (default: 20)
		- endTime: simulation end time (default: 300.0)
		- obs: interval of observations, i.e., every 'obs' time units, an observation of the model state is made (default: 1.0)
		- parallel: number of parallel executed replications (default: 1)
	- note: with the default parameter values, the experiment needs several hours to be executed
- scripts/static.r: contains all methods to create the runtime plot and the species amount plots based on the data produced by staticExperiment.jar
	1. start R (e.g., with RGui or RStudio)
	2. load static.r and compile it
	3. change the working directory to the directory of the staticExperiment.jar
	4a. call wntPlotRuntime(FALSE) to plot the runtimes in the R environment
	4b. call wntPlotRuntime(TRUE) to save the plot in a pdf 'wntRuntime.pdf'
		- the method has a second optional parameter to set the maximum value of the y-axis, e.g., wntPlotRuntime(FALSE,1000)
	5. call wntPlotSpecies(reps) to save the species amounts plot 'wntPlots.pdf'
		- reps -> number of replications used when executing staticExperiment.jar
		- for example, if staticExperiment.jar has been executed with default parameter values, call wntPlotSpecies(20)
		- the created pdf contains all plots - the number above each plot corresponds to the number of cells
		- note: the differences at the peak of the amount of beta-catenin is sometimes high with few cells, because the deviation is comparably high at this point 

#### Dicty Experiment (Section 7.2):

- hybridEditor.jar: the editor can be used to run the Dictyostelium discoideum amoebas model with different simulators to illustrate their runtime behavior
	- call java -jar hybridEditor.jar to start the editor
- hybridExperiment.jar: start the experiment used to produce the runtime data
	- call java -jar hybridExperiment.jar to start the experiment with default parameters
	- parameters:
		- reps: number of replications to be executed for the same simulator and the same model configuration (default: 20)
		- endTime: simulation end time (default: 5000.0)
		- obs: interval of observations, i.e., every 'obs' time units, an observation of the model state is made (default: 100.0)
		- parallel: number of parallel executed replications (default: 1)
	- note: with the default parameter values, the experiment needs several hours to be executed
- first experiment with default parameters:
	- execute java.jar hybridExperiment.jar with the default paraemeters
	-> the results can be used to illustrate the aggregation of the amoebas
	1. start R, load ./scripts/hybrid.r and compile it
	2. change the working directory to the directory of the hybridExperiment.jar
	3a. call dictyPlotRuntime(FALSE) to plot the runtimes in the R environment
	3b. call dictyPlotRuntime(TRUE) to save the plot in a pdf 'dictyRuntime.pdf'
		- the method has a second optional parameter to set the maximum value of the y-axis, e.g., dictyPlotRuntime(FALSE,3)
		- note: the y-axis shows runtimes in hours, not seconds
	4. call dictyCells(reps) to create the 'dictyCells.pdf' containing the aggregation plots of the cells for each observation, replication, and cell numbering
		- the circles represent the number of cells at each position
		- the plot size is not always suitable so that empty space occurs in the plot that does not have any meaning
- second experiment with adapted parameters:
	- since the observation interval of the first experiment is too high to observe the periodic behavior of the proteins, a second experiment with adapted parameters should be executed
	- note: reducing the observation interval in the first experiment would result in too much data (several GB)
	- use the following parameters:
		- reps: 20
		- endTime: 100
		- obs: 0.1
		- parallel: depends on computer
	- example call: java -jar hybridExperiment.jar -reps 20 -endTime 100 -obs 0.1
	-> NOTE: when starting the experiment all existing data from previous experiments in the directory 'dictyResults' are removed, i.e., make a backup if necessary before starting a new experiment
	1. start R, load ./scripts/hybrid.r and compile it
	2. change the working directory to the directory of the hybridExperiment.jar
	3. call drawProteinPlots(reps) to save the protein amount plots in 'hybridPlots.pdf'
		- reps -> number of replications used when executing hybridExperiment.jar
		- for example, if hybridExperiment.jar has been executed with default parameter values, call dictyPlotSpecies(20)
		- the method has a second optional parameter setting the maximum value of the y-axis, e.g., drawProteinPlots(20,1000000)
		- the created pdf contains all plots - the number above each plot corresponds to the number of cells
		
#### Mito Experiment (Section 7.3):

- linkEditor.jar: the editor can be used to run the mitochondria model with different simulators to illustrate their runtime behavior
	- call java -jar linkEditor.jar to start the editor
- linkExperiment.jar: start the experiment used to produce the runtime data
	- call java -jar linkExperiment.jar to start the experiment with default parameters
	- parameters:
		- reps: number of replications to be executed for the same simulator and the same model configuration (default: 20)
		- endTime: simulation end time (default: 100.0)
		- obs: interval of observations, i.e., every 'obs' time units, an observation of the model state is made (default: 1.0)
		- parallel: number of parallel executed replications (default: 1)
	- note: with the default parameter values, the experiment needs several hours to be executed
- scripts/link.r: contains all methods to create the runtime plot and the species amount plots of the linkExperiment.jar
	1. start R (e.g., with RGui or RStudio)
	2. load link.r and compile it
	3. change the working directory to the directory of the linkExperiment.jar
	4a. call mitoPlotRuntime(FALSE) to plot the runtimes in the R environment
	4b. call mitoPlotRuntime(TRUE) to save the plot in a pdf 'mitoRuntime.pdf'
		- the method has a second optional parameter setting the maximum value of the y-axis, e.g., mitoPlotRuntime(FALSE,1000)
	5. call mitoPlotSpecies(reps) to save the species amounts plot 'mitoPlots.pdf'
		- reps -> number of replications used when executing linkExperiment.jar
		- for example, if mitoExperiment has been executed with default parameter values, call mitoPlotSpecies(20)
		- the created pdf contains all plots - the number above each plot corresponds to the number of mitochondria
		- note: with few mitochondria, the deviation of the number of linked mitochondria without a linked tubule is comparably high, because the mitochondria cannot move in this model, so that the results strongly depend on their initial distribution