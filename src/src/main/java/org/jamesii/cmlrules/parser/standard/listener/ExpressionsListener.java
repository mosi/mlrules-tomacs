/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesBaseListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountExprContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountIDContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountINTContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AndOrContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ApplicationContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AssignContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AttributesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.BoolExprContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.CountShortContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.EmptySolutionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ExpValueContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FalseContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FreeContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FunctionCallContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.GuardContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.IdContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.IfThenElseContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.IntContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.MinusContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.NameContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.NotContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ParenContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.PlusContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.RoofContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SingleAssignContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SpeciesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SpeciesExprContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.StringContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SubSpeciesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.TrueContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.TupleAssignContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.WhereContext;
import org.jamesii.cmlrules.parser.standard.functions.Function;
import org.jamesii.cmlrules.parser.standard.nodes.FunctionCallNode;
import org.jamesii.cmlrules.parser.standard.nodes.MLRulesAddNode;
import org.jamesii.cmlrules.parser.standard.nodes.SpeciesAmountNode;
import org.jamesii.cmlrules.parser.standard.nodes.SpeciesPatternNode;
import org.jamesii.cmlrules.parser.standard.nodes.TupleNode;
import org.jamesii.cmlrules.util.Assignment;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.cmlrules.util.Nu;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.bool.AndNode;
import org.jamesii.core.math.parsetree.bool.IsEqualNode;
import org.jamesii.core.math.parsetree.bool.IsGreaterNode;
import org.jamesii.core.math.parsetree.bool.IsGreaterOrEqualNode;
import org.jamesii.core.math.parsetree.bool.IsLowerNode;
import org.jamesii.core.math.parsetree.bool.IsLowerOrEqualNode;
import org.jamesii.core.math.parsetree.bool.IsNotEqualNode;
import org.jamesii.core.math.parsetree.bool.NotNode;
import org.jamesii.core.math.parsetree.bool.OrNode;
import org.jamesii.core.math.parsetree.control.IfThenElseNode;
import org.jamesii.core.math.parsetree.math.DivNode;
import org.jamesii.core.math.parsetree.math.MinusNode;
import org.jamesii.core.math.parsetree.math.MultNode;
import org.jamesii.core.math.parsetree.math.PowerNode;
import org.jamesii.core.math.parsetree.variables.Identifier;

public class ExpressionsListener extends MLRulesBaseListener {

  private final MLEnvironment env;

  private final Map<ParserRuleContext, Object> data = new HashMap<>();

  public ExpressionsListener(MLEnvironment env) {
    this.env = env;
  }

  protected MLEnvironment getEnv() {
    return env;
  }

  @Override
  public void exitTrue(TrueContext ctx) {
    data.put(ctx, new ValueNode<Boolean>(true));
  }

  @Override
  public void exitFalse(FalseContext ctx) {
    data.put(ctx, new ValueNode<Boolean>(false));
  }

  @Override
  public void exitFree(FreeContext ctx) {
    data.put(ctx, new ValueNode<Nu>(Nu.FREE));
  }

  @Override
  public void exitString(StringContext ctx) {
    data.put(
        ctx,
        new ValueNode<String>(ctx.getText().substring(1,
            ctx.getText().length() - 1)));
  }

  @Override
  public void exitInt(IntContext ctx) {
    data.put(ctx, new ValueNode<Double>(Double.valueOf(ctx.getText())));
  }

  @Override
  public void exitReal(@NotNull MLRulesParser.RealContext ctx) {
    data.put(ctx, new ValueNode<Double>(Double.valueOf(ctx.getText())));
  }

  @Override
  public void exitId(IdContext ctx) {
    data.put(ctx, new Identifier<String>(ctx.getText()));
  }

  @Override
  public void exitExpValue(ExpValueContext ctx) {
    data.put(ctx, data.remove(ctx.getChild(0)));
  }

  @Override
  public void exitPlus(PlusContext ctx) {
    data.put(ctx, new MLRulesAddNode((Node) data.remove(ctx.getChild(0)),
        (Node) data.remove(ctx.getChild(2))));
  }

  @Override
  public void exitMinus(MinusContext ctx) {
    data.put(ctx, new MinusNode((Node) data.remove(ctx.getChild(0)),
        (Node) data.remove(ctx.getChild(2))));
  }

  @Override
  public void exitMultDiv(@NotNull MLRulesParser.MultDivContext ctx) {
    Node left = (Node) data.remove(ctx.getChild(0));
    Node right = (Node) data.remove(ctx.getChild(2));

    data.put(ctx, ctx.DIV() == null ? new MultNode(left, right) : new DivNode(
        left, right));
  }

  @Override
  public void exitRoof(RoofContext ctx) {
    data.put(ctx, new PowerNode((Node) data.remove(ctx.getChild(0)),
        (Node) data.remove(ctx.getChild(3))));
  }

  @Override
  public void exitAndOr(AndOrContext ctx) {
    Node left = (Node) data.remove(ctx.getChild(0));
    Node right = (Node) data.remove(ctx.getChild(2));

    data.put(ctx, ctx.OR() == null ? new AndNode(left, right) : new OrNode(
        left, right));
  }

  @Override
  public void exitParen(ParenContext ctx) {
    data.put(ctx, data.remove(ctx.getChild(1)));
  }

  @Override
  public void exitBoolExpr(BoolExprContext ctx) {
    Node left = (Node) data.remove(ctx.getChild(1));
    Node right = (Node) data.remove(ctx.getChild(3));

    Node node;
    if (ctx.EQUALS() != null) {
      node = new IsEqualNode(left, right);
    } else if (ctx.N_EQUALS() != null) {
      node = new IsNotEqualNode(left, right);
    } else if (ctx.GT() != null) {
      node = new IsGreaterNode(left, right);
    } else if (ctx.GT_EQ() != null) {
      node = new IsGreaterOrEqualNode(left, right);
    } else if (ctx.LT() != null) {
      node = new IsLowerNode(left, right);
    } else if (ctx.LT_EQ() != null) {
      node = new IsLowerOrEqualNode(left, right);
    } else {
      throw new SemanticsException(ctx,
          "boolean expression was not able to determine correct operation");
    }

    data.put(ctx, node);
  }

  @Override
  public void exitNot(NotContext ctx) {
    data.put(ctx, new NotNode((Node) data.remove(ctx.getChild(1))));
  }

  @Override
  public void exitMinusOne(@NotNull MLRulesParser.MinusOneContext ctx) {
    data.put(ctx, new MultNode((Node) data.remove(ctx.getChild(1)),
        new ValueNode<Integer>(-1)));
  }

  @Override
  public void exitIfThenElse(IfThenElseContext ctx) {
    data.put(
        ctx,
        new IfThenElseNode((Node) data.remove(ctx.getChild(1)), (Node) data
            .remove(ctx.getChild(3)), (Node) data.remove(ctx.getChild(5))));
  }

  @Override
  public void exitTuple(@NotNull MLRulesParser.TupleContext ctx) {
    List<INode> nodes = new ArrayList<>();
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof ParserRuleContext)
        nodes.add((Node) data.remove(ctx.getChild(i)));
    }

    data.put(ctx, new TupleNode(nodes));
  }

  @Override
  public void exitName(NameContext ctx) {
    if (ctx.ID_SPECIES() != null) {
      data.put(ctx, new ValueNode<String>(ctx.ID_SPECIES().getText()));
    } else {
      data.put(ctx, data.remove(ctx.getChild(1)));
    }
  }

  @Override
  public void exitSpecies(SpeciesContext ctx) {
    AmountContext aCtx = null;
    NameContext nCtx = null;
    AttributesContext attCtx = null;
    SubSpeciesContext subCtx = null;
    GuardContext gCtx = null;
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof AmountContext) {
        aCtx = (AmountContext) ctx.getChild(i);
      } else if (ctx.getChild(i) instanceof NameContext) {
        nCtx = (NameContext) ctx.getChild(i);
      } else if (ctx.getChild(i) instanceof AttributesContext) {
        attCtx = (AttributesContext) ctx.getChild(i);
      } else if (ctx.getChild(i) instanceof SubSpeciesContext) {
        subCtx = (SubSpeciesContext) ctx.getChild(i);
      } else if (ctx.getChild(i) instanceof GuardContext) {
        gCtx = (GuardContext) ctx.getChild(i);
      }
    }

    Node amount = aCtx == null ? new ValueNode<Integer>(1) : (Node) data
        .remove(aCtx);
    Node type = (Node) data.remove(nCtx);

    @SuppressWarnings("unchecked")
    List<Node> attsList = attCtx == null ? new ArrayList<>()
        : (List<Node>) data.remove(attCtx);
    Node[] atts = attsList.toArray(new Node[attsList.size()]);

    Node subNode = subCtx == null ? new ValueNode<Map<Species, Species>>(
        new HashMap<>()) : (Node) data.remove(subCtx);

    Node guardNode = gCtx == null ? null : (Node) data.remove(gCtx);

    data.put(ctx, new SpeciesPatternNode(amount, type, atts, subNode,
        guardNode, ctx.ID() != null ? ctx.ID().getText() : "$$$"));
  }

  @Override
  public void exitSpeciesExpr(SpeciesExprContext ctx) {
    data.put(ctx, data.remove(ctx.getChild(0)));
  }

  @Override
  public void exitEmptySolution(EmptySolutionContext ctx) {
    data.put(ctx, new ValueNode<Map<Species, Species>>(new HashMap<>()));
  }

  @Override
  public void exitApplication(ApplicationContext ctx) {
    Node node = (Node) data.remove(ctx.getChild(1));
    @SuppressWarnings("unchecked")
    List<Node> parameter = (List<Node>) data.remove(ctx.getChild(3));

    getData().put(ctx, new FunctionCallNode(node, new ArrayList<>(parameter)));
  }

  @Override
  public void exitAmountExpr(AmountExprContext ctx) {
    data.put(ctx, data.remove(ctx.getChild(1)));
  }

  @Override
  public void exitAmountID(AmountIDContext ctx) {
    data.put(ctx, new Identifier<String>(ctx.getText()));
  }

  @Override
  public void exitAmountINT(AmountINTContext ctx) {
    data.put(ctx, new ValueNode<Integer>(Integer.valueOf(ctx.getText())));
  }

  @Override
  public void exitSubSpecies(SubSpeciesContext ctx) {
    data.put(ctx, data.remove(ctx.getChild(1)));
  }

  @Override
  public void exitAttributes(AttributesContext ctx) {
    List<INode> atts = new ArrayList<>();
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof ParserRuleContext) {
        Node n = (Node) data.remove(ctx.getChild(i));
        if (n instanceof TupleNode) {
          TupleNode t = (TupleNode) n;
          atts.addAll((Collection<? extends INode>) t.getChildren());
        } else {
          atts.add(n);
        }
      }
    }
    data.put(ctx, atts);
  }

  @Override
  public void exitFunctionCall(FunctionCallContext ctx) {
    String name = ctx.ID().getText();

    Function f = (Function) env.getValue(name);
    if (f == null) {
      f = new Function(name, null);
    }

    List<Node> nodes = new ArrayList<>();
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof RuleContext) {
        nodes.add((Node) data.remove(ctx.getChild(i)));
      }
    }

    data.put(ctx, new FunctionCallNode(new ValueNode<Function>(f), nodes));
  }

  @Override
  public void exitCountShort(CountShortContext ctx) {
    data.put(ctx, new SpeciesAmountNode(ctx.ID().getText()));
  }

  public Map<ParserRuleContext, Object> getData() {
    return data;
  }

  @SuppressWarnings("unchecked")
  @Override
  public void exitAssign(AssignContext ctx) {
    getData().put(
        ctx,
        new Assignment((List<String>) getData().remove(ctx.getChild(0)),
            (Node) getData().remove(ctx.getChild(2))));
  }

  @Override
  public void exitTupleAssign(TupleAssignContext ctx) {
    getData().put(ctx,
        ctx.ID().stream().map(id -> id.getText()).collect(Collectors.toList()));
  }

  @Override
  public void exitSingleAssign(SingleAssignContext ctx) {
    getData().put(ctx, Arrays.asList(ctx.getText()));
  }

  @Override
  public void exitWhere(WhereContext ctx) {
    List<Assignment> assignments = new ArrayList<>();
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof RuleContext) {
        assignments.add((Assignment) getData().remove(ctx.getChild(i)));
      }
    }
    getData().put(ctx, assignments);
  }

  @Override
  public void exitGuard(GuardContext ctx) {
    data.put(ctx, data.remove(ctx.getChild(1)));
  }

}
