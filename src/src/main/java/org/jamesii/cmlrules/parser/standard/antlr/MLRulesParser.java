// Generated from MLRules.g4 by ANTLR 4.3
package org.jamesii.cmlrules.parser.standard.antlr;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MLRulesParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMMENT_SINGLE=1, COMMENT_MULTI=2, WS=3, AT=4, ATTIME=5, ATEACH=6, INIT=7, 
		TYPE_SEPERATOR=8, ARROW=9, LET=10, IN=11, WHERE=12, TYPE_REAL=13, TYPE_BOOL=14, 
		TYPE_STRING=15, TYPE_SPECIES=16, TYPE_PATTERN=17, TYPE_TUPLE=18, TYPE_SOL=19, 
		TYPE_LINK=20, IF=21, THEN=22, ELSE=23, FOR=24, WHILE=25, UNTIL=26, LENGTH=27, 
		ADD=28, FREE=29, ASSIGN=30, DOT=31, COMMA=32, COLON=33, SEMI=34, L_PAREN=35, 
		R_PAREN=36, L_BRAKET=37, R_BRAKET=38, L_CURLY=39, R_CURLY=40, PLUS=41, 
		MINUS=42, MULT=43, DIV=44, ROOF=45, EQUALS=46, N_EQUALS=47, LT=48, LT_EQ=49, 
		GT=50, GT_EQ=51, TRUE=52, FALSE=53, AND=54, OR=55, NOT=56, LAMBDA=57, 
		INT=58, REAL=59, BOOL=60, ID_SPECIES=61, ID=62, STRING=63, NU=64, COUNT=65, 
		QUESTION=66, ROOT=67;
	public static final String[] tokenNames = {
		"<INVALID>", "COMMENT_SINGLE", "COMMENT_MULTI", "WS", "'@'", "'@EXACT'", 
		"'@EACH'", "'>>INIT'", "'::'", "'->'", "'let'", "'in'", "'where'", "'num'", 
		"'bool'", "'string'", "'species'", "'pattern'", "'tuple'", "'sol'", "'link'", 
		"'if'", "'then'", "'else'", "'for'", "'while'", "'until'", "'length'", 
		"'add'", "'free'", "'='", "'.'", "','", "':'", "';'", "'('", "')'", "'['", 
		"']'", "'{'", "'}'", "'+'", "'-'", "'*'", "'/'", "'^'", "'=='", "'!='", 
		"'<'", "'<='", "'>'", "'>='", "'true'", "'false'", "'&&'", "'||'", "'!'", 
		"'\\'", "INT", "REAL", "BOOL", "ID_SPECIES", "ID", "STRING", "NU", "'#'", 
		"'?'", "'$$ROOT$$'"
	};
	public static final int
		RULE_model = 0, RULE_preamble = 1, RULE_constant = 2, RULE_speciesDefinition = 3, 
		RULE_initialSolution = 4, RULE_mlrule = 5, RULE_reactants = 6, RULE_products = 7, 
		RULE_rate = 8, RULE_expression = 9, RULE_species = 10, RULE_name = 11, 
		RULE_attributes = 12, RULE_subSpecies = 13, RULE_guard = 14, RULE_amount = 15, 
		RULE_value = 16, RULE_where = 17, RULE_assign = 18, RULE_assignName = 19, 
		RULE_function = 20, RULE_typeDefinition = 21, RULE_type = 22, RULE_functionDefinition = 23, 
		RULE_parameter = 24, RULE_atomicParameter = 25;
	public static final String[] ruleNames = {
		"model", "preamble", "constant", "speciesDefinition", "initialSolution", 
		"mlrule", "reactants", "products", "rate", "expression", "species", "name", 
		"attributes", "subSpecies", "guard", "amount", "value", "where", "assign", 
		"assignName", "function", "typeDefinition", "type", "functionDefinition", 
		"parameter", "atomicParameter"
	};

	@Override
	public String getGrammarFileName() { return "MLRules.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MLRulesParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ModelContext extends ParserRuleContext {
		public PreambleContext preamble(int i) {
			return getRuleContext(PreambleContext.class,i);
		}
		public List<PreambleContext> preamble() {
			return getRuleContexts(PreambleContext.class);
		}
		public TerminalNode EOF() { return getToken(MLRulesParser.EOF, 0); }
		public List<MlruleContext> mlrule() {
			return getRuleContexts(MlruleContext.class);
		}
		public MlruleContext mlrule(int i) {
			return getRuleContext(MlruleContext.class,i);
		}
		public InitialSolutionContext initialSolution() {
			return getRuleContext(InitialSolutionContext.class,0);
		}
		public ModelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_model; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterModel(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitModel(this);
		}
	}

	public final ModelContext model() throws RecognitionException {
		ModelContext _localctx = new ModelContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_model);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(55);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID_SPECIES || _la==ID) {
				{
				{
				setState(52); preamble();
				}
				}
				setState(57);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(58); initialSolution();
			setState(60); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(59); mlrule();
				}
				}
				setState(62); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << L_PAREN) | (1L << INT) | (1L << ID_SPECIES) | (1L << ID))) != 0) );
			setState(64); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PreambleContext extends ParserRuleContext {
		public ConstantContext constant() {
			return getRuleContext(ConstantContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(MLRulesParser.SEMI, 0); }
		public SpeciesDefinitionContext speciesDefinition() {
			return getRuleContext(SpeciesDefinitionContext.class,0);
		}
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public PreambleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_preamble; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterPreamble(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitPreamble(this);
		}
	}

	public final PreambleContext preamble() throws RecognitionException {
		PreambleContext _localctx = new PreambleContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_preamble);
		try {
			setState(73);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(66); speciesDefinition();
				setState(67); match(SEMI);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(69); constant();
				setState(70); match(SEMI);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(72); function();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public TerminalNode COLON() { return getToken(MLRulesParser.COLON, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitConstant(this);
		}
	}

	public final ConstantContext constant() throws RecognitionException {
		ConstantContext _localctx = new ConstantContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_constant);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(75); match(ID);
			setState(76); match(COLON);
			setState(77); expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SpeciesDefinitionContext extends ParserRuleContext {
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public TerminalNode INT() { return getToken(MLRulesParser.INT, 0); }
		public TerminalNode ID_SPECIES() { return getToken(MLRulesParser.ID_SPECIES, 0); }
		public SpeciesDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_speciesDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterSpeciesDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitSpeciesDefinition(this);
		}
	}

	public final SpeciesDefinitionContext speciesDefinition() throws RecognitionException {
		SpeciesDefinitionContext _localctx = new SpeciesDefinitionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_speciesDefinition);
		try {
			setState(86);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(79); match(ID_SPECIES);
				setState(80); match(L_PAREN);
				setState(81); match(R_PAREN);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(82); match(ID_SPECIES);
				setState(83); match(L_PAREN);
				setState(84); match(INT);
				setState(85); match(R_PAREN);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitialSolutionContext extends ParserRuleContext {
		public TerminalNode L_BRAKET() { return getToken(MLRulesParser.L_BRAKET, 0); }
		public TerminalNode INIT() { return getToken(MLRulesParser.INIT, 0); }
		public TerminalNode SEMI() { return getToken(MLRulesParser.SEMI, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode R_BRAKET() { return getToken(MLRulesParser.R_BRAKET, 0); }
		public InitialSolutionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_initialSolution; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterInitialSolution(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitInitialSolution(this);
		}
	}

	public final InitialSolutionContext initialSolution() throws RecognitionException {
		InitialSolutionContext _localctx = new InitialSolutionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_initialSolution);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(88); match(INIT);
			setState(89); match(L_BRAKET);
			setState(90); expression(0);
			setState(91); match(R_BRAKET);
			setState(92); match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MlruleContext extends ParserRuleContext {
		public TerminalNode AT() { return getToken(MLRulesParser.AT, 0); }
		public TerminalNode SEMI() { return getToken(MLRulesParser.SEMI, 0); }
		public TerminalNode ATEACH() { return getToken(MLRulesParser.ATEACH, 0); }
		public WhereContext where() {
			return getRuleContext(WhereContext.class,0);
		}
		public RateContext rate() {
			return getRuleContext(RateContext.class,0);
		}
		public TerminalNode ATTIME() { return getToken(MLRulesParser.ATTIME, 0); }
		public TerminalNode ARROW() { return getToken(MLRulesParser.ARROW, 0); }
		public ReactantsContext reactants() {
			return getRuleContext(ReactantsContext.class,0);
		}
		public ProductsContext products() {
			return getRuleContext(ProductsContext.class,0);
		}
		public MlruleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mlrule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterMlrule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitMlrule(this);
		}
	}

	public final MlruleContext mlrule() throws RecognitionException {
		MlruleContext _localctx = new MlruleContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_mlrule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(94); reactants();
			setState(95); match(ARROW);
			setState(96); products();
			setState(97);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << AT) | (1L << ATTIME) | (1L << ATEACH))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			setState(98); rate();
			setState(100);
			_la = _input.LA(1);
			if (_la==WHERE) {
				{
				setState(99); where();
				}
			}

			setState(102); match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReactantsContext extends ParserRuleContext {
		public SpeciesContext species(int i) {
			return getRuleContext(SpeciesContext.class,i);
		}
		public List<SpeciesContext> species() {
			return getRuleContexts(SpeciesContext.class);
		}
		public List<TerminalNode> PLUS() { return getTokens(MLRulesParser.PLUS); }
		public TerminalNode PLUS(int i) {
			return getToken(MLRulesParser.PLUS, i);
		}
		public ReactantsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reactants; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterReactants(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitReactants(this);
		}
	}

	public final ReactantsContext reactants() throws RecognitionException {
		ReactantsContext _localctx = new ReactantsContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_reactants);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(104); species();
			setState(109);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==PLUS) {
				{
				{
				setState(105); match(PLUS);
				setState(106); species();
				}
				}
				setState(111);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProductsContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ProductsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_products; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterProducts(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitProducts(this);
		}
	}

	public final ProductsContext products() throws RecognitionException {
		ProductsContext _localctx = new ProductsContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_products);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(113);
			_la = _input.LA(1);
			if (((((_la - 21)) & ~0x3f) == 0 && ((1L << (_la - 21)) & ((1L << (IF - 21)) | (1L << (FREE - 21)) | (1L << (L_PAREN - 21)) | (1L << (L_BRAKET - 21)) | (1L << (MINUS - 21)) | (1L << (LT - 21)) | (1L << (TRUE - 21)) | (1L << (FALSE - 21)) | (1L << (NOT - 21)) | (1L << (INT - 21)) | (1L << (REAL - 21)) | (1L << (ID_SPECIES - 21)) | (1L << (ID - 21)) | (1L << (STRING - 21)) | (1L << (COUNT - 21)))) != 0)) {
				{
				setState(112); expression(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RateContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public RateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterRate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitRate(this);
		}
	}

	public final RateContext rate() throws RecognitionException {
		RateContext _localctx = new RateContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_rate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(115); expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BoolExprContext extends ExpressionContext {
		public TerminalNode EQUALS() { return getToken(MLRulesParser.EQUALS, 0); }
		public TerminalNode N_EQUALS() { return getToken(MLRulesParser.N_EQUALS, 0); }
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public TerminalNode LT_EQ() { return getToken(MLRulesParser.LT_EQ, 0); }
		public TerminalNode LT() { return getToken(MLRulesParser.LT, 0); }
		public TerminalNode GT() { return getToken(MLRulesParser.GT, 0); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode GT_EQ() { return getToken(MLRulesParser.GT_EQ, 0); }
		public BoolExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterBoolExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitBoolExpr(this);
		}
	}
	public static class SpeciesExprContext extends ExpressionContext {
		public SpeciesContext species() {
			return getRuleContext(SpeciesContext.class,0);
		}
		public SpeciesExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterSpeciesExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitSpeciesExpr(this);
		}
	}
	public static class EmptySolutionContext extends ExpressionContext {
		public TerminalNode L_BRAKET() { return getToken(MLRulesParser.L_BRAKET, 0); }
		public TerminalNode R_BRAKET() { return getToken(MLRulesParser.R_BRAKET, 0); }
		public EmptySolutionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterEmptySolution(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitEmptySolution(this);
		}
	}
	public static class MultDivContext extends ExpressionContext {
		public TerminalNode MULT() { return getToken(MLRulesParser.MULT, 0); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode DIV() { return getToken(MLRulesParser.DIV, 0); }
		public MultDivContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterMultDiv(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitMultDiv(this);
		}
	}
	public static class TupleContext extends ExpressionContext {
		public List<TerminalNode> COMMA() { return getTokens(MLRulesParser.COMMA); }
		public TerminalNode LT() { return getToken(MLRulesParser.LT, 0); }
		public TerminalNode GT() { return getToken(MLRulesParser.GT, 0); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode COMMA(int i) {
			return getToken(MLRulesParser.COMMA, i);
		}
		public TupleContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterTuple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitTuple(this);
		}
	}
	public static class ExpValueContext extends ExpressionContext {
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public ExpValueContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterExpValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitExpValue(this);
		}
	}
	public static class MinusOneContext extends ExpressionContext {
		public TerminalNode MINUS() { return getToken(MLRulesParser.MINUS, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public MinusOneContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterMinusOne(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitMinusOne(this);
		}
	}
	public static class NotContext extends ExpressionContext {
		public TerminalNode NOT() { return getToken(MLRulesParser.NOT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public NotContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterNot(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitNot(this);
		}
	}
	public static class RoofContext extends ExpressionContext {
		public TerminalNode ROOF() { return getToken(MLRulesParser.ROOF, 0); }
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public RoofContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterRoof(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitRoof(this);
		}
	}
	public static class CountShortContext extends ExpressionContext {
		public TerminalNode COUNT() { return getToken(MLRulesParser.COUNT, 0); }
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public CountShortContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterCountShort(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitCountShort(this);
		}
	}
	public static class IfThenElseContext extends ExpressionContext {
		public TerminalNode ELSE() { return getToken(MLRulesParser.ELSE, 0); }
		public TerminalNode IF() { return getToken(MLRulesParser.IF, 0); }
		public TerminalNode THEN() { return getToken(MLRulesParser.THEN, 0); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public IfThenElseContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterIfThenElse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitIfThenElse(this);
		}
	}
	public static class FunctionCallContext extends ExpressionContext {
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public List<TerminalNode> COMMA() { return getTokens(MLRulesParser.COMMA); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode COMMA(int i) {
			return getToken(MLRulesParser.COMMA, i);
		}
		public FunctionCallContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterFunctionCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitFunctionCall(this);
		}
	}
	public static class PlusContext extends ExpressionContext {
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode PLUS() { return getToken(MLRulesParser.PLUS, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public PlusContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterPlus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitPlus(this);
		}
	}
	public static class ApplicationContext extends ExpressionContext {
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public AttributesContext attributes() {
			return getRuleContext(AttributesContext.class,0);
		}
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ApplicationContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterApplication(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitApplication(this);
		}
	}
	public static class AndOrContext extends ExpressionContext {
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode AND() { return getToken(MLRulesParser.AND, 0); }
		public TerminalNode OR() { return getToken(MLRulesParser.OR, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public AndOrContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterAndOr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitAndOr(this);
		}
	}
	public static class MinusContext extends ExpressionContext {
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode MINUS() { return getToken(MLRulesParser.MINUS, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public MinusContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterMinus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitMinus(this);
		}
	}
	public static class ParenContext extends ExpressionContext {
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ParenContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterParen(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitParen(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 18;
		enterRecursionRule(_localctx, 18, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(175);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				{
				_localctx = new MinusOneContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(118); match(MINUS);
				setState(119); expression(16);
				}
				break;

			case 2:
				{
				_localctx = new NotContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(120); match(NOT);
				setState(121); expression(15);
				}
				break;

			case 3:
				{
				_localctx = new ApplicationContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(122); match(L_PAREN);
				setState(123); expression(0);
				setState(124); match(R_PAREN);
				setState(125); attributes();
				}
				break;

			case 4:
				{
				_localctx = new IfThenElseContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(127); match(IF);
				setState(128); expression(0);
				setState(129); match(THEN);
				setState(130); expression(0);
				setState(131); match(ELSE);
				setState(132); expression(0);
				}
				break;

			case 5:
				{
				_localctx = new BoolExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(134); match(L_PAREN);
				setState(135); expression(0);
				setState(136);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUALS) | (1L << N_EQUALS) | (1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(137); expression(0);
				setState(138); match(R_PAREN);
				}
				break;

			case 6:
				{
				_localctx = new TupleContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(140); match(LT);
				setState(149);
				_la = _input.LA(1);
				if (((((_la - 21)) & ~0x3f) == 0 && ((1L << (_la - 21)) & ((1L << (IF - 21)) | (1L << (FREE - 21)) | (1L << (L_PAREN - 21)) | (1L << (L_BRAKET - 21)) | (1L << (MINUS - 21)) | (1L << (LT - 21)) | (1L << (TRUE - 21)) | (1L << (FALSE - 21)) | (1L << (NOT - 21)) | (1L << (INT - 21)) | (1L << (REAL - 21)) | (1L << (ID_SPECIES - 21)) | (1L << (ID - 21)) | (1L << (STRING - 21)) | (1L << (COUNT - 21)))) != 0)) {
					{
					setState(141); expression(0);
					setState(146);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(142); match(COMMA);
						setState(143); expression(0);
						}
						}
						setState(148);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(151); match(GT);
				}
				break;

			case 7:
				{
				_localctx = new FunctionCallContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(152); match(ID);
				setState(153); match(L_PAREN);
				setState(162);
				_la = _input.LA(1);
				if (((((_la - 21)) & ~0x3f) == 0 && ((1L << (_la - 21)) & ((1L << (IF - 21)) | (1L << (FREE - 21)) | (1L << (L_PAREN - 21)) | (1L << (L_BRAKET - 21)) | (1L << (MINUS - 21)) | (1L << (LT - 21)) | (1L << (TRUE - 21)) | (1L << (FALSE - 21)) | (1L << (NOT - 21)) | (1L << (INT - 21)) | (1L << (REAL - 21)) | (1L << (ID_SPECIES - 21)) | (1L << (ID - 21)) | (1L << (STRING - 21)) | (1L << (COUNT - 21)))) != 0)) {
					{
					setState(154); expression(0);
					setState(159);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(155); match(COMMA);
						setState(156); expression(0);
						}
						}
						setState(161);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(164); match(R_PAREN);
				}
				break;

			case 8:
				{
				_localctx = new ParenContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(165); match(L_PAREN);
				setState(166); expression(0);
				setState(167); match(R_PAREN);
				}
				break;

			case 9:
				{
				_localctx = new SpeciesExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(169); species();
				}
				break;

			case 10:
				{
				_localctx = new EmptySolutionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(170); match(L_BRAKET);
				setState(171); match(R_BRAKET);
				}
				break;

			case 11:
				{
				_localctx = new ExpValueContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(172); value();
				}
				break;

			case 12:
				{
				_localctx = new CountShortContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(173); match(COUNT);
				setState(174); match(ID);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(197);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(195);
					switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
					case 1:
						{
						_localctx = new MultDivContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(177);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(178);
						_la = _input.LA(1);
						if ( !(_la==MULT || _la==DIV) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(179); expression(14);
						}
						break;

					case 2:
						{
						_localctx = new PlusContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(180);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(181); match(PLUS);
						setState(182); expression(13);
						}
						break;

					case 3:
						{
						_localctx = new MinusContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(183);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(184); match(MINUS);
						setState(185); expression(12);
						}
						break;

					case 4:
						{
						_localctx = new AndOrContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(186);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(187);
						_la = _input.LA(1);
						if ( !(_la==AND || _la==OR) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(188); expression(10);
						}
						break;

					case 5:
						{
						_localctx = new RoofContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(189);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(190); match(ROOF);
						setState(191); match(L_PAREN);
						setState(192); expression(0);
						setState(193); match(R_PAREN);
						}
						break;
					}
					} 
				}
				setState(199);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class SpeciesContext extends ParserRuleContext {
		public GuardContext guard() {
			return getRuleContext(GuardContext.class,0);
		}
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public SubSpeciesContext subSpecies() {
			return getRuleContext(SubSpeciesContext.class,0);
		}
		public AttributesContext attributes() {
			return getRuleContext(AttributesContext.class,0);
		}
		public AmountContext amount() {
			return getRuleContext(AmountContext.class,0);
		}
		public TerminalNode COLON() { return getToken(MLRulesParser.COLON, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public SpeciesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_species; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterSpecies(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitSpecies(this);
		}
	}

	public final SpeciesContext species() throws RecognitionException {
		SpeciesContext _localctx = new SpeciesContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_species);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(201);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				{
				setState(200); amount();
				}
				break;
			}
			setState(203); name();
			setState(205);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				{
				setState(204); attributes();
				}
				break;
			}
			setState(208);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				{
				setState(207); subSpecies();
				}
				break;
			}
			setState(211);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				{
				setState(210); guard();
				}
				break;
			}
			setState(215);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				{
				setState(213); match(COLON);
				setState(214); match(ID);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NameContext extends ParserRuleContext {
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode ID_SPECIES() { return getToken(MLRulesParser.ID_SPECIES, 0); }
		public NameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitName(this);
		}
	}

	public final NameContext name() throws RecognitionException {
		NameContext _localctx = new NameContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_name);
		try {
			setState(222);
			switch (_input.LA(1)) {
			case ID_SPECIES:
				enterOuterAlt(_localctx, 1);
				{
				setState(217); match(ID_SPECIES);
				}
				break;
			case L_PAREN:
				enterOuterAlt(_localctx, 2);
				{
				setState(218); match(L_PAREN);
				setState(219); expression(0);
				setState(220); match(R_PAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AttributesContext extends ParserRuleContext {
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public List<TerminalNode> COMMA() { return getTokens(MLRulesParser.COMMA); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode COMMA(int i) {
			return getToken(MLRulesParser.COMMA, i);
		}
		public AttributesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attributes; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterAttributes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitAttributes(this);
		}
	}

	public final AttributesContext attributes() throws RecognitionException {
		AttributesContext _localctx = new AttributesContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_attributes);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(224); match(L_PAREN);
			setState(233);
			_la = _input.LA(1);
			if (((((_la - 21)) & ~0x3f) == 0 && ((1L << (_la - 21)) & ((1L << (IF - 21)) | (1L << (FREE - 21)) | (1L << (L_PAREN - 21)) | (1L << (L_BRAKET - 21)) | (1L << (MINUS - 21)) | (1L << (LT - 21)) | (1L << (TRUE - 21)) | (1L << (FALSE - 21)) | (1L << (NOT - 21)) | (1L << (INT - 21)) | (1L << (REAL - 21)) | (1L << (ID_SPECIES - 21)) | (1L << (ID - 21)) | (1L << (STRING - 21)) | (1L << (COUNT - 21)))) != 0)) {
				{
				setState(225); expression(0);
				setState(230);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(226); match(COMMA);
					setState(227); expression(0);
					}
					}
					setState(232);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(235); match(R_PAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubSpeciesContext extends ParserRuleContext {
		public TerminalNode L_BRAKET() { return getToken(MLRulesParser.L_BRAKET, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode R_BRAKET() { return getToken(MLRulesParser.R_BRAKET, 0); }
		public SubSpeciesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subSpecies; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterSubSpecies(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitSubSpecies(this);
		}
	}

	public final SubSpeciesContext subSpecies() throws RecognitionException {
		SubSpeciesContext _localctx = new SubSpeciesContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_subSpecies);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(237); match(L_BRAKET);
			setState(239);
			_la = _input.LA(1);
			if (((((_la - 21)) & ~0x3f) == 0 && ((1L << (_la - 21)) & ((1L << (IF - 21)) | (1L << (FREE - 21)) | (1L << (L_PAREN - 21)) | (1L << (L_BRAKET - 21)) | (1L << (MINUS - 21)) | (1L << (LT - 21)) | (1L << (TRUE - 21)) | (1L << (FALSE - 21)) | (1L << (NOT - 21)) | (1L << (INT - 21)) | (1L << (REAL - 21)) | (1L << (ID_SPECIES - 21)) | (1L << (ID - 21)) | (1L << (STRING - 21)) | (1L << (COUNT - 21)))) != 0)) {
				{
				setState(238); expression(0);
				}
			}

			setState(241); match(R_BRAKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GuardContext extends ParserRuleContext {
		public TerminalNode L_CURLY() { return getToken(MLRulesParser.L_CURLY, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode R_CURLY() { return getToken(MLRulesParser.R_CURLY, 0); }
		public GuardContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_guard; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterGuard(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitGuard(this);
		}
	}

	public final GuardContext guard() throws RecognitionException {
		GuardContext _localctx = new GuardContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_guard);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(243); match(L_CURLY);
			setState(245);
			_la = _input.LA(1);
			if (((((_la - 21)) & ~0x3f) == 0 && ((1L << (_la - 21)) & ((1L << (IF - 21)) | (1L << (FREE - 21)) | (1L << (L_PAREN - 21)) | (1L << (L_BRAKET - 21)) | (1L << (MINUS - 21)) | (1L << (LT - 21)) | (1L << (TRUE - 21)) | (1L << (FALSE - 21)) | (1L << (NOT - 21)) | (1L << (INT - 21)) | (1L << (REAL - 21)) | (1L << (ID_SPECIES - 21)) | (1L << (ID - 21)) | (1L << (STRING - 21)) | (1L << (COUNT - 21)))) != 0)) {
				{
				setState(244); expression(0);
				}
			}

			setState(247); match(R_CURLY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AmountContext extends ParserRuleContext {
		public AmountContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_amount; }
	 
		public AmountContext() { }
		public void copyFrom(AmountContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AmountIDContext extends AmountContext {
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public AmountIDContext(AmountContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterAmountID(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitAmountID(this);
		}
	}
	public static class AmountExprContext extends AmountContext {
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AmountExprContext(AmountContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterAmountExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitAmountExpr(this);
		}
	}
	public static class AmountINTContext extends AmountContext {
		public TerminalNode INT() { return getToken(MLRulesParser.INT, 0); }
		public AmountINTContext(AmountContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterAmountINT(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitAmountINT(this);
		}
	}

	public final AmountContext amount() throws RecognitionException {
		AmountContext _localctx = new AmountContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_amount);
		try {
			setState(255);
			switch (_input.LA(1)) {
			case INT:
				_localctx = new AmountINTContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(249); match(INT);
				}
				break;
			case ID:
				_localctx = new AmountIDContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(250); match(ID);
				}
				break;
			case L_PAREN:
				_localctx = new AmountExprContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(251); match(L_PAREN);
				setState(252); expression(0);
				setState(253); match(R_PAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
	 
		public ValueContext() { }
		public void copyFrom(ValueContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class RealContext extends ValueContext {
		public TerminalNode REAL() { return getToken(MLRulesParser.REAL, 0); }
		public RealContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterReal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitReal(this);
		}
	}
	public static class TrueContext extends ValueContext {
		public TerminalNode TRUE() { return getToken(MLRulesParser.TRUE, 0); }
		public TrueContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterTrue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitTrue(this);
		}
	}
	public static class FalseContext extends ValueContext {
		public TerminalNode FALSE() { return getToken(MLRulesParser.FALSE, 0); }
		public FalseContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterFalse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitFalse(this);
		}
	}
	public static class StringContext extends ValueContext {
		public TerminalNode STRING() { return getToken(MLRulesParser.STRING, 0); }
		public StringContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitString(this);
		}
	}
	public static class IdContext extends ValueContext {
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public IdContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterId(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitId(this);
		}
	}
	public static class FreeContext extends ValueContext {
		public TerminalNode FREE() { return getToken(MLRulesParser.FREE, 0); }
		public FreeContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterFree(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitFree(this);
		}
	}
	public static class IntContext extends ValueContext {
		public TerminalNode INT() { return getToken(MLRulesParser.INT, 0); }
		public IntContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterInt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitInt(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_value);
		try {
			setState(264);
			switch (_input.LA(1)) {
			case INT:
				_localctx = new IntContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(257); match(INT);
				}
				break;
			case REAL:
				_localctx = new RealContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(258); match(REAL);
				}
				break;
			case TRUE:
				_localctx = new TrueContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(259); match(TRUE);
				}
				break;
			case FALSE:
				_localctx = new FalseContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(260); match(FALSE);
				}
				break;
			case STRING:
				_localctx = new StringContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(261); match(STRING);
				}
				break;
			case FREE:
				_localctx = new FreeContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(262); match(FREE);
				}
				break;
			case ID:
				_localctx = new IdContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(263); match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhereContext extends ParserRuleContext {
		public List<TerminalNode> COMMA() { return getTokens(MLRulesParser.COMMA); }
		public List<AssignContext> assign() {
			return getRuleContexts(AssignContext.class);
		}
		public TerminalNode WHERE() { return getToken(MLRulesParser.WHERE, 0); }
		public AssignContext assign(int i) {
			return getRuleContext(AssignContext.class,i);
		}
		public TerminalNode COMMA(int i) {
			return getToken(MLRulesParser.COMMA, i);
		}
		public WhereContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_where; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterWhere(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitWhere(this);
		}
	}

	public final WhereContext where() throws RecognitionException {
		WhereContext _localctx = new WhereContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_where);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(266); match(WHERE);
			setState(267); assign();
			setState(272);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(268); match(COMMA);
				setState(269); assign();
				}
				}
				setState(274);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignContext extends ParserRuleContext {
		public TerminalNode ASSIGN() { return getToken(MLRulesParser.ASSIGN, 0); }
		public AssignNameContext assignName() {
			return getRuleContext(AssignNameContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AssignContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterAssign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitAssign(this);
		}
	}

	public final AssignContext assign() throws RecognitionException {
		AssignContext _localctx = new AssignContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_assign);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(275); assignName();
			setState(276); match(ASSIGN);
			setState(277); expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignNameContext extends ParserRuleContext {
		public AssignNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignName; }
	 
		public AssignNameContext() { }
		public void copyFrom(AssignNameContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SingleAssignContext extends AssignNameContext {
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public SingleAssignContext(AssignNameContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterSingleAssign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitSingleAssign(this);
		}
	}
	public static class TupleAssignContext extends AssignNameContext {
		public List<TerminalNode> ID() { return getTokens(MLRulesParser.ID); }
		public List<TerminalNode> COMMA() { return getTokens(MLRulesParser.COMMA); }
		public TerminalNode LT() { return getToken(MLRulesParser.LT, 0); }
		public TerminalNode GT() { return getToken(MLRulesParser.GT, 0); }
		public TerminalNode ID(int i) {
			return getToken(MLRulesParser.ID, i);
		}
		public TerminalNode COMMA(int i) {
			return getToken(MLRulesParser.COMMA, i);
		}
		public TupleAssignContext(AssignNameContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterTupleAssign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitTupleAssign(this);
		}
	}

	public final AssignNameContext assignName() throws RecognitionException {
		AssignNameContext _localctx = new AssignNameContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_assignName);
		int _la;
		try {
			setState(290);
			switch (_input.LA(1)) {
			case ID:
				_localctx = new SingleAssignContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(279); match(ID);
				}
				break;
			case LT:
				_localctx = new TupleAssignContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(280); match(LT);
				setState(281); match(ID);
				setState(286);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(282); match(COMMA);
					setState(283); match(ID);
					}
					}
					setState(288);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(289); match(GT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public TypeDefinitionContext typeDefinition() {
			return getRuleContext(TypeDefinitionContext.class,0);
		}
		public FunctionDefinitionContext functionDefinition(int i) {
			return getRuleContext(FunctionDefinitionContext.class,i);
		}
		public List<FunctionDefinitionContext> functionDefinition() {
			return getRuleContexts(FunctionDefinitionContext.class);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitFunction(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_function);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(292); typeDefinition();
			setState(294); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(293); functionDefinition();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(296); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeDefinitionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public TerminalNode SEMI() { return getToken(MLRulesParser.SEMI, 0); }
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TerminalNode ARROW(int i) {
			return getToken(MLRulesParser.ARROW, i);
		}
		public TerminalNode TYPE_SEPERATOR() { return getToken(MLRulesParser.TYPE_SEPERATOR, 0); }
		public List<TerminalNode> ARROW() { return getTokens(MLRulesParser.ARROW); }
		public TypeDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterTypeDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitTypeDefinition(this);
		}
	}

	public final TypeDefinitionContext typeDefinition() throws RecognitionException {
		TypeDefinitionContext _localctx = new TypeDefinitionContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_typeDefinition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(298); match(ID);
			setState(299); match(TYPE_SEPERATOR);
			setState(300); type();
			setState(305);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ARROW) {
				{
				{
				setState(301); match(ARROW);
				setState(302); type();
				}
				}
				setState(307);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(308); match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
	 
		public TypeContext() { }
		public void copyFrom(TypeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BaseTypeFloatContext extends TypeContext {
		public TerminalNode TYPE_REAL() { return getToken(MLRulesParser.TYPE_REAL, 0); }
		public BaseTypeFloatContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterBaseTypeFloat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitBaseTypeFloat(this);
		}
	}
	public static class SpeciesTypeContext extends TypeContext {
		public TerminalNode TYPE_SPECIES() { return getToken(MLRulesParser.TYPE_SPECIES, 0); }
		public SpeciesTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterSpeciesType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitSpeciesType(this);
		}
	}
	public static class BaseTypeStringContext extends TypeContext {
		public TerminalNode TYPE_STRING() { return getToken(MLRulesParser.TYPE_STRING, 0); }
		public BaseTypeStringContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterBaseTypeString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitBaseTypeString(this);
		}
	}
	public static class BaseTypeLinkContext extends TypeContext {
		public TerminalNode TYPE_LINK() { return getToken(MLRulesParser.TYPE_LINK, 0); }
		public BaseTypeLinkContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterBaseTypeLink(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitBaseTypeLink(this);
		}
	}
	public static class TupleTypeContext extends TypeContext {
		public TerminalNode TYPE_TUPLE() { return getToken(MLRulesParser.TYPE_TUPLE, 0); }
		public TupleTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterTupleType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitTupleType(this);
		}
	}
	public static class FunctionTypeContext extends TypeContext {
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TerminalNode ARROW(int i) {
			return getToken(MLRulesParser.ARROW, i);
		}
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public List<TerminalNode> ARROW() { return getTokens(MLRulesParser.ARROW); }
		public FunctionTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterFunctionType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitFunctionType(this);
		}
	}
	public static class BaseTypeBoolContext extends TypeContext {
		public TerminalNode TYPE_BOOL() { return getToken(MLRulesParser.TYPE_BOOL, 0); }
		public BaseTypeBoolContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterBaseTypeBool(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitBaseTypeBool(this);
		}
	}
	public static class SolutionTypeContext extends TypeContext {
		public TerminalNode TYPE_SOL() { return getToken(MLRulesParser.TYPE_SOL, 0); }
		public SolutionTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterSolutionType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitSolutionType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_type);
		int _la;
		try {
			setState(327);
			switch (_input.LA(1)) {
			case TYPE_REAL:
				_localctx = new BaseTypeFloatContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(310); match(TYPE_REAL);
				}
				break;
			case TYPE_BOOL:
				_localctx = new BaseTypeBoolContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(311); match(TYPE_BOOL);
				}
				break;
			case TYPE_STRING:
				_localctx = new BaseTypeStringContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(312); match(TYPE_STRING);
				}
				break;
			case TYPE_SPECIES:
				_localctx = new SpeciesTypeContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(313); match(TYPE_SPECIES);
				}
				break;
			case TYPE_SOL:
				_localctx = new SolutionTypeContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(314); match(TYPE_SOL);
				}
				break;
			case TYPE_TUPLE:
				_localctx = new TupleTypeContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(315); match(TYPE_TUPLE);
				}
				break;
			case TYPE_LINK:
				_localctx = new BaseTypeLinkContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(316); match(TYPE_LINK);
				}
				break;
			case L_PAREN:
				_localctx = new FunctionTypeContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(317); match(L_PAREN);
				setState(318); type();
				setState(321); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(319); match(ARROW);
					setState(320); type();
					}
					}
					setState(323); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==ARROW );
				setState(325); match(R_PAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDefinitionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public TerminalNode ASSIGN() { return getToken(MLRulesParser.ASSIGN, 0); }
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public TerminalNode SEMI() { return getToken(MLRulesParser.SEMI, 0); }
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public WhereContext where() {
			return getRuleContext(WhereContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public FunctionDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterFunctionDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitFunctionDefinition(this);
		}
	}

	public final FunctionDefinitionContext functionDefinition() throws RecognitionException {
		FunctionDefinitionContext _localctx = new FunctionDefinitionContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_functionDefinition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(329); match(ID);
			setState(333);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << FREE) | (1L << L_BRAKET) | (1L << LT) | (1L << TRUE) | (1L << FALSE) | (1L << INT) | (1L << REAL) | (1L << ID_SPECIES) | (1L << ID) | (1L << STRING))) != 0)) {
				{
				{
				setState(330); parameter();
				}
				}
				setState(335);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(336); match(ASSIGN);
			setState(337); expression(0);
			setState(339);
			_la = _input.LA(1);
			if (_la==WHERE) {
				{
				setState(338); where();
				}
			}

			setState(341); match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
	 
		public ParameterContext() { }
		public void copyFrom(ParameterContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ParamTupleContext extends ParameterContext {
		public AtomicParameterContext atomicParameter(int i) {
			return getRuleContext(AtomicParameterContext.class,i);
		}
		public List<AtomicParameterContext> atomicParameter() {
			return getRuleContexts(AtomicParameterContext.class);
		}
		public List<TerminalNode> COMMA() { return getTokens(MLRulesParser.COMMA); }
		public TerminalNode LT() { return getToken(MLRulesParser.LT, 0); }
		public TerminalNode GT() { return getToken(MLRulesParser.GT, 0); }
		public TerminalNode COMMA(int i) {
			return getToken(MLRulesParser.COMMA, i);
		}
		public ParamTupleContext(ParameterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterParamTuple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitParamTuple(this);
		}
	}
	public static class ParamSingleContext extends ParameterContext {
		public AtomicParameterContext atomicParameter() {
			return getRuleContext(AtomicParameterContext.class,0);
		}
		public ParamSingleContext(ParameterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterParamSingle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitParamSingle(this);
		}
	}
	public static class ParamSpeciesContext extends ParameterContext {
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(MLRulesParser.COMMA); }
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public TerminalNode COMMA(int i) {
			return getToken(MLRulesParser.COMMA, i);
		}
		public TerminalNode ID_SPECIES() { return getToken(MLRulesParser.ID_SPECIES, 0); }
		public ParamSpeciesContext(ParameterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterParamSpecies(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitParamSpecies(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_parameter);
		int _la;
		try {
			setState(368);
			switch (_input.LA(1)) {
			case FREE:
			case L_BRAKET:
			case TRUE:
			case FALSE:
			case INT:
			case REAL:
			case ID:
			case STRING:
				_localctx = new ParamSingleContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(343); atomicParameter();
				}
				break;
			case LT:
				_localctx = new ParamTupleContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(344); match(LT);
				setState(345); atomicParameter();
				setState(350);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(346); match(COMMA);
					setState(347); atomicParameter();
					}
					}
					setState(352);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(353); match(GT);
				}
				break;
			case ID_SPECIES:
				_localctx = new ParamSpeciesContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(355); match(ID_SPECIES);
				setState(356); match(L_PAREN);
				setState(365);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << FREE) | (1L << TRUE) | (1L << FALSE) | (1L << INT) | (1L << REAL) | (1L << ID) | (1L << STRING))) != 0)) {
					{
					setState(357); value();
					setState(362);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(358); match(COMMA);
						setState(359); value();
						}
						}
						setState(364);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(367); match(R_PAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomicParameterContext extends ParserRuleContext {
		public AtomicParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atomicParameter; }
	 
		public AtomicParameterContext() { }
		public void copyFrom(AtomicParameterContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ParamSolContext extends AtomicParameterContext {
		public List<TerminalNode> ID() { return getTokens(MLRulesParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(MLRulesParser.ID, i);
		}
		public TerminalNode PLUS() { return getToken(MLRulesParser.PLUS, 0); }
		public ParamSolContext(AtomicParameterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterParamSol(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitParamSol(this);
		}
	}
	public static class EmptySolContext extends AtomicParameterContext {
		public TerminalNode L_BRAKET() { return getToken(MLRulesParser.L_BRAKET, 0); }
		public TerminalNode R_BRAKET() { return getToken(MLRulesParser.R_BRAKET, 0); }
		public EmptySolContext(AtomicParameterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterEmptySol(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitEmptySol(this);
		}
	}
	public static class ParamSimpleContext extends AtomicParameterContext {
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public ParamSimpleContext(AtomicParameterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).enterParamSimple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MLRulesListener ) ((MLRulesListener)listener).exitParamSimple(this);
		}
	}

	public final AtomicParameterContext atomicParameter() throws RecognitionException {
		AtomicParameterContext _localctx = new AtomicParameterContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_atomicParameter);
		try {
			setState(376);
			switch ( getInterpreter().adaptivePredict(_input,39,_ctx) ) {
			case 1:
				_localctx = new ParamSimpleContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(370); value();
				}
				break;

			case 2:
				_localctx = new ParamSolContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(371); match(ID);
				setState(372); match(PLUS);
				setState(373); match(ID);
				}
				break;

			case 3:
				_localctx = new EmptySolContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(374); match(L_BRAKET);
				setState(375); match(R_BRAKET);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 9: return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return precpred(_ctx, 13);

		case 1: return precpred(_ctx, 12);

		case 2: return precpred(_ctx, 11);

		case 3: return precpred(_ctx, 9);

		case 4: return precpred(_ctx, 14);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3E\u017d\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\3\2\7\28\n\2\f\2\16\2;\13\2\3\2\3\2\6\2?\n\2\r\2"+
		"\16\2@\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3L\n\3\3\4\3\4\3\4\3\4\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5Y\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\5\7g\n\7\3\7\3\7\3\b\3\b\3\b\7\bn\n\b\f\b\16\bq\13\b\3"+
		"\t\5\tt\n\t\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\7\13\u0093\n\13\f\13\16\13\u0096\13\13\5\13\u0098\n\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\7\13\u00a0\n\13\f\13\16\13\u00a3\13\13"+
		"\5\13\u00a5\n\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\5\13\u00b2\n\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\7\13\u00c6\n\13\f\13\16\13\u00c9\13"+
		"\13\3\f\5\f\u00cc\n\f\3\f\3\f\5\f\u00d0\n\f\3\f\5\f\u00d3\n\f\3\f\5\f"+
		"\u00d6\n\f\3\f\3\f\5\f\u00da\n\f\3\r\3\r\3\r\3\r\3\r\5\r\u00e1\n\r\3\16"+
		"\3\16\3\16\3\16\7\16\u00e7\n\16\f\16\16\16\u00ea\13\16\5\16\u00ec\n\16"+
		"\3\16\3\16\3\17\3\17\5\17\u00f2\n\17\3\17\3\17\3\20\3\20\5\20\u00f8\n"+
		"\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21\5\21\u0102\n\21\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\5\22\u010b\n\22\3\23\3\23\3\23\3\23\7\23\u0111"+
		"\n\23\f\23\16\23\u0114\13\23\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3"+
		"\25\7\25\u011f\n\25\f\25\16\25\u0122\13\25\3\25\5\25\u0125\n\25\3\26\3"+
		"\26\6\26\u0129\n\26\r\26\16\26\u012a\3\27\3\27\3\27\3\27\3\27\7\27\u0132"+
		"\n\27\f\27\16\27\u0135\13\27\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30\3"+
		"\30\3\30\3\30\3\30\3\30\6\30\u0144\n\30\r\30\16\30\u0145\3\30\3\30\5\30"+
		"\u014a\n\30\3\31\3\31\7\31\u014e\n\31\f\31\16\31\u0151\13\31\3\31\3\31"+
		"\3\31\5\31\u0156\n\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\7\32\u015f\n"+
		"\32\f\32\16\32\u0162\13\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\7\32\u016b"+
		"\n\32\f\32\16\32\u016e\13\32\5\32\u0170\n\32\3\32\5\32\u0173\n\32\3\33"+
		"\3\33\3\33\3\33\3\33\3\33\5\33\u017b\n\33\3\33\2\3\24\34\2\4\6\b\n\f\16"+
		"\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\2\6\3\2\6\b\3\2\60\65\3\2-"+
		".\3\289\u01a6\29\3\2\2\2\4K\3\2\2\2\6M\3\2\2\2\bX\3\2\2\2\nZ\3\2\2\2\f"+
		"`\3\2\2\2\16j\3\2\2\2\20s\3\2\2\2\22u\3\2\2\2\24\u00b1\3\2\2\2\26\u00cb"+
		"\3\2\2\2\30\u00e0\3\2\2\2\32\u00e2\3\2\2\2\34\u00ef\3\2\2\2\36\u00f5\3"+
		"\2\2\2 \u0101\3\2\2\2\"\u010a\3\2\2\2$\u010c\3\2\2\2&\u0115\3\2\2\2(\u0124"+
		"\3\2\2\2*\u0126\3\2\2\2,\u012c\3\2\2\2.\u0149\3\2\2\2\60\u014b\3\2\2\2"+
		"\62\u0172\3\2\2\2\64\u017a\3\2\2\2\668\5\4\3\2\67\66\3\2\2\28;\3\2\2\2"+
		"9\67\3\2\2\29:\3\2\2\2:<\3\2\2\2;9\3\2\2\2<>\5\n\6\2=?\5\f\7\2>=\3\2\2"+
		"\2?@\3\2\2\2@>\3\2\2\2@A\3\2\2\2AB\3\2\2\2BC\7\2\2\3C\3\3\2\2\2DE\5\b"+
		"\5\2EF\7$\2\2FL\3\2\2\2GH\5\6\4\2HI\7$\2\2IL\3\2\2\2JL\5*\26\2KD\3\2\2"+
		"\2KG\3\2\2\2KJ\3\2\2\2L\5\3\2\2\2MN\7@\2\2NO\7#\2\2OP\5\24\13\2P\7\3\2"+
		"\2\2QR\7?\2\2RS\7%\2\2SY\7&\2\2TU\7?\2\2UV\7%\2\2VW\7<\2\2WY\7&\2\2XQ"+
		"\3\2\2\2XT\3\2\2\2Y\t\3\2\2\2Z[\7\t\2\2[\\\7\'\2\2\\]\5\24\13\2]^\7(\2"+
		"\2^_\7$\2\2_\13\3\2\2\2`a\5\16\b\2ab\7\13\2\2bc\5\20\t\2cd\t\2\2\2df\5"+
		"\22\n\2eg\5$\23\2fe\3\2\2\2fg\3\2\2\2gh\3\2\2\2hi\7$\2\2i\r\3\2\2\2jo"+
		"\5\26\f\2kl\7+\2\2ln\5\26\f\2mk\3\2\2\2nq\3\2\2\2om\3\2\2\2op\3\2\2\2"+
		"p\17\3\2\2\2qo\3\2\2\2rt\5\24\13\2sr\3\2\2\2st\3\2\2\2t\21\3\2\2\2uv\5"+
		"\24\13\2v\23\3\2\2\2wx\b\13\1\2xy\7,\2\2y\u00b2\5\24\13\22z{\7:\2\2{\u00b2"+
		"\5\24\13\21|}\7%\2\2}~\5\24\13\2~\177\7&\2\2\177\u0080\5\32\16\2\u0080"+
		"\u00b2\3\2\2\2\u0081\u0082\7\27\2\2\u0082\u0083\5\24\13\2\u0083\u0084"+
		"\7\30\2\2\u0084\u0085\5\24\13\2\u0085\u0086\7\31\2\2\u0086\u0087\5\24"+
		"\13\2\u0087\u00b2\3\2\2\2\u0088\u0089\7%\2\2\u0089\u008a\5\24\13\2\u008a"+
		"\u008b\t\3\2\2\u008b\u008c\5\24\13\2\u008c\u008d\7&\2\2\u008d\u00b2\3"+
		"\2\2\2\u008e\u0097\7\62\2\2\u008f\u0094\5\24\13\2\u0090\u0091\7\"\2\2"+
		"\u0091\u0093\5\24\13\2\u0092\u0090\3\2\2\2\u0093\u0096\3\2\2\2\u0094\u0092"+
		"\3\2\2\2\u0094\u0095\3\2\2\2\u0095\u0098\3\2\2\2\u0096\u0094\3\2\2\2\u0097"+
		"\u008f\3\2\2\2\u0097\u0098\3\2\2\2\u0098\u0099\3\2\2\2\u0099\u00b2\7\64"+
		"\2\2\u009a\u009b\7@\2\2\u009b\u00a4\7%\2\2\u009c\u00a1\5\24\13\2\u009d"+
		"\u009e\7\"\2\2\u009e\u00a0\5\24\13\2\u009f\u009d\3\2\2\2\u00a0\u00a3\3"+
		"\2\2\2\u00a1\u009f\3\2\2\2\u00a1\u00a2\3\2\2\2\u00a2\u00a5\3\2\2\2\u00a3"+
		"\u00a1\3\2\2\2\u00a4\u009c\3\2\2\2\u00a4\u00a5\3\2\2\2\u00a5\u00a6\3\2"+
		"\2\2\u00a6\u00b2\7&\2\2\u00a7\u00a8\7%\2\2\u00a8\u00a9\5\24\13\2\u00a9"+
		"\u00aa\7&\2\2\u00aa\u00b2\3\2\2\2\u00ab\u00b2\5\26\f\2\u00ac\u00ad\7\'"+
		"\2\2\u00ad\u00b2\7(\2\2\u00ae\u00b2\5\"\22\2\u00af\u00b0\7C\2\2\u00b0"+
		"\u00b2\7@\2\2\u00b1w\3\2\2\2\u00b1z\3\2\2\2\u00b1|\3\2\2\2\u00b1\u0081"+
		"\3\2\2\2\u00b1\u0088\3\2\2\2\u00b1\u008e\3\2\2\2\u00b1\u009a\3\2\2\2\u00b1"+
		"\u00a7\3\2\2\2\u00b1\u00ab\3\2\2\2\u00b1\u00ac\3\2\2\2\u00b1\u00ae\3\2"+
		"\2\2\u00b1\u00af\3\2\2\2\u00b2\u00c7\3\2\2\2\u00b3\u00b4\f\17\2\2\u00b4"+
		"\u00b5\t\4\2\2\u00b5\u00c6\5\24\13\20\u00b6\u00b7\f\16\2\2\u00b7\u00b8"+
		"\7+\2\2\u00b8\u00c6\5\24\13\17\u00b9\u00ba\f\r\2\2\u00ba\u00bb\7,\2\2"+
		"\u00bb\u00c6\5\24\13\16\u00bc\u00bd\f\13\2\2\u00bd\u00be\t\5\2\2\u00be"+
		"\u00c6\5\24\13\f\u00bf\u00c0\f\20\2\2\u00c0\u00c1\7/\2\2\u00c1\u00c2\7"+
		"%\2\2\u00c2\u00c3\5\24\13\2\u00c3\u00c4\7&\2\2\u00c4\u00c6\3\2\2\2\u00c5"+
		"\u00b3\3\2\2\2\u00c5\u00b6\3\2\2\2\u00c5\u00b9\3\2\2\2\u00c5\u00bc\3\2"+
		"\2\2\u00c5\u00bf\3\2\2\2\u00c6\u00c9\3\2\2\2\u00c7\u00c5\3\2\2\2\u00c7"+
		"\u00c8\3\2\2\2\u00c8\25\3\2\2\2\u00c9\u00c7\3\2\2\2\u00ca\u00cc\5 \21"+
		"\2\u00cb\u00ca\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\u00cd\3\2\2\2\u00cd\u00cf"+
		"\5\30\r\2\u00ce\u00d0\5\32\16\2\u00cf\u00ce\3\2\2\2\u00cf\u00d0\3\2\2"+
		"\2\u00d0\u00d2\3\2\2\2\u00d1\u00d3\5\34\17\2\u00d2\u00d1\3\2\2\2\u00d2"+
		"\u00d3\3\2\2\2\u00d3\u00d5\3\2\2\2\u00d4\u00d6\5\36\20\2\u00d5\u00d4\3"+
		"\2\2\2\u00d5\u00d6\3\2\2\2\u00d6\u00d9\3\2\2\2\u00d7\u00d8\7#\2\2\u00d8"+
		"\u00da\7@\2\2\u00d9\u00d7\3\2\2\2\u00d9\u00da\3\2\2\2\u00da\27\3\2\2\2"+
		"\u00db\u00e1\7?\2\2\u00dc\u00dd\7%\2\2\u00dd\u00de\5\24\13\2\u00de\u00df"+
		"\7&\2\2\u00df\u00e1\3\2\2\2\u00e0\u00db\3\2\2\2\u00e0\u00dc\3\2\2\2\u00e1"+
		"\31\3\2\2\2\u00e2\u00eb\7%\2\2\u00e3\u00e8\5\24\13\2\u00e4\u00e5\7\"\2"+
		"\2\u00e5\u00e7\5\24\13\2\u00e6\u00e4\3\2\2\2\u00e7\u00ea\3\2\2\2\u00e8"+
		"\u00e6\3\2\2\2\u00e8\u00e9\3\2\2\2\u00e9\u00ec\3\2\2\2\u00ea\u00e8\3\2"+
		"\2\2\u00eb\u00e3\3\2\2\2\u00eb\u00ec\3\2\2\2\u00ec\u00ed\3\2\2\2\u00ed"+
		"\u00ee\7&\2\2\u00ee\33\3\2\2\2\u00ef\u00f1\7\'\2\2\u00f0\u00f2\5\24\13"+
		"\2\u00f1\u00f0\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2\u00f3\3\2\2\2\u00f3\u00f4"+
		"\7(\2\2\u00f4\35\3\2\2\2\u00f5\u00f7\7)\2\2\u00f6\u00f8\5\24\13\2\u00f7"+
		"\u00f6\3\2\2\2\u00f7\u00f8\3\2\2\2\u00f8\u00f9\3\2\2\2\u00f9\u00fa\7*"+
		"\2\2\u00fa\37\3\2\2\2\u00fb\u0102\7<\2\2\u00fc\u0102\7@\2\2\u00fd\u00fe"+
		"\7%\2\2\u00fe\u00ff\5\24\13\2\u00ff\u0100\7&\2\2\u0100\u0102\3\2\2\2\u0101"+
		"\u00fb\3\2\2\2\u0101\u00fc\3\2\2\2\u0101\u00fd\3\2\2\2\u0102!\3\2\2\2"+
		"\u0103\u010b\7<\2\2\u0104\u010b\7=\2\2\u0105\u010b\7\66\2\2\u0106\u010b"+
		"\7\67\2\2\u0107\u010b\7A\2\2\u0108\u010b\7\37\2\2\u0109\u010b\7@\2\2\u010a"+
		"\u0103\3\2\2\2\u010a\u0104\3\2\2\2\u010a\u0105\3\2\2\2\u010a\u0106\3\2"+
		"\2\2\u010a\u0107\3\2\2\2\u010a\u0108\3\2\2\2\u010a\u0109\3\2\2\2\u010b"+
		"#\3\2\2\2\u010c\u010d\7\16\2\2\u010d\u0112\5&\24\2\u010e\u010f\7\"\2\2"+
		"\u010f\u0111\5&\24\2\u0110\u010e\3\2\2\2\u0111\u0114\3\2\2\2\u0112\u0110"+
		"\3\2\2\2\u0112\u0113\3\2\2\2\u0113%\3\2\2\2\u0114\u0112\3\2\2\2\u0115"+
		"\u0116\5(\25\2\u0116\u0117\7 \2\2\u0117\u0118\5\24\13\2\u0118\'\3\2\2"+
		"\2\u0119\u0125\7@\2\2\u011a\u011b\7\62\2\2\u011b\u0120\7@\2\2\u011c\u011d"+
		"\7\"\2\2\u011d\u011f\7@\2\2\u011e\u011c\3\2\2\2\u011f\u0122\3\2\2\2\u0120"+
		"\u011e\3\2\2\2\u0120\u0121\3\2\2\2\u0121\u0123\3\2\2\2\u0122\u0120\3\2"+
		"\2\2\u0123\u0125\7\64\2\2\u0124\u0119\3\2\2\2\u0124\u011a\3\2\2\2\u0125"+
		")\3\2\2\2\u0126\u0128\5,\27\2\u0127\u0129\5\60\31\2\u0128\u0127\3\2\2"+
		"\2\u0129\u012a\3\2\2\2\u012a\u0128\3\2\2\2\u012a\u012b\3\2\2\2\u012b+"+
		"\3\2\2\2\u012c\u012d\7@\2\2\u012d\u012e\7\n\2\2\u012e\u0133\5.\30\2\u012f"+
		"\u0130\7\13\2\2\u0130\u0132\5.\30\2\u0131\u012f\3\2\2\2\u0132\u0135\3"+
		"\2\2\2\u0133\u0131\3\2\2\2\u0133\u0134\3\2\2\2\u0134\u0136\3\2\2\2\u0135"+
		"\u0133\3\2\2\2\u0136\u0137\7$\2\2\u0137-\3\2\2\2\u0138\u014a\7\17\2\2"+
		"\u0139\u014a\7\20\2\2\u013a\u014a\7\21\2\2\u013b\u014a\7\22\2\2\u013c"+
		"\u014a\7\25\2\2\u013d\u014a\7\24\2\2\u013e\u014a\7\26\2\2\u013f\u0140"+
		"\7%\2\2\u0140\u0143\5.\30\2\u0141\u0142\7\13\2\2\u0142\u0144\5.\30\2\u0143"+
		"\u0141\3\2\2\2\u0144\u0145\3\2\2\2\u0145\u0143\3\2\2\2\u0145\u0146\3\2"+
		"\2\2\u0146\u0147\3\2\2\2\u0147\u0148\7&\2\2\u0148\u014a\3\2\2\2\u0149"+
		"\u0138\3\2\2\2\u0149\u0139\3\2\2\2\u0149\u013a\3\2\2\2\u0149\u013b\3\2"+
		"\2\2\u0149\u013c\3\2\2\2\u0149\u013d\3\2\2\2\u0149\u013e\3\2\2\2\u0149"+
		"\u013f\3\2\2\2\u014a/\3\2\2\2\u014b\u014f\7@\2\2\u014c\u014e\5\62\32\2"+
		"\u014d\u014c\3\2\2\2\u014e\u0151\3\2\2\2\u014f\u014d\3\2\2\2\u014f\u0150"+
		"\3\2\2\2\u0150\u0152\3\2\2\2\u0151\u014f\3\2\2\2\u0152\u0153\7 \2\2\u0153"+
		"\u0155\5\24\13\2\u0154\u0156\5$\23\2\u0155\u0154\3\2\2\2\u0155\u0156\3"+
		"\2\2\2\u0156\u0157\3\2\2\2\u0157\u0158\7$\2\2\u0158\61\3\2\2\2\u0159\u0173"+
		"\5\64\33\2\u015a\u015b\7\62\2\2\u015b\u0160\5\64\33\2\u015c\u015d\7\""+
		"\2\2\u015d\u015f\5\64\33\2\u015e\u015c\3\2\2\2\u015f\u0162\3\2\2\2\u0160"+
		"\u015e\3\2\2\2\u0160\u0161\3\2\2\2\u0161\u0163\3\2\2\2\u0162\u0160\3\2"+
		"\2\2\u0163\u0164\7\64\2\2\u0164\u0173\3\2\2\2\u0165\u0166\7?\2\2\u0166"+
		"\u016f\7%\2\2\u0167\u016c\5\"\22\2\u0168\u0169\7\"\2\2\u0169\u016b\5\""+
		"\22\2\u016a\u0168\3\2\2\2\u016b\u016e\3\2\2\2\u016c\u016a\3\2\2\2\u016c"+
		"\u016d\3\2\2\2\u016d\u0170\3\2\2\2\u016e\u016c\3\2\2\2\u016f\u0167\3\2"+
		"\2\2\u016f\u0170\3\2\2\2\u0170\u0171\3\2\2\2\u0171\u0173\7&\2\2\u0172"+
		"\u0159\3\2\2\2\u0172\u015a\3\2\2\2\u0172\u0165\3\2\2\2\u0173\63\3\2\2"+
		"\2\u0174\u017b\5\"\22\2\u0175\u0176\7@\2\2\u0176\u0177\7+\2\2\u0177\u017b"+
		"\7@\2\2\u0178\u0179\7\'\2\2\u0179\u017b\7(\2\2\u017a\u0174\3\2\2\2\u017a"+
		"\u0175\3\2\2\2\u017a\u0178\3\2\2\2\u017b\65\3\2\2\2*9@KXfos\u0094\u0097"+
		"\u00a1\u00a4\u00b1\u00c5\u00c7\u00cb\u00cf\u00d2\u00d5\u00d9\u00e0\u00e8"+
		"\u00eb\u00f1\u00f7\u0101\u010a\u0112\u0120\u0124\u012a\u0133\u0145\u0149"+
		"\u014f\u0155\u0160\u016c\u016f\u0172\u017a";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}