/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.listener.typecheck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesBaseListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountExprContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountIDContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountINTContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AndOrContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ApplicationContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AttributesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.BoolExprContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ConstantContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.CountShortContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.EmptySolutionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ExpValueContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FalseContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FreeContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FunctionCallContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.GuardContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.IdContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.IfThenElseContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.IntContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.MinusContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.NameContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.NotContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ParenContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.PlusContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ProductsContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ReactantsContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.RoofContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SpeciesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SpeciesExprContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.StringContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SubSpeciesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.TrueContext;
import org.jamesii.cmlrules.parser.standard.functions.Function;
import org.jamesii.cmlrules.parser.standard.listener.SemanticsException;
import org.jamesii.cmlrules.parser.standard.types.BaseType;
import org.jamesii.cmlrules.parser.standard.types.FunctionType;
import org.jamesii.cmlrules.parser.standard.types.Type;
import org.jamesii.cmlrules.util.MLEnvironment;

public class ExpressionTypesListener extends MLRulesBaseListener {

  private MLEnvironment env;

  private final Map<ParserRuleContext, Object> data = new HashMap<>();

  private boolean expectRootProduct = false;

  public ExpressionTypesListener(MLEnvironment env) {
    this.env = env;
  }

  protected MLEnvironment getEnv() {
    return env;
  }

  protected void setEnv(MLEnvironment env) {
    this.env = env;
  }

  @Override
  public void exitFree(FreeContext ctx) {
    data.put(ctx, BaseType.LINK);
  }

  @Override
  public void exitTrue(TrueContext ctx) {
    data.put(ctx, BaseType.BOOL);
  }

  @Override
  public void exitFalse(FalseContext ctx) {
    data.put(ctx, BaseType.BOOL);
  }

  @Override
  public void exitString(StringContext ctx) {
    data.put(ctx, BaseType.STRING);
  }

  @Override
  public void exitReal(@NotNull MLRulesParser.RealContext ctx) {
    data.put(ctx, BaseType.NUM);
  }

  @Override
  public void exitInt(IntContext ctx) {
    data.put(ctx, BaseType.NUM);
  }

  @Override
  public void exitId(IdContext ctx) {
    if (env.getValue(ctx.getText()) == null) {
      throw new SemanticsException(ctx, String.format(
          "variable %s is not defined", ctx.getText()));
    }
    data.put(ctx, env.getValue(ctx.getText()));
  }

  @Override
  public void exitExpValue(ExpValueContext ctx) {
    data.put(ctx, data.remove(ctx.getChild(0)));
  }

  @Override
  public void exitPlus(PlusContext ctx) {
    try {
      data.put(
          ctx,
          Type.plus((Type) data.remove(ctx.getChild(0)),
              (Type) data.remove(ctx.getChild(2))));
    } catch (IllegalArgumentException e) {
      throw new SemanticsException(ctx, e.getMessage());
    }
  }

  @Override
  public void exitMinus(MinusContext ctx) {
    try {
      data.put(
          ctx,
          Type.arithmetic((Type) data.remove(ctx.getChild(0)),
              (Type) data.remove(ctx.getChild(2))));
    } catch (IllegalArgumentException e) {
      throw new SemanticsException(ctx, e.getMessage());
    }
  }

  @Override
  public void exitMultDiv(@NotNull MLRulesParser.MultDivContext ctx) {
    try {
      data.put(
          ctx,
          Type.arithmetic((Type) data.remove(ctx.getChild(0)),
              (Type) data.remove(ctx.getChild(2))));
    } catch (IllegalArgumentException e) {
      throw new SemanticsException(ctx, e.getMessage());
    }
  }

  @Override
  public void exitRoof(RoofContext ctx) {
    try {
      data.put(
          ctx,
          Type.arithmetic((Type) data.remove(ctx.getChild(0)),
              (Type) data.remove(ctx.getChild(3))));
    } catch (IllegalArgumentException e) {
      throw new SemanticsException(ctx, e.getMessage());
    }
  }

  @Override
  public void exitAndOr(AndOrContext ctx) {
    try {
      data.put(
          ctx,
          Type.bool((Type) data.remove(ctx.getChild(0)),
              (Type) data.remove(ctx.getChild(2))));
    } catch (IllegalArgumentException e) {
      throw new SemanticsException(ctx, e.getMessage());
    }
  }

  @Override
  public void exitParen(ParenContext ctx) {
    data.put(ctx, data.remove(ctx.getChild(1)));
  }

  @Override
  public void exitBoolExpr(BoolExprContext ctx) {
    try {
      data.put(
          ctx,
          Type.comparison((Type) data.remove(ctx.getChild(1)),
              (Type) data.remove(ctx.getChild(3))));
    } catch (IllegalArgumentException e) {
      throw new SemanticsException(ctx, e.getMessage());
    }
  }

  @Override
  public void exitNot(NotContext ctx) {
    Type t = (Type) data.remove(ctx.getChild(1));
    if (t != BaseType.BOOL && t != BaseType.UNKNOWN) {
      throw new SemanticsException(ctx, String.format(
          "wrong type %s for not operator", t.toString()));
    }
    data.put(ctx, BaseType.BOOL);
  }

  @Override
  public void exitMinusOne(@NotNull MLRulesParser.MinusOneContext ctx) {
    data.put(ctx, data.remove(ctx.getChild(1)));
  }

  @Override
  public void exitIfThenElse(IfThenElseContext ctx) {
    Type i = (Type) data.remove(ctx.getChild(1));
    Type t = (Type) data.remove(ctx.getChild(3));
    Type e = (Type) data.remove(ctx.getChild(5));

    if (i != BaseType.BOOL && i != BaseType.UNKNOWN) {
      throw new SemanticsException(ctx, String.format(
          "invalid condition type %s", i));
    }
    if ((e == BaseType.SOL && t == BaseType.SPECIES)
        || (e == BaseType.SPECIES && t == BaseType.SOL)) {
      e = BaseType.SOL;
      t = BaseType.SOL;
    }
    if (t != BaseType.UNKNOWN && e != BaseType.UNKNOWN && !t.equals(e)) {
      throw new SemanticsException(ctx, String.format(
          "types of then and else part are not equal: %s vs. %s", t, e));
    }

    data.put(ctx,
        (t == BaseType.UNKNOWN || e == BaseType.UNKNOWN) ? BaseType.UNKNOWN : t);
  }

  @Override
  public void exitTuple(@NotNull MLRulesParser.TupleContext ctx) {
    data.put(ctx, BaseType.TUPLE);
  }

  @Override
  public void exitName(NameContext ctx) {
    if (ctx.ID_SPECIES() != null) {
      SpeciesType st = (SpeciesType) env.getValue(ctx.ID_SPECIES().getText());
      if (st == null) {
        throw new SemanticsException(ctx, String.format(
            "unknown species type %s", ctx.ID_SPECIES().getText()));
      }
      if (st.getName().equals("$$ROOT$$")) {
        try {
          if (ctx.getParent().getParent() instanceof ReactantsContext) {
            if (ctx.getParent().getParent().getChildCount() != 1) {
              throw new SemanticsException(
                  ctx,
                  String
                      .format("when using the $$ROOT$$ species, only one reactant is allowed"));
            }
            expectRootProduct = true;
          } else if (ctx.getParent().getParent().getParent() instanceof ProductsContext) {
            if (!expectRootProduct) {
              throw new SemanticsException(ctx,
                  String.format("$$ROOT$$ species is not used as a reactant"));
            }
            if (ctx.getParent().getParent().getParent().getChildCount() != 1) {
              throw new SemanticsException(
                  ctx,
                  String
                      .format("when using the $$ROOT$$ specie,s only one product is allowed"));
            }
            expectRootProduct = false;
          } else {
            throw new SemanticsException(ctx,
                "invalid usage of the $$ROOT$$ species");
          }
        } catch (NullPointerException e) {
          throw new SemanticsException(ctx,
              "invalid usage of the $$ROOT$$ species");
        }
      }
      data.put(ctx, st);
    } else {
      Type t = (Type) data.remove(ctx.getChild(1));
      if (t == null) {
        throw new SemanticsException(ctx, String.format(
            "cannot find species name with expression %s", ctx.getChild(1)));
      }
      if (t != BaseType.STRING) {
        throw new SemanticsException(ctx, String.format(
            "Species type must be specified with a string and not %s", t));
      }
    }
  }

  @Override
  public void exitSpecies(SpeciesContext ctx) {
    AmountContext aCtx = null;
    NameContext nCtx = null;
    AttributesContext attCtx = null;
    SubSpeciesContext subCtx = null;
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof AmountContext) {
        aCtx = (AmountContext) ctx.getChild(i);
      } else if (ctx.getChild(i) instanceof NameContext) {
        nCtx = (NameContext) ctx.getChild(i);
      } else if (ctx.getChild(i) instanceof AttributesContext) {
        attCtx = (AttributesContext) ctx.getChild(i);
      } else if (ctx.getChild(i) instanceof SubSpeciesContext) {
        subCtx = (SubSpeciesContext) ctx.getChild(i);
      }
    }

    Type amount = aCtx == null ? BaseType.NUM : (Type) data.remove(aCtx);
    if (amount == null
        || (amount != BaseType.UNKNOWN && amount != BaseType.NUM)) {
      throw new SemanticsException(ctx, String.format("invalid amount type %s",
          amount));
    }

    SpeciesType st = null;
    Object type = (Object) data.remove(nCtx);
    if (type != null) {
      st = (SpeciesType) type;
    }

    // st can be null if the name of the species is specified by a function
    // the value of this function is not known at this point here - it is only
    // possible to calculate this value during the simulation
    if (st != null) {
      @SuppressWarnings("unchecked")
      List<Type> atts =
          attCtx == null ? Collections.emptyList() : (List<Type>) data
              .remove(attCtx);
      if (st.getAttributes() != atts.size()) {
        throw new SemanticsException(
            ctx,
            String
                .format(
                    "wrong number of attributes: expected %s attributes but found %s attributes",
                    st.getAttributes(), atts.size()));
      }
    }

    Type sub = subCtx == null ? BaseType.SOL : (Type) data.remove(subCtx);
    if (sub != BaseType.SOL && sub != BaseType.SPECIES
        && sub != BaseType.UNKNOWN) {
      throw new SemanticsException(ctx, String.format(
          "invalid type %s for sub species", sub));
    }

    if (ctx.ID() != null && env.containsIdent(ctx.ID().getText())) {
      throw new SemanticsException(ctx, String.format(
          "variable %s already defined", ctx.ID().getText()));
    }

    data.put(ctx, BaseType.SPECIES);
  }

  @Override
  public void exitSpeciesExpr(SpeciesExprContext ctx) {
    data.put(ctx, BaseType.SPECIES);
  }

  @Override
  public void exitEmptySolution(EmptySolutionContext ctx) {
    data.put(ctx, BaseType.SOL);
  }

  @Override
  public void exitApplication(ApplicationContext ctx) {
    Object o = data.remove(ctx.getChild(1));
    if (!(o instanceof FunctionType)) {
      throw new SemanticsException(ctx, String.format(
          "Invalid function call %s", ctx.getChild(1)));
    }
    FunctionType f = (FunctionType) o;
    @SuppressWarnings("unchecked")
    List<Type> parameter = (List<Type>) data.remove(ctx.getChild(3));
    for (int i = 0; i < f.getSubTypes().size(); ++i) {
      if (parameter.get(i) != BaseType.UNKNOWN
          && f.getSubTypes().get(i) != BaseType.UNKNOWN
          && !f.getSubTypes().get(i).equals(parameter.get(i))) {
        throw new SemanticsException(ctx, String.format(
            "invalid function call types: expected %s but found %s", f
                .getSubTypes().toString(), parameter.toString()));
      }
    }
    data.put(ctx, f.getSubTypes().get(f.getSubTypes().size() - 1));
  }

  @Override
  public void exitAmountExpr(AmountExprContext ctx) {
    Type t = (Type) data.remove(ctx.getChild(1));
    if (t != BaseType.UNKNOWN && t != BaseType.NUM) {
      throw new SemanticsException(ctx, String.format("invalid amount type %s",
          t));
    }
    data.put(ctx, BaseType.NUM);
  }

  @Override
  public void exitAmountID(AmountIDContext ctx) {
    Type t = (Type) env.getValue(ctx.getText());
    if (t != BaseType.UNKNOWN && t != BaseType.NUM) {
      throw new SemanticsException(ctx, String.format("invalid amount type %s",
          t));
    }
    data.put(ctx, BaseType.NUM);
  }

  @Override
  public void exitAmountINT(AmountINTContext ctx) {
    data.put(ctx, BaseType.NUM);
  }

  @Override
  public void exitSubSpecies(SubSpeciesContext ctx) {
    Type t = (Type) data.remove(ctx.getChild(1));

    if (t != BaseType.UNKNOWN && t != BaseType.SOL && t != BaseType.SPECIES) {
      throw new SemanticsException(ctx, String.format(
          "invalid sub species type %s", t));
    }

    data.put(ctx, BaseType.SOL);
  }

  @Override
  public void exitGuard(GuardContext ctx) {
    Type t = (Type) data.remove(ctx.getChild(1));
    if (t != BaseType.UNKNOWN && t != BaseType.BOOL) {
      throw new SemanticsException(ctx, String.format("invalid guard type %s",
          t));
    }

    data.put(ctx, BaseType.BOOL);
  }

  @Override
  public void exitAttributes(AttributesContext ctx) {
    List<Type> types = new ArrayList<>();
    boolean tuple = false;
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof ParserRuleContext) {
        Type t = (Type) data.remove(ctx.getChild(i));
        if (t == BaseType.SOL || t == BaseType.SPECIES
            || t instanceof FunctionType) {
          throw new SemanticsException(ctx, String.format(
              "invalid species attribute type %s", t));
        }
        if (t == BaseType.TUPLE) {
          tuple = true;
        }
        types.add(t);
      }
    }
    if (tuple && types.size() != 1) {
      throw new SemanticsException(
          ctx,
          "tuple is only allowed for species attributes if it is the only given attribute");
    }
    data.put(ctx, types);
  }

  @Override
  public void exitFunctionCall(FunctionCallContext ctx) {
    String name = ctx.ID().getText();

    Object o = env.getValue(name);
    FunctionType type = null;
    if (o instanceof Function) {
      Function f = (Function) env.getValue(name);
      if (f == null) {
        throw new SemanticsException(ctx, String.format(
            "unknown function call %s", name));
      }
      type = (FunctionType) f.getType();
    } else if (o instanceof FunctionType) {
      type = (FunctionType) env.getValue(name);
    }
    if (type == null) {
      throw new SemanticsException(ctx, String.format(
          "could not compute type of function %s", name));
    }
    int counter = 0;
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof RuleContext) {
        if (counter < type.getSubTypes().size() - 1) {
          Type t = (Type) data.remove(ctx.getChild(i));
          if (t != BaseType.UNKNOWN
              && type.getSubTypes().get(counter) != t
              && (type.getSubTypes().get(counter) != BaseType.SOL || t != BaseType.SPECIES)) {
            throw new SemanticsException(
                ctx,
                String
                    .format(
                        "invalid function call argument type: %s given, but %s expected",
                        t, type.getSubTypes().get(counter)));
          }
        }
        ++counter;
      }
    }
    if (counter != type.getSubTypes().size() - 1) {
      throw new SemanticsException(ctx, String.format(
          "invalid number of arguments, %s  arguments given, but %s expected",
          counter, type.getSubTypes().size() - 1));
    }

    data.put(ctx, type.getSubTypes().get(type.getSubTypes().size() - 1));

  }

  @Override
  public void exitCountShort(CountShortContext ctx) {
    if (!env.containsIdent(ctx.ID().getText())) {
      throw new SemanticsException(ctx, String.format("unknown variable %s",
          ctx.ID().getText()));
    }
    data.put(ctx, BaseType.NUM);
  }

  public Map<ParserRuleContext, Object> getData() {
    return data;
  }

  public void exitConstant(ConstantContext ctx) {
    env.setGlobalValue(ctx.ID().toString(), (Type) data.remove(ctx.getChild(2)));
  }

  public boolean isExpectingRoot() {
    return expectRootProduct;
  }

}
