/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.simple.listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jamesii.cmlrules.model.standard.rule.Reactant;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.Identifier;

/**
 * This class provides methods to check that a rule does not change the
 * structure of the model, i.e., all compartment species contain a rest
 * solution, no attributes of compartment species are changed and the rest
 * solutions are assigned properly.
 * 
 * @author Tobias Helms
 *
 */
public class StaticRule {

  /**
   * Check whether the given reactant and the given product describe the same
   * compartment structure, e.g., the reactant C(1)[s?] and the product C(1)[A +
   * s?] describe the same structure.
   */
  private static boolean checkReactantProduct(Reactant reactant, Reactant product,
      Map<Reactant, Reactant> ruleMapping) {
    if (product.getRest() == "$sol?"
        || reactant.getRest() == "$sol"
        || !product.getRest().equals(reactant.getRest())
        || reactant.getType() != product.getType()) {
      return false;
    }

    for (int i = 0; i < reactant.getType().getAttributes(); ++i) {
      Node rNode = reactant.getAttributeNodes().get(i);
      Node pNode = product.getAttributeNodes().get(i);
      if (rNode instanceof Identifier<?>) {
        if (pNode instanceof Identifier<?>) {
          if (!((Identifier<?>) rNode).getIdent().equals(((Identifier<?>) pNode).getIdent())) {
            return false;
          }
        } else {
          return false;
        }
      } else {
        if (!((ValueNode<?>) rNode).getValue().equals(((ValueNode<?>) pNode).getValue())) {
          return false;
        }
      }
    }

    ruleMapping.put(reactant, product);

    List<Reactant> validProducts = new ArrayList<>();
    for (Reactant subReactant : reactant.getSubReactants()) {
      if (!subReactant.getSubReactants().isEmpty() || !subReactant.getRest().startsWith("$")) {
        for (Reactant subProduct : product.getSubReactants()) {
          if (!validProducts.contains(subProduct)
              && checkReactantProduct(subReactant, subProduct, ruleMapping)) {
            validProducts.add(subProduct);
            break;
          }
        }
      }
    }

    for (Reactant subProduct : product.getSubReactants()) {
      if (!subProduct.getSubReactants().isEmpty()
          && !validProducts.contains(subProduct)) {
        return false;
      }
    }

    return true;
  }

  private static boolean isFlat(List<Reactant> reactants, List<Reactant> products) {
    return !(reactants.stream().anyMatch(r -> !r.getRest().startsWith("$") || !r.getSubReactants().isEmpty())
        || products.stream().anyMatch(p -> !p.getRest().startsWith("$") || !p.getSubReactants().isEmpty()));
  }

  public static boolean isValidTauRule(List<Reactant> reactants, List<Reactant> products, Map<Reactant, Reactant> mapping) {

    if (!isFlat(reactants, products)) {
      for (int i = 0; i < reactants.size(); ++i) {
        if (!reactants.get(i).isCompartment()) {
          continue;
        }
        boolean found = false;
        for (int j = 0; j < products.size(); ++j) {
          if (checkReactantProduct(reactants.get(i), products.get(j),
              mapping)) {
            found = true;
            break;
          }
        }
        if (!found) {
           return false;
        }
      }
      for (int i = 0; i < products.size(); ++i) {
        final Reactant product = products.get(i);
        if (product.isCompartment() && !mapping.entrySet().stream().anyMatch(e -> e.getValue() == product)) {
          return false;
        }
      }
    }
    return true;
  }

}
