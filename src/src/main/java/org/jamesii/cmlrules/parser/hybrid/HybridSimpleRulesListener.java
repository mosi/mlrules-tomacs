/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.jamesii.cmlrules.parser.hybrid;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.jamesii.cmlrules.model.Model;
import org.jamesii.cmlrules.model.simple.SimpleModel;
import org.jamesii.cmlrules.model.simple.reaction.PeriodicRule;
import org.jamesii.cmlrules.model.simple.reaction.Reaction;
import org.jamesii.cmlrules.model.simple.reaction.Rule;
import org.jamesii.cmlrules.model.standard.rule.Reactant;
import org.jamesii.cmlrules.model.standard.species.AbstractSpecies;
import org.jamesii.cmlrules.model.standard.species.MapSpecies;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.parser.simple.listener.SimpleRulesListener;
import org.jamesii.cmlrules.parser.simple.listener.StaticRule;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountExprContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AttributesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.IdContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.InitialSolutionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.MlruleContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ProductsContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.RateContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ReactantsContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SingleAssignContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.TupleAssignContext;
import org.jamesii.cmlrules.parser.standard.listener.ExpressionsListener;
import org.jamesii.cmlrules.parser.standard.listener.SemanticsException;
import org.jamesii.cmlrules.parser.standard.nodes.MLRulesAddNode;
import org.jamesii.cmlrules.parser.standard.nodes.SpeciesPatternNode;
import org.jamesii.cmlrules.parser.standard.nodes.TupleNode;
import org.jamesii.cmlrules.simulator.simple.SimpleReactionCreator;
import org.jamesii.cmlrules.util.Assignment;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.cmlrules.util.NodeHelper;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;

public class HybridSimpleRulesListener extends ExpressionsListener {

  public HybridSimpleRulesListener(MLEnvironment env) {
    super(env);
  }

  private final Map<Integer, Rule> rules = new HashMap<>();

  private final List<Rule> timedRules = new ArrayList<>();

  private final Map<Reactant, Reactant> reactantProducts = new HashMap<>();

  private boolean inRule = false;

  private boolean inReactant = false;

  private boolean inProduct = false;

  private Set<String> variables = new HashSet<>();

  private boolean validRule = true;

  private int counter = 0;

  @Override
  public void exitAttributes(AttributesContext ctx) {
    List<INode> atts = new ArrayList<>();
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof ParserRuleContext) {
        Node n = (Node) getData().remove(ctx.getChild(i));
        if (n instanceof TupleNode) {
          TupleNode t = (TupleNode) n;
          atts.addAll((Collection<? extends INode>) t.getChildren());
        } else {
          atts.add(n);
        }
      }
    }
    // if (inReactant || inProduct) {
    // atts.forEach(a -> {
    // if (!(a instanceof ValueNode<?>) && !(a instanceof Identifier<?>)) {
    // throw new SemanticsException(
    // ctx,
    // "in reactants and products only values and variables are allowed, but not expressions");
    // }
    // });
    // }
    getData().put(ctx, atts);
  }

  @Override
  public void exitAmountExpr(AmountExprContext ctx) {
    if (!validRule) {
      return;
    }
    if (inReactant || inProduct) {
      validRule = false;
      return;
    }
    super.exitAmountExpr(ctx);
  }

  @Override
  public void exitId(IdContext ctx) {
    if (!validRule) {
      return;
    }
    if (inReactant) {
      variables.add(ctx.getText());
    }
    if (inProduct && !variables.contains(ctx.getText())) {
      validRule = false;
    }
    super.exitId(ctx);
  }

  @Override
  public void exitSingleAssign(SingleAssignContext ctx) {
    if (!validRule) {
      return;
    }
    if (inRule) {
      if (variables.contains(ctx.ID().getText())) {
        validRule = false;
      }
    }
    super.exitSingleAssign(ctx);
  };

  @Override
  public void exitTupleAssign(TupleAssignContext ctx) {
    if (!validRule) {
      return;
    }
    if (inRule) {
      ctx.ID().forEach(id -> {
        if (variables.contains(id.getText())) {
          validRule = false;
        }
      });
    }
    super.exitTupleAssign(ctx);
  }

  @Override
  public void exitReactants(ReactantsContext ctx) {
    if (!validRule) {
      return;
    }
    List<Reactant> reactants = new ArrayList<>();
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof RuleContext) {
        SpeciesPatternNode node = (SpeciesPatternNode) getData().remove(
            ctx.getChild(i));
        reactants.add(node.toReactant(getEnv()));
      }
    }
    getData().put(ctx, reactants);
    inReactant = false;
  }

  private void createReactants(ProductsContext ctx, MLRulesAddNode node,
      List<Reactant> reactants, MLEnvironment env) {
    if (!validRule) {
      return;
    }
    if (node.getLeft() instanceof SpeciesPatternNode) {
      reactants.add(((SpeciesPatternNode) node.getLeft()).toReactant(env));
    } else if (node.getLeft() instanceof MLRulesAddNode) {
      createReactants(ctx, (MLRulesAddNode) node.getLeft(), reactants, env);
    } else {
      validRule = false;
    }
    if (node.getRight() instanceof SpeciesPatternNode) {
      reactants.add(((SpeciesPatternNode) node.getRight()).toReactant(env));
    } else if (node.getRight() instanceof MLRulesAddNode) {
      createReactants(ctx, (MLRulesAddNode) node.getRight(), reactants, env);
    } else {
      validRule = false;
    }
  }

  @Override
  public void exitProducts(ProductsContext ctx) {
    if (!validRule) {
      return;
    }
    Node node = (Node) getData().remove(ctx.getChild(0));

    List<Reactant> products = new ArrayList<>();
    if (node instanceof SpeciesPatternNode) {
      SpeciesPatternNode spn = (SpeciesPatternNode) node;
      products.add(spn.toReactant(getEnv()));
    } else if (node instanceof MLRulesAddNode) {
      MLRulesAddNode addNode = (MLRulesAddNode) node;
      createReactants(ctx, addNode, products, getEnv());
    } else if (node == null) {
      // do nothing
    } else {
      validRule = false;
    }
    if (products.stream().anyMatch(p -> p.getBoundTo() != "$$$")) {
      validRule = false;
    }
    getData().put(ctx, products);
    inProduct = false;
  }

  @Override
  public void enterMlrule(MlruleContext ctx) {
    inRule = true;
    validRule = true;
  }

  @Override
  public void enterReactants(ReactantsContext ctx) {
    if (!validRule) {
      return;
    }
    inReactant = true;
    super.enterReactants(ctx);
  }

  @Override
  public void enterProducts(ProductsContext ctx) {
    if (!validRule) {
      return;
    }
    inProduct = true;
    super.enterProducts(ctx);
  }

  @Override
  @SuppressWarnings("unchecked")
  public void exitMlrule(MlruleContext ctx) {
    if (validRule && ctx.AT() != null) {
      List<Assignment> assignments = (List<Assignment>) getData().remove(
          ctx.getChild(5));
      List<Reactant> reactants = (List<Reactant>) getData().remove(
          ctx.getChild(0));
      List<Reactant> products = (List<Reactant>) getData().remove(
          ctx.getChild(2));
      if (StaticRule.isValidTauRule(reactants, products, reactantProducts)) {
        rules.put(counter, (new Rule(reactants, products, (Node) getData()
            .remove(ctx.getChild(4)), assignments == null ? new ArrayList<>()
            : assignments)));
      }
    }
    if (ctx.AT() != null) {
      ++counter;
    }
    getData().clear();
    variables.clear();
    inRule = false;
  }

  @Override
  public void exitRate(RateContext ctx) {
    getData().put(ctx, getData().remove(ctx.getChild(0)));
  }

  public Map<Integer, Rule> getRules() {
    return rules;
  }

  public Map<Reactant, Reactant> getReactantProducts() {
    return reactantProducts;
  }

}
