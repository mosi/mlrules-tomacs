/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.listener.typecheck;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountExprContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountIDContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountINTContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AndOrContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ApplicationContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AssignContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AttributesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.BoolExprContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ConstantContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.CountShortContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.EmptySolContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.EmptySolutionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ExpValueContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FalseContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FunctionCallContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FunctionDefinitionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.GuardContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.IdContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.IfThenElseContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.IntContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.MinusContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.MlruleContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.NameContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.NotContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ParamSingleContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ParamSolContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ParamSpeciesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ParamTupleContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ParenContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.PlusContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ReactantsContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.RoofContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SingleAssignContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SpeciesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SpeciesExprContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.StringContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SubSpeciesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.TrueContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.TupleAssignContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.WhereContext;
import org.jamesii.cmlrules.parser.standard.functions.Function;
import org.jamesii.cmlrules.parser.standard.listener.SemanticsException;
import org.jamesii.cmlrules.parser.standard.types.BaseType;
import org.jamesii.cmlrules.parser.standard.types.Type;
import org.jamesii.cmlrules.util.MLEnvironment;

public class WhereVariablesListener extends ExpressionTypesListener {

  private MLEnvironment envCopy;

  private final Map<ParserRuleContext, Object> data = new HashMap<>();

  private final Map<ParserRuleContext, Map<String, Type>> ruleVariables =
      new HashMap<>();

  private final Map<ParserRuleContext, Map<String, Type>> functionVariables =
      new HashMap<>();

  private ParserRuleContext current;

  private Function currentFunction;

  private boolean isWhere = false;

  private boolean isTuple = false;

  private boolean isSpecies = false;

  private boolean isSingle = false;

  private int attCounter = 0;

  public WhereVariablesListener(MLEnvironment env) {
    super(env);
  }

  @Override
  public void enterWhere(WhereContext ctx) {
    isWhere = true;
  }

  @Override
  public void exitWhere(WhereContext ctx) {
    isWhere = false;
  }

  @SuppressWarnings("unchecked")
  @Override
  public void exitAssign(AssignContext ctx) {
    Object o = data.remove(ctx.getChild(0));
    if (o instanceof String) {
      Type t = (Type) getData().remove(ctx.getChild(2));
      if (t == null) {
        throw new SemanticsException(ctx, String.format(
            "could not compute where variable %s", o));
      }
      if (getEnv().containsIdent((String) o)) {
        throw new SemanticsException(ctx, String.format(
            "variable %s already defined", (String) o));
      }
      if (current instanceof MlruleContext) {
        if (ruleVariables.get(current).containsKey((String) o)) {
          throw new SemanticsException(ctx, String.format(
              "variable %s is defined twice", (String) o));
        }
        ruleVariables.get(current).put((String) o, t);
      } else {
        if (functionVariables.get(current).containsKey((String) o)) {
          throw new SemanticsException(ctx, String.format(
              "variable %s is defined twice", (String) o));
        }
        functionVariables.get(current).put((String) o, t);
      }
      getEnv().setValue((String) o, t);
    } else {
      Type t = (Type) getData().remove(ctx.getChild(2));
      if (t != BaseType.TUPLE) {
        throw new SemanticsException(ctx, String.format(
            "could assign tuple values because %s is returned instead", t));
      }
      List<String> list = (List<String>) o;
      if (current instanceof MlruleContext) {
        list.forEach(s -> {
          if (getEnv().containsIdent((String) s)) {
            throw new SemanticsException(ctx, String.format(
                "variable %s already defined", (String) s));
          }
          if (ruleVariables.get(current).containsKey(s)) {
            throw new SemanticsException(ctx, String.format(
                "variable %s is defined twice", s));
          }
          ruleVariables.get(current).put(s, BaseType.UNKNOWN);
          getEnv().setValue(s, BaseType.UNKNOWN);
        });
      } else {
        list.forEach(s -> {
          if (getEnv().containsIdent((String) s)) {
            throw new SemanticsException(ctx, String.format(
                "variable %s already defined", (String) s));
          }
          if (functionVariables.get(current).containsKey(s)) {
            throw new SemanticsException(ctx, String.format(
                "variable %s is defined twice", s));
          }
          functionVariables.get(current).put(s, BaseType.UNKNOWN);
          getEnv().setValue(s, BaseType.UNKNOWN);
        });
      }
    }
  }

  @Override
  public void exitSingleAssign(SingleAssignContext ctx) {
    data.put(ctx, ctx.ID().getText());
  }

  @Override
  public void exitTupleAssign(TupleAssignContext ctx) {
    data.put(ctx,
        ctx.ID().stream().map(i -> i.getText()).collect(Collectors.toList()));
  }

  @Override
  public void enterMlrule(MlruleContext ctx) {
    ruleVariables.put(ctx, new HashMap<>());
    current = ctx;
    envCopy = getEnv();
    setEnv(getEnv().completeCopy());
  }

  @Override
  public void exitMlrule(MlruleContext ctx) {
    setEnv(envCopy);
  }

  @Override
  public void exitFunctionDefinition(FunctionDefinitionContext ctx) {
    if (attCounter != currentFunction.getType().getSubTypes().size() - 1) {
      throw new SemanticsException(ctx, String.format(
          "expect %s arguments but %s arguments are given", currentFunction
              .getType().getSubTypes().size() - 1, attCounter));
    }
    setEnv(envCopy);
  }

  private boolean isReactant(ParserRuleContext ctx) {
    while (ctx != null) {
      if (ctx instanceof ReactantsContext) {
        return true;
      }
      ctx = ctx.getParent();
    }
    return false;
  }

  private boolean isRestSolution(IdContext ctx) {
    if (!ctx.ID().getText().endsWith("?")) {
      return false;
    }
    ParserRuleContext parent = ctx.getParent();
    ParserRuleContext child = ctx;
    boolean isSubSpecies = false;
    boolean wrongPosition = false;
    while (parent != null) {
      if (parent instanceof SubSpeciesContext) {
        isSubSpecies = true;
        break;
      } else if (!(parent instanceof ExpValueContext)
          && !(parent instanceof PlusContext)) {
        return false;
      }
      if (parent instanceof PlusContext && parent.getChild(2) != child) {
        wrongPosition = true;
      }
      child = parent;
      parent = parent.getParent();
    }
    if (!isSubSpecies) {
      return false;
    }
    if (wrongPosition) {
      throw new SemanticsException(ctx, String.format(
          "rest solution %s has an invalid position", ctx.ID().getText()));
    }
    return true;
  }

  @Override
  public void exitId(IdContext ctx) {
    try {
      if (ctx.getParent() instanceof ExpValueContext
          && ctx.getParent().getParent() instanceof AttributesContext) {
        if (isReactant(ctx.getParent())) {
          if (getEnv().getValue(ctx.ID().getText()) == null) {
            ruleVariables.get(current)
                .put(ctx.ID().getText(), BaseType.UNKNOWN);
            getEnv().setValue(ctx.ID().getText(), BaseType.UNKNOWN);
          }
        }
      } else if (isReactant(ctx.getParent()) && isRestSolution(ctx)) {
        if (getEnv().getValue(ctx.ID().getText()) == null) {
          ruleVariables.get(current).put(ctx.ID().getText(), BaseType.UNKNOWN);
          getEnv().setValue(ctx.ID().getText(), BaseType.UNKNOWN);
        } else {
          throw new SemanticsException(ctx, String.format(
              "rest solution %s defined twice", ctx.ID().getText()));
        }
      } else if (isTuple) {
        functionVariables.get(current)
            .put(ctx.ID().getText(), BaseType.UNKNOWN);
        getEnv().setValue(ctx.ID().getText(), BaseType.UNKNOWN);
      } else if (isSpecies) {
        functionVariables.get(current)
            .put(ctx.ID().getText(), BaseType.UNKNOWN);
        getEnv().setValue(ctx.ID().getText(), BaseType.UNKNOWN);
      } else if (isSingle) {
        functionVariables.get(current).put(ctx.ID().getText(),
            currentFunction.getType().getSubTypes().get(attCounter));
        getEnv().setValue(ctx.ID().getText(),
            currentFunction.getType().getSubTypes().get(attCounter));
        ++attCounter;
      } else if (isWhere) {
        super.exitId(ctx);
      }
    } catch (NullPointerException e) {
      // do nothing
    }
  }

  @Override
  public void enterParamTuple(ParamTupleContext ctx) {
    if (currentFunction.getType().getSubTypes().get(attCounter) != BaseType.TUPLE) {
      throw new SemanticsException(ctx, String.format(
          "argument %s of function %s is not tuple but %s", attCounter,
          currentFunction.getName(), currentFunction.getType().getSubTypes()
              .get(attCounter)));
    }
    isTuple = true;
    ++attCounter;
  }

  @Override
  public void exitParamTuple(ParamTupleContext ctx) {
    isTuple = false;
  }

  @Override
  public void exitParamSol(ParamSolContext ctx) {
    String x = ctx.ID().get(0).getText();
    String xs = ctx.ID().get(1).getText();
    if (!isTuple
        && currentFunction.getType().getSubTypes().get(attCounter) != BaseType.SOL) {
      throw new SemanticsException(ctx, String.format(
          "argument %s of function %s is not sol but %s", attCounter,
          currentFunction.getName(), currentFunction.getType().getSubTypes()
              .get(attCounter)));
    }
    if (!isTuple) {
      ++attCounter;
    }
    functionVariables.get(current).put(x, BaseType.SPECIES);
    functionVariables.get(current).put(xs, BaseType.SOL);
    getEnv().setValue(x, BaseType.SPECIES);
    getEnv().setValue(xs, BaseType.SOL);
  }

  @Override
  public void exitEmptySol(EmptySolContext ctx) {
    if (!isTuple
        && currentFunction.getType().getSubTypes().get(attCounter) != BaseType.SOL) {
      throw new SemanticsException(ctx, String.format(
          "argument %s of function %s is not sol but %s", attCounter,
          currentFunction.getName(), currentFunction.getType().getSubTypes()
              .get(attCounter)));
    }
    ++attCounter;
  }

  @Override
  public void enterParamSpecies(ParamSpeciesContext ctx) {
    if (currentFunction.getType().getSubTypes().get(attCounter) != BaseType.SPECIES) {
      throw new SemanticsException(ctx, String.format(
          "argument %s of function %s is not species but %s", attCounter,
          currentFunction.getName(), currentFunction.getType().getSubTypes()
              .get(attCounter)));
    }
    ++attCounter;
    isSpecies = true;
  }

  @Override
  public void exitParamSpecies(ParamSpeciesContext ctx) {
    isSpecies = false;
  }

  @Override
  public void enterParamSingle(ParamSingleContext ctx) {
    isSingle = true;
  }

  @Override
  public void exitParamSingle(ParamSingleContext ctx) {
    isSingle = false;
  }

  @Override
  public void enterFunctionDefinition(FunctionDefinitionContext ctx) {
    Object o = getEnv().getValue(ctx.ID().getText());
    if (!(o instanceof Function)) {
      throw new SemanticsException(ctx, String.format("unknown function %s",
          ctx.ID().getText()));
    }
    currentFunction = (Function) o;

    functionVariables.put(ctx, new HashMap<>());
    current = ctx;
    attCounter = 0;

    envCopy = getEnv();
    setEnv(getEnv().completeCopy());
    functionVariables.get(ctx).forEach((k, v) -> getEnv().setValue(k, v));
  }

  @Override
  public void exitTrue(TrueContext ctx) {
    if (isWhere) {
      super.exitTrue(ctx);
    } else if (isSingle) {
      if (currentFunction.getType().getSubTypes().get(attCounter) != BaseType.BOOL) {
        throw new SemanticsException(ctx, String.format(
            "argument %s of function %s is not boolean but %s", attCounter,
            currentFunction.getName(), currentFunction.getType().getSubTypes()
                .get(attCounter)));
      }
      ++attCounter;
    }
  }

  @Override
  public void exitFalse(FalseContext ctx) {
    if (isWhere) {
      super.exitFalse(ctx);
    } else if (isSingle) {
      if (currentFunction.getType().getSubTypes().get(attCounter) != BaseType.BOOL) {
        throw new SemanticsException(ctx, String.format(
            "argument %s of function %s is not boolean but %s", attCounter,
            currentFunction.getName(), currentFunction.getType().getSubTypes()
                .get(attCounter)));
      }
      ++attCounter;
    }
  }

  @Override
  public void exitString(StringContext ctx) {
    if (isWhere) {
      super.exitString(ctx);
    } else if (isSingle) {
      if (currentFunction.getType().getSubTypes().get(attCounter) != BaseType.STRING) {
        throw new SemanticsException(ctx, String.format(
            "argument %s of function %s is not string but %s", attCounter,
            currentFunction.getName(), currentFunction.getType().getSubTypes()
                .get(attCounter)));
      }
      ++attCounter;
    }
  }

  @Override
  public void exitReal(@NotNull MLRulesParser.RealContext ctx) {
    if (isWhere) {
      super.exitReal(ctx);
    } else if (isSingle) {
      if (currentFunction.getType().getSubTypes().get(attCounter) != BaseType.NUM) {
        throw new SemanticsException(ctx, String.format(
            "argument %s of function %s is not numerical but %s", attCounter,
            currentFunction.getName(), currentFunction.getType().getSubTypes()
                .get(attCounter)));
      }
      ++attCounter;
    }
  }

  @Override
  public void exitInt(IntContext ctx) {
    if (isWhere) {
      super.exitInt(ctx);
    } else if (isSingle) {
      if (currentFunction.getType().getSubTypes().get(attCounter) != BaseType.NUM) {
        throw new SemanticsException(ctx, String.format(
            "argument %s of function %s is not numerical but %s", attCounter,
            currentFunction.getName(), currentFunction.getType().getSubTypes()
                .get(attCounter)));
      }
      ++attCounter;
    }
  }

  @Override
  public void exitExpValue(ExpValueContext ctx) {
    if (isWhere) {
      super.exitExpValue(ctx);
    }
  }

  @Override
  public void exitPlus(PlusContext ctx) {
    if (isWhere) {
      super.exitPlus(ctx);
    }
  }

  @Override
  public void exitMinus(MinusContext ctx) {
    if (isWhere) {
      super.exitMinus(ctx);
    }
  }

  @Override
  public void exitMultDiv(@NotNull MLRulesParser.MultDivContext ctx) {
    if (isWhere) {
      super.exitMultDiv(ctx);
    }
  }

  @Override
  public void exitRoof(RoofContext ctx) {
    if (isWhere) {
      super.exitRoof(ctx);
    }
  }

  @Override
  public void exitAndOr(AndOrContext ctx) {
    if (isWhere) {
      super.exitAndOr(ctx);
    }
  }

  @Override
  public void exitParen(ParenContext ctx) {
    if (isWhere) {
      super.exitParen(ctx);
    }
  }

  @Override
  public void exitBoolExpr(BoolExprContext ctx) {
    if (isWhere) {
      super.exitBoolExpr(ctx);
    }
  }

  @Override
  public void exitNot(NotContext ctx) {
    if (isWhere) {
      super.exitNot(ctx);
    }
  }

  @Override
  public void exitMinusOne(@NotNull MLRulesParser.MinusOneContext ctx) {
    if (isWhere) {
      super.exitMinusOne(ctx);
    }
  }

  @Override
  public void exitIfThenElse(IfThenElseContext ctx) {
    if (isWhere) {
      super.exitIfThenElse(ctx);
    }
  }

  @Override
  public void exitTuple(@NotNull MLRulesParser.TupleContext ctx) {
    if (isWhere) {
      super.exitTuple(ctx);
    }
  }

  @Override
  public void exitName(NameContext ctx) {
    if (isWhere) {
      super.exitName(ctx);
    }
  }

  @Override
  public void exitSpecies(SpeciesContext ctx) {
    if (isWhere) {
      super.exitSpecies(ctx);
    }
    if (ctx.ID() != null) {
      getEnv().setValue(ctx.ID().getText(), BaseType.SPECIES);
    }
  }

  @Override
  public void exitSpeciesExpr(SpeciesExprContext ctx) {
    if (isWhere) {
      super.exitSpeciesExpr(ctx);
    }
  }

  @Override
  public void exitEmptySolution(EmptySolutionContext ctx) {
    if (isWhere) {
      super.exitEmptySolution(ctx);
    }
  }

  @Override
  public void exitApplication(ApplicationContext ctx) {
    if (isWhere) {
      super.exitApplication(ctx);
    }
  }

  @Override
  public void exitAmountExpr(AmountExprContext ctx) {
    if (isWhere) {
      super.exitAmountExpr(ctx);
    }
  }

  @Override
  public void exitAmountID(AmountIDContext ctx) {
    if (isWhere) {
      super.exitAmountID(ctx);
    }
  }

  @Override
  public void exitAmountINT(AmountINTContext ctx) {
    if (isWhere) {
      super.exitAmountINT(ctx);
    }
  }

  @Override
  public void exitSubSpecies(SubSpeciesContext ctx) {
    if (isWhere) {
      super.exitSubSpecies(ctx);
    }
  }

  @Override
  public void exitGuard(GuardContext ctx) {
    if (isWhere) {
      throw new SemanticsException(ctx,
          "guards are not allowed in where assignments");
    }
  }

  @Override
  public void exitAttributes(AttributesContext ctx) {
    if (isWhere) {
      super.exitAttributes(ctx);
    }
  }

  @Override
  public void exitFunctionCall(FunctionCallContext ctx) {
    if (isWhere) {
      super.exitFunctionCall(ctx);
    }
  }

  @Override
  public void exitCountShort(CountShortContext ctx) {
    if (isWhere) {
      super.exitCountShort(ctx);
    }
  }

  public Map<ParserRuleContext, Map<String, Type>> getRuleVariables() {
    return ruleVariables;
  }

  public Map<ParserRuleContext, Map<String, Type>> getFunctionVariables() {
    return functionVariables;
  }

  public void enterConstant(ConstantContext ctx) {
    isWhere = true;
  }

  public void exitConstant(ConstantContext ctx) {
    super.exitConstant(ctx);
    isWhere = false;
  }

  public boolean isWhere() {
    return isWhere;
  }

}
