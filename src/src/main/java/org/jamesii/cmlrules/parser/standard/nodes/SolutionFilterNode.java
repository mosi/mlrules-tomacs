package org.jamesii.cmlrules.parser.standard.nodes;

import java.util.HashMap;
import java.util.Map;

import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;

public class SolutionFilterNode extends Node {

  private static final long serialVersionUID = 1L;

  public static final String SOL = "§sol";

  public static final String NAME = "§name";

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    if (cEnv instanceof MLEnvironment) {
      MLEnvironment env = (MLEnvironment) cEnv;
      Object s = env.getValue(SOL);
      Object n = env.getValue(NAME);
      if (s instanceof Map<?, ?> && n instanceof String) {
        Map<?, ?> list = (Map<?, ?>) s;
        Map<Species,Species> filtered = new HashMap<>();
        String name = (String) n;

        for (Object species : list.keySet()) {
          Species tmp = (Species) species;
          if (tmp.getType().getName().equals(name)) {
            filtered.put(tmp, tmp);
          }
        }
        return (N) new ValueNode<Map<Species, Species>>(filtered);
      }
    }
    throw new IllegalArgumentException("Could not filter the given solution");
  }

  @Override
  public String toString() {
    return "filter()";
  }

}
