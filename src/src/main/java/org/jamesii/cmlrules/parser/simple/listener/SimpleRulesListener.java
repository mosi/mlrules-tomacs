/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.simple.listener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.jamesii.cmlrules.model.Model;
import org.jamesii.cmlrules.model.simple.SimpleModel;
import org.jamesii.cmlrules.model.simple.reaction.PeriodicRule;
import org.jamesii.cmlrules.model.simple.reaction.Reaction;
import org.jamesii.cmlrules.model.simple.reaction.Rule;
import org.jamesii.cmlrules.model.standard.rule.Reactant;
import org.jamesii.cmlrules.model.standard.species.AbstractSpecies;
import org.jamesii.cmlrules.model.standard.species.MapSpecies;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountExprContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AttributesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.IdContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.InitialSolutionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.MlruleContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ProductsContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.RateContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ReactantsContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SingleAssignContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.TupleAssignContext;
import org.jamesii.cmlrules.parser.standard.listener.ExpressionsListener;
import org.jamesii.cmlrules.parser.standard.listener.ModelCreationListener;
import org.jamesii.cmlrules.parser.standard.listener.SemanticsException;
import org.jamesii.cmlrules.parser.standard.nodes.MLRulesAddNode;
import org.jamesii.cmlrules.parser.standard.nodes.SpeciesPatternNode;
import org.jamesii.cmlrules.parser.standard.nodes.TupleNode;
import org.jamesii.cmlrules.simulator.simple.SimpleReactionCreator;
import org.jamesii.cmlrules.util.Assignment;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.cmlrules.util.NodeHelper;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.Identifier;

/**
 * This rules listener accepts rules of the most simple ML-Rules version, i.e.,
 * the rules set of the beginning is not changing during the simulation. <br/>
 * <br/>
 * <b>This listener cannot identify species that are not present at the beginning,
 * i.e., species with amount 0 must be declared by the user at the beginning.</b>
 * 
 * @author Tobias Helms
 *
 */
public class SimpleRulesListener extends ExpressionsListener implements
    ModelCreationListener {

  private Species initialSolution;

  private final List<Rule> rules = new ArrayList<>();

  private final List<Rule> timedRules = new ArrayList<>();
  
  private final Set<SpeciesType> compartments = new HashSet<>();

  private final Set<SpeciesType> leaves = new HashSet<>();
  
  private final Map<Reactant, Reactant> reactantProducts = new HashMap<>();

  private boolean inRule = false;

  private boolean inReactant = false;

  private boolean inProduct = false;

  private Set<String> variables = new HashSet<>();

  public SimpleRulesListener(MLEnvironment env) {
    super(env);
  }

  private void determineCompartmentsAndLeaves(SpeciesType compartment,
      Species s, InitialSolutionContext ctx) {
    if (s.isLeaf()) {
      if (compartments.contains(s)) {
        throw new SemanticsException(ctx, String.format(
            "compartment species %s is not allowed to be used as leaf species",
            s.getType()));
      }
      leaves.add(s.getType());
    } else {
      leaves.remove(s.getType());
      compartments.add(s.getType());
      s.getSubSpeciesStream().forEach(
          sub -> determineCompartmentsAndLeaves(s.getType(), sub, ctx));
    }
  }

  private void determineCompartmentsAndLeaves(
      Optional<SpeciesType> compartment, Reactant reactant,
      ParserRuleContext ctx) {
    if (reactant.getRest().startsWith("$") && reactant.getSubReactants().isEmpty()) {
      if (compartments.contains(reactant.getType())) {
        throw new SemanticsException(
            ctx,
            String
                .format(
                    "compartment species %s are not allowed to be used as leaf species",
                    reactant.getType()));
      }
      leaves.add(reactant.getType());
    } else {
      if (leaves.contains(reactant.getType())) {
        throw new SemanticsException(ctx, String.format(
            "compartment species %s is not allowed to be used as leaf species",
            reactant.getType()));
      }
      compartments.add(reactant.getType());
      reactant.getSubReactants().forEach(
          r -> determineCompartmentsAndLeaves(Optional.of(reactant.getType()),
              r, ctx));
    }
  }

  @Override
  public void exitInitialSolution(InitialSolutionContext ctx) {
    Node node = null;
    try {
      node = (Node) getData().remove(ctx.getChild(2));
      @SuppressWarnings("unchecked")
      Map<Species, Species> species =
          (Map<Species, Species>) ((ValueNode<?>) node.calc(getEnv()))
              .getValue();
      initialSolution =
          new MapSpecies(new HashMap<>(), 1.0, SpeciesType.ROOT, new Object[0],
              AbstractSpecies.UNKNOWN);
      Species root =
          new MapSpecies(species, 1.0, SpeciesType.ROOT, new Object[0],
              initialSolution);
      initialSolution.add(root);
      root.getSubSpeciesStream().forEach(s -> s.setContext(root));
      root.individualize(new HashMap<>());
    } catch (Exception e) {
      throw new SemanticsException(ctx, String.format(
          "Could not create the initial solution %s: %s", node.toString(),
          e.getMessage()));
    }
    compartments.add(initialSolution.getType());
    initialSolution.getSubSpeciesStream().forEach(
        sub -> determineCompartmentsAndLeaves(initialSolution.getType(), sub, ctx));
  }

  @Override
  public void exitAttributes(AttributesContext ctx) {
    List<INode> atts = new ArrayList<>();
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof ParserRuleContext) {
        Node n = (Node) getData().remove(ctx.getChild(i));
        if (n instanceof TupleNode) {
          TupleNode t = (TupleNode) n;
          atts.addAll((Collection<? extends INode>) t.getChildren());
        } else {
          atts.add(n);
        }
      }
    }
//    if (inReactant || inProduct) {
//      atts.forEach(a -> {
//        if (!(a instanceof ValueNode<?>) && !(a instanceof Identifier<?>)) {
//          throw new SemanticsException(
//              ctx,
//              "in reactants and products only values and variables are allowed, but not expressions");
//        }
//      });
//    }
    getData().put(ctx, atts);
  }

  @Override
  public void exitAmountExpr(AmountExprContext ctx) {
    if (inReactant || inProduct) {
      throw new SemanticsException(ctx,
          "amount expressions are not allowed within reactants and products");
    }
    super.exitAmountExpr(ctx);
  }

  @Override
  public void exitId(IdContext ctx) {
    if (inReactant) {
      variables.add(ctx.getText());
    }
    if (inProduct && !variables.contains(ctx.getText())) {
      throw new SemanticsException(
          ctx,
          String
              .format(
                  "variable %s has not been defined within the reactants (where variables are not considered here)",
                  ctx.getText()));
    }
    super.exitId(ctx);
  }

  @Override
  public void exitSingleAssign(SingleAssignContext ctx) {
    if (inRule) {
      if (variables.contains(ctx.ID().getText())) {
        throw new SemanticsException(
            ctx,
            String
                .format(
                    "variables defined within the where part of a rule are not allowed to be used within the reactants of products.",
                    ctx.ID().getText()));
      }
    }
    super.exitSingleAssign(ctx);
  };

  @Override
  public void exitTupleAssign(TupleAssignContext ctx) {
    if (inRule) {
      ctx.ID()
          .forEach(
              id -> {
                if (variables.contains(id.getText())) {
                  throw new SemanticsException(
                      ctx,
                      String
                          .format(
                              "variables defined within the where part of a rule are not allowed to be used within the reactants of products.",
                              id.getText()));
                }
              });
    }
    super.exitTupleAssign(ctx);
  }

  @Override
  public void exitReactants(ReactantsContext ctx) {
    List<Reactant> reactants = new ArrayList<>();
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof RuleContext) {
        SpeciesPatternNode node =
            (SpeciesPatternNode) getData().remove(ctx.getChild(i));
        reactants.add(node.toReactant(getEnv()));
      }
    }
    reactants.forEach(r -> determineCompartmentsAndLeaves(Optional.empty(), r,
        ctx));
    getData().put(ctx, reactants);
    inReactant = false;
  }

  private void createReactants(ProductsContext ctx, MLRulesAddNode node,
      List<Reactant> reactants, MLEnvironment env) {
    if (node.getLeft() instanceof SpeciesPatternNode) {
      reactants.add(((SpeciesPatternNode) node.getLeft()).toReactant(env));
    } else if (node.getLeft() instanceof MLRulesAddNode) {
      createReactants(ctx, (MLRulesAddNode) node.getLeft(), reactants, env);
    } else {
      throw new SemanticsException(ctx,
          "only species are allowed in the products");
    }
    if (node.getRight() instanceof SpeciesPatternNode) {
      reactants.add(((SpeciesPatternNode) node.getRight()).toReactant(env));
    } else if (node.getRight() instanceof MLRulesAddNode) {
      createReactants(ctx, (MLRulesAddNode) node.getRight(), reactants, env);
    } else {
      throw new SemanticsException(ctx,
          "only species are allowed in the products");
    }
  }

  @Override
  public void exitProducts(ProductsContext ctx) {
    Node node = (Node) getData().remove(ctx.getChild(0));

    List<Reactant> products = new ArrayList<>();
    if (node instanceof SpeciesPatternNode) {
      SpeciesPatternNode spn = (SpeciesPatternNode) node;
      products.add(spn.toReactant(getEnv()));
    } else if (node instanceof MLRulesAddNode) {
      MLRulesAddNode addNode = (MLRulesAddNode) node;
      createReactants(ctx, addNode, products, getEnv());
    } else if (node == null) {
      // do nothing
    } else {
      throw new SemanticsException(ctx,
          "only species are allowed in the products");
    }
    if (products.stream().anyMatch(p -> p.getBoundTo() != "$$$")) {
      throw new SemanticsException(ctx,
          "product species cannot be bound to variables");
    }
    getData().put(ctx, products);
    products.forEach(r -> determineCompartmentsAndLeaves(Optional.empty(), r,
        ctx));
    inProduct = false;
  }

  @Override
  public void enterMlrule(MlruleContext ctx) {
    inRule = true;
  }

  @Override
  public void enterReactants(ReactantsContext ctx) {
    inReactant = true;
    super.enterReactants(ctx);
  }

  @Override
  public void enterProducts(ProductsContext ctx) {
    inProduct = true;
    super.enterProducts(ctx);
  }

  @Override
  @SuppressWarnings("unchecked")
  public void exitMlrule(MlruleContext ctx) {
    List<Assignment> assignments =
        (List<Assignment>) getData().remove(ctx.getChild(5));
    List<Reactant> reactants =
        (List<Reactant>) getData().remove(ctx.getChild(0));
    List<Reactant> products =
        (List<Reactant>) getData().remove(ctx.getChild(2));
    if (!StaticRule.isValidTauRule(reactants, products, reactantProducts)) {
      throw new SemanticsException(ctx,
          "rules must not change the structure of the model");
    }

    if (ctx.AT() != null) {
      rules.add(
          new Rule(reactants, products, (Node) getData().remove(ctx.getChild(4)),
              assignments == null ? new ArrayList<>() : assignments));
    } else {
      double time =
          NodeHelper.getDouble((Node) getData().remove(ctx.getChild(4)),
              getEnv());
      if (ctx.ATEACH() != null) {
        timedRules.add(
            new PeriodicRule(time, reactants, products, new ValueNode<Double>(time),
                assignments == null ? new ArrayList<>() : assignments));
      } else {
        timedRules.add(
            new Rule(reactants, products, new ValueNode<Double>(time),
                assignments == null ? new ArrayList<>() : assignments));
      }
    }
    getData().clear();

    variables.clear();
    inRule = false;
  }

  @Override
  public void exitRate(RateContext ctx) {
    getData().put(ctx, getData().remove(ctx.getChild(0)));
  }
  
  @Override
  public Model create() {
    SimpleReactionCreator creator = new SimpleReactionCreator();
    Map<Species, List<Reaction>> reactions = new HashMap<>();
    List<Reaction> infiniteReactions = new ArrayList<>();
    Map<Species, List<Reaction>> timedReactions = new HashMap<>();
    creator.createInitialReactions(initialSolution, rules, reactions, infiniteReactions, getEnv(), reactantProducts);
    for (Iterator<Entry<Species, List<Reaction>>> i = reactions.entrySet().iterator(); i.hasNext(); ) {
      if (i.next().getValue().isEmpty()) {
        i.remove();
      }
    }
    
    creator.createInitialReactions(initialSolution, timedRules, timedReactions, Collections.emptyList(), getEnv(), reactantProducts);
    timedReactions.values().forEach(l -> l.forEach(tr -> tr.updateAlways()));
    for (Iterator<Entry<Species, List<Reaction>>> i = timedReactions.entrySet().iterator(); i.hasNext(); ) {
      if (i.next().getValue().isEmpty()) {
        i.remove();
      }
    }
    infiniteReactions.forEach(ir -> ir.updateAlways());
    
    return new SimpleModel(initialSolution, reactions, infiniteReactions, timedReactions, getEnv());
  }

}
