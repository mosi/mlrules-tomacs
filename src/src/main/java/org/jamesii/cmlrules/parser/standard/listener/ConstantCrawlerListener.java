/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.listener;

import java.util.Map;

import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ConstantContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FunctionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.MlruleContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SpeciesDefinitionContext;
import org.jamesii.cmlrules.parser.standard.functions.Function;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;

public class ConstantCrawlerListener extends ExpressionsListener {

  private final static int CONST_CHILD = 2;

  Map<String, Object> parameter;

  public ConstantCrawlerListener(MLEnvironment env,
      Map<String, Object> parameter) {
    super(env);
    this.parameter = parameter;
  }

  @Override
  public void exitConstant(ConstantContext ctx) {
    String name = ctx.ID().toString();
    try {
      if (getEnv().containsIdent(name)
          && !(getEnv().getValue(name) instanceof Function && ((Function) getEnv()
              .getValue(name)).getDefinitions().isEmpty())) {
        throw new SemanticsException(ctx, String.format(
            "constant name %s is already used", name));
      }

      if (parameter.containsKey(name)) {
        getEnv().setGlobalValue(name, parameter.get(name));
      } else {
        Node node = (Node) getData().remove(ctx.getChild(CONST_CHILD));
        Node result = node.calc(getEnv());
        if (result instanceof ValueNode<?>
            && ((ValueNode<?>) result).getValue() != null) {

          getEnv().setGlobalValue(name, ((ValueNode<?>) result).getValue());
        } else {
          throw new SemanticsException(ctx, String.format(
              "could not calculate node %s", node.toString()));
        }
      }
    } catch (Exception e) {
      throw new SemanticsException(ctx, String.format(
          "could not compute the value of %s", name));
    }
  }

  @Override
  public void exitFunction(FunctionContext ctx) {
    getData().clear();
  }

  @Override
  public void exitMlrule(MlruleContext ctx) {
    getData().clear();
  }

  @Override
  public void exitSpeciesDefinition(SpeciesDefinitionContext ctx) {
    getData().clear();
  }

}
