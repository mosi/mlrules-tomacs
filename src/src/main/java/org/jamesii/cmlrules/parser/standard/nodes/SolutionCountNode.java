/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.nodes;

import java.util.Map;

import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;

public class SolutionCountNode extends Node {

  private static final long serialVersionUID = 1L;

  public static final String SOL = "§sol";

  public static final String NAME = "§name";

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    if (cEnv instanceof MLEnvironment) {
      MLEnvironment env = (MLEnvironment) cEnv;
      Object s = env.getValue(SOL);
      Object n = env.getValue(NAME);
      if (s instanceof Map<?, ?> && n instanceof String) {
        Map<?, ?> list = (Map<?, ?>) s;
        String name = (String) n;

        double num = 0D;
        for (Object species : list.keySet()) {
          Species tmp = (Species) species;
          if (tmp.getType().getName().equals(name)) {
            num += ((Species) species).getAmount();
          }
        }
        return (N) new ValueNode<Double>(num);
      }
    }
    throw new IllegalArgumentException(
        "Could not compute the number of species");
  }

  @Override
  public String toString() {
    return "count()";
  }

}
