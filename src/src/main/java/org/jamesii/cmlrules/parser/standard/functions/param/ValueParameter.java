/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.functions.param;

import java.util.Map;

import org.jamesii.core.math.parsetree.ValueNode;

public class ValueParameter implements Parameter {

  private final ValueNode<?> value;

  public ValueParameter(ValueNode<?> value) {
    this.value = value;
  }

  @Override
  public String toString() {
    try {
    return value.getValue().toString();
    }catch (Exception e) {
      System.out.println("here");
    }
    return "";
  }

  @Override
  public boolean match(Object value, Map<String, Object> vars) {
    return value.equals(this.value.getValue());
  }

}
