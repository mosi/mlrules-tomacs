// Generated from Expressions.g4 by ANTLR 4.3
package org.jamesii.cmlrules.parser.standard.antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ExpressionsParser}.
 */
public interface ExpressionsListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code BoolExpr}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBoolExpr(@NotNull ExpressionsParser.BoolExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BoolExpr}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBoolExpr(@NotNull ExpressionsParser.BoolExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExpressionsParser#guard}.
	 * @param ctx the parse tree
	 */
	void enterGuard(@NotNull ExpressionsParser.GuardContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionsParser#guard}.
	 * @param ctx the parse tree
	 */
	void exitGuard(@NotNull ExpressionsParser.GuardContext ctx);

	/**
	 * Enter a parse tree produced by the {@code True}
	 * labeled alternative in {@link ExpressionsParser#value}.
	 * @param ctx the parse tree
	 */
	void enterTrue(@NotNull ExpressionsParser.TrueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code True}
	 * labeled alternative in {@link ExpressionsParser#value}.
	 * @param ctx the parse tree
	 */
	void exitTrue(@NotNull ExpressionsParser.TrueContext ctx);

	/**
	 * Enter a parse tree produced by the {@code False}
	 * labeled alternative in {@link ExpressionsParser#value}.
	 * @param ctx the parse tree
	 */
	void enterFalse(@NotNull ExpressionsParser.FalseContext ctx);
	/**
	 * Exit a parse tree produced by the {@code False}
	 * labeled alternative in {@link ExpressionsParser#value}.
	 * @param ctx the parse tree
	 */
	void exitFalse(@NotNull ExpressionsParser.FalseContext ctx);

	/**
	 * Enter a parse tree produced by the {@code String}
	 * labeled alternative in {@link ExpressionsParser#value}.
	 * @param ctx the parse tree
	 */
	void enterString(@NotNull ExpressionsParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by the {@code String}
	 * labeled alternative in {@link ExpressionsParser#value}.
	 * @param ctx the parse tree
	 */
	void exitString(@NotNull ExpressionsParser.StringContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Int}
	 * labeled alternative in {@link ExpressionsParser#value}.
	 * @param ctx the parse tree
	 */
	void enterInt(@NotNull ExpressionsParser.IntContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Int}
	 * labeled alternative in {@link ExpressionsParser#value}.
	 * @param ctx the parse tree
	 */
	void exitInt(@NotNull ExpressionsParser.IntContext ctx);

	/**
	 * Enter a parse tree produced by the {@code tuple}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterTuple(@NotNull ExpressionsParser.TupleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tuple}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitTuple(@NotNull ExpressionsParser.TupleContext ctx);

	/**
	 * Enter a parse tree produced by the {@code ExpValue}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpValue(@NotNull ExpressionsParser.ExpValueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExpValue}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpValue(@NotNull ExpressionsParser.ExpValueContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Roof}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterRoof(@NotNull ExpressionsParser.RoofContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Roof}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitRoof(@NotNull ExpressionsParser.RoofContext ctx);

	/**
	 * Enter a parse tree produced by the {@code amountID}
	 * labeled alternative in {@link ExpressionsParser#amount}.
	 * @param ctx the parse tree
	 */
	void enterAmountID(@NotNull ExpressionsParser.AmountIDContext ctx);
	/**
	 * Exit a parse tree produced by the {@code amountID}
	 * labeled alternative in {@link ExpressionsParser#amount}.
	 * @param ctx the parse tree
	 */
	void exitAmountID(@NotNull ExpressionsParser.AmountIDContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Real}
	 * labeled alternative in {@link ExpressionsParser#value}.
	 * @param ctx the parse tree
	 */
	void enterReal(@NotNull ExpressionsParser.RealContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Real}
	 * labeled alternative in {@link ExpressionsParser#value}.
	 * @param ctx the parse tree
	 */
	void exitReal(@NotNull ExpressionsParser.RealContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExpressionsParser#where}.
	 * @param ctx the parse tree
	 */
	void enterWhere(@NotNull ExpressionsParser.WhereContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionsParser#where}.
	 * @param ctx the parse tree
	 */
	void exitWhere(@NotNull ExpressionsParser.WhereContext ctx);

	/**
	 * Enter a parse tree produced by the {@code FunctionCall}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(@NotNull ExpressionsParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code FunctionCall}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(@NotNull ExpressionsParser.FunctionCallContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Plus}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPlus(@NotNull ExpressionsParser.PlusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Plus}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPlus(@NotNull ExpressionsParser.PlusContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExpressionsParser#subSpecies}.
	 * @param ctx the parse tree
	 */
	void enterSubSpecies(@NotNull ExpressionsParser.SubSpeciesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionsParser#subSpecies}.
	 * @param ctx the parse tree
	 */
	void exitSubSpecies(@NotNull ExpressionsParser.SubSpeciesContext ctx);

	/**
	 * Enter a parse tree produced by the {@code amountExpr}
	 * labeled alternative in {@link ExpressionsParser#amount}.
	 * @param ctx the parse tree
	 */
	void enterAmountExpr(@NotNull ExpressionsParser.AmountExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code amountExpr}
	 * labeled alternative in {@link ExpressionsParser#amount}.
	 * @param ctx the parse tree
	 */
	void exitAmountExpr(@NotNull ExpressionsParser.AmountExprContext ctx);

	/**
	 * Enter a parse tree produced by the {@code SpeciesExpr}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSpeciesExpr(@NotNull ExpressionsParser.SpeciesExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SpeciesExpr}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSpeciesExpr(@NotNull ExpressionsParser.SpeciesExprContext ctx);

	/**
	 * Enter a parse tree produced by the {@code singleAssign}
	 * labeled alternative in {@link ExpressionsParser#assignName}.
	 * @param ctx the parse tree
	 */
	void enterSingleAssign(@NotNull ExpressionsParser.SingleAssignContext ctx);
	/**
	 * Exit a parse tree produced by the {@code singleAssign}
	 * labeled alternative in {@link ExpressionsParser#assignName}.
	 * @param ctx the parse tree
	 */
	void exitSingleAssign(@NotNull ExpressionsParser.SingleAssignContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Free}
	 * labeled alternative in {@link ExpressionsParser#value}.
	 * @param ctx the parse tree
	 */
	void enterFree(@NotNull ExpressionsParser.FreeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Free}
	 * labeled alternative in {@link ExpressionsParser#value}.
	 * @param ctx the parse tree
	 */
	void exitFree(@NotNull ExpressionsParser.FreeContext ctx);

	/**
	 * Enter a parse tree produced by the {@code EmptySolution}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterEmptySolution(@NotNull ExpressionsParser.EmptySolutionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code EmptySolution}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitEmptySolution(@NotNull ExpressionsParser.EmptySolutionContext ctx);

	/**
	 * Enter a parse tree produced by the {@code MultDiv}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMultDiv(@NotNull ExpressionsParser.MultDivContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MultDiv}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMultDiv(@NotNull ExpressionsParser.MultDivContext ctx);

	/**
	 * Enter a parse tree produced by the {@code MinusOne}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMinusOne(@NotNull ExpressionsParser.MinusOneContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MinusOne}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMinusOne(@NotNull ExpressionsParser.MinusOneContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Not}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNot(@NotNull ExpressionsParser.NotContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Not}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNot(@NotNull ExpressionsParser.NotContext ctx);

	/**
	 * Enter a parse tree produced by the {@code countShort}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCountShort(@NotNull ExpressionsParser.CountShortContext ctx);
	/**
	 * Exit a parse tree produced by the {@code countShort}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCountShort(@NotNull ExpressionsParser.CountShortContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExpressionsParser#species}.
	 * @param ctx the parse tree
	 */
	void enterSpecies(@NotNull ExpressionsParser.SpeciesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionsParser#species}.
	 * @param ctx the parse tree
	 */
	void exitSpecies(@NotNull ExpressionsParser.SpeciesContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExpressionsParser#name}.
	 * @param ctx the parse tree
	 */
	void enterName(@NotNull ExpressionsParser.NameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionsParser#name}.
	 * @param ctx the parse tree
	 */
	void exitName(@NotNull ExpressionsParser.NameContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExpressionsParser#attributes}.
	 * @param ctx the parse tree
	 */
	void enterAttributes(@NotNull ExpressionsParser.AttributesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionsParser#attributes}.
	 * @param ctx the parse tree
	 */
	void exitAttributes(@NotNull ExpressionsParser.AttributesContext ctx);

	/**
	 * Enter a parse tree produced by the {@code IfThenElse}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIfThenElse(@NotNull ExpressionsParser.IfThenElseContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IfThenElse}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIfThenElse(@NotNull ExpressionsParser.IfThenElseContext ctx);

	/**
	 * Enter a parse tree produced by the {@code amountINT}
	 * labeled alternative in {@link ExpressionsParser#amount}.
	 * @param ctx the parse tree
	 */
	void enterAmountINT(@NotNull ExpressionsParser.AmountINTContext ctx);
	/**
	 * Exit a parse tree produced by the {@code amountINT}
	 * labeled alternative in {@link ExpressionsParser#amount}.
	 * @param ctx the parse tree
	 */
	void exitAmountINT(@NotNull ExpressionsParser.AmountINTContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Id}
	 * labeled alternative in {@link ExpressionsParser#value}.
	 * @param ctx the parse tree
	 */
	void enterId(@NotNull ExpressionsParser.IdContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Id}
	 * labeled alternative in {@link ExpressionsParser#value}.
	 * @param ctx the parse tree
	 */
	void exitId(@NotNull ExpressionsParser.IdContext ctx);

	/**
	 * Enter a parse tree produced by the {@code tupleAssign}
	 * labeled alternative in {@link ExpressionsParser#assignName}.
	 * @param ctx the parse tree
	 */
	void enterTupleAssign(@NotNull ExpressionsParser.TupleAssignContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tupleAssign}
	 * labeled alternative in {@link ExpressionsParser#assignName}.
	 * @param ctx the parse tree
	 */
	void exitTupleAssign(@NotNull ExpressionsParser.TupleAssignContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Application}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterApplication(@NotNull ExpressionsParser.ApplicationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Application}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitApplication(@NotNull ExpressionsParser.ApplicationContext ctx);

	/**
	 * Enter a parse tree produced by the {@code AndOr}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAndOr(@NotNull ExpressionsParser.AndOrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AndOr}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAndOr(@NotNull ExpressionsParser.AndOrContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Minus}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMinus(@NotNull ExpressionsParser.MinusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Minus}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMinus(@NotNull ExpressionsParser.MinusContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Paren}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterParen(@NotNull ExpressionsParser.ParenContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Paren}
	 * labeled alternative in {@link ExpressionsParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitParen(@NotNull ExpressionsParser.ParenContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExpressionsParser#assign}.
	 * @param ctx the parse tree
	 */
	void enterAssign(@NotNull ExpressionsParser.AssignContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExpressionsParser#assign}.
	 * @param ctx the parse tree
	 */
	void exitAssign(@NotNull ExpressionsParser.AssignContext ctx);
}