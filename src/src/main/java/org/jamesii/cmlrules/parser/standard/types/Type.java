/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.types;

/**
 * A type is determined by a primitive type {@link Primitives} and possibly by
 * sub types, e.g., a list of integers.
 * 
 * @author Tobias Helms
 *
 */
public interface Type {

  Primitives getType();

  public static Type plus(Type p1, Type p2) {
    if (p1 != null && p2 != null) {
      switch (p1.getType()) {
      case NUM:
        switch (p2.getType()) {
        case NUM:
          return BaseType.NUM;
        case UNKNOWN:
          return BaseType.UNKNOWN;
        default:
        }
        break;
      case SPECIES:
        switch (p2.getType()) {
        case SPECIES:
        case SOL:
        case UNKNOWN:
          return BaseType.SOL;
        default:
        }
        break;
      case SOL:
        switch (p2.getType()) {
        case SPECIES:
        case SOL:
        case UNKNOWN:
          return BaseType.SOL;
        default:
        }
        break;
      case UNKNOWN:
        switch (p2.getType()) {
        case NUM:
        case UNKNOWN:
          return BaseType.UNKNOWN;
        case SPECIES:
        case SOL:
          return BaseType.SOL;
        default:
        }
        break;
      default:
      }
    }
    throw new IllegalArgumentException(String.format(
        "incompatible argument types for plus operation %s and %s", p1, p2));
  }

  public static Type arithmetic(Type p1, Type p2) {
    if (p1 != null && p2 != null) {
      switch (p1.getType()) {
      case NUM:
        switch (p2.getType()) {
        case NUM:
          return BaseType.NUM;
        case UNKNOWN:
          return BaseType.UNKNOWN;
        default:
        }
        break;
      case UNKNOWN:
        switch (p2.getType()) {
        case NUM:
        case UNKNOWN:
          return BaseType.UNKNOWN;
        default:
        }
        break;
      default:
      }
    }
    throw new IllegalArgumentException(String.format(
        "incompatible argument types for arithmetic operation %s and %s", p1,
        p2));
  }

  public static Type bool(Type p1, Type p2) {
    if (p1 != null && p2 != null
        && (p1 == BaseType.BOOL || p1 == BaseType.UNKNOWN)
        && (p2 == BaseType.BOOL || p2 == BaseType.UNKNOWN)) {
      return BaseType.BOOL;
    }
    throw new IllegalArgumentException(String.format(
        "incompatible argument types for boolean operation %s and %s", p1, p2));
  }

  public static Type comparison(Type p1, Type p2) {
    if (p1 != null && p2 != null) {
      switch (p1.getType()) {
      case NUM:
        switch (p2.getType()) {
        case NUM:
        case UNKNOWN:
          return BaseType.BOOL;
        default:
          ;
        }
        break;
      case BOOL:
      case TUPLE:
      case STRING:
        if (p1.getType() == p2.getType() || p2.equals(BaseType.UNKNOWN)) {
          return BaseType.BOOL;
        }
        break;
      case UNKNOWN:
        if (p2.getType() != Primitives.FUNCTION) {
          return BaseType.BOOL;
        }
      default:
        ;
      }
    }
    throw new IllegalArgumentException(String.format(
        "Incompatible argument types for comparison operation %s and %s", p1,
        p2));
  }

}
