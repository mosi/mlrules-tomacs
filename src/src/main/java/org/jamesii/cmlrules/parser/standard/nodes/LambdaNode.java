/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.nodes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jamesii.cmlrules.parser.standard.functions.Function;
import org.jamesii.cmlrules.parser.standard.functions.FunctionDefinition;
import org.jamesii.cmlrules.parser.standard.functions.param.Parameter;
import org.jamesii.cmlrules.parser.standard.types.FunctionType;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.core.math.parsetree.variables.Identifier;

public class LambdaNode extends Node {

  private static final long serialVersionUID = 1L;

  private final List<Parameter> parameter;

  private final Node function;

  private final FunctionType type;

  @Override
  public List<Node> getChildren() {
    return Arrays.asList(function);
  }

  private void replaceIdentifier(Node node, MLEnvironment env, Map<String, Object> values) {
    if (node instanceof Identifier<?>) {
      String name = (String) ((Identifier<?>) node).getIdent();
      Object o = env.getValue(name);
      if (o != null) {
        values.put(name, new ValueNode<Object>(o));
      }
    } else {
      node.getChildren().forEach(c -> replaceIdentifier((Node) c, env, values));
    }
  }

  public LambdaNode(List<Parameter> parameter, FunctionType type, Node function) {
    this.parameter = parameter;
    this.function = function;
    this.type = type;
  }

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    try {
      Node copy = (Node) function.clone();
      Map<String, Object> values = new HashMap<>();
      replaceIdentifier(copy, (MLEnvironment) cEnv, values);
      Function f = new Function("§lambda", null, values);
      f.init(Arrays.asList(new FunctionDefinition(parameter, copy,
          new ArrayList<>())));
      return (N) new ValueNode<Function>(f);
    } catch (CloneNotSupportedException e) {
      throw new IllegalArgumentException(e.getMessage());
    }
  }

  @Override
  public String toString() {
    return "\\" + parameter.toString() + function.toString();
  }

}
