/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.functions.param;

import java.util.List;
import java.util.Map;

import org.jamesii.cmlrules.parser.standard.types.Tuple;

public class TupleParameter implements Parameter {

  private final List<Parameter> subParameter;

  public TupleParameter(List<Parameter> subParameter) {
    this.subParameter = subParameter;
  }

  @Override
  public String toString() {
    return subParameter.toString();
  }

  @Override
  public boolean match(Object value, Map<String, Object> vars) {
    if (value instanceof Tuple) {
      Tuple tuple = (Tuple) value;
      if (tuple.size() != subParameter.size()) {
        return false;
      }
      for (int i = 0; i < tuple.size(); ++i) {
        if (!subParameter.get(i).match(tuple.get(i), vars)) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

}
