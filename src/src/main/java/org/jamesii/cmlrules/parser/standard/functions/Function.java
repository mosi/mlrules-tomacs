/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.functions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import org.jamesii.cmlrules.parser.standard.types.FunctionType;
import org.jamesii.cmlrules.parser.standard.types.Type;

public class Function {

  private final String name;

  private final FunctionType type;
  
  private final List<FunctionDefinition> definitions = new ArrayList<>();
  
  private final Map<String, Object> values;

  public Function(String name, Type type) {
    this.name = name;
    this.type = (FunctionType) type;
    values = new HashMap<>();
  }
  
  public Function(String name, Type type, Map<String, Object> values) {
    this.name = name;
    this.type = (FunctionType) type;
    this.values = values;
  }

  public String getName() {
    return name;
  }

  public List<FunctionDefinition> getDefinitions() {
    return definitions;
  }

  public Map<String, Object> getValues() {
    return values;
  }
  
  public FunctionType getType() {
    return type;
  }
  
  public void init(List<FunctionDefinition> definitions) {
    if (this.definitions.isEmpty()) {
      this.definitions.addAll(definitions);
    } else {
      throw new IllegalArgumentException(String.format(
          "Function %s cannot be initialized twice.", name));
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(name);
    StringJoiner joiner = new StringJoiner(",", "[", "]");
    definitions.forEach(d -> joiner.add(d.toString()));
    builder.append(joiner.toString());
    return builder.toString();
  }
}
