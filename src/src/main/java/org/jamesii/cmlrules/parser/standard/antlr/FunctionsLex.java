// Generated from FunctionsLex.g4 by ANTLR 4.3
package org.jamesii.cmlrules.parser.standard.antlr;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class FunctionsLex extends Lexer {
	static { RuntimeMetaData.checkVersion("4.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		TYPE_SEPERATOR=1, ARROW=2, LET=3, IN=4, WHERE=5, TYPE_REAL=6, TYPE_BOOL=7, 
		TYPE_STRING=8, TYPE_SPECIES=9, TYPE_PATTERN=10, TYPE_TUPLE=11, TYPE_SOL=12, 
		TYPE_LINK=13;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'"
	};
	public static final String[] ruleNames = {
		"TYPE_SEPERATOR", "ARROW", "LET", "IN", "WHERE", "TYPE_REAL", "TYPE_BOOL", 
		"TYPE_STRING", "TYPE_SPECIES", "TYPE_PATTERN", "TYPE_TUPLE", "TYPE_SOL", 
		"TYPE_LINK"
	};


	public FunctionsLex(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "FunctionsLex.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\17_\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\3\2\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3\4\3\4"+
		"\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3"+
		"\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3"+
		"\13\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r"+
		"\3\r\3\16\3\16\3\16\3\16\3\16\2\2\17\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21"+
		"\n\23\13\25\f\27\r\31\16\33\17\3\2\2^\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2"+
		"\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2"+
		"\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\3\35\3"+
		"\2\2\2\5 \3\2\2\2\7#\3\2\2\2\t\'\3\2\2\2\13*\3\2\2\2\r\60\3\2\2\2\17\64"+
		"\3\2\2\2\219\3\2\2\2\23@\3\2\2\2\25H\3\2\2\2\27P\3\2\2\2\31V\3\2\2\2\33"+
		"Z\3\2\2\2\35\36\7<\2\2\36\37\7<\2\2\37\4\3\2\2\2 !\7/\2\2!\"\7@\2\2\""+
		"\6\3\2\2\2#$\7n\2\2$%\7g\2\2%&\7v\2\2&\b\3\2\2\2\'(\7k\2\2()\7p\2\2)\n"+
		"\3\2\2\2*+\7y\2\2+,\7j\2\2,-\7g\2\2-.\7t\2\2./\7g\2\2/\f\3\2\2\2\60\61"+
		"\7p\2\2\61\62\7w\2\2\62\63\7o\2\2\63\16\3\2\2\2\64\65\7d\2\2\65\66\7q"+
		"\2\2\66\67\7q\2\2\678\7n\2\28\20\3\2\2\29:\7u\2\2:;\7v\2\2;<\7t\2\2<="+
		"\7k\2\2=>\7p\2\2>?\7i\2\2?\22\3\2\2\2@A\7u\2\2AB\7r\2\2BC\7g\2\2CD\7e"+
		"\2\2DE\7k\2\2EF\7g\2\2FG\7u\2\2G\24\3\2\2\2HI\7r\2\2IJ\7c\2\2JK\7v\2\2"+
		"KL\7v\2\2LM\7g\2\2MN\7t\2\2NO\7p\2\2O\26\3\2\2\2PQ\7v\2\2QR\7w\2\2RS\7"+
		"r\2\2ST\7n\2\2TU\7g\2\2U\30\3\2\2\2VW\7u\2\2WX\7q\2\2XY\7n\2\2Y\32\3\2"+
		"\2\2Z[\7n\2\2[\\\7k\2\2\\]\7p\2\2]^\7m\2\2^\34\3\2\2\2\3\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}