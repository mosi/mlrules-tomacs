/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.nodes;

import java.util.Map;

import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.math.AddNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;

public class MLRulesAddNode extends AddNode {

  private static final long serialVersionUID = 1L;

  public MLRulesAddNode(Node left, Node right) {
    super(left, right);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    Node leftResult = getLeft().calc(cEnv);
    Node rightResult = getRight().calc(cEnv);

    if (leftResult instanceof ValueNode<?>
        && rightResult instanceof ValueNode<?>) {
      if (((ValueNode<?>) leftResult).getValue() instanceof Map<?, ?>
          && ((ValueNode<?>) rightResult).getValue() instanceof Map<?, ?>) {
        Map<Species, Species> leftSpecies =
            (Map<Species, Species>) ((ValueNode<?>) leftResult).getValue();
        Map<Species, Species> rightSpecies =
            (Map<Species, Species>) ((ValueNode<?>) rightResult).getValue();
        rightSpecies.keySet().stream().forEach(s -> {
          Species species = leftSpecies.computeIfAbsent(s, s2 -> s2);
          if (species != s) {
            species.setAmount(species.getAmount() + s.getAmount());
          }
        });
        return (N) new ValueNode<Map<Species, Species>>(leftSpecies);
      }
      return (N) super.calc((ValueNode<?>)leftResult, (ValueNode<?>)rightResult);
    }
    return super.calc(cEnv);
  }

}
