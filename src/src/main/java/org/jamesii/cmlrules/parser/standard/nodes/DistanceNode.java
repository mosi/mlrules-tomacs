package org.jamesii.cmlrules.parser.standard.nodes;

import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;

public class DistanceNode extends Node {

  private static final long serialVersionUID = 1L;

  public static final String X1 = "§x1";

  public static final String X2 = "§x2";

  public static final String Y1 = "§y1";

  public static final String Y2 = "§y2";

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    if (cEnv instanceof MLEnvironment) {
      MLEnvironment env = (MLEnvironment) cEnv;
      Double x1 = (Double) env.getValue(X1);
      Double x2 = (Double) env.getValue(X2);
      Double y1 = (Double) env.getValue(Y1);
      Double y2 = (Double) env.getValue(Y2);
      if (x1 == null || x2 == null || y1 == null || y2 == null) {
        throw new IllegalArgumentException(String.format(
            "invalid coordinates (%s,%s) and (%s,%s)", x1, y1, x2, y2));
      }
      return (N) new ValueNode<Double>(Math.sqrt(Math.pow(x1 - x2, 2)
          + Math.pow(y1 - y2, 2)));
    }
    throw new IllegalArgumentException(
        "the given environment to calculate the distance of two coordinates is not an MLEnvironment");
  }

}
