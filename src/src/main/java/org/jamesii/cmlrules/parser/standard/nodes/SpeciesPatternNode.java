/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.nodes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;

import org.jamesii.cmlrules.model.standard.rule.Reactant;
import org.jamesii.cmlrules.model.standard.species.AbstractSpecies;
import org.jamesii.cmlrules.model.standard.species.MapSpecies;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.parser.standard.types.Tuple;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.core.math.parsetree.variables.Identifier;

public class SpeciesPatternNode extends Node {

  private static final long serialVersionUID = 1L;

  private final Node amountNode;

  private final Node typeNode;

  private final Node[] attributeNodes;

  private final Node subNode;

  private final String boundTo;
  
  private final Node guardNode;

  @SuppressWarnings("unchecked")
  private String createReactants(MLRulesAddNode node, List<Reactant> reactants,
      MLEnvironment env) {
    if (node.getLeft() instanceof SpeciesPatternNode) {
      reactants.add(((SpeciesPatternNode) node.getLeft()).toReactant(env));
    } else if (node.getLeft() instanceof MLRulesAddNode) {
      createReactants((MLRulesAddNode) node.getLeft(), reactants, env);
    }
    if (node.getRight() instanceof SpeciesPatternNode) {
      reactants.add(((SpeciesPatternNode) node.getRight()).toReactant(env));
    } else if (node.getRight() instanceof MLRulesAddNode) {
      return createReactants((MLRulesAddNode) node.getRight(), reactants, env);
    } else if (node.getRight() instanceof Identifier<?>) {
      return ((Identifier<String>) node.getRight()).getIdent();
    }
    return "$sol?";
  }

  public SpeciesPatternNode(Node amount, Node type, Node[] attributes,
      Node subNode, Node guardNode, String boundTo) {
    this.amountNode = amount;
    this.typeNode = type;
    this.attributeNodes = attributes;
    this.subNode = subNode;
    this.boundTo = boundTo;
    this.guardNode = guardNode;
  }

  @Override
  public List<Node> getChildren() {
    List<Node> result = Arrays.asList(amountNode);
    if (subNode != null) {
      result.add(subNode);
    }
    result.add(typeNode);
    result.addAll(Arrays.asList(attributeNodes));
    return result;
  }

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    MLEnvironment env = (MLEnvironment) cEnv;

    if (!boundTo.startsWith("$")) {
      return (N) this;
    }
    
    if (guardNode != null) {
      return (N) this;
    }

    Node tmp = amountNode.calc(cEnv);
    int amount;
    if (tmp instanceof ValueNode<?>) {
      if (((ValueNode<?>) tmp).getValue() instanceof Double) {
        amount = ((Double) ((ValueNode<Double>) tmp).getValue()).intValue();
      } else {
        amount = (Integer) ((ValueNode<Integer>) tmp).getValue();
      }
    } else {
      return (N) this;
    }

    if (amount < 0) {
      return (N) new ValueNode<Map<Species, Species>>(new HashMap<>());
    }

    tmp = typeNode.calc(cEnv);
    String name = null;
    if (tmp instanceof ValueNode<?>
        && ((ValueNode<?>) tmp).getValue() instanceof String) {
      name = (String) ((ValueNode<?>) tmp).getValue();
    }
    SpeciesType type = (SpeciesType) env.getValue(name);
    if (type == null) {
      throw new IllegalArgumentException(String.format(
          "Unknown species %s cannot be created.", name));
    }

    Object[] attributes = new Object[type.getAttributes()];
    for (int i = 0; i < attributeNodes.length; ++i) {
      tmp = attributeNodes[i].calc(cEnv);
      if (tmp instanceof ValueNode<?>
          && ((ValueNode<?>) tmp).getValue() != null
          && ((ValueNode<?>) tmp).getValue() instanceof Tuple) {
        Tuple values = (Tuple) ((ValueNode<?>) tmp).getValue();
        if (attributeNodes.length != 1 || values.size() != attributes.length) {
          throw new IllegalArgumentException(String.format(
              "invalid number of attributes for %s", type.getName()));
        }
        for (int j = 0; j < values.size(); ++j) {
          attributes[j] = values.get(j);
          if (attributes[j] == null) {
            throw new IllegalArgumentException(String.format(
                "attribute %s of species type %s is null", j, type.getName()));
          }
        }
      } else if (tmp instanceof ValueNode<?>) {
        attributes[i] = ((ValueNode<?>) tmp).getValue();
        if (attributes[i] == null) {
          throw new IllegalArgumentException(String.format(
              "attribute %s of species type %s is null", i, type.getName()));
        }
      } else {
        return (N) this;
      }
    }

    Map<Species, Species> subSpecies = new HashMap<>();
    if (subNode != null) {
      Node result = subNode.calc(cEnv);
      if (result instanceof ValueNode<?>
          && ((ValueNode<?>) result).getValue() instanceof Map<?, ?>) {
        ((Map<Species, Species>) ((ValueNode<?>) result).getValue()).forEach((
            k, v) -> {
          Species copy = k.copy();
          subSpecies.put(copy, copy);
        });
      } else {
        return (N) this;
      }
    }

    Species newSpecies =
        new MapSpecies(subSpecies, amount, type, attributes,
            AbstractSpecies.UNKNOWN);
    Map<Species, Species> resultMap = new HashMap<>();
    subSpecies.keySet().forEach(s -> s.setContext(newSpecies));
    if (!subSpecies.isEmpty()) {
      for (int i = 0; i < amount; ++i) {
        Species copy = newSpecies.copy();
        copy.setAmount(1.0);
        resultMap.put(copy,copy);
      }
    } else {
    resultMap.put(newSpecies, newSpecies);
    }
    return (N) new ValueNode<Map<Species, Species>>(resultMap);
  }

  @SuppressWarnings("unchecked")
  public Reactant toReactant(MLEnvironment env) {
    Node tmp = typeNode.calc(env);
    String name = null;
    if (tmp instanceof ValueNode<?>
        && ((ValueNode<?>) tmp).getValue() instanceof String) {
      name = (String) ((ValueNode<?>) tmp).getValue();
    }
    SpeciesType type = (SpeciesType) env.getValue(name);

    List<Reactant> subReactants = new ArrayList<>();
    String rest = "$sol?";
    if (subNode instanceof SpeciesPatternNode) {
      subReactants.add(((SpeciesPatternNode) subNode).toReactant(env));
    } else if (subNode instanceof MLRulesAddNode) {
      rest = createReactants((MLRulesAddNode) subNode, subReactants, env);
    } else if (subNode instanceof Identifier<?>) {
      rest = ((Identifier<String>) subNode).getIdent();
    }

    return new Reactant(type, amountNode, Arrays.asList(attributeNodes), rest,
        boundTo, Optional.ofNullable(guardNode), subReactants);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(amountNode.toString());
    builder.append(typeNode.toString());
    StringJoiner joiner = new StringJoiner(",", "[", "]");
    for (Node n : attributeNodes) {
      joiner.add(n.toString());
    }
    builder.append(joiner.toString());
    if (subNode != null) {
      builder.append(subNode.toString());
    }
    builder.append("{");
    builder.append(guardNode);
    builder.append("}");
    builder.append(":" + boundTo);
    return builder.toString();
  }
}
