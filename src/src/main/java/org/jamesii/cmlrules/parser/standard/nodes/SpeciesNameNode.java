/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.nodes;

import java.util.Map;

import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;

public class SpeciesNameNode extends Node {

  private static final long serialVersionUID = 1L;

  public static final String NAME = "§species";

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    if (cEnv instanceof MLEnvironment) {
      MLEnvironment env = (MLEnvironment) cEnv;
      Object n = env.getValue(NAME);
      if (n instanceof Map<?, ?>) {
        Map<?, ?> species = (Map<?, ?>) n;
        if (species.size() != 1) {
          throw new IllegalArgumentException(
              String
                  .format(
                      "The name function is only applicable for one species and not for %s.",
                      species));
        }
        Object o = species.entrySet().iterator().next().getKey();
        if (o instanceof Species) {
          return (N) new ValueNode<String>(((Species) o).getType().getName());
        }
      }
    }
    throw new IllegalArgumentException(String.format(
        "Could not compute the name of the species bound to %s", NAME));
  }

  @Override
  public String toString() {
    return "name()";
  }

}
