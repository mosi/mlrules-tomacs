/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

lexer grammar FunctionsLex;

TYPE_SEPERATOR  :   '::';
ARROW           :   '->';
LET             :   'let';
IN				:   'in';
WHERE			:   'where';

// type keywords
TYPE_REAL       :   'num';
TYPE_BOOL       :   'bool';
TYPE_STRING     :   'string';
TYPE_SPECIES    :   'species';
TYPE_PATTERN    :   'pattern';
TYPE_TUPLE		:   'tuple';
TYPE_SOL		:   'sol';
TYPE_LINK		:	'link';
