/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.listener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ConstantContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FunctionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FunctionDefinitionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.MlruleContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ParamSimpleContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ParamSingleContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ParamSpeciesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ParamTupleContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SpeciesDefinitionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.TypeDefinitionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.WhereContext;
import org.jamesii.cmlrules.parser.standard.functions.Function;
import org.jamesii.cmlrules.parser.standard.functions.FunctionDefinition;
import org.jamesii.cmlrules.parser.standard.functions.param.EmptySolParameter;
import org.jamesii.cmlrules.parser.standard.functions.param.Parameter;
import org.jamesii.cmlrules.parser.standard.functions.param.SimpleParameter;
import org.jamesii.cmlrules.parser.standard.functions.param.SolParameter;
import org.jamesii.cmlrules.parser.standard.functions.param.SpeciesParameter;
import org.jamesii.cmlrules.parser.standard.functions.param.TupleParameter;
import org.jamesii.cmlrules.parser.standard.functions.param.ValueParameter;
import org.jamesii.cmlrules.util.Assignment;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.Identifier;

public class FunctionDefinitionListener extends ExpressionsListener {

  public FunctionDefinitionListener(MLEnvironment env) {
    super(env);
  }

  @SuppressWarnings("unchecked")
  @Override
  public void exitParamSimple(ParamSimpleContext ctx) {
    Node node = (Node) getData().remove(ctx.getChild(0));
    if (node instanceof Identifier<?>) {
      getData().put(ctx,
          new SimpleParameter(((Identifier<String>) node).getIdent()));
    } else {
      getData().put(ctx, new ValueParameter((ValueNode<?>) node));
    }
  }

  @Override
  public void exitParamSol(@NotNull MLRulesParser.ParamSolContext ctx) {
    getData().put(ctx,
        new SolParameter(ctx.ID(0).getText(), ctx.ID(1).getText()));
  }

  @Override
  public void exitEmptySol(@NotNull MLRulesParser.EmptySolContext ctx) {
    getData().put(ctx, new EmptySolParameter());
  }

  @Override
  public void exitParamTuple(@NotNull MLRulesParser.ParamTupleContext ctx) {
    List<Parameter> subParameter = new ArrayList<>();
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof RuleContext) {
        subParameter.add((Parameter) getData().remove(ctx.getChild(i)));
      }
    }
    getData().put(ctx, new TupleParameter(subParameter));
  }

  @SuppressWarnings("unchecked")
  @Override
  public void exitParamSpecies(ParamSpeciesContext ctx) {
    List<Parameter> subParameter = new ArrayList<>();
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof RuleContext) {
        Node node = (Node) getData().remove(ctx.getChild(i));
        if (node instanceof Identifier<?>) {
          subParameter.add(new SimpleParameter(((Identifier<String>) node)
              .getIdent()));
        } else {
          subParameter.add(new ValueParameter((ValueNode<?>) node));
        }
      }
    }

    SpeciesType type = (SpeciesType) getEnv().getValue(
        ctx.ID_SPECIES().getText());
    
    if (type.getAttributes() != subParameter.size()) {
      throw new SemanticsException(ctx, String.format(
          "Species %s has %s attributes and not %s attributes.",
          type.getName(), type.getAttributes(), subParameter.size()));
    }
    
    getData().put(
        ctx,
        new SpeciesParameter(type, subParameter));
  }

  @Override
  public void exitParamSingle(ParamSingleContext ctx) {
    getData().put(ctx, getData().remove(ctx.getChild(0)));
  }

  @SuppressWarnings("unchecked")
  @Override
  public void exitFunctionDefinition(FunctionDefinitionContext ctx) {
    List<Parameter> parameter = new ArrayList<>();
    List<Assignment> assignments = new ArrayList<>();
    Node expr = null;
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof ParamSingleContext
          || ctx.getChild(i) instanceof ParamTupleContext
          || ctx.getChild(i) instanceof ParamSpeciesContext) {
        parameter.add((Parameter) getData().remove(ctx.getChild(i)));
      } else if (ctx.getChild(i) instanceof WhereContext) {
        assignments.addAll((Collection<? extends Assignment>) getData().remove(
            ctx.getChild(i)));
      } else if (ctx.getChild(i) instanceof RuleContext) {
        expr = (Node) getData().remove(ctx.getChild(i));
      }
    }

    getData().put(ctx, new FunctionDefinition(parameter, expr, assignments));
  }

  @Override
  public void exitFunction(FunctionContext ctx) {
    String name = ((TypeDefinitionContext) ctx.getChild(0)).ID().getText();
    List<FunctionDefinition> definitions = new ArrayList<>();
    for (int i = 1; i < ctx.getChildCount(); ++i) {
      definitions.add((FunctionDefinition) getData().remove(ctx.getChild(i)));
    }
    Function f = (Function) getEnv().getValue(name);
    f.init(definitions);
  }

  @Override
  public void exitConstant(ConstantContext ctx) {
    getData().clear();
  }

  @Override
  public void exitMlrule(MlruleContext ctx) {
    getData().clear();
  }

  @Override
  public void exitSpeciesDefinition(SpeciesDefinitionContext ctx) {
    getData().clear();
  }

}
