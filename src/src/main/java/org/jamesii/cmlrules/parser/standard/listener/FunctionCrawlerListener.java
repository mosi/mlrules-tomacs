/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesBaseListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FunctionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FunctionDefinitionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.TypeDefinitionContext;
import org.jamesii.cmlrules.parser.standard.functions.Function;
import org.jamesii.cmlrules.parser.standard.types.BaseType;
import org.jamesii.cmlrules.parser.standard.types.FunctionType;
import org.jamesii.cmlrules.parser.standard.types.Type;
import org.jamesii.cmlrules.util.MLEnvironment;

public class FunctionCrawlerListener extends MLRulesBaseListener {

  private final Map<ParserRuleContext, Object> data = new HashMap<>();

  private final MLEnvironment env;

  public FunctionCrawlerListener(MLEnvironment env) {
    this.env = env;
  }

  @Override
  public void exitTypeDefinition(TypeDefinitionContext ctx) {
    if (env.containsIdent(ctx.ID().getText())) {
      throw new SemanticsException(ctx, String.format(
          "name %s is used ambigously", ctx.ID().getText()));
    }

    List<Type> subTypes = new ArrayList<>();
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof ParserRuleContext) {
        subTypes.add((Type) data.remove(ctx.getChild(i)));
      }
    }
    if (subTypes.isEmpty()) {
      throw new SemanticsException(ctx, "types of function are empty");
    }

    env.setGlobalValue(ctx.ID().getText(), new Function(ctx.ID().getText(),
        new FunctionType(subTypes)));
    data.put(ctx, ctx.ID().getText());
  }

  @Override
  public void exitBaseTypeBool(@NotNull MLRulesParser.BaseTypeBoolContext ctx) {
    data.put(ctx, BaseType.BOOL);
  }

  @Override
  public void exitBaseTypeFloat(@NotNull MLRulesParser.BaseTypeFloatContext ctx) {
    data.put(ctx, BaseType.NUM);
  }

  @Override
  public void exitBaseTypeLink(@NotNull MLRulesParser.BaseTypeLinkContext ctx) {
    data.put(ctx, BaseType.LINK);
  }

  @Override
  public void exitBaseTypeString(
      @NotNull MLRulesParser.BaseTypeStringContext ctx) {
    data.put(ctx, BaseType.STRING);
  }

  @Override
  public void exitFunctionType(@NotNull MLRulesParser.FunctionTypeContext ctx) {
    List<Type> subTypes = new ArrayList<>();
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof ParserRuleContext) {
        subTypes.add((Type) data.remove(ctx.getChild(i)));
      }
    }
    if (subTypes.isEmpty()) {
      throw new SemanticsException(ctx, "types of function are empty");
    }
    data.put(ctx, new FunctionType(subTypes));
  }

  @Override
  public void exitSpeciesType(@NotNull MLRulesParser.SpeciesTypeContext ctx) {
    data.put(ctx, BaseType.SPECIES);
  }

  @Override
  public void exitSolutionType(@NotNull MLRulesParser.SolutionTypeContext ctx) {
    data.put(ctx, BaseType.SOL);
  }

  @Override
  public void exitTupleType(@NotNull MLRulesParser.TupleTypeContext ctx) {
    data.put(ctx, BaseType.TUPLE);
  }

  @Override
  public void exitFunctionDefinition(FunctionDefinitionContext ctx) {
    if (ctx.ID() != null) {
    data.put(ctx, ctx.ID().getText());
    } else {
      throw new SemanticsException(ctx, "function definition error");
    }
  }

  @Override
  public void exitFunction(FunctionContext ctx) {
    String name = (String) data.remove(ctx.getChild(0));
    for (int i = 1; i < ctx.getChildCount(); ++i) {
      String other = (String) data.remove(ctx.getChild(i));
      if (!name.equals(other)) {
        throw new SemanticsException(ctx, String.format(
            "function definition %s is mixed with type definition %s", other,
            name));
      }
    }
  }

}
