/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.functions.param;

import java.util.HashMap;
import java.util.Map;

import org.jamesii.cmlrules.model.standard.species.Species;

public class SolParameter implements Parameter {

  private final String head;

  private final String tail;

  public SolParameter(String head, String tail) {
    this.head = head;
    this.tail = tail;
  }

  @Override
  public String toString() {
    return head + ":" + tail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public boolean match(Object value, Map<String, Object> vars) {
    if (value instanceof Map<?,?>) {
      Map<?,?> list = (Map<?,?>) value;
      if (list.isEmpty()) {
        return false;
      }
      Object o = list.entrySet().iterator().next().getKey();
      if (o instanceof Species) {
        Map<Species, Species> headMap = new HashMap<>();
        headMap.put((Species)o,(Species)o);
        vars.put(head, headMap);
        Map<Species,Species> tailMap = new HashMap<>((Map<Species,Species>)list);
        tailMap.remove(o);
        vars.put(tail, tailMap);
        return true;
      }
    }
    return false;
  }

}
