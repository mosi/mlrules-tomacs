/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.listener;

import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesBaseListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SpeciesDefinitionContext;
import org.jamesii.cmlrules.util.MLEnvironment;

public class SpeciesTypeCrawlerListener extends MLRulesBaseListener {

  private final MLEnvironment env;

  public SpeciesTypeCrawlerListener(MLEnvironment env) {
    this.env = env;
  }

  @Override
  public void exitSpeciesDefinition(SpeciesDefinitionContext ctx) {
    if (ctx.ID_SPECIES() != null) {
      String name = ctx.ID_SPECIES().getText();
      Integer attributes =
          ctx.INT() != null ? Integer.valueOf(ctx.INT().getText()) : 0;

      if (attributes < 0) {
        throw new SemanticsException(ctx, String.format(
            "species %s cannot have less than 0 attributes", name));
      }

      if (env.containsIdent(ctx.ID_SPECIES().getText())) {
        throw new SemanticsException(ctx, String.format(
            "name %s is used ambigously", ctx.ID_SPECIES().getText()));
      }
      env.setGlobalValue(name, new SpeciesType(name, attributes));
    } else {
      throw new SemanticsException(ctx,
          "invalid species definition");
    }
  }

}
