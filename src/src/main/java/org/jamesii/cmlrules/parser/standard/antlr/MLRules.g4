/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

grammar MLRules;

import Expressions, Functions;

COMMENT_SINGLE : '//' ~('\n'|'\r')* ('\r'? '\n')?	-> channel(HIDDEN);
COMMENT_MULTI  : '/*' .*? '*/'                      -> channel(HIDDEN);
WS      :   [ \t\r\n]+                      		-> skip;

// needed for rules and initial solution
AT      :   '@';
ATTIME	:	'@EXACT';
ATEACH : '@EACH';
INIT    : '>>INIT';

TYPE_SEPERATOR  :   '::';
ARROW           :   '->';
LET				:   'let';
IN				:   'in';
WHERE			:   'where';

// type keywords
TYPE_REAL       :   'num';
TYPE_BOOL       :   'bool';
TYPE_STRING     :   'string';
TYPE_SPECIES    :   'species';
TYPE_PATTERN    :   'pattern';
TYPE_TUPLE		:   'tuple';
TYPE_SOL		:   'sol';
TYPE_LINK		:	'link';

// keywords
IF      :   'if';
THEN    :   'then';
ELSE    :   'else';
FOR     :   'for';
WHILE   :   'while';
UNTIL   :   'until';
LENGTH  :   'length';
ADD		:	'add';
FREE	:	'free';

// seperators
ASSIGN  :   '=';
DOT     :   '.';
COMMA   :   ',';
COLON   :   ':';
SEMI    :   ';';
L_PAREN :   '(';
R_PAREN :   ')';
L_BRAKET:   '[';
R_BRAKET:   ']';
L_CURLY :   '{';
R_CURLY :   '}';  

// mathematical operators
PLUS    :   '+';
MINUS   :   '-';
MULT    :   '*';
DIV     :   '/';
ROOF    :   '^';

// comparator operators
EQUALS  :   '==';
N_EQUALS:   '!=';
LT      :   '<';
LT_EQ   :   '<=';
GT      :   '>';
GT_EQ   :   '>=';

// boolean operators
TRUE    :   'true';
FALSE   :   'false';
AND     :   '&&';
OR      :   '||';
NOT     :   '!';

// misc
LAMBDA  :   '\\';

// data types
INT     :   DIGIT+;
REAL  
    :   DIGIT+ DOT DIGIT* EXPONENT? // e.g. 1.4 (e+10)
    |   DIGIT+ EXPONENT             // e.g. 12 e+10
    ;        

fragment    
EXPONENT
    :   ('e'|'E') ('+'|'-')? DIGIT+
    ;
    
BOOL
    :   (TRUE|FALSE)
    ;

ID_SPECIES: CAPITAL (CHAR | DIGIT)* | ROOT;

ID: SMALL (CHAR | DIGIT)* QUESTION?;

fragment
CAPITAL: [A-Z];

fragment
SMALL: [a-z];

fragment
CHAR
    :   [a-z|A-Z|_]
    ;

STRING  :   '\'' (ESCAPE | ~('\''|'\\' ))* '\'';

fragment
ESCAPE
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\''|'\"'|'\\')
    |   UNICODE_ESCAPE
    |   OCTAL_ESCAPE
    ;

fragment
UNICODE_ESCAPE
    :   '\\u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
    ;

fragment
OCTAL_ESCAPE
    :   '\\' [0-3] [0-7] [0-7]
    |   '\\' [0-7] [0-7]?
    ;

fragment
DIGIT   :   [0-9];

fragment
HEX_DIGIT : [0-9|'a'-'f'|'A'-'F'];


// other
NU      :   '$' ID;
COUNT   :   '#';
QUESTION:   '?';
ROOT	:	'$$ROOT$$';

model : preamble* initialSolution mlrule+ EOF;

preamble 
	: speciesDefinition SEMI
	| constant SEMI
	| function
	;
        
constant : ID COLON expression;

speciesDefinition
    :   ID_SPECIES L_PAREN R_PAREN
    |   ID_SPECIES L_PAREN INT R_PAREN           				
    ;    
    
initialSolution : INIT L_BRAKET expression R_BRAKET SEMI;

mlrule : reactants ARROW products (AT | ATTIME | ATEACH) rate where? SEMI;

reactants : species (PLUS species)*;

products : expression?;

rate : expression;