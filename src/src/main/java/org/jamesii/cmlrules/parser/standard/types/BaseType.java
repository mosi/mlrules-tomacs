/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.types;


/**
 * A {@link BaseType} is a primitive type without sub types, i.e., all types but
 * the function type are primitives.
 * 
 * @author Tobias Helms
 *
 */
public class BaseType implements Type {

  private final Primitives type;

  private BaseType(Primitives type) {
    this.type = type;
  }

  public static final Type NUM = new BaseType(Primitives.NUM);

  public static final Type BOOL = new BaseType(Primitives.BOOL);

  public static final Type STRING = new BaseType(Primitives.STRING);

  public static final Type SPECIES = new BaseType(Primitives.SPECIES);

  public static final Type UNKNOWN = new BaseType(Primitives.UNKNOWN);

  public static final Type TUPLE = new BaseType(Primitives.TUPLE);

  public static final Type SOL = new BaseType(Primitives.SOL);

  public static final Type LINK = new BaseType(Primitives.LINK);

  @Override
  public Primitives getType() {
    return type;
  }

  @Override
  public String toString() {
    return type.toString();
  }

}
