/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.listener.editor;

import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyledDocument;

import org.antlr.v4.runtime.misc.NotNull;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesBaseListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FalseContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FreeContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FunctionDefinitionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.IfThenElseContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.InitialSolutionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ModelContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.TrueContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.TypeDefinitionContext;

public class SyntaxListener extends MLRulesBaseListener {

  private final JTextPane pane;

  private final int pos;

  private final String text;

  private final StyledDocument doc;

  private int currentIndex = 0;

  public SyntaxListener(JTextPane pane) {
    this.pane = pane;
    this.pos = pane.getCaretPosition();
    this.text = pane.getText();

    this.doc = pane.getStyledDocument();
    try {
      this.doc.remove(0, this.doc.getLength());
    } catch (BadLocationException e) {
      e.printStackTrace();
    }
  }

  private void add(int start, int stop, Style style) {
    try {
      if (currentIndex > start) {
        doc.setCharacterAttributes(start, stop - start + 1, style, true);
      } else {
        if (currentIndex < start) {
          doc.insertString(currentIndex, text.substring(currentIndex, start),
              new SimpleAttributeSet());
        }
        doc.insertString(start, text.substring(start, stop + 1), style);
        currentIndex = stop + 1;
      }
    } catch (BadLocationException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void exitTrue(TrueContext ctx) {
    add(ctx.getStart().getStartIndex(), ctx.getStart().getStopIndex(),
        doc.getStyle(Styles.BOOL));
  }

  @Override
  public void exitModel(ModelContext ctx) {
    try {
      if (currentIndex < text.length()) {
        doc.insertString(currentIndex,
            text.substring(currentIndex, text.length()),
            new SimpleAttributeSet());
        if (doc.getLength() > text.length()) {
          throw new IllegalArgumentException(
              String
                  .format(
                      "The syntax checker created new characters.\nBefore:%s\nAfter:%s",
                      text, doc.getText(0, doc.getLength())));
        }
      }
      pane.setStyledDocument(doc);
      if (pos < pane.getText().length()) {
        pane.setCaretPosition(pos);
      }
    } catch (BadLocationException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void exitConstant(@NotNull MLRulesParser.ConstantContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID().getSymbol().getStartIndex(), ctx.ID().getSymbol()
          .getStopIndex(), doc.getStyle(Styles.CONST));
    }
  }

  @Override
  public void exitTupleAssign(@NotNull MLRulesParser.TupleAssignContext ctx) {
    if (ctx.ID() != null) {
      ctx.ID().forEach(
          i -> add(i.getSymbol().getStartIndex(), i.getSymbol().getStopIndex(),
              doc.getStyle(Styles.CONST)));
    }
  }

  @Override
  public void exitSingleAssign(@NotNull MLRulesParser.SingleAssignContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID().getSymbol().getStartIndex(), ctx.ID().getSymbol()
          .getStopIndex(), doc.getStyle(Styles.CONST));
    }
  }

  @Override
  public void exitFalse(FalseContext ctx) {
    add(ctx.getStart().getStartIndex(), ctx.getStart().getStopIndex(),
        doc.getStyle(Styles.BOOL));
  }

  @Override
  public void exitFree(FreeContext ctx) {
    add(ctx.getStart().getStartIndex(), ctx.getStart().getStopIndex(),
        doc.getStyle(Styles.KEY));
  }

  @Override
  public void exitString(@NotNull MLRulesParser.StringContext ctx) {
    add(ctx.getStart().getStartIndex(), ctx.getStart().getStopIndex(),
        doc.getStyle(Styles.STRING));
  }

  @Override
  public void exitAmountID(@NotNull MLRulesParser.AmountIDContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID().getSymbol().getStartIndex(), ctx.ID().getSymbol()
          .getStopIndex(), doc.getStyle(Styles.CONST));
    }
  }

  @Override
  public void exitCountShort(@NotNull MLRulesParser.CountShortContext ctx) {
    if (ctx.COUNT() != null) {
      add(ctx.COUNT().getSymbol().getStartIndex(), ctx.COUNT().getSymbol()
          .getStopIndex(), doc.getStyle(Styles.FUNCTION));
    }
    if (ctx.ID() != null) {
      add(ctx.ID().getSymbol().getStartIndex(), ctx.ID().getSymbol()
          .getStopIndex(), doc.getStyle(Styles.CONST));
    }
  }

  @Override
  public void exitName(@NotNull MLRulesParser.NameContext ctx) {
    if (ctx.ID_SPECIES() != null) {
      add(ctx.ID_SPECIES().getSymbol().getStartIndex(), ctx.ID_SPECIES()
          .getSymbol().getStopIndex(), doc.getStyle(Styles.SPECIES));
    }
  }

  @Override
  public void exitId(@NotNull MLRulesParser.IdContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID().getSymbol().getStartIndex(), ctx.ID().getSymbol()
          .getStopIndex(), doc.getStyle(Styles.CONST));
    }
  }

  @Override
  public void exitSpeciesDefinition(
      @NotNull MLRulesParser.SpeciesDefinitionContext ctx) {
    if (ctx.ID_SPECIES() != null) {
      add(ctx.ID_SPECIES().getSymbol().getStartIndex(), ctx.ID_SPECIES()
          .getSymbol().getStopIndex(), doc.getStyle(Styles.SPECIES));
    }
  }

  @Override
  public void exitWhere(@NotNull MLRulesParser.WhereContext ctx) {
    if (ctx.WHERE() != null) {
      add(ctx.WHERE().getSymbol().getStartIndex(), ctx.WHERE().getSymbol()
          .getStopIndex(), pane.getStyle(Styles.KEY));
    }
  }

  @Override
  public void exitFunctionCall(@NotNull MLRulesParser.FunctionCallContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID().getSymbol().getStartIndex(), ctx.ID().getSymbol()
          .getStopIndex(), doc.getStyle(Styles.FUNCTION));
    }
  }

  @Override
  public void exitParamSol(@NotNull MLRulesParser.ParamSolContext ctx) {
    if (ctx.ID(0) != null && ctx.ID(1) != null) {
      add(ctx.ID(0).getSymbol().getStartIndex(), ctx.ID(0).getSymbol()
          .getStopIndex(), doc.getStyle(Styles.CONST));
      add(ctx.ID(1).getSymbol().getStartIndex(), ctx.ID(1).getSymbol()
          .getStopIndex(), doc.getStyle(Styles.CONST));
    }
  }

  @Override
  public void exitSpecies(@NotNull MLRulesParser.SpeciesContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID().getSymbol().getStartIndex(), ctx.ID().getSymbol()
          .getStopIndex(), doc.getStyle(Styles.CONST));
    }
  }

  @Override
  public void exitIfThenElse(IfThenElseContext ctx) {
    if (ctx.IF() != null) {
      add(ctx.IF().getSymbol().getStartIndex(), ctx.IF().getSymbol()
          .getStopIndex(), pane.getStyle(Styles.KEY));
    }
    if (ctx.THEN() != null) {
      add(ctx.THEN().getSymbol().getStartIndex(), ctx.THEN().getSymbol()
          .getStopIndex(), pane.getStyle(Styles.KEY));
    }
    if (ctx.ELSE() != null) {
      add(ctx.ELSE().getSymbol().getStartIndex(), ctx.ELSE().getSymbol()
          .getStopIndex(), pane.getStyle(Styles.KEY));
    }
  }

  @Override
  public void exitInitialSolution(InitialSolutionContext ctx) {
    if (ctx.INIT() != null) {
      add(ctx.INIT().getSymbol().getStartIndex(), ctx.INIT().getSymbol()
          .getStopIndex(), pane.getStyle(Styles.BOLD));
    }
  }

  public StyledDocument getNewDoc() {
    return doc;
  }

  @Override
  public void exitBaseTypeBool(@NotNull MLRulesParser.BaseTypeBoolContext ctx) {
    if (ctx.TYPE_BOOL() != null) {
      add(ctx.TYPE_BOOL().getSymbol().getStartIndex(), ctx.TYPE_BOOL()
          .getSymbol().getStopIndex(), doc.getStyle(Styles.KEY));
    }
  }

  @Override
  public void exitBaseTypeFloat(@NotNull MLRulesParser.BaseTypeFloatContext ctx) {
    if (ctx.TYPE_REAL() != null) {
      add(ctx.TYPE_REAL().getSymbol().getStartIndex(), ctx.TYPE_REAL()
          .getSymbol().getStopIndex(), doc.getStyle(Styles.KEY));
    }
  }

  @Override
  public void exitBaseTypeLink(@NotNull MLRulesParser.BaseTypeLinkContext ctx) {
    if (ctx.TYPE_LINK() != null) {
      add(ctx.TYPE_LINK().getSymbol().getStartIndex(), ctx.TYPE_LINK()
          .getSymbol().getStopIndex(), doc.getStyle(Styles.KEY));
    }
  }

  @Override
  public void exitBaseTypeString(
      @NotNull MLRulesParser.BaseTypeStringContext ctx) {
    if (ctx.TYPE_STRING() != null) {
      add(ctx.TYPE_STRING().getSymbol().getStartIndex(), ctx.TYPE_STRING()
          .getSymbol().getStopIndex(), doc.getStyle(Styles.KEY));
    }
  }

  @Override
  public void exitSpeciesType(@NotNull MLRulesParser.SpeciesTypeContext ctx) {
    if (ctx.TYPE_SPECIES() != null) {
      add(ctx.TYPE_SPECIES().getSymbol().getStartIndex(), ctx.TYPE_SPECIES()
          .getSymbol().getStopIndex(), doc.getStyle(Styles.KEY));
    }
  }

  @Override
  public void exitSolutionType(@NotNull MLRulesParser.SolutionTypeContext ctx) {
    if (ctx.TYPE_SOL() != null) {
      add(ctx.TYPE_SOL().getSymbol().getStartIndex(), ctx.TYPE_SOL()
          .getSymbol().getStopIndex(), doc.getStyle(Styles.KEY));
    }
  }

  @Override
  public void exitTupleType(@NotNull MLRulesParser.TupleTypeContext ctx) {
    if (ctx.TYPE_TUPLE() != null) {
      add(ctx.TYPE_TUPLE().getSymbol().getStartIndex(), ctx.TYPE_TUPLE()
          .getSymbol().getStopIndex(), doc.getStyle(Styles.KEY));
    }
  }

  @Override
  public void exitFunctionDefinition(FunctionDefinitionContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID().getSymbol().getStartIndex(), ctx.ID().getSymbol()
          .getStopIndex(), doc.getStyle(Styles.FUNCTION));
    }
  }

  @Override
  public void exitTypeDefinition(TypeDefinitionContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID().getSymbol().getStartIndex(), ctx.ID().getSymbol()
          .getStopIndex(), doc.getStyle(Styles.FUNCTION));
    }
  }

}
