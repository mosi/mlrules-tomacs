package org.jamesii.cmlrules.parser.links.listener.typecheck;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.antlr.v4.runtime.ParserRuleContext;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AttributesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.NameContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SpeciesContext;
import org.jamesii.cmlrules.parser.standard.listener.SemanticsException;
import org.jamesii.cmlrules.parser.standard.listener.typecheck.SemanticsListener;
import org.jamesii.cmlrules.parser.standard.types.BaseType;
import org.jamesii.cmlrules.parser.standard.types.Type;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.cmlrules.util.Pair;

public class LinkSemanticsListener extends SemanticsListener {

  final Map<SpeciesType, Set<Integer>> linkAttributes;

  final Map<SpeciesType, Set<Integer>> noLinkAttributes;
  
  public LinkSemanticsListener(Map<SpeciesType, Set<Integer>> linkAttributes, Map<SpeciesType, Set<Integer>> noLinkAttributes, MLEnvironment env,
      Map<ParserRuleContext, Map<String, Type>> ruleVariables,
      Map<ParserRuleContext, Map<String, Type>> functionVariables) {
    super(env, ruleVariables, functionVariables);
    this.linkAttributes = linkAttributes;
    this.noLinkAttributes = noLinkAttributes;
  }
  
  @SuppressWarnings("unchecked")
  @Override
  public void exitSpecies(SpeciesContext ctx) {
    if (!isWhere()) {
    NameContext nCtx = null;
    AttributesContext attCtx = null;
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof NameContext) {
        nCtx = (NameContext) ctx.getChild(i);
      } else if (ctx.getChild(i) instanceof AttributesContext) {
        attCtx = (AttributesContext) ctx.getChild(i);
      }
    }

    List<Type> atts =
        attCtx == null ? Collections.emptyList() : (List<Type>) getData().get(
            attCtx);
    Object type = (Object) getData().get(nCtx);
    SpeciesType st = type != null ? (SpeciesType) type : null;

    if (st == null && atts.stream().anyMatch(a -> a == BaseType.LINK)) {
      throw new SemanticsException(
          ctx,
          String
              .format("is is not allowed to determine the species type with a function and it contains a link attribute"));
    }

    AtomicInteger i = new AtomicInteger(0);
    List<Pair<Type, Integer>> list =
        atts.stream()
            .map(a -> new Pair<>(a, i.getAndIncrement()))
            .collect(Collectors.toList());
    list.stream()
        .forEach(
            a -> {
              if (a.fst() == BaseType.LINK) {
                linkAttributes.computeIfAbsent(st, k -> new HashSet<>()).add(
                    a.snd());
                if (noLinkAttributes
                    .getOrDefault(st, Collections.emptySet()).stream()
                    .anyMatch(index -> index == a.snd())) {
                  throw new SemanticsException(
                      ctx,
                      String
                          .format(
                              "Species type %s cannot mix link and non link attribute at index %x",
                              st, a.snd()));
                }
              } else if (a.fst() != BaseType.UNKNOWN) {
                noLinkAttributes.computeIfAbsent(st, k -> new HashSet<>()).add(
                    a.snd());
                if (linkAttributes
                    .getOrDefault(st, Collections.emptySet()).stream()
                    .anyMatch(index -> index == a.snd())) {
                  throw new SemanticsException(
                      ctx,
                      String
                          .format(
                              "Species type %s cannot mix link and non link attribute at index %x",
                              st, a.snd()));
                }
              }
            });
    }
    super.exitSpecies(ctx);
  }

  public Map<SpeciesType, Set<Integer>> getLinkAttributes() {
    return linkAttributes;
  }

}
