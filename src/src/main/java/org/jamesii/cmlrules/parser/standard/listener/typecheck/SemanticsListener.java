/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.listener.typecheck;

import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountExprContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountIDContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AmountINTContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AndOrContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ApplicationContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.AttributesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.BoolExprContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.CountShortContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.EmptySolutionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ExpValueContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ExpressionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FalseContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FunctionCallContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FunctionDefinitionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.GuardContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.IfThenElseContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.IntContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.MinusContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.MlruleContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.NameContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.NotContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ParenContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.PlusContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.RoofContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SpeciesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SpeciesExprContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.StringContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SubSpeciesContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.TrueContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.WhereContext;
import org.jamesii.cmlrules.parser.standard.functions.Function;
import org.jamesii.cmlrules.parser.standard.listener.SemanticsException;
import org.jamesii.cmlrules.parser.standard.types.BaseType;
import org.jamesii.cmlrules.parser.standard.types.Type;
import org.jamesii.cmlrules.util.MLEnvironment;

public class SemanticsListener extends ExpressionTypesListener {

  private MLEnvironment envCopy;

  private final Map<ParserRuleContext, Map<String, Type>> ruleVariables;

  private final Map<ParserRuleContext, Map<String, Type>> functionVariables;

  private boolean isWhere = false;

  public SemanticsListener(MLEnvironment env,
      Map<ParserRuleContext, Map<String, Type>> ruleVariables,
      Map<ParserRuleContext, Map<String, Type>> functionVariables) {
    super(env);
    this.ruleVariables = ruleVariables;
    this.functionVariables = functionVariables;
  }

  @Override
  public void enterWhere(WhereContext ctx) {
    isWhere = true;
  }

  @Override
  public void exitWhere(WhereContext ctx) {
    isWhere = false;
  }

  @Override
  public void enterMlrule(MlruleContext ctx) {
    envCopy = getEnv();
    setEnv(getEnv().completeCopy());
    ruleVariables.get(ctx).forEach((k, v) -> getEnv().setValue(k, v));
  }

  @Override
  public void exitSpecies(SpeciesContext ctx) {
    if (!isWhere) {
      super.exitSpecies(ctx);
      if (ctx.ID() != null) {
        getEnv().setValue(ctx.ID().getText(), BaseType.SPECIES);
      }
    }
  }

  @Override
  public void enterFunctionDefinition(FunctionDefinitionContext ctx) {
    envCopy = getEnv();
    setEnv(getEnv().completeCopy());
    functionVariables.get(ctx).forEach((k, v) -> getEnv().setValue(k, v));
  }

  @Override
  public void exitFunctionDefinition(FunctionDefinitionContext ctx) {
    setEnv(envCopy);

    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof ExpressionContext) {
        Type t = (Type) getData().remove(ctx.getChild(i));
        Object o = getEnv().getValue(ctx.ID().getText());
        if (!(o instanceof Function)) {
          throw new SemanticsException(ctx, String.format(
              "unknown function %s", ctx.ID().getText()));
        }
        Function f = (Function) o;
        Type result =
            f.getType().getSubTypes().get(f.getType().getSubTypes().size() - 1);
        if (t != BaseType.UNKNOWN && !t.equals(result) && (t != BaseType.SPECIES || result != BaseType.SOL)) {
          throw new SemanticsException(ctx, String.format(
              "function %s must return %s and not %s", f.getName(), result, t));
        }
      }
    }

  }

  @Override
  public void exitMlrule(MlruleContext ctx) {
    if (isExpectingRoot()) {
      throw new SemanticsException(ctx,
          "When using the $$ROOT$$ species as reactant, it must also be used as product.");
    }
    setEnv(envCopy);
  }

  @Override
  public void exitTrue(TrueContext ctx) {
    if (!isWhere) {
      super.exitTrue(ctx);
    }
  }

  @Override
  public void exitFalse(FalseContext ctx) {
    if (!isWhere) {
      super.exitFalse(ctx);
    }
  }

  @Override
  public void exitString(StringContext ctx) {
    if (!isWhere) {
      super.exitString(ctx);
    }
  }

  @Override
  public void exitReal(@NotNull MLRulesParser.RealContext ctx) {
    if (!isWhere) {
      super.exitReal(ctx);
    }
  }

  @Override
  public void exitInt(IntContext ctx) {
    if (!isWhere) {
      super.exitInt(ctx);
    }
  }

  @Override
  public void exitExpValue(ExpValueContext ctx) {
    if (!isWhere) {
      super.exitExpValue(ctx);
    }
  }

  @Override
  public void exitPlus(PlusContext ctx) {
    if (!isWhere) {
      super.exitPlus(ctx);
    }
  }

  @Override
  public void exitMinus(MinusContext ctx) {
    if (!isWhere) {
      super.exitMinus(ctx);
    }
  }

  @Override
  public void exitMultDiv(@NotNull MLRulesParser.MultDivContext ctx) {
    if (!isWhere) {
      super.exitMultDiv(ctx);
    }
  }

  @Override
  public void exitRoof(RoofContext ctx) {
    if (!isWhere) {
      super.exitRoof(ctx);
    }
  }

  @Override
  public void exitAndOr(AndOrContext ctx) {
    if (!isWhere) {
      super.exitAndOr(ctx);
    }
  }

  @Override
  public void exitParen(ParenContext ctx) {
    if (!isWhere) {
      super.exitParen(ctx);
    }
  }

  @Override
  public void exitBoolExpr(BoolExprContext ctx) {
    if (!isWhere) {
      super.exitBoolExpr(ctx);
    }
  }

  @Override
  public void exitNot(NotContext ctx) {
    if (!isWhere) {
      super.exitNot(ctx);
    }
  }

  @Override
  public void exitMinusOne(@NotNull MLRulesParser.MinusOneContext ctx) {
    if (!isWhere) {
      super.exitMinusOne(ctx);
    }
  }

  @Override
  public void exitIfThenElse(IfThenElseContext ctx) {
    if (!isWhere) {
      super.exitIfThenElse(ctx);
    }
  }

  @Override
  public void exitTuple(@NotNull MLRulesParser.TupleContext ctx) {
    if (!isWhere) {
      super.exitTuple(ctx);
    }
  }

  @Override
  public void exitName(NameContext ctx) {
    if (!isWhere) {
      super.exitName(ctx);
    }
  }

  @Override
  public void exitSpeciesExpr(SpeciesExprContext ctx) {
    if (!isWhere) {
      super.exitSpeciesExpr(ctx);
    }
  }

  @Override
  public void exitEmptySolution(EmptySolutionContext ctx) {
    if (!isWhere) {
      super.exitEmptySolution(ctx);
    }
  }

  @Override
  public void exitApplication(ApplicationContext ctx) {
    if (!isWhere) {
      super.exitApplication(ctx);
    }
  }

  @Override
  public void exitAmountExpr(AmountExprContext ctx) {
    if (!isWhere) {
      super.exitAmountExpr(ctx);
    }
  }

  @Override
  public void exitAmountID(AmountIDContext ctx) {
    if (!isWhere) {
      super.exitAmountID(ctx);
    }
  }

  @Override
  public void exitAmountINT(AmountINTContext ctx) {
    if (!isWhere) {
      super.exitAmountINT(ctx);
    }
  }

  @Override
  public void exitSubSpecies(SubSpeciesContext ctx) {
    if (!isWhere) {
      super.exitSubSpecies(ctx);
    }
  }

  @Override
  public void exitGuard(GuardContext ctx) {
    if (!isWhere) {
      super.exitGuard(ctx);
    }
  }
  
  @Override
  public void exitAttributes(AttributesContext ctx) {
    if (!isWhere) {
      super.exitAttributes(ctx);
    }
  }

  @Override
  public void exitFunctionCall(FunctionCallContext ctx) {
    if (!isWhere) {
      super.exitFunctionCall(ctx);
    }
  }

  @Override
  public void exitCountShort(CountShortContext ctx) {
    if (!isWhere) {
      super.exitCountShort(ctx);
    }
  }

  public boolean isWhere() {
    return isWhere;
  }
  
}
