package org.jamesii.cmlrules.parser.hybrid;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jamesii.cmlrules.model.Model;
import org.jamesii.cmlrules.model.hybrid.HybridModel;
import org.jamesii.cmlrules.model.simple.reaction.Rule;
import org.jamesii.cmlrules.model.standard.rule.Reactant;
import org.jamesii.cmlrules.model.standard.rule.RuleComparator;
import org.jamesii.cmlrules.parser.standard.listener.RulesListener;
import org.jamesii.cmlrules.util.MLEnvironment;

public class HybridRulesListener extends RulesListener {

  private final Map<Integer, Rule> simpleRules;
  
  private final Map<Reactant, Reactant> reactantProducts;
  
  public HybridRulesListener(MLEnvironment env, Map<Integer, Rule> simpleRules, Map<Reactant, Reactant> reactantProducts) {
    super(env);
    this.simpleRules = simpleRules;
    this.reactantProducts = reactantProducts;
  }
  
  @Override
  public Model create() {
    
    List<Rule> simple = new ArrayList<>();
    List<org.jamesii.cmlrules.model.standard.rule.Rule> complex = new ArrayList<>();
    
    for (int i = 0; i < getRules().size(); ++i) {
      if (simpleRules.containsKey(i)) {
        simple.add(simpleRules.get(i));
      } else {
        complex.add(getRules().get(i));
      }
    }
    
    getTimedRules().sort(RuleComparator.instance);
    return new HybridModel(getInitialSolution(), simple, complex, reactantProducts, getTimedRules(), getEnv());
  }

}
