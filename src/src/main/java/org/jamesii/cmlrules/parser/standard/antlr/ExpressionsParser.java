// Generated from Expressions.g4 by ANTLR 4.3
package org.jamesii.cmlrules.parser.standard.antlr;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ExpressionsParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		IF=1, THEN=2, ELSE=3, FOR=4, WHILE=5, UNTIL=6, LENGTH=7, ADD=8, WHERE=9, 
		FREE=10, ASSIGN=11, DOT=12, COMMA=13, COLON=14, SEMI=15, L_PAREN=16, R_PAREN=17, 
		L_BRAKET=18, R_BRAKET=19, L_CURLY=20, R_CURLY=21, PLUS=22, MINUS=23, MULT=24, 
		DIV=25, ROOF=26, EQUALS=27, N_EQUALS=28, LT=29, LT_EQ=30, GT=31, GT_EQ=32, 
		TRUE=33, FALSE=34, AND=35, OR=36, NOT=37, LAMBDA=38, INT=39, REAL=40, 
		BOOL=41, ID_SPECIES=42, ID=43, STRING=44, NU=45, COUNT=46, QUESTION=47, 
		ROOT=48;
	public static final String[] tokenNames = {
		"<INVALID>", "'if'", "'then'", "'else'", "'for'", "'while'", "'until'", 
		"'length'", "'add'", "'where'", "'free'", "'='", "'.'", "','", "':'", 
		"';'", "'('", "')'", "'['", "']'", "'{'", "'}'", "'+'", "'-'", "'*'", 
		"'/'", "'^'", "'=='", "'!='", "'<'", "'<='", "'>'", "'>='", "'true'", 
		"'false'", "'&&'", "'||'", "'!'", "'\\'", "INT", "REAL", "BOOL", "ID_SPECIES", 
		"ID", "STRING", "NU", "'#'", "'?'", "'$$ROOT$$'"
	};
	public static final int
		RULE_expression = 0, RULE_species = 1, RULE_name = 2, RULE_attributes = 3, 
		RULE_subSpecies = 4, RULE_guard = 5, RULE_amount = 6, RULE_value = 7, 
		RULE_where = 8, RULE_assign = 9, RULE_assignName = 10;
	public static final String[] ruleNames = {
		"expression", "species", "name", "attributes", "subSpecies", "guard", 
		"amount", "value", "where", "assign", "assignName"
	};

	@Override
	public String getGrammarFileName() { return "Expressions.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ExpressionsParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BoolExprContext extends ExpressionContext {
		public TerminalNode EQUALS() { return getToken(ExpressionsParser.EQUALS, 0); }
		public TerminalNode N_EQUALS() { return getToken(ExpressionsParser.N_EQUALS, 0); }
		public TerminalNode R_PAREN() { return getToken(ExpressionsParser.R_PAREN, 0); }
		public TerminalNode LT_EQ() { return getToken(ExpressionsParser.LT_EQ, 0); }
		public TerminalNode LT() { return getToken(ExpressionsParser.LT, 0); }
		public TerminalNode GT() { return getToken(ExpressionsParser.GT, 0); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode L_PAREN() { return getToken(ExpressionsParser.L_PAREN, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode GT_EQ() { return getToken(ExpressionsParser.GT_EQ, 0); }
		public BoolExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterBoolExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitBoolExpr(this);
		}
	}
	public static class SpeciesExprContext extends ExpressionContext {
		public SpeciesContext species() {
			return getRuleContext(SpeciesContext.class,0);
		}
		public SpeciesExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterSpeciesExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitSpeciesExpr(this);
		}
	}
	public static class EmptySolutionContext extends ExpressionContext {
		public TerminalNode L_BRAKET() { return getToken(ExpressionsParser.L_BRAKET, 0); }
		public TerminalNode R_BRAKET() { return getToken(ExpressionsParser.R_BRAKET, 0); }
		public EmptySolutionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterEmptySolution(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitEmptySolution(this);
		}
	}
	public static class MultDivContext extends ExpressionContext {
		public TerminalNode MULT() { return getToken(ExpressionsParser.MULT, 0); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode DIV() { return getToken(ExpressionsParser.DIV, 0); }
		public MultDivContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterMultDiv(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitMultDiv(this);
		}
	}
	public static class TupleContext extends ExpressionContext {
		public List<TerminalNode> COMMA() { return getTokens(ExpressionsParser.COMMA); }
		public TerminalNode LT() { return getToken(ExpressionsParser.LT, 0); }
		public TerminalNode GT() { return getToken(ExpressionsParser.GT, 0); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode COMMA(int i) {
			return getToken(ExpressionsParser.COMMA, i);
		}
		public TupleContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterTuple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitTuple(this);
		}
	}
	public static class ExpValueContext extends ExpressionContext {
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public ExpValueContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterExpValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitExpValue(this);
		}
	}
	public static class MinusOneContext extends ExpressionContext {
		public TerminalNode MINUS() { return getToken(ExpressionsParser.MINUS, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public MinusOneContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterMinusOne(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitMinusOne(this);
		}
	}
	public static class NotContext extends ExpressionContext {
		public TerminalNode NOT() { return getToken(ExpressionsParser.NOT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public NotContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterNot(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitNot(this);
		}
	}
	public static class RoofContext extends ExpressionContext {
		public TerminalNode ROOF() { return getToken(ExpressionsParser.ROOF, 0); }
		public TerminalNode R_PAREN() { return getToken(ExpressionsParser.R_PAREN, 0); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode L_PAREN() { return getToken(ExpressionsParser.L_PAREN, 0); }
		public RoofContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterRoof(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitRoof(this);
		}
	}
	public static class CountShortContext extends ExpressionContext {
		public TerminalNode COUNT() { return getToken(ExpressionsParser.COUNT, 0); }
		public TerminalNode ID() { return getToken(ExpressionsParser.ID, 0); }
		public CountShortContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterCountShort(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitCountShort(this);
		}
	}
	public static class IfThenElseContext extends ExpressionContext {
		public TerminalNode ELSE() { return getToken(ExpressionsParser.ELSE, 0); }
		public TerminalNode IF() { return getToken(ExpressionsParser.IF, 0); }
		public TerminalNode THEN() { return getToken(ExpressionsParser.THEN, 0); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public IfThenElseContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterIfThenElse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitIfThenElse(this);
		}
	}
	public static class FunctionCallContext extends ExpressionContext {
		public TerminalNode ID() { return getToken(ExpressionsParser.ID, 0); }
		public TerminalNode R_PAREN() { return getToken(ExpressionsParser.R_PAREN, 0); }
		public List<TerminalNode> COMMA() { return getTokens(ExpressionsParser.COMMA); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode L_PAREN() { return getToken(ExpressionsParser.L_PAREN, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode COMMA(int i) {
			return getToken(ExpressionsParser.COMMA, i);
		}
		public FunctionCallContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterFunctionCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitFunctionCall(this);
		}
	}
	public static class PlusContext extends ExpressionContext {
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode PLUS() { return getToken(ExpressionsParser.PLUS, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public PlusContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterPlus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitPlus(this);
		}
	}
	public static class ApplicationContext extends ExpressionContext {
		public TerminalNode R_PAREN() { return getToken(ExpressionsParser.R_PAREN, 0); }
		public AttributesContext attributes() {
			return getRuleContext(AttributesContext.class,0);
		}
		public TerminalNode L_PAREN() { return getToken(ExpressionsParser.L_PAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ApplicationContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterApplication(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitApplication(this);
		}
	}
	public static class AndOrContext extends ExpressionContext {
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode AND() { return getToken(ExpressionsParser.AND, 0); }
		public TerminalNode OR() { return getToken(ExpressionsParser.OR, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public AndOrContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterAndOr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitAndOr(this);
		}
	}
	public static class MinusContext extends ExpressionContext {
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode MINUS() { return getToken(ExpressionsParser.MINUS, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public MinusContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterMinus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitMinus(this);
		}
	}
	public static class ParenContext extends ExpressionContext {
		public TerminalNode R_PAREN() { return getToken(ExpressionsParser.R_PAREN, 0); }
		public TerminalNode L_PAREN() { return getToken(ExpressionsParser.L_PAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ParenContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterParen(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitParen(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 0;
		enterRecursionRule(_localctx, 0, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(80);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				{
				_localctx = new MinusOneContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(23); match(MINUS);
				setState(24); expression(16);
				}
				break;

			case 2:
				{
				_localctx = new NotContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(25); match(NOT);
				setState(26); expression(15);
				}
				break;

			case 3:
				{
				_localctx = new ApplicationContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(27); match(L_PAREN);
				setState(28); expression(0);
				setState(29); match(R_PAREN);
				setState(30); attributes();
				}
				break;

			case 4:
				{
				_localctx = new IfThenElseContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(32); match(IF);
				setState(33); expression(0);
				setState(34); match(THEN);
				setState(35); expression(0);
				setState(36); match(ELSE);
				setState(37); expression(0);
				}
				break;

			case 5:
				{
				_localctx = new BoolExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(39); match(L_PAREN);
				setState(40); expression(0);
				setState(41);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUALS) | (1L << N_EQUALS) | (1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(42); expression(0);
				setState(43); match(R_PAREN);
				}
				break;

			case 6:
				{
				_localctx = new TupleContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(45); match(LT);
				setState(54);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IF) | (1L << FREE) | (1L << L_PAREN) | (1L << L_BRAKET) | (1L << MINUS) | (1L << LT) | (1L << TRUE) | (1L << FALSE) | (1L << NOT) | (1L << INT) | (1L << REAL) | (1L << ID_SPECIES) | (1L << ID) | (1L << STRING) | (1L << COUNT))) != 0)) {
					{
					setState(46); expression(0);
					setState(51);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(47); match(COMMA);
						setState(48); expression(0);
						}
						}
						setState(53);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(56); match(GT);
				}
				break;

			case 7:
				{
				_localctx = new FunctionCallContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(57); match(ID);
				setState(58); match(L_PAREN);
				setState(67);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IF) | (1L << FREE) | (1L << L_PAREN) | (1L << L_BRAKET) | (1L << MINUS) | (1L << LT) | (1L << TRUE) | (1L << FALSE) | (1L << NOT) | (1L << INT) | (1L << REAL) | (1L << ID_SPECIES) | (1L << ID) | (1L << STRING) | (1L << COUNT))) != 0)) {
					{
					setState(59); expression(0);
					setState(64);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(60); match(COMMA);
						setState(61); expression(0);
						}
						}
						setState(66);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(69); match(R_PAREN);
				}
				break;

			case 8:
				{
				_localctx = new ParenContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(70); match(L_PAREN);
				setState(71); expression(0);
				setState(72); match(R_PAREN);
				}
				break;

			case 9:
				{
				_localctx = new SpeciesExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(74); species();
				}
				break;

			case 10:
				{
				_localctx = new EmptySolutionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(75); match(L_BRAKET);
				setState(76); match(R_BRAKET);
				}
				break;

			case 11:
				{
				_localctx = new ExpValueContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(77); value();
				}
				break;

			case 12:
				{
				_localctx = new CountShortContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(78); match(COUNT);
				setState(79); match(ID);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(102);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(100);
					switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
					case 1:
						{
						_localctx = new MultDivContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(82);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(83);
						_la = _input.LA(1);
						if ( !(_la==MULT || _la==DIV) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(84); expression(14);
						}
						break;

					case 2:
						{
						_localctx = new PlusContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(85);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(86); match(PLUS);
						setState(87); expression(13);
						}
						break;

					case 3:
						{
						_localctx = new MinusContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(88);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(89); match(MINUS);
						setState(90); expression(12);
						}
						break;

					case 4:
						{
						_localctx = new AndOrContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(91);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(92);
						_la = _input.LA(1);
						if ( !(_la==AND || _la==OR) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(93); expression(10);
						}
						break;

					case 5:
						{
						_localctx = new RoofContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(94);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(95); match(ROOF);
						setState(96); match(L_PAREN);
						setState(97); expression(0);
						setState(98); match(R_PAREN);
						}
						break;
					}
					} 
				}
				setState(104);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class SpeciesContext extends ParserRuleContext {
		public GuardContext guard() {
			return getRuleContext(GuardContext.class,0);
		}
		public TerminalNode ID() { return getToken(ExpressionsParser.ID, 0); }
		public SubSpeciesContext subSpecies() {
			return getRuleContext(SubSpeciesContext.class,0);
		}
		public AttributesContext attributes() {
			return getRuleContext(AttributesContext.class,0);
		}
		public AmountContext amount() {
			return getRuleContext(AmountContext.class,0);
		}
		public TerminalNode COLON() { return getToken(ExpressionsParser.COLON, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public SpeciesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_species; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterSpecies(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitSpecies(this);
		}
	}

	public final SpeciesContext species() throws RecognitionException {
		SpeciesContext _localctx = new SpeciesContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_species);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(106);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				{
				setState(105); amount();
				}
				break;
			}
			setState(108); name();
			setState(110);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				{
				setState(109); attributes();
				}
				break;
			}
			setState(113);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				{
				setState(112); subSpecies();
				}
				break;
			}
			setState(116);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				{
				setState(115); guard();
				}
				break;
			}
			setState(120);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				{
				setState(118); match(COLON);
				setState(119); match(ID);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NameContext extends ParserRuleContext {
		public TerminalNode R_PAREN() { return getToken(ExpressionsParser.R_PAREN, 0); }
		public TerminalNode L_PAREN() { return getToken(ExpressionsParser.L_PAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode ID_SPECIES() { return getToken(ExpressionsParser.ID_SPECIES, 0); }
		public NameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitName(this);
		}
	}

	public final NameContext name() throws RecognitionException {
		NameContext _localctx = new NameContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_name);
		try {
			setState(127);
			switch (_input.LA(1)) {
			case ID_SPECIES:
				enterOuterAlt(_localctx, 1);
				{
				setState(122); match(ID_SPECIES);
				}
				break;
			case L_PAREN:
				enterOuterAlt(_localctx, 2);
				{
				setState(123); match(L_PAREN);
				setState(124); expression(0);
				setState(125); match(R_PAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AttributesContext extends ParserRuleContext {
		public TerminalNode R_PAREN() { return getToken(ExpressionsParser.R_PAREN, 0); }
		public List<TerminalNode> COMMA() { return getTokens(ExpressionsParser.COMMA); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode L_PAREN() { return getToken(ExpressionsParser.L_PAREN, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode COMMA(int i) {
			return getToken(ExpressionsParser.COMMA, i);
		}
		public AttributesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attributes; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterAttributes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitAttributes(this);
		}
	}

	public final AttributesContext attributes() throws RecognitionException {
		AttributesContext _localctx = new AttributesContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_attributes);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(129); match(L_PAREN);
			setState(138);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IF) | (1L << FREE) | (1L << L_PAREN) | (1L << L_BRAKET) | (1L << MINUS) | (1L << LT) | (1L << TRUE) | (1L << FALSE) | (1L << NOT) | (1L << INT) | (1L << REAL) | (1L << ID_SPECIES) | (1L << ID) | (1L << STRING) | (1L << COUNT))) != 0)) {
				{
				setState(130); expression(0);
				setState(135);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(131); match(COMMA);
					setState(132); expression(0);
					}
					}
					setState(137);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(140); match(R_PAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubSpeciesContext extends ParserRuleContext {
		public TerminalNode L_BRAKET() { return getToken(ExpressionsParser.L_BRAKET, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode R_BRAKET() { return getToken(ExpressionsParser.R_BRAKET, 0); }
		public SubSpeciesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subSpecies; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterSubSpecies(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitSubSpecies(this);
		}
	}

	public final SubSpeciesContext subSpecies() throws RecognitionException {
		SubSpeciesContext _localctx = new SubSpeciesContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_subSpecies);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(142); match(L_BRAKET);
			setState(144);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IF) | (1L << FREE) | (1L << L_PAREN) | (1L << L_BRAKET) | (1L << MINUS) | (1L << LT) | (1L << TRUE) | (1L << FALSE) | (1L << NOT) | (1L << INT) | (1L << REAL) | (1L << ID_SPECIES) | (1L << ID) | (1L << STRING) | (1L << COUNT))) != 0)) {
				{
				setState(143); expression(0);
				}
			}

			setState(146); match(R_BRAKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GuardContext extends ParserRuleContext {
		public TerminalNode L_CURLY() { return getToken(ExpressionsParser.L_CURLY, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode R_CURLY() { return getToken(ExpressionsParser.R_CURLY, 0); }
		public GuardContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_guard; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterGuard(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitGuard(this);
		}
	}

	public final GuardContext guard() throws RecognitionException {
		GuardContext _localctx = new GuardContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_guard);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(148); match(L_CURLY);
			setState(150);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IF) | (1L << FREE) | (1L << L_PAREN) | (1L << L_BRAKET) | (1L << MINUS) | (1L << LT) | (1L << TRUE) | (1L << FALSE) | (1L << NOT) | (1L << INT) | (1L << REAL) | (1L << ID_SPECIES) | (1L << ID) | (1L << STRING) | (1L << COUNT))) != 0)) {
				{
				setState(149); expression(0);
				}
			}

			setState(152); match(R_CURLY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AmountContext extends ParserRuleContext {
		public AmountContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_amount; }
	 
		public AmountContext() { }
		public void copyFrom(AmountContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AmountIDContext extends AmountContext {
		public TerminalNode ID() { return getToken(ExpressionsParser.ID, 0); }
		public AmountIDContext(AmountContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterAmountID(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitAmountID(this);
		}
	}
	public static class AmountExprContext extends AmountContext {
		public TerminalNode R_PAREN() { return getToken(ExpressionsParser.R_PAREN, 0); }
		public TerminalNode L_PAREN() { return getToken(ExpressionsParser.L_PAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AmountExprContext(AmountContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterAmountExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitAmountExpr(this);
		}
	}
	public static class AmountINTContext extends AmountContext {
		public TerminalNode INT() { return getToken(ExpressionsParser.INT, 0); }
		public AmountINTContext(AmountContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterAmountINT(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitAmountINT(this);
		}
	}

	public final AmountContext amount() throws RecognitionException {
		AmountContext _localctx = new AmountContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_amount);
		try {
			setState(160);
			switch (_input.LA(1)) {
			case INT:
				_localctx = new AmountINTContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(154); match(INT);
				}
				break;
			case ID:
				_localctx = new AmountIDContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(155); match(ID);
				}
				break;
			case L_PAREN:
				_localctx = new AmountExprContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(156); match(L_PAREN);
				setState(157); expression(0);
				setState(158); match(R_PAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
	 
		public ValueContext() { }
		public void copyFrom(ValueContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class RealContext extends ValueContext {
		public TerminalNode REAL() { return getToken(ExpressionsParser.REAL, 0); }
		public RealContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterReal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitReal(this);
		}
	}
	public static class TrueContext extends ValueContext {
		public TerminalNode TRUE() { return getToken(ExpressionsParser.TRUE, 0); }
		public TrueContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterTrue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitTrue(this);
		}
	}
	public static class FalseContext extends ValueContext {
		public TerminalNode FALSE() { return getToken(ExpressionsParser.FALSE, 0); }
		public FalseContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterFalse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitFalse(this);
		}
	}
	public static class StringContext extends ValueContext {
		public TerminalNode STRING() { return getToken(ExpressionsParser.STRING, 0); }
		public StringContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitString(this);
		}
	}
	public static class IdContext extends ValueContext {
		public TerminalNode ID() { return getToken(ExpressionsParser.ID, 0); }
		public IdContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterId(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitId(this);
		}
	}
	public static class FreeContext extends ValueContext {
		public TerminalNode FREE() { return getToken(ExpressionsParser.FREE, 0); }
		public FreeContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterFree(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitFree(this);
		}
	}
	public static class IntContext extends ValueContext {
		public TerminalNode INT() { return getToken(ExpressionsParser.INT, 0); }
		public IntContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterInt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitInt(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_value);
		try {
			setState(169);
			switch (_input.LA(1)) {
			case INT:
				_localctx = new IntContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(162); match(INT);
				}
				break;
			case REAL:
				_localctx = new RealContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(163); match(REAL);
				}
				break;
			case TRUE:
				_localctx = new TrueContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(164); match(TRUE);
				}
				break;
			case FALSE:
				_localctx = new FalseContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(165); match(FALSE);
				}
				break;
			case STRING:
				_localctx = new StringContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(166); match(STRING);
				}
				break;
			case FREE:
				_localctx = new FreeContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(167); match(FREE);
				}
				break;
			case ID:
				_localctx = new IdContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(168); match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhereContext extends ParserRuleContext {
		public List<TerminalNode> COMMA() { return getTokens(ExpressionsParser.COMMA); }
		public List<AssignContext> assign() {
			return getRuleContexts(AssignContext.class);
		}
		public TerminalNode WHERE() { return getToken(ExpressionsParser.WHERE, 0); }
		public AssignContext assign(int i) {
			return getRuleContext(AssignContext.class,i);
		}
		public TerminalNode COMMA(int i) {
			return getToken(ExpressionsParser.COMMA, i);
		}
		public WhereContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_where; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterWhere(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitWhere(this);
		}
	}

	public final WhereContext where() throws RecognitionException {
		WhereContext _localctx = new WhereContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_where);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(171); match(WHERE);
			setState(172); assign();
			setState(177);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(173); match(COMMA);
				setState(174); assign();
				}
				}
				setState(179);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignContext extends ParserRuleContext {
		public TerminalNode ASSIGN() { return getToken(ExpressionsParser.ASSIGN, 0); }
		public AssignNameContext assignName() {
			return getRuleContext(AssignNameContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AssignContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterAssign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitAssign(this);
		}
	}

	public final AssignContext assign() throws RecognitionException {
		AssignContext _localctx = new AssignContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_assign);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(180); assignName();
			setState(181); match(ASSIGN);
			setState(182); expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignNameContext extends ParserRuleContext {
		public AssignNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignName; }
	 
		public AssignNameContext() { }
		public void copyFrom(AssignNameContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SingleAssignContext extends AssignNameContext {
		public TerminalNode ID() { return getToken(ExpressionsParser.ID, 0); }
		public SingleAssignContext(AssignNameContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterSingleAssign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitSingleAssign(this);
		}
	}
	public static class TupleAssignContext extends AssignNameContext {
		public List<TerminalNode> ID() { return getTokens(ExpressionsParser.ID); }
		public List<TerminalNode> COMMA() { return getTokens(ExpressionsParser.COMMA); }
		public TerminalNode LT() { return getToken(ExpressionsParser.LT, 0); }
		public TerminalNode GT() { return getToken(ExpressionsParser.GT, 0); }
		public TerminalNode ID(int i) {
			return getToken(ExpressionsParser.ID, i);
		}
		public TerminalNode COMMA(int i) {
			return getToken(ExpressionsParser.COMMA, i);
		}
		public TupleAssignContext(AssignNameContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).enterTupleAssign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExpressionsListener ) ((ExpressionsListener)listener).exitTupleAssign(this);
		}
	}

	public final AssignNameContext assignName() throws RecognitionException {
		AssignNameContext _localctx = new AssignNameContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_assignName);
		int _la;
		try {
			setState(195);
			switch (_input.LA(1)) {
			case ID:
				_localctx = new SingleAssignContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(184); match(ID);
				}
				break;
			case LT:
				_localctx = new TupleAssignContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(185); match(LT);
				setState(186); match(ID);
				setState(191);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(187); match(COMMA);
					setState(188); match(ID);
					}
					}
					setState(193);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(194); match(GT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 0: return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return precpred(_ctx, 13);

		case 1: return precpred(_ctx, 12);

		case 2: return precpred(_ctx, 11);

		case 3: return precpred(_ctx, 9);

		case 4: return precpred(_ctx, 14);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\62\u00c8\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2"+
		"\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\7\2\64\n\2\f\2\16"+
		"\2\67\13\2\5\29\n\2\3\2\3\2\3\2\3\2\3\2\3\2\7\2A\n\2\f\2\16\2D\13\2\5"+
		"\2F\n\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2S\n\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\7\2g\n"+
		"\2\f\2\16\2j\13\2\3\3\5\3m\n\3\3\3\3\3\5\3q\n\3\3\3\5\3t\n\3\3\3\5\3w"+
		"\n\3\3\3\3\3\5\3{\n\3\3\4\3\4\3\4\3\4\3\4\5\4\u0082\n\4\3\5\3\5\3\5\3"+
		"\5\7\5\u0088\n\5\f\5\16\5\u008b\13\5\5\5\u008d\n\5\3\5\3\5\3\6\3\6\5\6"+
		"\u0093\n\6\3\6\3\6\3\7\3\7\5\7\u0099\n\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\5\b\u00a3\n\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00ac\n\t\3\n\3\n\3\n"+
		"\3\n\7\n\u00b2\n\n\f\n\16\n\u00b5\13\n\3\13\3\13\3\13\3\13\3\f\3\f\3\f"+
		"\3\f\3\f\7\f\u00c0\n\f\f\f\16\f\u00c3\13\f\3\f\5\f\u00c6\n\f\3\f\2\3\2"+
		"\r\2\4\6\b\n\f\16\20\22\24\26\2\5\3\2\35\"\3\2\32\33\3\2%&\u00e5\2R\3"+
		"\2\2\2\4l\3\2\2\2\6\u0081\3\2\2\2\b\u0083\3\2\2\2\n\u0090\3\2\2\2\f\u0096"+
		"\3\2\2\2\16\u00a2\3\2\2\2\20\u00ab\3\2\2\2\22\u00ad\3\2\2\2\24\u00b6\3"+
		"\2\2\2\26\u00c5\3\2\2\2\30\31\b\2\1\2\31\32\7\31\2\2\32S\5\2\2\22\33\34"+
		"\7\'\2\2\34S\5\2\2\21\35\36\7\22\2\2\36\37\5\2\2\2\37 \7\23\2\2 !\5\b"+
		"\5\2!S\3\2\2\2\"#\7\3\2\2#$\5\2\2\2$%\7\4\2\2%&\5\2\2\2&\'\7\5\2\2\'("+
		"\5\2\2\2(S\3\2\2\2)*\7\22\2\2*+\5\2\2\2+,\t\2\2\2,-\5\2\2\2-.\7\23\2\2"+
		".S\3\2\2\2/8\7\37\2\2\60\65\5\2\2\2\61\62\7\17\2\2\62\64\5\2\2\2\63\61"+
		"\3\2\2\2\64\67\3\2\2\2\65\63\3\2\2\2\65\66\3\2\2\2\669\3\2\2\2\67\65\3"+
		"\2\2\28\60\3\2\2\289\3\2\2\29:\3\2\2\2:S\7!\2\2;<\7-\2\2<E\7\22\2\2=B"+
		"\5\2\2\2>?\7\17\2\2?A\5\2\2\2@>\3\2\2\2AD\3\2\2\2B@\3\2\2\2BC\3\2\2\2"+
		"CF\3\2\2\2DB\3\2\2\2E=\3\2\2\2EF\3\2\2\2FG\3\2\2\2GS\7\23\2\2HI\7\22\2"+
		"\2IJ\5\2\2\2JK\7\23\2\2KS\3\2\2\2LS\5\4\3\2MN\7\24\2\2NS\7\25\2\2OS\5"+
		"\20\t\2PQ\7\60\2\2QS\7-\2\2R\30\3\2\2\2R\33\3\2\2\2R\35\3\2\2\2R\"\3\2"+
		"\2\2R)\3\2\2\2R/\3\2\2\2R;\3\2\2\2RH\3\2\2\2RL\3\2\2\2RM\3\2\2\2RO\3\2"+
		"\2\2RP\3\2\2\2Sh\3\2\2\2TU\f\17\2\2UV\t\3\2\2Vg\5\2\2\20WX\f\16\2\2XY"+
		"\7\30\2\2Yg\5\2\2\17Z[\f\r\2\2[\\\7\31\2\2\\g\5\2\2\16]^\f\13\2\2^_\t"+
		"\4\2\2_g\5\2\2\f`a\f\20\2\2ab\7\34\2\2bc\7\22\2\2cd\5\2\2\2de\7\23\2\2"+
		"eg\3\2\2\2fT\3\2\2\2fW\3\2\2\2fZ\3\2\2\2f]\3\2\2\2f`\3\2\2\2gj\3\2\2\2"+
		"hf\3\2\2\2hi\3\2\2\2i\3\3\2\2\2jh\3\2\2\2km\5\16\b\2lk\3\2\2\2lm\3\2\2"+
		"\2mn\3\2\2\2np\5\6\4\2oq\5\b\5\2po\3\2\2\2pq\3\2\2\2qs\3\2\2\2rt\5\n\6"+
		"\2sr\3\2\2\2st\3\2\2\2tv\3\2\2\2uw\5\f\7\2vu\3\2\2\2vw\3\2\2\2wz\3\2\2"+
		"\2xy\7\20\2\2y{\7-\2\2zx\3\2\2\2z{\3\2\2\2{\5\3\2\2\2|\u0082\7,\2\2}~"+
		"\7\22\2\2~\177\5\2\2\2\177\u0080\7\23\2\2\u0080\u0082\3\2\2\2\u0081|\3"+
		"\2\2\2\u0081}\3\2\2\2\u0082\7\3\2\2\2\u0083\u008c\7\22\2\2\u0084\u0089"+
		"\5\2\2\2\u0085\u0086\7\17\2\2\u0086\u0088\5\2\2\2\u0087\u0085\3\2\2\2"+
		"\u0088\u008b\3\2\2\2\u0089\u0087\3\2\2\2\u0089\u008a\3\2\2\2\u008a\u008d"+
		"\3\2\2\2\u008b\u0089\3\2\2\2\u008c\u0084\3\2\2\2\u008c\u008d\3\2\2\2\u008d"+
		"\u008e\3\2\2\2\u008e\u008f\7\23\2\2\u008f\t\3\2\2\2\u0090\u0092\7\24\2"+
		"\2\u0091\u0093\5\2\2\2\u0092\u0091\3\2\2\2\u0092\u0093\3\2\2\2\u0093\u0094"+
		"\3\2\2\2\u0094\u0095\7\25\2\2\u0095\13\3\2\2\2\u0096\u0098\7\26\2\2\u0097"+
		"\u0099\5\2\2\2\u0098\u0097\3\2\2\2\u0098\u0099\3\2\2\2\u0099\u009a\3\2"+
		"\2\2\u009a\u009b\7\27\2\2\u009b\r\3\2\2\2\u009c\u00a3\7)\2\2\u009d\u00a3"+
		"\7-\2\2\u009e\u009f\7\22\2\2\u009f\u00a0\5\2\2\2\u00a0\u00a1\7\23\2\2"+
		"\u00a1\u00a3\3\2\2\2\u00a2\u009c\3\2\2\2\u00a2\u009d\3\2\2\2\u00a2\u009e"+
		"\3\2\2\2\u00a3\17\3\2\2\2\u00a4\u00ac\7)\2\2\u00a5\u00ac\7*\2\2\u00a6"+
		"\u00ac\7#\2\2\u00a7\u00ac\7$\2\2\u00a8\u00ac\7.\2\2\u00a9\u00ac\7\f\2"+
		"\2\u00aa\u00ac\7-\2\2\u00ab\u00a4\3\2\2\2\u00ab\u00a5\3\2\2\2\u00ab\u00a6"+
		"\3\2\2\2\u00ab\u00a7\3\2\2\2\u00ab\u00a8\3\2\2\2\u00ab\u00a9\3\2\2\2\u00ab"+
		"\u00aa\3\2\2\2\u00ac\21\3\2\2\2\u00ad\u00ae\7\13\2\2\u00ae\u00b3\5\24"+
		"\13\2\u00af\u00b0\7\17\2\2\u00b0\u00b2\5\24\13\2\u00b1\u00af\3\2\2\2\u00b2"+
		"\u00b5\3\2\2\2\u00b3\u00b1\3\2\2\2\u00b3\u00b4\3\2\2\2\u00b4\23\3\2\2"+
		"\2\u00b5\u00b3\3\2\2\2\u00b6\u00b7\5\26\f\2\u00b7\u00b8\7\r\2\2\u00b8"+
		"\u00b9\5\2\2\2\u00b9\25\3\2\2\2\u00ba\u00c6\7-\2\2\u00bb\u00bc\7\37\2"+
		"\2\u00bc\u00c1\7-\2\2\u00bd\u00be\7\17\2\2\u00be\u00c0\7-\2\2\u00bf\u00bd"+
		"\3\2\2\2\u00c0\u00c3\3\2\2\2\u00c1\u00bf\3\2\2\2\u00c1\u00c2\3\2\2\2\u00c2"+
		"\u00c4\3\2\2\2\u00c3\u00c1\3\2\2\2\u00c4\u00c6\7!\2\2\u00c5\u00ba\3\2"+
		"\2\2\u00c5\u00bb\3\2\2\2\u00c6\27\3\2\2\2\30\658BERfhlpsvz\u0081\u0089"+
		"\u008c\u0092\u0098\u00a2\u00ab\u00b3\u00c1\u00c5";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}