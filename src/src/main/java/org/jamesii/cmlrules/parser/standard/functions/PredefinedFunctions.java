/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.functions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.jamesii.cmlrules.model.standard.StandardModel;
import org.jamesii.cmlrules.parser.standard.functions.param.SimpleParameter;
import org.jamesii.cmlrules.parser.standard.nodes.CosNode;
import org.jamesii.cmlrules.parser.standard.nodes.DistanceNode;
import org.jamesii.cmlrules.parser.standard.nodes.LogNode;
import org.jamesii.cmlrules.parser.standard.nodes.NormNode;
import org.jamesii.cmlrules.parser.standard.nodes.NuNode;
import org.jamesii.cmlrules.parser.standard.nodes.SinNode;
import org.jamesii.cmlrules.parser.standard.nodes.SolutionCountNode;
import org.jamesii.cmlrules.parser.standard.nodes.SolutionCountTwoAttributesNode;
import org.jamesii.cmlrules.parser.standard.nodes.SolutionFilterNode;
import org.jamesii.cmlrules.parser.standard.nodes.SpeciesAmountNode;
import org.jamesii.cmlrules.parser.standard.nodes.SpeciesAttributeNode;
import org.jamesii.cmlrules.parser.standard.nodes.SpeciesNameNode;
import org.jamesii.cmlrules.parser.standard.nodes.SpeciesSubNode;
import org.jamesii.cmlrules.parser.standard.nodes.ToIntNode;
import org.jamesii.cmlrules.parser.standard.nodes.UnifNode;
import org.jamesii.cmlrules.parser.standard.types.BaseType;
import org.jamesii.cmlrules.parser.standard.types.FunctionType;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.Identifier;

/**
 * Within this class useful functions which should be available in all ML-Rules
 * models are defined:
 * <ol>
 * amount(species) - return the amount of the given species
 * </ol>
 * <ol>
 * att(species) - return the attributes of the given species
 * </ol>
 * <ol>
 * sub(species) - return the sub species of the given species
 * </ol>
 * <ol>
 * name(species) - return the type name of the given species
 * </ol>
 * <ol>
 * norm(mean,var) - return a random number sampled from a normal distribution
 * with the given mean and variance
 * </ol>
 * <ol>
 * unif(min,max) - return a random number sampled from a uniformed distribution
 * with the given min and max
 * </ol>
 * <ol>
 * toInt(double) - return the int value of the given double value
 * </ol>
 * <ol>
 * nu() - return a new link value
 * </ol>
 * <ol>
 * simtime() - return the current simulation time
 * </ol>
 * <ol>
 * infinity() - return the double value infinity
 * </ol>
 * <ol>
 * count(sol,string) - return the amount of all species within the given
 * solution with the given name (no sub species considered)
 * </ol>
 * <ol>
 * count(sol,string,num,num) - return the amount of all species within the given
 * solution with the given name and both given attribute values
 * </ol>
 * <ol>
 * dist(num,num,num,num) - return the distance of two coordinates (the first two
 * numbers are x1 and y1; the second two numbers are x2 and y2)
 * </ol>
 * <ol>
 * filter(sol,string) - return a solution no containing species with the given
 * name (no filtering of sub species)
 * </ol>
 * <ol>
 * sin(num) - compute the sine of the given angle
 * </ol>
 * <ol>
 * cos(num) - compute the cosine of the given angle
 * </ol>
 * <ol>
 * log(num) - compute the natural logarithm of the given value
 * </ol>
 * 
 * @author Tobias Helms
 *
 */
public class PredefinedFunctions {

  private PredefinedFunctions() {
  }

  /**
   * Add all predefined functions to the given environment.
   */
  public static final void addAll(MLEnvironment env) {
    att(env);
    amount(env);
    sub(env);
    name(env);
    norm(env);
    unif(env);
    toInt(env);
    nu(env);
    simtime(env);
    infinity(env);
    count(env);
    countTwoAtts(env);
    dist(env);
    filter(env);
    sin(env);
    cos(env);
    log(env);
  }

  public static final void amount(MLEnvironment env) {
    String name = "amount";
    Function f = new Function(name, new FunctionType(Arrays.asList(
        BaseType.SPECIES, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays
        .asList(new SimpleParameter(SpeciesAmountNode.NAME)),
        new SpeciesAmountNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void name(MLEnvironment env) {
    String name = "name";
    Function f = new Function(name, new FunctionType(Arrays.asList(
        BaseType.SPECIES, BaseType.STRING)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays
        .asList(new SimpleParameter(SpeciesNameNode.NAME)),
        new SpeciesNameNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void att(MLEnvironment env) {
    String name = "att";
    Function f = new Function(name, new FunctionType(Arrays.asList(
        BaseType.SPECIES, BaseType.TUPLE)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays
        .asList(new SimpleParameter(SpeciesAttributeNode.NAME)),
        new SpeciesAttributeNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void sub(MLEnvironment env) {
    String name = "sub";
    Function f = new Function(name, new FunctionType(Arrays.asList(
        BaseType.SPECIES, BaseType.SOL)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays
        .asList(new SimpleParameter(SpeciesSubNode.NAME)),
        new SpeciesSubNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void norm(MLEnvironment env) {
    String name = "norm";
    Function f = new Function(name, new FunctionType(Arrays.asList(
        BaseType.NUM, BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(
        new SimpleParameter(NormNode.MEAN), new SimpleParameter(NormNode.VAR)),
        new NormNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void unif(MLEnvironment env) {
    String name = "unif";
    Function f = new Function(name, new FunctionType(Arrays.asList(
        BaseType.NUM, BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(
        new SimpleParameter(UnifNode.MIN), new SimpleParameter(UnifNode.MAX)),
        new UnifNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void toInt(MLEnvironment env) {
    String name = "toInt";
    Function f = new Function(name, new FunctionType(Arrays.asList(
        BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays
        .asList(new SimpleParameter(ToIntNode.VALUE)), new ToIntNode(),
        new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void nu(MLEnvironment env) {
    String name = "nu";
    Function f = new Function(name, new FunctionType(
        Arrays.asList(BaseType.LINK)));
    f.init(Arrays.asList(new FunctionDefinition(new ArrayList<>(),
        new NuNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void simtime(MLEnvironment env) {
    String name = "simtime";
    Function f = new Function(name, new FunctionType(
        Arrays.asList(BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Collections.emptyList(),
        new Identifier<String>(StandardModel.TIME), Collections.emptyList())));
    env.setGlobalValue(name, f);
    env.setGlobalValue(StandardModel.TIME, 0.0);
  }

  public static final void infinity(MLEnvironment env) {
    String name = "infinity";
    Function f = new Function(name, new FunctionType(
        Arrays.asList(BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Collections.emptyList(),
        new ValueNode<Double>(Double.POSITIVE_INFINITY), Collections
            .emptyList())));
    env.setGlobalValue(name, f);
  }

  public static final void count(MLEnvironment env) {
    String name = "count";
    Function f = new Function(name, new FunctionType(Arrays.asList(
        BaseType.SOL, BaseType.STRING, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(
        new SimpleParameter(SolutionCountNode.SOL), new SimpleParameter(
            SolutionCountNode.NAME)), new SolutionCountNode(),
        new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void countTwoAtts(MLEnvironment env) {
    String name = "countTwoAtts";
    Function f = new Function(name, new FunctionType(
        Arrays.asList(BaseType.SOL, BaseType.STRING, BaseType.NUM,
            BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(
        new SimpleParameter(SolutionCountTwoAttributesNode.SOL),
        new SimpleParameter(SolutionCountTwoAttributesNode.NAME),
        new SimpleParameter(SolutionCountTwoAttributesNode.ATT_1),
        new SimpleParameter(SolutionCountTwoAttributesNode.ATT_2)),
        new SolutionCountTwoAttributesNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void dist(MLEnvironment env) {
    String name = "dist";
    Function f = new Function(name, new FunctionType(Arrays.asList(
        BaseType.NUM, BaseType.NUM, BaseType.NUM, BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(
        new SimpleParameter(DistanceNode.X1), new SimpleParameter(
            DistanceNode.Y1), new SimpleParameter(DistanceNode.X2),
        new SimpleParameter(DistanceNode.Y2)), new DistanceNode(),
        new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void filter(MLEnvironment env) {
    String name = "filter";
    Function f = new Function(name, new FunctionType(Arrays.asList(
        BaseType.SOL, BaseType.STRING, BaseType.SOL)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(
        new SimpleParameter(SolutionFilterNode.SOL), new SimpleParameter(
            SolutionFilterNode.NAME)), new SolutionFilterNode(),
        new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void sin(MLEnvironment env) {
    String name = "sin";
    Function f = new Function(name, new FunctionType(Arrays.asList(
        BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays
        .asList(new SimpleParameter(SinNode.VAL)), new SinNode(),
        new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void cos(MLEnvironment env) {
    String name = "cos";
    Function f = new Function(name, new FunctionType(Arrays.asList(
        BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays
        .asList(new SimpleParameter(CosNode.VAL)), new CosNode(),
        new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void log(MLEnvironment env) {
    String name = "log";
    Function f = new Function(name, new FunctionType(Arrays.asList(
        BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays
        .asList(new SimpleParameter(LogNode.VAL)), new LogNode(),
        new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

}
