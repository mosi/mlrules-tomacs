package org.jamesii.cmlrules.parser.standard.nodes;

import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;

public class LogNode extends Node {

  private static final long serialVersionUID = 1L;
  
  public static final String VAL = "§val";
  
  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    if (cEnv instanceof MLEnvironment) {
      MLEnvironment env = (MLEnvironment) cEnv;
      Double x1 = ((Number) env.getValue(VAL)).doubleValue();
      if (x1 == null) {
        throw new IllegalArgumentException(
            "angle is null");
      }
      return (N) new ValueNode<Double>(Math.log(x1));
    }
    throw new IllegalArgumentException(
        "the given environment to calculate the logarithm is not an MLEnvironment");
  }
}
