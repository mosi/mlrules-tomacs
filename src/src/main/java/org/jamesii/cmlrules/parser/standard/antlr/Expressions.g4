/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

grammar Expressions;

import ExpressionsLex;

expression
    :	L_PAREN expression R_PAREN attributes 				# Application
    |   MINUS expression                                    # MinusOne
    |   NOT expression                                      # Not
    |   expression ROOF L_PAREN expression R_PAREN          # Roof
    |   expression (MULT | DIV) expression                  # MultDiv
    |   expression PLUS expression							# Plus
    |   expression MINUS expression                			# Minus
    |   IF expression THEN expression ELSE expression       # IfThenElse
    |   expression (AND | OR) expression                    # AndOr
    |   L_PAREN expression (LT | GT | LT_EQ | GT_EQ | EQUALS | N_EQUALS) expression R_PAREN            # BoolExpr
    |   LT (expression (COMMA expression)*)? GT				# tuple
    |   ID L_PAREN (expression (COMMA expression)*)? R_PAREN# FunctionCall
    |   L_PAREN expression R_PAREN                          # Paren
    //|   LAMBDA L_PAREN ID (COMMA ID)* R_PAREN L_PAREN expression R_PAREN	# LambdaFunction
    |	species 											# SpeciesExpr
    |   L_BRAKET R_BRAKET									# EmptySolution
    |   value                                               # ExpValue
    |	COUNT ID											# countShort
    ;

species : amount? name attributes? subSpecies? guard? (COLON ID)?;

name : ID_SPECIES | L_PAREN expression R_PAREN;

attributes: L_PAREN (expression (COMMA expression)*)? R_PAREN;

subSpecies: L_BRAKET expression? R_BRAKET;

guard: L_CURLY expression? R_CURLY;

amount
	: INT							#amountINT
	| ID							#amountID
	| L_PAREN expression R_PAREN	#amountExpr
	;
    
value
    :   INT         # Int
    |   REAL        # Real
    |   TRUE        # True
    |   FALSE       # False
    |   STRING      # String
    |	FREE		# Free
    |   ID          # Id
    ;
    
where : WHERE assign (COMMA assign)*; 
    
assign : assignName ASSIGN expression;

assignName
	: ID					#singleAssign						
	| LT ID (COMMA ID)* GT	#tupleAssign
	;
