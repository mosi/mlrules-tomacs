/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.nodes;

import org.jamesii.cmlrules.model.standard.StandardModel;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.core.math.random.distributions.UniformDistribution;
import org.jamesii.core.math.random.generators.IRandom;

public class UnifNode extends Node {

  private static final long serialVersionUID = 1L;

  public static String MIN = "§min";

  public static String MAX = "§max";

  public static String UNIF = "§unif";

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    if (cEnv instanceof MLEnvironment) {
      MLEnvironment env = (MLEnvironment) cEnv;
      if (env.getValue(MIN) instanceof Number
          && env.getValue(MAX) instanceof Number) {
        UniformDistribution dist = (UniformDistribution) env.getValue(UNIF);
        if (dist == null) {
          IRandom rng = (IRandom) env.getValue(StandardModel.RNG);
          dist = new UniformDistribution(rng);
          env.setGlobalValue(UNIF, dist);
        }
        dist.setLowerBorder(((Number) env.getValue(MIN)).doubleValue());
        dist.setUpperBorder(((Number) env.getValue(MAX)).doubleValue());
        return (N) new ValueNode<Double>(dist.getRandomNumber());
      }
    }
    throw new IllegalArgumentException(
        String.format("Could not compute a uniform distributed number."));
  }

}
