package org.jamesii.cmlrules.parser.links.listener.typecheck;

import java.util.Map;
import java.util.Set;

import org.jamesii.cmlrules.model.Model;
import org.jamesii.cmlrules.model.links.LinkModel;
import org.jamesii.cmlrules.model.standard.rule.RuleComparator;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.parser.standard.listener.ModelCreationListener;
import org.jamesii.cmlrules.parser.standard.listener.RulesListener;
import org.jamesii.cmlrules.util.MLEnvironment;

public class LinkRulesListener extends RulesListener implements ModelCreationListener {

  final Map<SpeciesType, Set<Integer>> linkAttributes;
  
  public LinkRulesListener(MLEnvironment env, Map<SpeciesType, Set<Integer>> linkAttributes) {
    super(env);
    this.linkAttributes = linkAttributes;
  }
  
  @Override
  public Model create() {
    getTimedRules().sort(RuleComparator.instance);
    return new LinkModel(getInitialSolution(), getRules(), getTimedRules(), linkAttributes, getEnv());
  }

}
