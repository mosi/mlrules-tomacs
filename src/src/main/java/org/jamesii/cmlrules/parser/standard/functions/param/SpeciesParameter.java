/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.functions.param;

import java.util.List;
import java.util.Map;

import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;

public class SpeciesParameter implements Parameter {

  private final SpeciesType type;

  private final List<Parameter> subParameter;

  public SpeciesParameter(SpeciesType name, List<Parameter> subParameter) {
    this.type = name;
    this.subParameter = subParameter;
  }

  /**
   * Return true if the given species has the same type as the parameter and the
   * same number of attributes. Map attribute values to given attribute
   * variables, e.g., the species <b>A(1,3)</b> would match the parameter
   * <b>A(x,3)</b> and the variable x would be bound to <b>1</b>.
   */
  private boolean matchSpecies(Species species, Map<String, Object> vars) {
    if (!type.equals(species.getType())
        || species.getType().getAttributes() != subParameter.size()) {
      return false;
    }
    for (int i = 0; i < species.getType().getAttributes(); ++i) {
      if (!subParameter.get(i).match(species.getAttribute(i), vars)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String toString() {
    return subParameter.toString();
  }

  @Override
  public boolean match(Object value, Map<String, Object> vars) {
    if (!(value instanceof Map<?, ?>)) {
      return false;
    }
    Map<?, ?> map = (Map<?, ?>) value;
    if (map.size() != 1) {
      return false;
    }
    Object o = map.entrySet().iterator().next().getKey();
    if (!(o instanceof Species)) {
      return false;
    }
    return matchSpecies((Species) o, vars);
  }
}
