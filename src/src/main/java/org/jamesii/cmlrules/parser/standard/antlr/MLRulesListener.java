// Generated from MLRules.g4 by ANTLR 4.3
package org.jamesii.cmlrules.parser.standard.antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MLRulesParser}.
 */
public interface MLRulesListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MLRulesParser#preamble}.
	 * @param ctx the parse tree
	 */
	void enterPreamble(@NotNull MLRulesParser.PreambleContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#preamble}.
	 * @param ctx the parse tree
	 */
	void exitPreamble(@NotNull MLRulesParser.PreambleContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(@NotNull MLRulesParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(@NotNull MLRulesParser.ConstantContext ctx);

	/**
	 * Enter a parse tree produced by the {@code baseTypeString}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void enterBaseTypeString(@NotNull MLRulesParser.BaseTypeStringContext ctx);
	/**
	 * Exit a parse tree produced by the {@code baseTypeString}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void exitBaseTypeString(@NotNull MLRulesParser.BaseTypeStringContext ctx);

	/**
	 * Enter a parse tree produced by the {@code True}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 */
	void enterTrue(@NotNull MLRulesParser.TrueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code True}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 */
	void exitTrue(@NotNull MLRulesParser.TrueContext ctx);

	/**
	 * Enter a parse tree produced by the {@code tupleType}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void enterTupleType(@NotNull MLRulesParser.TupleTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tupleType}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void exitTupleType(@NotNull MLRulesParser.TupleTypeContext ctx);

	/**
	 * Enter a parse tree produced by the {@code False}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 */
	void enterFalse(@NotNull MLRulesParser.FalseContext ctx);
	/**
	 * Exit a parse tree produced by the {@code False}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 */
	void exitFalse(@NotNull MLRulesParser.FalseContext ctx);

	/**
	 * Enter a parse tree produced by the {@code String}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 */
	void enterString(@NotNull MLRulesParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by the {@code String}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 */
	void exitString(@NotNull MLRulesParser.StringContext ctx);

	/**
	 * Enter a parse tree produced by the {@code paramSpecies}
	 * labeled alternative in {@link MLRulesParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParamSpecies(@NotNull MLRulesParser.ParamSpeciesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code paramSpecies}
	 * labeled alternative in {@link MLRulesParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParamSpecies(@NotNull MLRulesParser.ParamSpeciesContext ctx);

	/**
	 * Enter a parse tree produced by the {@code paramSol}
	 * labeled alternative in {@link MLRulesParser#atomicParameter}.
	 * @param ctx the parse tree
	 */
	void enterParamSol(@NotNull MLRulesParser.ParamSolContext ctx);
	/**
	 * Exit a parse tree produced by the {@code paramSol}
	 * labeled alternative in {@link MLRulesParser#atomicParameter}.
	 * @param ctx the parse tree
	 */
	void exitParamSol(@NotNull MLRulesParser.ParamSolContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#initialSolution}.
	 * @param ctx the parse tree
	 */
	void enterInitialSolution(@NotNull MLRulesParser.InitialSolutionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#initialSolution}.
	 * @param ctx the parse tree
	 */
	void exitInitialSolution(@NotNull MLRulesParser.InitialSolutionContext ctx);

	/**
	 * Enter a parse tree produced by the {@code amountID}
	 * labeled alternative in {@link MLRulesParser#amount}.
	 * @param ctx the parse tree
	 */
	void enterAmountID(@NotNull MLRulesParser.AmountIDContext ctx);
	/**
	 * Exit a parse tree produced by the {@code amountID}
	 * labeled alternative in {@link MLRulesParser#amount}.
	 * @param ctx the parse tree
	 */
	void exitAmountID(@NotNull MLRulesParser.AmountIDContext ctx);

	/**
	 * Enter a parse tree produced by the {@code emptySol}
	 * labeled alternative in {@link MLRulesParser#atomicParameter}.
	 * @param ctx the parse tree
	 */
	void enterEmptySol(@NotNull MLRulesParser.EmptySolContext ctx);
	/**
	 * Exit a parse tree produced by the {@code emptySol}
	 * labeled alternative in {@link MLRulesParser#atomicParameter}.
	 * @param ctx the parse tree
	 */
	void exitEmptySol(@NotNull MLRulesParser.EmptySolContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#typeDefinition}.
	 * @param ctx the parse tree
	 */
	void enterTypeDefinition(@NotNull MLRulesParser.TypeDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#typeDefinition}.
	 * @param ctx the parse tree
	 */
	void exitTypeDefinition(@NotNull MLRulesParser.TypeDefinitionContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(@NotNull MLRulesParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(@NotNull MLRulesParser.FunctionContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Real}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 */
	void enterReal(@NotNull MLRulesParser.RealContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Real}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 */
	void exitReal(@NotNull MLRulesParser.RealContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#model}.
	 * @param ctx the parse tree
	 */
	void enterModel(@NotNull MLRulesParser.ModelContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#model}.
	 * @param ctx the parse tree
	 */
	void exitModel(@NotNull MLRulesParser.ModelContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Plus}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPlus(@NotNull MLRulesParser.PlusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Plus}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPlus(@NotNull MLRulesParser.PlusContext ctx);

	/**
	 * Enter a parse tree produced by the {@code baseTypeBool}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void enterBaseTypeBool(@NotNull MLRulesParser.BaseTypeBoolContext ctx);
	/**
	 * Exit a parse tree produced by the {@code baseTypeBool}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void exitBaseTypeBool(@NotNull MLRulesParser.BaseTypeBoolContext ctx);

	/**
	 * Enter a parse tree produced by the {@code solutionType}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void enterSolutionType(@NotNull MLRulesParser.SolutionTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code solutionType}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void exitSolutionType(@NotNull MLRulesParser.SolutionTypeContext ctx);

	/**
	 * Enter a parse tree produced by the {@code speciesType}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void enterSpeciesType(@NotNull MLRulesParser.SpeciesTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code speciesType}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void exitSpeciesType(@NotNull MLRulesParser.SpeciesTypeContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#subSpecies}.
	 * @param ctx the parse tree
	 */
	void enterSubSpecies(@NotNull MLRulesParser.SubSpeciesContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#subSpecies}.
	 * @param ctx the parse tree
	 */
	void exitSubSpecies(@NotNull MLRulesParser.SubSpeciesContext ctx);

	/**
	 * Enter a parse tree produced by the {@code SpeciesExpr}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSpeciesExpr(@NotNull MLRulesParser.SpeciesExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SpeciesExpr}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSpeciesExpr(@NotNull MLRulesParser.SpeciesExprContext ctx);

	/**
	 * Enter a parse tree produced by the {@code singleAssign}
	 * labeled alternative in {@link MLRulesParser#assignName}.
	 * @param ctx the parse tree
	 */
	void enterSingleAssign(@NotNull MLRulesParser.SingleAssignContext ctx);
	/**
	 * Exit a parse tree produced by the {@code singleAssign}
	 * labeled alternative in {@link MLRulesParser#assignName}.
	 * @param ctx the parse tree
	 */
	void exitSingleAssign(@NotNull MLRulesParser.SingleAssignContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Free}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 */
	void enterFree(@NotNull MLRulesParser.FreeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Free}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 */
	void exitFree(@NotNull MLRulesParser.FreeContext ctx);

	/**
	 * Enter a parse tree produced by the {@code EmptySolution}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterEmptySolution(@NotNull MLRulesParser.EmptySolutionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code EmptySolution}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitEmptySolution(@NotNull MLRulesParser.EmptySolutionContext ctx);

	/**
	 * Enter a parse tree produced by the {@code MultDiv}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMultDiv(@NotNull MLRulesParser.MultDivContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MultDiv}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMultDiv(@NotNull MLRulesParser.MultDivContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Not}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNot(@NotNull MLRulesParser.NotContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Not}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNot(@NotNull MLRulesParser.NotContext ctx);

	/**
	 * Enter a parse tree produced by the {@code countShort}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCountShort(@NotNull MLRulesParser.CountShortContext ctx);
	/**
	 * Exit a parse tree produced by the {@code countShort}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCountShort(@NotNull MLRulesParser.CountShortContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#name}.
	 * @param ctx the parse tree
	 */
	void enterName(@NotNull MLRulesParser.NameContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#name}.
	 * @param ctx the parse tree
	 */
	void exitName(@NotNull MLRulesParser.NameContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Id}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 */
	void enterId(@NotNull MLRulesParser.IdContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Id}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 */
	void exitId(@NotNull MLRulesParser.IdContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#mlrule}.
	 * @param ctx the parse tree
	 */
	void enterMlrule(@NotNull MLRulesParser.MlruleContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#mlrule}.
	 * @param ctx the parse tree
	 */
	void exitMlrule(@NotNull MLRulesParser.MlruleContext ctx);

	/**
	 * Enter a parse tree produced by the {@code tupleAssign}
	 * labeled alternative in {@link MLRulesParser#assignName}.
	 * @param ctx the parse tree
	 */
	void enterTupleAssign(@NotNull MLRulesParser.TupleAssignContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tupleAssign}
	 * labeled alternative in {@link MLRulesParser#assignName}.
	 * @param ctx the parse tree
	 */
	void exitTupleAssign(@NotNull MLRulesParser.TupleAssignContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Application}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterApplication(@NotNull MLRulesParser.ApplicationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Application}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitApplication(@NotNull MLRulesParser.ApplicationContext ctx);

	/**
	 * Enter a parse tree produced by the {@code AndOr}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAndOr(@NotNull MLRulesParser.AndOrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AndOr}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAndOr(@NotNull MLRulesParser.AndOrContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Minus}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMinus(@NotNull MLRulesParser.MinusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Minus}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMinus(@NotNull MLRulesParser.MinusContext ctx);

	/**
	 * Enter a parse tree produced by the {@code baseTypeFloat}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void enterBaseTypeFloat(@NotNull MLRulesParser.BaseTypeFloatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code baseTypeFloat}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void exitBaseTypeFloat(@NotNull MLRulesParser.BaseTypeFloatContext ctx);

	/**
	 * Enter a parse tree produced by the {@code BoolExpr}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBoolExpr(@NotNull MLRulesParser.BoolExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BoolExpr}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBoolExpr(@NotNull MLRulesParser.BoolExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#guard}.
	 * @param ctx the parse tree
	 */
	void enterGuard(@NotNull MLRulesParser.GuardContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#guard}.
	 * @param ctx the parse tree
	 */
	void exitGuard(@NotNull MLRulesParser.GuardContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Int}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 */
	void enterInt(@NotNull MLRulesParser.IntContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Int}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 */
	void exitInt(@NotNull MLRulesParser.IntContext ctx);

	/**
	 * Enter a parse tree produced by the {@code paramSimple}
	 * labeled alternative in {@link MLRulesParser#atomicParameter}.
	 * @param ctx the parse tree
	 */
	void enterParamSimple(@NotNull MLRulesParser.ParamSimpleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code paramSimple}
	 * labeled alternative in {@link MLRulesParser#atomicParameter}.
	 * @param ctx the parse tree
	 */
	void exitParamSimple(@NotNull MLRulesParser.ParamSimpleContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#speciesDefinition}.
	 * @param ctx the parse tree
	 */
	void enterSpeciesDefinition(@NotNull MLRulesParser.SpeciesDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#speciesDefinition}.
	 * @param ctx the parse tree
	 */
	void exitSpeciesDefinition(@NotNull MLRulesParser.SpeciesDefinitionContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#products}.
	 * @param ctx the parse tree
	 */
	void enterProducts(@NotNull MLRulesParser.ProductsContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#products}.
	 * @param ctx the parse tree
	 */
	void exitProducts(@NotNull MLRulesParser.ProductsContext ctx);

	/**
	 * Enter a parse tree produced by the {@code tuple}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterTuple(@NotNull MLRulesParser.TupleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tuple}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitTuple(@NotNull MLRulesParser.TupleContext ctx);

	/**
	 * Enter a parse tree produced by the {@code ExpValue}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpValue(@NotNull MLRulesParser.ExpValueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExpValue}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpValue(@NotNull MLRulesParser.ExpValueContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Roof}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterRoof(@NotNull MLRulesParser.RoofContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Roof}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitRoof(@NotNull MLRulesParser.RoofContext ctx);

	/**
	 * Enter a parse tree produced by the {@code paramTuple}
	 * labeled alternative in {@link MLRulesParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParamTuple(@NotNull MLRulesParser.ParamTupleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code paramTuple}
	 * labeled alternative in {@link MLRulesParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParamTuple(@NotNull MLRulesParser.ParamTupleContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#rate}.
	 * @param ctx the parse tree
	 */
	void enterRate(@NotNull MLRulesParser.RateContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#rate}.
	 * @param ctx the parse tree
	 */
	void exitRate(@NotNull MLRulesParser.RateContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#where}.
	 * @param ctx the parse tree
	 */
	void enterWhere(@NotNull MLRulesParser.WhereContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#where}.
	 * @param ctx the parse tree
	 */
	void exitWhere(@NotNull MLRulesParser.WhereContext ctx);

	/**
	 * Enter a parse tree produced by the {@code FunctionCall}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(@NotNull MLRulesParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code FunctionCall}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(@NotNull MLRulesParser.FunctionCallContext ctx);

	/**
	 * Enter a parse tree produced by the {@code functionType}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void enterFunctionType(@NotNull MLRulesParser.FunctionTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionType}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void exitFunctionType(@NotNull MLRulesParser.FunctionTypeContext ctx);

	/**
	 * Enter a parse tree produced by the {@code amountExpr}
	 * labeled alternative in {@link MLRulesParser#amount}.
	 * @param ctx the parse tree
	 */
	void enterAmountExpr(@NotNull MLRulesParser.AmountExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code amountExpr}
	 * labeled alternative in {@link MLRulesParser#amount}.
	 * @param ctx the parse tree
	 */
	void exitAmountExpr(@NotNull MLRulesParser.AmountExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDefinition(@NotNull MLRulesParser.FunctionDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDefinition(@NotNull MLRulesParser.FunctionDefinitionContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#reactants}.
	 * @param ctx the parse tree
	 */
	void enterReactants(@NotNull MLRulesParser.ReactantsContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#reactants}.
	 * @param ctx the parse tree
	 */
	void exitReactants(@NotNull MLRulesParser.ReactantsContext ctx);

	/**
	 * Enter a parse tree produced by the {@code paramSingle}
	 * labeled alternative in {@link MLRulesParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParamSingle(@NotNull MLRulesParser.ParamSingleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code paramSingle}
	 * labeled alternative in {@link MLRulesParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParamSingle(@NotNull MLRulesParser.ParamSingleContext ctx);

	/**
	 * Enter a parse tree produced by the {@code baseTypeLink}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void enterBaseTypeLink(@NotNull MLRulesParser.BaseTypeLinkContext ctx);
	/**
	 * Exit a parse tree produced by the {@code baseTypeLink}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 */
	void exitBaseTypeLink(@NotNull MLRulesParser.BaseTypeLinkContext ctx);

	/**
	 * Enter a parse tree produced by the {@code MinusOne}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMinusOne(@NotNull MLRulesParser.MinusOneContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MinusOne}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMinusOne(@NotNull MLRulesParser.MinusOneContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#species}.
	 * @param ctx the parse tree
	 */
	void enterSpecies(@NotNull MLRulesParser.SpeciesContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#species}.
	 * @param ctx the parse tree
	 */
	void exitSpecies(@NotNull MLRulesParser.SpeciesContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#attributes}.
	 * @param ctx the parse tree
	 */
	void enterAttributes(@NotNull MLRulesParser.AttributesContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#attributes}.
	 * @param ctx the parse tree
	 */
	void exitAttributes(@NotNull MLRulesParser.AttributesContext ctx);

	/**
	 * Enter a parse tree produced by the {@code IfThenElse}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIfThenElse(@NotNull MLRulesParser.IfThenElseContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IfThenElse}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIfThenElse(@NotNull MLRulesParser.IfThenElseContext ctx);

	/**
	 * Enter a parse tree produced by the {@code amountINT}
	 * labeled alternative in {@link MLRulesParser#amount}.
	 * @param ctx the parse tree
	 */
	void enterAmountINT(@NotNull MLRulesParser.AmountINTContext ctx);
	/**
	 * Exit a parse tree produced by the {@code amountINT}
	 * labeled alternative in {@link MLRulesParser#amount}.
	 * @param ctx the parse tree
	 */
	void exitAmountINT(@NotNull MLRulesParser.AmountINTContext ctx);

	/**
	 * Enter a parse tree produced by the {@code Paren}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterParen(@NotNull MLRulesParser.ParenContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Paren}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitParen(@NotNull MLRulesParser.ParenContext ctx);

	/**
	 * Enter a parse tree produced by {@link MLRulesParser#assign}.
	 * @param ctx the parse tree
	 */
	void enterAssign(@NotNull MLRulesParser.AssignContext ctx);
	/**
	 * Exit a parse tree produced by {@link MLRulesParser#assign}.
	 * @param ctx the parse tree
	 */
	void exitAssign(@NotNull MLRulesParser.AssignContext ctx);
}