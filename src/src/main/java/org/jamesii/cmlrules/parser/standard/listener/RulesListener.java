/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.RuleContext;
import org.jamesii.cmlrules.model.Model;
import org.jamesii.cmlrules.model.standard.StandardModel;
import org.jamesii.cmlrules.model.standard.rule.PeriodicRule;
import org.jamesii.cmlrules.model.standard.rule.Reactant;
import org.jamesii.cmlrules.model.standard.rule.Rule;
import org.jamesii.cmlrules.model.standard.rule.RuleComparator;
import org.jamesii.cmlrules.model.standard.species.AbstractSpecies;
import org.jamesii.cmlrules.model.standard.species.MapSpecies;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ConstantContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.FunctionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.InitialSolutionContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.MlruleContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ProductsContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.RateContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ReactantsContext;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.SpeciesDefinitionContext;
import org.jamesii.cmlrules.parser.standard.nodes.SpeciesPatternNode;
import org.jamesii.cmlrules.util.Assignment;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.cmlrules.util.NodeHelper;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;

public class RulesListener extends ExpressionsListener implements
    ModelCreationListener {

  private Species initialSolution;

  private final List<Rule> rules = new ArrayList<>();

  private final List<Rule> timedRules = new ArrayList<>();

  public RulesListener(MLEnvironment env) {
    super(env);
  }

  @Override
  public void exitInitialSolution(InitialSolutionContext ctx) {
    Node node = null;
    try {
      node = (Node) getData().remove(ctx.getChild(2));
      @SuppressWarnings("unchecked")
      Map<Species, Species> species =
          (Map<Species, Species>) ((ValueNode<?>) node.calc(getEnv()))
              .getValue();
      initialSolution =
          new MapSpecies(new HashMap<>(), 1.0, SpeciesType.ROOT, new Object[0],
              AbstractSpecies.UNKNOWN);
      Species root =
          new MapSpecies(species, 1.0, SpeciesType.ROOT, new Object[0],
              initialSolution);
      initialSolution.add(root);
      root.getSubSpeciesStream().forEach(s -> s.setContext(root));
      root.individualize(new HashMap<>());
    } catch (Exception e) {
      throw new SemanticsException(ctx, String.format(
          "Could not create the initial solution %s: %s", node.toString(),
          e.getMessage()));
    }
  }

  @Override
  public void exitRate(RateContext ctx) {
    getData().put(ctx, getData().remove(ctx.getChild(0)));
  }

  @Override
  public void exitReactants(ReactantsContext ctx) {
    List<Reactant> reactants = new ArrayList<>();
    for (int i = 0; i < ctx.getChildCount(); ++i) {
      if (ctx.getChild(i) instanceof RuleContext) {
        SpeciesPatternNode node =
            (SpeciesPatternNode) getData().remove(ctx.getChild(i));
        reactants.add(node.toReactant(getEnv()));
      }
    }
    getData().put(ctx, reactants);
  }

  @Override
  public void exitProducts(ProductsContext ctx) {
    Node node = (Node) getData().remove(ctx.getChild(0));
    getData().put(
        ctx,
        node == null ? new ValueNode<Map<Species, Species>>(new HashMap<>())
            : node);
  }

  @Override
  @SuppressWarnings("unchecked")
  public void exitMlrule(MlruleContext ctx) {
    List<Assignment> assignments =
        (List<Assignment>) getData().remove(ctx.getChild(5));
    List<Reactant> reactants =
        (List<Reactant>) getData().remove(ctx.getChild(0));
    Node product = (Node) getData().remove(ctx.getChild(2));
    if (ctx.AT() != null) {
      rules.add(new Rule(reactants, product, (Node) getData().remove(
          ctx.getChild(4)), assignments == null ? new ArrayList<>()
          : assignments));
    } else {
      double time =
          NodeHelper.getDouble((Node) getData().remove(ctx.getChild(4)),
              getEnv());
      if (ctx.ATEACH() != null) {
        timedRules.add(new PeriodicRule(time, reactants, product,
            new ValueNode<Double>(time),
            assignments == null ? new ArrayList<>() : assignments));
      } else {
        timedRules.add(new Rule(reactants, product,
            new ValueNode<Double>(time),
            assignments == null ? new ArrayList<>() : assignments));
      }
    }
    getData().clear();
  }

  public Species getInitialSolution() {
    return initialSolution;
  }

  @Override
  public void exitConstant(ConstantContext ctx) {
    getData().clear();
  }

  @Override
  public void exitFunction(FunctionContext ctx) {
    getData().clear();
  }

  @Override
  public void exitSpeciesDefinition(SpeciesDefinitionContext ctx) {
    getData().clear();
  }

  @Override
  public Model create() {
    timedRules.sort(RuleComparator.instance);
    return new StandardModel(getInitialSolution(), rules, timedRules, getEnv());
  }

  public List<Rule> getRules() {
    return rules;
  }

  public List<Rule> getTimedRules() {
    return timedRules;
  }

}
