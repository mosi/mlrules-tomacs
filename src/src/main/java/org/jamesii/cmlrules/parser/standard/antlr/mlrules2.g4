grammar mlrules2;

// how to deal with comments (// ... is allowed and /* .... */ is allowed) and whitespace to skip

COMMENT_SINGLE : '//' ~('\n'|'\r')* ('\r'? '\n')?	-> channel(HIDDEN);
COMMENT_MULTI  : '/*' .*? '*/'                      -> channel(HIDDEN);
WS      :   [ \t\r\n]+                      		-> skip;

// ##### BEGIN SYMBOLS ######s

// needed for rules and initial solution
AT      :   '@';
ATTIME	:	'@EXACT';
ATEACH : '@EACH';
INIT    : '>>INIT';

TYPE_SEPERATOR  :   '::';
ARROW           :   '->';
LET				:   'let';
IN				:   'in';
WHERE			:   'where';

// type keywords
TYPE_REAL       :   'num';
TYPE_BOOL       :   'bool';
TYPE_STRING     :   'string';
TYPE_SPECIES    :   'species';
TYPE_PATTERN    :   'pattern';
TYPE_TUPLE		:   'tuple';
TYPE_SOL		:   'sol';
TYPE_LINK		:	'link';

// keywords
IF      :   'if';
THEN    :   'then';
ELSE    :   'else';
FOR     :   'for';
WHILE   :   'while';
UNTIL   :   'until';
LENGTH  :   'length';
ADD		:	'add';
FREE	:	'free';

// seperators
ASSIGN  :   '=';
DOT     :   '.';
COMMA   :   ',';
COLON   :   ':';
SEMI    :   ';';
L_PAREN :   '(';
R_PAREN :   ')';
L_BRAKET:   '[';
R_BRAKET:   ']';
L_CURLY :   '{';
R_CURLY :   '}';  

// mathematical operators
PLUS    :   '+';
MINUS   :   '-';
MULT    :   '*';
DIV     :   '/';
ROOF    :   '^';

// comparator operators
EQUALS  :   '==';
N_EQUALS:   '!=';
LT      :   '<';
LT_EQ   :   '<=';
GT      :   '>';
GT_EQ   :   '>=';

// boolean operators
TRUE    :   'true';
FALSE   :   'false';
AND     :   '&&';
OR      :   '||';
NOT     :   '!';

// misc
LAMBDA  :   '\\';

// data types
INT     :   DIGIT+;
REAL  
    :   DIGIT+ DOT DIGIT* EXPONENT? // e.g. 1.4 (e+10)
    |   DIGIT+ EXPONENT             // e.g. 12 e+10
    ;        

fragment    
EXPONENT
    :   ('e'|'E') ('+'|'-')? DIGIT+
    ;
    
BOOL
    :   (TRUE|FALSE)
    ;

ID_SPECIES: CAPITAL (CHAR | DIGIT)* | ROOT;

ID: SMALL (CHAR | DIGIT)* QUESTION?;

fragment
CAPITAL: [A-Z];

fragment
SMALL: [a-z];

fragment
CHAR
    :   [a-z|A-Z|_]
    ;

STRING  :   '\'' (ESCAPE | ~('\''|'\\' ))* '\'';

fragment
ESCAPE
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\''|'\"'|'\\')
    |   UNICODE_ESCAPE
    |   OCTAL_ESCAPE
    ;

fragment
UNICODE_ESCAPE
    :   '\\u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
    ;

fragment
OCTAL_ESCAPE
    :   '\\' [0-3] [0-7] [0-7]
    |   '\\' [0-7] [0-7]?
    ;

fragment
DIGIT   :   [0-9];

fragment
HEX_DIGIT : [0-9|'a'-'f'|'A'-'F'];


// other
NU      :   '$' ID;
COUNT   :   '#';
QUESTION:   '?';
ROOT	:	'$$ROOT$$';


// ###### BEGIN RULES ######s


model : preamble* initialSolution mlrule+ EOF;

preamble 
	: speciesDefinition SEMI
	| constant SEMI
	| function
	;
        
constant : ID COLON expression;

speciesDefinition
    :   ID_SPECIES L_PAREN R_PAREN
    |   ID_SPECIES L_PAREN INT R_PAREN           				
    ;    
    
initialSolution : INIT L_BRAKET expression R_BRAKET SEMI;

mlrule : reactants ARROW products (AT | ATTIME | ATEACH) rate where? SEMI;

reactants : species (PLUS species)*;

products : expression?;

rate : expression;

function: typeDefinition functionDefinition+;

typeDefinition
    :   ID TYPE_SEPERATOR type (ARROW type)* SEMI
    ;

type
    :   TYPE_REAL                               # baseTypeFloat
    |   TYPE_BOOL                               # baseTypeBool
    |   TYPE_STRING                             # baseTypeString
    |   TYPE_SPECIES							# speciesType
    |	TYPE_SOL								# solutionType
    |   TYPE_TUPLE								# tupleType
    |	TYPE_LINK								# baseTypeLink
    |	L_PAREN type (ARROW type)+ R_PAREN		# functionType                    
    ;
    
functionDefinition
    :   ID parameter* ASSIGN expression where? SEMI
    ; 
    
parameter
	: atomicParameter													# paramSingle
	| LT atomicParameter (COMMA atomicParameter)* GT					# paramTuple
	| ID_SPECIES L_PAREN (value (COMMA value)*)? R_PAREN 				# paramSpecies
	;

atomicParameter
	: value										# paramSimple
	| ID PLUS ID								# paramSol
	| L_BRAKET R_BRAKET							# emptySol
    ;
    
expression
    :	L_PAREN expression R_PAREN attributes 				# Application
    |   MINUS expression                                    # MinusOne
    |   NOT expression                                      # Not
    |   expression ROOF L_PAREN expression R_PAREN          # Roof
    |   expression (MULT | DIV) expression                  # MultDiv
    |   expression PLUS expression							# Plus
    |   expression MINUS expression                			# Minus
    |   IF expression THEN expression ELSE expression       # IfThenElse
    |   expression (AND | OR) expression                    # AndOr
    |   L_PAREN expression (LT | GT | LT_EQ | GT_EQ | EQUALS | N_EQUALS) expression R_PAREN            # BoolExpr
    |   LT (expression (COMMA expression)*)? GT				# tuple
    |   ID L_PAREN (expression (COMMA expression)*)? R_PAREN# FunctionCall
    |   L_PAREN expression R_PAREN                          # Paren
    //|   LAMBDA L_PAREN ID (COMMA ID)* R_PAREN L_PAREN expression R_PAREN	# LambdaFunction
    |	species 											# SpeciesExpr
    |   L_BRAKET R_BRAKET									# EmptySolution
    |   value                                               # ExpValue
    |	COUNT ID											# countShort
    ;

species : amount? name attributes? subSpecies? guard? (COLON ID)?;

name : ID_SPECIES | L_PAREN expression R_PAREN;

attributes: L_PAREN (expression (COMMA expression)*)? R_PAREN;

subSpecies: L_BRAKET expression? R_BRAKET;

guard: L_CURLY expression? R_CURLY;

amount
	: INT							#amountINT
	| ID							#amountID
	| L_PAREN expression R_PAREN	#amountExpr
	;
    
value
    :   INT         # Int
    |   REAL        # Real
    |   TRUE        # True
    |   FALSE       # False
    |   STRING      # String
    |	FREE		# Free
    |   ID          # Id
    ;
    
where : WHERE assign (COMMA assign)*; 
    
assign : assignName ASSIGN expression;

assignName
	: ID					#singleAssign						
	| LT ID (COMMA ID)* GT	#tupleAssign
	;