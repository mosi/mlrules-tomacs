/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.parser.standard.nodes;

import java.util.List;
import java.util.stream.Collectors;

import org.jamesii.cmlrules.parser.standard.types.Primitives;
import org.jamesii.cmlrules.parser.standard.types.Tuple;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.lists.ListNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;

/**
 * The tuple node represents nodes with a list of various sub types. Since
 * {@link ListNode} already realizes this, the tuple node inherits this class.
 * 
 * @author Tobias Helms
 *
 */
public class TupleNode extends ListNode {

  public TupleNode(List<INode> nodes) {
    super(nodes);
  }

  private static final long serialVersionUID = 5281627129977417776L;

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    INode result = super.calc(cEnv);
    if (result instanceof ListNode) {
      return (N) new ValueNode<Tuple>(new Tuple(((ListNode) result)
          .getChildren().stream().map(c -> ((ValueNode<?>) c).getValue())
          .collect(Collectors.toList())));
    }
    throw new IllegalArgumentException(String.format(
        "Tuple node %s has not returned a list node internally.",
        this.toString()));
  }

  @Override
  public String toString() {
    return Primitives.TUPLE.toString() + getValue().toString();
  }

}
