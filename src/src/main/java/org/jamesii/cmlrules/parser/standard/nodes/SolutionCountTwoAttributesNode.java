package org.jamesii.cmlrules.parser.standard.nodes;

import java.util.Map;

import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;

public class SolutionCountTwoAttributesNode extends Node {

  private static final long serialVersionUID = 1L;

  public static final String SOL = "§sol";

  public static final String NAME = "§name";
  
  public static final String ATT_1 = "§att1";
  
  public static final String ATT_2 = "§att2";

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    if (cEnv instanceof MLEnvironment) {
      MLEnvironment env = (MLEnvironment) cEnv;
      Object s = env.getValue(SOL);
      Object n = env.getValue(NAME);
      Object att1 = env.getValue(ATT_1);
      Object att2 = env.getValue(ATT_2);
      if (s instanceof Map<?, ?> && n instanceof String) {
        Map<?, ?> list = (Map<?, ?>) s;
        String name = (String) n;

        double num = 0D;
        for (Object species : list.keySet()) {
          Species tmp = (Species) species;
          if (tmp.getType().getName().equals(name) && tmp.getAttribute(0).equals(att1) && tmp.getAttribute(1).equals(att2)) {
            num += ((Species) species).getAmount();
          }
        }
        return (N) new ValueNode<Double>(num);
      }
    }
    throw new IllegalArgumentException(
        "Could not compute the number of species");
  }

  @Override
  public String toString() {
    return "countTwoAtts()";
  }

}
