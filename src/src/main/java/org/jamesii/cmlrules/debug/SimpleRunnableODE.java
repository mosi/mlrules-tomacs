/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.debug;


public class SimpleRunnableODE {

  public void start() {

//    Pair<Compartment, Set<ReactionOLD>> init = Wnt1.createModel();
//    CMLRulesModel model =
//        new CMLRulesModel(init.getSecondValue(), init.getFirstValue());
//
//    Listener l = new AllListener();
//    Set<Listener> listener = new HashSet<>();
//    listener.add(l);
//
//    AllObserver observer = new AllObserver(model, 1.0, listener);
//    Set<Observer> observers = new HashSet<>();
//    observers.add(observer);
//
//    ODESimulator simulator =
//        new ODESimulator(model, observers, 1.0e-8,
//            new DormandPrince853Integrator(1.0e-8, 100.0, 1.0e-10, 1.0e-10));
//
//    while (simulator.getCurrentTime() < 300.0) {
//      simulator.nextStep();
//    }

  }

  public static void main(String[] args) {
//    Logger.getGlobal().setLevel(Level.SEVERE);
//    long time = System.currentTimeMillis();
//    for (int i = 0; i < 1; ++i) {
//      new SimpleRunnableODE().start();
//      System.out.println(i);
//    }
//    System.out.println(System.currentTimeMillis() - time);
  }

}
