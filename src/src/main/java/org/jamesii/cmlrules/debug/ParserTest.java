/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.debug;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.io.FileUtils;
import org.jamesii.cmlrules.model.Model;
import org.jamesii.cmlrules.model.links.LinkModel;
import org.jamesii.cmlrules.model.standard.StandardModel;
import org.jamesii.cmlrules.model.standard.species.SpeciesModel;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.observation.Listener;
import org.jamesii.cmlrules.observation.Observer;
import org.jamesii.cmlrules.observation.visualization.VisListener;
import org.jamesii.cmlrules.observation.visualization.VisObserver;
import org.jamesii.cmlrules.parser.links.listener.typecheck.LinkRulesListener;
import org.jamesii.cmlrules.parser.links.listener.typecheck.LinkSemanticsListener;
import org.jamesii.cmlrules.parser.links.listener.typecheck.LinkWhereVariablesListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesLexer;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ModelContext;
import org.jamesii.cmlrules.parser.standard.functions.PredefinedFunctions;
import org.jamesii.cmlrules.parser.standard.listener.ConstantCrawlerListener;
import org.jamesii.cmlrules.parser.standard.listener.FunctionCrawlerListener;
import org.jamesii.cmlrules.parser.standard.listener.FunctionDefinitionListener;
import org.jamesii.cmlrules.parser.standard.listener.RulesListener;
import org.jamesii.cmlrules.parser.standard.listener.SpeciesTypeCrawlerListener;
import org.jamesii.cmlrules.simulator.links.LinkSimulator;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;

public class ParserTest {

  private ParserTest() {
  }

  public static void main(String[] args) throws URISyntaxException, IOException {

    String model = FileUtils.readFileToString(new File("./caro.mlrj"));

    Logger.getGlobal().info(model);

    // Get our lexer
    MLRulesLexer lexer = new MLRulesLexer(new ANTLRInputStream(model));

    // Get a list of matched tokens
    CommonTokenStream tokens = new CommonTokenStream(lexer);

    // Pass the tokens to the parser
    MLRulesParser parser = new MLRulesParser(tokens);

    // Specify our entry point
    ModelContext context = parser.model();

    // Walk it and attach our listener
    ParseTreeWalker walker = new ParseTreeWalker();

    MLEnvironment env = new MLEnvironment();
    env.setGlobalValue(StandardModel.RNG, new MersenneTwister(1));
    PredefinedFunctions.addAll(env);
    env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);

    MLRulesListener functionCrawler = new FunctionCrawlerListener(env);
    walker.walk(functionCrawler, context);

    MLRulesListener speciesCrawler = new SpeciesTypeCrawlerListener(env);
    walker.walk(speciesCrawler, context);

    MLRulesListener functionDefCrawler = new FunctionDefinitionListener(env);
    walker.walk(functionDefCrawler, context);

    MLRulesListener constantsCrawler = new ConstantCrawlerListener(env,
        Collections.emptyMap());
    walker.walk(constantsCrawler, context);

    // SimpleRulesListener rulesListener2 = new SimpleRulesListener(env);
    // walker.walk(rulesListener2, context);
    Map<SpeciesType, Set<Integer>> linkAttributes = new HashMap<>();
    Map<SpeciesType, Set<Integer>> noLinkAttributes = new HashMap<>();

    MLEnvironment copy = env.completeCopy();
    LinkWhereVariablesListener variablesChecker = new LinkWhereVariablesListener(
        linkAttributes, noLinkAttributes, copy);
    walker.walk(variablesChecker, context);

    LinkSemanticsListener semanticsChecker = new LinkSemanticsListener(
        linkAttributes, noLinkAttributes, copy,
        variablesChecker.getRuleVariables(),
        variablesChecker.getFunctionVariables());
    walker.walk(semanticsChecker, context);

    RulesListener rulesListener = new LinkRulesListener(env, linkAttributes);
    walker.walk(rulesListener, context);

    Model mlrulesModel = rulesListener.create();

    Logger.getGlobal().info(mlrulesModel.toString());

    // Model model2 = rulesListener2.create();

    // System.in.read();

    double time = 1000.0;
    Listener l = new VisListener(null);
    Set<Listener> listener = new HashSet<>();
    listener.add(l);
    VisObserver observer;// = new VisObserver((SpeciesModel) model2, time, 1.0,
                         // true, true, false, listener);
    Set<Observer> observers = new HashSet<>();
    // observers.add(observer);

    // StandardSimulator simulator = new StandardSimulator((StandardModel)
    // mlrulesModel, observers);
    // SimpleSimulator simulator = new SimpleSimulator((SimpleModel) model2,
    // observers);
    // ODESimulator simulator = new ODESimulator((SimpleModel) model2,
    // observers, 1.0e-8, new DormandPrince853Integrator(1.0e-08, 10.0, 1.0e-10,
    // 1.0e-10));
    Long start = System.currentTimeMillis();
    // while (observers.stream()
    // .flatMap(o -> o.getListener().stream())
    // .anyMatch(l2 -> l2.isActive())
    // && simulator.getCurrentTime() < time) {
    // simulator.nextStep();
    // }
    // Logger.getGlobal().log(Level.INFO,
    // String.format("Finished simulation in %s ms", System.currentTimeMillis()
    // - start));

    observer = new VisObserver((SpeciesModel) mlrulesModel, time, 10.0, true,
        true, true);
    observer.register(new VisListener(null));
    observers = new HashSet<>();
    observers.add(observer);

    LinkSimulator simulator2 = new LinkSimulator((LinkModel) mlrulesModel,
        observers,true);
    // SimpleSimulator simulator = new SimpleSimulator((SimpleModel) model2,
    // observers);
    // ODESimulator simulator = new ODESimulator(mlrulesModel, observers,
    // 1.0e-8, new DormandPrince853Integrator(1.0e-8, 100.0, 1.0e-10, 1.0e-10));
    start = System.currentTimeMillis();
    while (observers.stream().flatMap(o -> o.getListener().stream())
        .anyMatch(l2 -> l2.isActive())
        && simulator2.getCurrentTime() < time) {
      simulator2.nextStep();
    }
    Logger.getGlobal().log(
        Level.INFO,
        String.format("Finished simulation in %s ms",
            System.currentTimeMillis() - start));

    //
  }

}
