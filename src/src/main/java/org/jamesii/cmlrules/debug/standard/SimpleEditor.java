/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.debug.standard;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultStyledDocument;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.math3.ode.nonstiff.DormandPrince853Integrator;
import org.apache.commons.math3.ode.nonstiff.EulerIntegrator;
import org.jamesii.cmlrules.model.Model;
import org.jamesii.cmlrules.model.simple.SimpleModel;
import org.jamesii.cmlrules.model.standard.StandardModel;
import org.jamesii.cmlrules.model.standard.species.SpeciesModel;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.observation.IntervalObserver;
import org.jamesii.cmlrules.observation.Observer;
import org.jamesii.cmlrules.observation.save.SaveAllListener;
import org.jamesii.cmlrules.observation.visualization.VisListener;
import org.jamesii.cmlrules.observation.visualization.VisObserver;
import org.jamesii.cmlrules.parser.simple.listener.SimpleRulesListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesLexer;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ModelContext;
import org.jamesii.cmlrules.parser.standard.functions.PredefinedFunctions;
import org.jamesii.cmlrules.parser.standard.listener.ConstantCrawlerListener;
import org.jamesii.cmlrules.parser.standard.listener.FunctionCrawlerListener;
import org.jamesii.cmlrules.parser.standard.listener.FunctionDefinitionListener;
import org.jamesii.cmlrules.parser.standard.listener.RulesListener;
import org.jamesii.cmlrules.parser.standard.listener.SemanticsException;
import org.jamesii.cmlrules.parser.standard.listener.SpeciesTypeCrawlerListener;
import org.jamesii.cmlrules.parser.standard.listener.editor.Styles;
import org.jamesii.cmlrules.parser.standard.listener.editor.SyntaxListener;
import org.jamesii.cmlrules.parser.standard.listener.editor.VerboseErrorListener;
import org.jamesii.cmlrules.parser.standard.listener.typecheck.SemanticsListener;
import org.jamesii.cmlrules.parser.standard.listener.typecheck.WhereVariablesListener;
import org.jamesii.cmlrules.simulator.Simulator;
import org.jamesii.cmlrules.simulator.simple.ODESimulator;
import org.jamesii.cmlrules.simulator.simple.SimpleSimulatorSSA;
import org.jamesii.cmlrules.simulator.standard.StandardSimulator;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;

public class SimpleEditor {

  private final JFrame main;

  private final JSplitPane top;

  private final JTextPane logging;

  private final JTextPane pane;

  private final JScrollPane scroll;

  private final JTextPane linenumbers;

  private final AtomicLong update;

  private final MyListener textListener = new MyListener();

  private final LineNumberListener lineListener = new LineNumberListener();

  private final JTextField endtime;

  private final JTextField interval;

  private final JButton simulate;

  private final JButton save;

  private final JButton load;

  private final JComboBox<String> listScroller;

  private String savePath = null;
  private boolean saveDirectly = false;

  private String loadPath = null;

  private boolean hierarchy;

  private boolean attributes;

  private boolean showReals;

  private boolean theme = false;

  private boolean visualization = true;

  private double endtimeValue;

  private double intervalValue;

  private String tmpModelFile;

  private String initialText;

  private int initialHeight;

  private int initialWidth;

  private int initialDividerPosition;

  private int initialTextSize;

  private class ButtonListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent arg0) {
      Runnable doAssist = new Runnable() {
        @Override
        public void run() {
          new Thread() {
            @Override
            public void run() {
              double time = 0;
              double intervalV = 0;

              try {
                time = Double.valueOf(endtime.getText());
                intervalV = Double.valueOf(interval.getText());
                if (Double.compare(time, 0.0) <= 0) {
                  throw new Exception("Time must be positive.");
                }
                if (intervalV <= 0) {
                  throw new Exception("Interval must be positive");
                }

                String model = pane.getText();
                MLRulesLexer lexer = new MLRulesLexer(new ANTLRInputStream(
                    model));
                CommonTokenStream tokens = new CommonTokenStream(lexer);
                MLRulesParser parser = new MLRulesParser(tokens);
                ModelContext context = parser.model();
                ParseTreeWalker walker = new ParseTreeWalker();

                MLEnvironment env = new MLEnvironment();
                env.setGlobalValue(StandardModel.RNG, new MersenneTwister(
                    System.currentTimeMillis()));
                PredefinedFunctions.addAll(env);
                env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);

                MLRulesListener functionCrawler = new FunctionCrawlerListener(
                    env);
                walker.walk(functionCrawler, context);
                MLRulesListener speciesCrawler = new SpeciesTypeCrawlerListener(
                    env);
                walker.walk(speciesCrawler, context);
                MLRulesListener functionDefCrawler = new FunctionDefinitionListener(
                    env);
                walker.walk(functionDefCrawler, context);
                MLRulesListener constantsCrawler = new ConstantCrawlerListener(
                    env, Collections.emptyMap());
                walker.walk(constantsCrawler, context);

                Model mlrulesModel = null;
                switch (listScroller.getSelectedIndex()) {
                case 0:
                  RulesListener rulesListener = new RulesListener(env);
                  walker.walk(rulesListener, context);
                  mlrulesModel = rulesListener.create();
                  break;
                case 1:
                case 2:
                  SimpleRulesListener simpleRulesListener = new SimpleRulesListener(
                      env);
                  walker.walk(simpleRulesListener, context);
                  mlrulesModel = simpleRulesListener.create();
                  break;
                default:
                  // do nothing
                }

                if (mlrulesModel == null) {
                  throw new IllegalArgumentException(
                      "could not create the model object");
                }

                Logger.getGlobal().info(mlrulesModel.toString());

                Set<Observer> observers = new HashSet<>();
                if (visualization) {
                  VisObserver observer = new VisObserver(
                      (SpeciesModel) mlrulesModel, time, intervalV, attributes,
                      hierarchy, !showReals);
                  observer.register(new VisListener(new File(loadPath)));
                  observers.add(observer);
                }

                if (saveDirectly) {
                  IntervalObserver observer = new IntervalObserver(
                      (SpeciesModel) mlrulesModel, intervalV);
                  observer.register(new SaveAllListener(new File(savePath),
                      new SimpleDateFormat("yyyy-MM-dd---HH-mm-ss")
                          .format(Calendar.getInstance().getTime())));
                  observers.add(observer);
                }

                Simulator simulator = null;

                switch (listScroller.getSelectedIndex()) {
                case 0:
                  simulator = new StandardSimulator(
                      (StandardModel) mlrulesModel, observers,true);
                  break;
                case 1:
                  simulator = new SimpleSimulatorSSA((SimpleModel) mlrulesModel,
                      observers,true);
                  break;
                case 2:
                  simulator = new ODESimulator((SimpleModel) mlrulesModel,
                      observers, 1.0e-8, new DormandPrince853Integrator(
                          1.0e-07, 10.0, 1.0e-10, 1.0e-12));
//                    simulator = new ODESimulator((SimpleModel) mlrulesModel, observers, 1.0e-8, new EulerIntegrator(0.0001));
                  break;
                default:
                  // do nothing
                }

                if (simulator == null) {
                  throw new IllegalArgumentException(
                      "no simulator could be created");
                }

                Long start = System.currentTimeMillis();
                while (observers.stream()
                    .flatMap(o -> o.getListener().stream())
                    .anyMatch(l -> l.isActive())
                    && simulator.getCurrentTime() < time) {
                  simulator.nextStep();
                }
                Logger.getGlobal().log(
                    Level.INFO,
                    String.format("Finished simulation in %s ms",
                        System.currentTimeMillis() - start));
                observers.stream().flatMap(o -> o.getListener().stream())
                    .forEach(l -> l.finish());
              } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, e.getMessage());
              }
            }
          }.start();
        }
      };
      SwingUtilities.invokeLater(doAssist);
    }

  }

  private class MyListener implements DocumentListener {
    public void changedUpdate(DocumentEvent e) {
    }

    @Override
    public void insertUpdate(DocumentEvent arg0) {
      update.set(System.currentTimeMillis());
    }

    @Override
    public void removeUpdate(DocumentEvent arg0) {
      update.set(System.currentTimeMillis());
    }
  }

  private class LineNumberListener implements DocumentListener {

    private boolean doUpdate = false;

    private int countLines() {
      return pane.getText().split("\n", -1).length;
    }

    private void update() {
      doUpdate = true;
      Runnable doAssist = new Runnable() {
        @Override
        public void run() {
          linenumbers.setCaret(null);
          linenumbers.setText(IntStream.iterate(1, x -> x + 1)
              .limit(countLines()).boxed().map(i -> String.valueOf(i))
              .collect(Collectors.joining("\r\n")));
          doUpdate = false;
        }
      };
      SwingUtilities.invokeLater(doAssist);
    }

    @Override
    public void changedUpdate(DocumentEvent arg0) {
      if (!doUpdate) {
        update();
      }
    }

    @Override
    public void insertUpdate(DocumentEvent arg0) {
      if (!doUpdate) {
        update();
      }
    }

    @Override
    public void removeUpdate(DocumentEvent arg0) {
      if (!doUpdate) {
        update();
      }
    }
  }

  private void loadProperties() {
    try {
      File file = new File("./editor.properties");
      if (!file.exists()) {
        file.createNewFile();
      }
      Properties prop = new Properties();
      InputStream inputStream = new FileInputStream("./editor.properties");
      prop.load(inputStream);

      savePath = prop.getProperty("save");
      loadPath = prop.getProperty("load");
      attributes = Boolean.valueOf(prop.getProperty("attributes"));
      hierarchy = Boolean.valueOf(prop.getProperty("hierarchy"));
      showReals = Boolean.valueOf(prop.getProperty("reals"));
      visualization = prop.containsKey("visualization") ? Boolean.valueOf(prop
          .getProperty("visualization")) : true;
      saveDirectly = Boolean.valueOf(prop.getProperty("saveDirectly"));
      endtimeValue = prop.containsKey("endTime") ? Double.valueOf(prop
          .getProperty("endTime")) : 100.0;
      intervalValue = prop.containsKey("interval") ? Double.valueOf(prop
          .getProperty("interval")) : 1.0;
      tmpModelFile = prop.getProperty("modelfile");
      theme = prop.containsKey("theme") ? Boolean.valueOf(prop
          .getProperty("theme")) : true;

      initialDividerPosition = prop.containsKey("logging") ? Integer
          .valueOf(prop.getProperty("logging")) : 500;
      initialHeight = prop.containsKey("height") ? Integer.valueOf(prop
          .getProperty("height")) : 700;
      initialWidth = prop.containsKey("width") ? Integer.valueOf(prop
          .getProperty("width")) : 1000;
      initialTextSize = prop.containsKey("textsize") ? Integer.valueOf(prop
          .getProperty("textsize")) : 12;

      if (tmpModelFile == null) {
        tmpModelFile = System.currentTimeMillis() + ".mlrj";
        this.initialText = "//constants\na1:0.1;\na2:0.05;\na3:0.01;\n\n//species\nA();B();C();\n\n//initial solution\n>>INIT[100 A + 0 B + 0 C];\n\n//reaction rules\nA:a -> B @#a*a1;\nB:b -> A @#b*a2;\nB:b -> C@#b*a3;";
      } else {
        FileReader fr = new FileReader(tmpModelFile);
        BufferedReader br = new BufferedReader(fr);
        StringBuilder builder = new StringBuilder();
        br.lines().forEach(l -> builder.append(l + "\n"));
        br.close();
        fr.close();
        this.initialText = builder.toString();
      }

      if (savePath == null) {
        savePath = "./";
      }
      if (loadPath == null) {
        loadPath = "./";
      }

    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  private void saveProperties() {
    try {
      Properties prop = new Properties();
      prop.setProperty("load", loadPath);
      prop.setProperty("save", savePath);
      prop.setProperty("attributes", Boolean.toString(attributes));
      prop.setProperty("hierarchy", Boolean.toString(hierarchy));
      prop.setProperty("reals", Boolean.toString(showReals));
      prop.setProperty("visualization", Boolean.toString(visualization));
      prop.setProperty("saveDirectly", Boolean.toString(saveDirectly));
      prop.setProperty("endTime", endtime.getText());
      prop.setProperty("interval", interval.getText());
      prop.setProperty("modelfile", tmpModelFile);
      prop.setProperty("width",
          Integer.toString((int) main.getSize().getWidth()));
      prop.setProperty("height",
          Integer.toString((int) main.getSize().getHeight()));
      prop.setProperty("logging", Integer.toString(top.getDividerLocation()));
      prop.setProperty("theme", Boolean.toString(!theme));
      prop.setProperty("textsize",
          Integer.toString(linenumbers.getFont().getSize()));

      FileWriter fw = new FileWriter(tmpModelFile);
      BufferedWriter bw = new BufferedWriter(fw);
      bw.write(pane.getText());
      bw.flush();
      bw.close();
      fw.close();

      File f = new File("./editor.properties");
      OutputStream out = new FileOutputStream(f);
      prop.store(out, "Current configuration of ML-Rules editor.");
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public SimpleEditor() {

    loadProperties();

    this.update = new AtomicLong(System.currentTimeMillis());

    this.main = new JFrame("ML-Rules 2 Sandbox - Status: Alpha - For Demonstration Purposes Only - University of Rostock");
    main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    main.setSize(initialWidth, initialHeight);
    main.setMinimumSize(new Dimension(1100, 700));
    JPanel mainPanel = new JPanel(new BorderLayout(), true);
    main.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        saveProperties();
      }
    });

    this.logging = new JTextPane(new DefaultStyledDocument()) {
      private static final long serialVersionUID = 1L;

      @Override
      public boolean getScrollableTracksViewportWidth() {
        return getUI().getPreferredSize(this).width <= getParent().getSize().width;
      }
    };
    logging.setEditable(false);
    logging.setFont(new Font("Courier New", Font.PLAIN, initialTextSize));
    JScrollPane scroll2 = new JScrollPane(logging);
    scroll2.setBorder(new EmptyBorder(new Insets(0, 0, 0, 0)));
    scroll2.setViewportBorder(new EmptyBorder(new Insets(0, 0, 0, 0)));
    scroll2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

    this.pane = new JTextPane(new DefaultStyledDocument()) {
      private static final long serialVersionUID = 1L;

      @Override
      public boolean getScrollableTracksViewportWidth() {
        return getUI().getPreferredSize(this).width <= getParent().getSize().width;
      }
    };

    Styles.addStyles(this.pane.getStyledDocument());

    JPanel editor = new JPanel(new BorderLayout());
    linenumbers = new JTextPane(new DefaultStyledDocument());
    editor.add(linenumbers, BorderLayout.WEST);
    editor.add(pane, BorderLayout.CENTER);
    linenumbers.setEditable(false);
    linenumbers.setCaret(null);
    linenumbers.setFont(new Font("Courier New", Font.PLAIN, initialTextSize));
    this.pane.getDocument().addDocumentListener(lineListener);
    this.pane.setText(initialText);
    this.pane.getDocument().addDocumentListener(textListener);
    this.pane.setFont(new Font("Courier New", Font.PLAIN, initialTextSize));
    scroll = new JScrollPane(editor);
    scroll.setBorder(new EmptyBorder(new Insets(0, 0, 0, 0)));
    scroll.setViewportBorder(new EmptyBorder(new Insets(0, 0, 0, 0)));
    scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    scroll.getVerticalScrollBar().setUnitIncrement(20);

    scroll.setBorder(BorderFactory.createTitledBorder("Editor"));
    scroll2.setBorder(BorderFactory.createTitledBorder("Logger"));
    this.top = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scroll, scroll2);
    top.setDividerLocation(initialDividerPosition);
    top.setResizeWeight(1);
    mainPanel.add(top, BorderLayout.CENTER);

    JPanel bottom = new JPanel(true);

    JPopupMenu popup = new JPopupMenu();
    JMenuItem switchTheme = new JMenuItem("switch color theme");
    popup.add(switchTheme);
    switchTheme.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        if (!theme) {
          pane.setBackground(Color.DARK_GRAY);
          linenumbers.setBackground(Color.DARK_GRAY);
          logging.setBackground(Color.DARK_GRAY);
          pane.setForeground(Color.LIGHT_GRAY);
          linenumbers.setForeground(Color.LIGHT_GRAY);
          logging.setForeground(Color.LIGHT_GRAY);
          pane.setCaretColor(Color.LIGHT_GRAY);
          logging.setCaretColor(Color.LIGHT_GRAY);
          theme = true;
        } else {
          pane.setBackground(Color.WHITE);
          linenumbers.setBackground(Color.WHITE);
          logging.setBackground(Color.WHITE);
          pane.setForeground(Color.BLACK);
          linenumbers.setForeground(Color.BLACK);
          logging.setForeground(Color.BLACK);
          pane.setCaretColor(Color.BLACK);
          logging.setCaretColor(Color.BLACK);
          theme = false;
        }
      }

    });
    switchTheme.doClick();
    JMenuItem changeTextSize = new JMenuItem("change text size");
    popup.add(changeTextSize);
    changeTextSize.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        Object[] possibilities = { "4", "8", "12", "16", "20", "24", "28" };
        String s = (String) JOptionPane.showInputDialog(main, String.format(
            "Current text size: %s \nSelect new text size:", pane.getFont()
                .getSize()), "Select Text Size", JOptionPane.PLAIN_MESSAGE,
            null, possibilities, String.valueOf(pane.getFont().getSize()));
        if (s != null) {
          int size = Integer.valueOf(s);
          linenumbers.setFont(new Font("Courier New", Font.PLAIN, size));
          pane.setFont(new Font("Courier New", Font.PLAIN, size));
          logging.setFont(new Font("Courier New", Font.PLAIN, size));
        }
      }
    });

    MouseListener popupListener = new MouseAdapter() {
      @Override
      public void mousePressed(MouseEvent e) {
        maybeShowPopup(e);
      }

      @Override
      public void mouseReleased(MouseEvent e) {
        maybeShowPopup(e);
      }

      private void maybeShowPopup(MouseEvent e) {
        if (e.isPopupTrigger()) {
          popup.show(e.getComponent(), e.getX(), e.getY());
        }
      }

    };
    pane.addMouseListener(popupListener);
    linenumbers.addMouseListener(popupListener);
    logging.addMouseListener(popupListener);

    load = new JButton("Load");
    bottom.add(load);
    load.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        Runnable doAssist = new Runnable() {
          @Override
          public void run() {
            JFileChooser f = new JFileChooser();
            f.setCurrentDirectory(new File(loadPath));
            int returnVal = f.showOpenDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
              try {
                File file = f.getSelectedFile();
                loadPath = file.getParent();
                FileReader reader = new FileReader(file);
                BufferedReader buffer = new BufferedReader(reader);
                StringBuilder builder = new StringBuilder();
                String line;
                while ((line = buffer.readLine()) != null) {
                  builder.append(line);
                  builder.append("\n");
                }
                pane.setText(builder.toString());
                buffer.close();
                reader.close();
              } catch (IOException e) {
                // do nothing
              }
            }
          }
        };
        SwingUtilities.invokeLater(doAssist);
      }

    });
    save = new JButton("Save");
    save.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        Runnable doAssist = new Runnable() {
          @Override
          public void run() {
            JFileChooser f = new JFileChooser();
            f.setCurrentDirectory(new File(loadPath));
            int returnVal = f.showSaveDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
              try {
                File file = f.getSelectedFile();
                loadPath = file.getParent();
                FileWriter writer = new FileWriter(file);
                BufferedWriter buffer = new BufferedWriter(writer);
                buffer.write(pane.getText());
                buffer.flush();
                buffer.close();
                writer.close();
              } catch (IOException e) {
                // do nothing
              }
            }
          }
        };
        SwingUtilities.invokeLater(doAssist);
      }

    });
    bottom.add(save);

    endtime = new JTextField(String.valueOf(endtimeValue), 4);
    interval = new JTextField(String.valueOf(intervalValue), 4);
    bottom.add(new JLabel("End Time:"));
    bottom.add(endtime);
    bottom.add(new JLabel("Interval:"));
    bottom.add(interval);

    JCheckBox vis = new JCheckBox("Visualization");
    vis.setMnemonic(KeyEvent.VK_C);
    vis.setSelected(visualization);
    bottom.add(vis);
//    vis.setVisible(false);
    vis.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (ItemEvent.SELECTED == e.getStateChange()) {
          visualization = true;
        } else {
          visualization = false;
        }
      }
    });

    JCheckBox att = new JCheckBox("Attributes");
    att.setMnemonic(KeyEvent.VK_C);
    att.setSelected(attributes);
    bottom.add(att);
    att.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (ItemEvent.SELECTED == e.getStateChange()) {
          attributes = true;
        } else {
          attributes = false;
        }
      }
    });

    JCheckBox hie = new JCheckBox("Hierarchies");
    hie.setMnemonic(KeyEvent.VK_C);
    hie.setSelected(hierarchy);
    bottom.add(hie);
    hie.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (ItemEvent.SELECTED == e.getStateChange()) {
          hierarchy = true;
        } else {
          hierarchy = false;
        }
      }
    });

    JCheckBox real = new JCheckBox("Reals");
    real.setMnemonic(KeyEvent.VK_C);
    real.setSelected(showReals);
    bottom.add(real);
    real.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (ItemEvent.SELECTED == e.getStateChange()) {
          showReals = true;
        } else {
          showReals = false;
        }
      }
    });

    JCheckBox sd = new JCheckBox("Save Directly");
    sd.setMnemonic(KeyEvent.VK_C);
    sd.setSelected(saveDirectly);
    bottom.add(sd);
//    sd.setVisible(false);
    sd.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (ItemEvent.SELECTED == e.getStateChange()) {
          if (saveDirectly) {
            return;
          }
          JFileChooser f = new JFileChooser();
          f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
          f.setDialogTitle("Where to save the results?");
          f.setCurrentDirectory(new File(savePath));
          int returnVal = f.showOpenDialog(null);
          if (returnVal == JFileChooser.APPROVE_OPTION) {
            saveDirectly = true;
            sd.setSelected(true);
            File file = f.getSelectedFile();
            savePath = file.getPath();
          }
        } else if (saveDirectly) {
          saveDirectly = false;
        }
      }
    });

    listScroller = new JComboBox<String>(new String[] { "Standard SSA",
        "Static SSA", "Static ODE" });
    listScroller.setSelectedIndex(0);
    bottom.add(listScroller);
//    listScroller.setVisible(false);

    simulate = new JButton("Start Simulation");
    simulate.addActionListener(new ButtonListener());
    bottom.add(simulate);

    mainPanel.add(bottom, BorderLayout.SOUTH);

    main.add(mainPanel);
    main.setVisible(true);
  }

  public void update() {
    Runnable doAssist = new Runnable() {
      @Override
      public void run() {
        pane.getDocument().removeDocumentListener(textListener);
        pane.getDocument().removeDocumentListener(lineListener);
        MLRulesLexer lexer = new MLRulesLexer(new ANTLRInputStream(
            pane.getText()));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MLRulesParser parser = new MLRulesParser(tokens);
        parser.removeErrorListeners();
        VerboseErrorListener error = new VerboseErrorListener();
        parser.addErrorListener(error);
        ModelContext context = parser.model();
        ParseTreeWalker walker = new ParseTreeWalker();
        SyntaxListener crawler = new SyntaxListener(pane);
        walker.walk(crawler, context);
        tokens
            .getTokens()
            .stream()
            .filter(t -> t.getChannel() == Token.HIDDEN_CHANNEL)
            .forEach(
                t -> pane.getStyledDocument().setCharacterAttributes(
                    t.getStartIndex(),
                    t.getStopIndex() - t.getStartIndex() + 1,
                    pane.getStyle(Styles.COMMENT), true));

        if (!error.getSymbols().isEmpty()) {
          error.getSymbols().forEach(
              o -> {
                if (o instanceof Token) {
                  Token t = (Token) o;
                  pane.getStyledDocument().setCharacterAttributes(
                      t.getStartIndex(), t.getStopIndex(),
                      pane.getStyle(Styles.ERROR), true);
                }
              });
          StringBuilder builder = new StringBuilder();
          error.getErrors().forEach(e -> builder.append(e + "\n"));
          logging.setText(builder.toString());
          simulate.setEnabled(false);
        } else {
          MLEnvironment env = new MLEnvironment();
          env.setGlobalValue(StandardModel.RNG, new MersenneTwister(1));
          PredefinedFunctions.addAll(env);
          env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);
          try {
            MLRulesListener functionCrawler = new FunctionCrawlerListener(env);
            walker.walk(functionCrawler, context);

            MLRulesListener speciesCrawler = new SpeciesTypeCrawlerListener(env);
            walker.walk(speciesCrawler, context);

            MLRulesListener functionDefCrawler = new FunctionDefinitionListener(
                env);
            walker.walk(functionDefCrawler, context);

            MLRulesListener constantsCrawler = new ConstantCrawlerListener(env,
                Collections.emptyMap());
            walker.walk(constantsCrawler, context);

            try {
              SimpleRulesListener simpleRulesListener = new SimpleRulesListener(
                  env);
              walker.walk(simpleRulesListener, context);
              if (simpleRulesListener.create() != null) {
                if (listScroller.getItemCount() == 1) {
                  listScroller.addItem("Static SSA");
                  listScroller.addItem("Static ODE");
                }
              }
            } catch (Exception e) {
              Logger
                  .getGlobal()
                  .log(
                      Level.INFO,
                      "given model is not static, i.e., simple simulators cannot be used",
                      e);
              if (listScroller.getItemCount() != 1) {
                listScroller.setSelectedIndex(0);
                listScroller.removeItemAt(2);
                listScroller.removeItemAt(1);
              }
            }

            WhereVariablesListener variablesChecker = new WhereVariablesListener(
                env);
            walker.walk(variablesChecker, context);

            MLRulesListener semanticsChecker = new SemanticsListener(env,
                variablesChecker.getRuleVariables(),
                variablesChecker.getFunctionVariables());
            walker.walk(semanticsChecker, context);

            logging.setText("no errors");
            simulate.setEnabled(true);
          } catch (SemanticsException e) {
            if (e.getCtx() != null
                && pane.getText().length() > e.getCtx().getStop()
                    .getStopIndex()) {
              pane.getStyledDocument().setCharacterAttributes(
                  e.getCtx().getStart().getStartIndex(),
                  e.getCtx().getStop().getStopIndex()
                      - e.getCtx().getStart().getStartIndex() + 1,
                  pane.getStyle(Styles.ERROR), true);
            }
            logging.setText(e.getMsg());
            simulate.setEnabled(false);
          } catch (Exception e) {
            e.printStackTrace();
            logging.setText("no line available: " + e.getMessage());
            simulate.setEnabled(false);
          }
        }
        pane.getDocument().addDocumentListener(textListener);
        pane.getDocument().addDocumentListener(lineListener);
        update.set(Long.MAX_VALUE);
      }
    };
    SwingUtilities.invokeLater(doAssist);
  }

  public static void main(String[] args) throws IOException {
    // Logger.getGlobal().setLevel(Level.WARNING);
    SimpleEditor e = new SimpleEditor();
    Thread worker = new Thread(new UpdateRunnable(e));
    worker.start();
  }

  public AtomicLong getUpdate() {
    return update;
  }

}
