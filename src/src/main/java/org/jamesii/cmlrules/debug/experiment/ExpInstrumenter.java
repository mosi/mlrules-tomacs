/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.debug.experiment;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.jamesii.cmlrules.experiment.Job;
import org.jamesii.cmlrules.model.Model;
import org.jamesii.cmlrules.model.standard.species.SpeciesModel;
import org.jamesii.cmlrules.observation.Instrumenter;
import org.jamesii.cmlrules.observation.Observer;
import org.jamesii.cmlrules.observation.IntervalObserver;
import org.jamesii.cmlrules.observation.save.SaveAllListener;

/**
 * This instrumenter creates a {@link IntervalObserver} and attaches a
 * {@link SaveAllListener} to this observer.
 * 
 * @author Tobias Helms
 *
 */
public class ExpInstrumenter implements Instrumenter {

  private final File directory;

  private final double interval;

  public ExpInstrumenter(File directory, double interval) {
    this.directory = directory;
    this.interval = interval;
  }

  @Override
  public Set<Observer> create(Job job, Model model) {
    Set<Observer> set = new HashSet<>();
    Observer o = new IntervalObserver((SpeciesModel) model, interval);
    o.register(new SaveAllListener(directory, Integer.toString(job.getID())));
    set.add(o);
    return set;
  }

}
