/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.debug.experiment;

import java.io.File;
import java.util.HashMap;

import org.jamesii.cmlrules.experiment.Experiment;
import org.jamesii.cmlrules.experiment.Job;
import org.jamesii.cmlrules.experiment.stop.SimTimeStopConditionFactory;
import org.jamesii.cmlrules.experiment.stop.StopConditionFactory;

public class SampleExperiment {

  public static void main(String[] args) {

    String modelLocation = "./wnt.mlrj";
    int cores = 2;
    StopConditionFactory stop = new SimTimeStopConditionFactory(100);
    File directory = new File("./");

    Experiment exp = new Experiment(modelLocation, new ExpInstrumenter(directory,0.1), stop, cores);

    for (int i = 0; i < 10; ++i) {
      exp.addJob(new Job(i, new HashMap<>()));
    }

    exp.execute();

  }

}
