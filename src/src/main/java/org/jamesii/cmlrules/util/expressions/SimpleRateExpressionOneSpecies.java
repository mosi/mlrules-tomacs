package org.jamesii.cmlrules.util.expressions;

import org.jamesii.cmlrules.model.standard.species.Species;

public class SimpleRateExpressionOneSpecies implements SimpleRateExpressions {

  private final Species species;
  
  private final double constant;
  
  public SimpleRateExpressionOneSpecies(Species species, double constant) {
    this.species = species;
    this.constant = constant;
  }
  
  @Override
  public double calc() {
    return species.getAmount() * constant;
  }
  
}
