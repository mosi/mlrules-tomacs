/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.util;

import java.util.HashMap;
import java.util.Map;

import org.jamesii.core.math.parsetree.variables.IEnvironment;

/**
 * This environment maintains two maps of entries, one for global entries that
 * are fixed and shall be shared by various environments and a private
 * ('current') map containing temporary variables of this environment.
 * 
 * @author Tobias Helms
 *
 */
public class MLEnvironment implements IEnvironment<String> {

  private static final long serialVersionUID = 1L;

  private final Map<String, Object> global;

  private final Map<String, Object> current;

  private MLEnvironment(Map<String, Object> global) {
    this.global = global;
    current = new HashMap<>();
  }

  private MLEnvironment(Map<String, Object> global, Map<String, Object> current) {
    this.global = global;
    this.current = new HashMap<>(current);
  }

  public MLEnvironment() {
    global = new HashMap<>();
    current = new HashMap<>();
  }

  public MLEnvironment newLevel() {
    return new MLEnvironment(global);
  }

  public void setGlobalValue(String ident, Object value) {
    global.put(ident, value);
  }

  public void addDynamicValues(MLEnvironment env) {
    current.putAll(env.getDynamicValues());
  }

  public Map<String, Object> getDynamicValues() {
    return current;
  }

  public MLEnvironment copy() {
    return new MLEnvironment(global, current);
  }

  public MLEnvironment completeCopy() {
    return new MLEnvironment(new HashMap<>(global), current);
  }

  @Override
  public void setValue(String ident, Object value) {
    current.put(ident, value);
  }

  @Override
  public Object getValue(String ident) {
    Object result = current.getOrDefault(ident,
        global.getOrDefault(ident, null));
    if (result instanceof LazyInitialization) {
      ((LazyInitialization) result).computeValue(this);
      return current.getOrDefault(ident, null);
    }
    if (result instanceof RestSolution) {
      ((RestSolution) result).computeValue(this);
      return current.getOrDefault(ident, null);
    }
    return result;
  }

  @Override
  public boolean containsIdent(String ident) {
    return global.containsKey(ident) || current.containsKey(ident);
  }

  @Override
  public void removeValue(String ident) {
    current.remove(ident);
  }

  @Override
  public void removeAll() {
    throw new IllegalStateException(
        "Not supported operation with MLEnvironment.");
  }

  @Override
  public String toString() {
    return "Global: " + global.toString() + " Current: " + current.toString();
  }

}
