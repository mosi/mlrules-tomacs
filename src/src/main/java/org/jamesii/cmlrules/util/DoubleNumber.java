package org.jamesii.cmlrules.util;

public class DoubleNumber {
  
  private double number;

  public DoubleNumber(double number) {
    this.number = number;
  }

  public double addAndGet(double other) {
    number += other;
    return number;
  }

  public void set(double number) {
    this.number = number;
  }

  public double get() {
    return number;
  }
}
