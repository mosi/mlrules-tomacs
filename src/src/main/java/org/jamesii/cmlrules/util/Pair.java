/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.util;

import org.jamesii.core.util.misc.Comparators;

/**
 * A simple pair implementation.
 * 
 * @author Tobias Helms
 */
public class Pair<E1,E2> {

  private E1 fst = null;

  private E2 snd = null;

  public Pair(E1 e1, E2 e2) {
    this.fst = e1;
    this.snd = e2;
  }

  public Pair(Pair<? extends E1, ? extends E2> pair) {
    this.fst = pair.fst();
    this.snd = pair.snd();
  }

  public Pair() {
  }

  public E1 fst() {
    return fst;
  }

  public E2 snd() {
    return snd;
  }

  public void setFst(E1 firstValue) {
    this.fst = firstValue;
  }

  public void setSnd(E2 secondValue) {
    this.snd = secondValue;
  }

  @Override
  public String toString() {
    return "(" + fst + "," + snd + ")";
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Pair<?, ?>)) {
      return false;
    }
    Pair<?, ?> p = (Pair<?, ?>) obj;
    return Comparators.equal(fst, p.fst())
        && Comparators.equal(snd, p.snd());
  }

  @Override
  public int hashCode() {
    return (fst == null ? 0 : fst.hashCode())
        * (snd == null ? 0 : snd.hashCode());
  }
  
}
