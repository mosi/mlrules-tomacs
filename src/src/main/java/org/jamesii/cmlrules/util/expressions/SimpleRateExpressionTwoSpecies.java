package org.jamesii.cmlrules.util.expressions;

import org.jamesii.cmlrules.model.standard.species.Species;

public class SimpleRateExpressionTwoSpecies implements SimpleRateExpressions {

  private final Species species1;
  
  private final Species species2;
  
  private final double constant;
  
  public SimpleRateExpressionTwoSpecies(Species species1, Species species2, double constant) {
    this.species1 = species1;
    this.species2 = species2;
    this.constant = constant;
  }
  
  @Override
  public double calc() {
    return species1.getAmount() * species2.getAmount() * constant;
  }

}
