/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.util;

import java.util.HashMap;
import java.util.Map;

import org.jamesii.cmlrules.model.standard.species.Species;

/**
 * The rest solution is similar to the {@link LazyInitialization}, i.e., rest
 * solutions are not evaluated until they are used.
 * 
 * @author Tobias Helms
 *
 */
public class RestSolution {

  private final String name;

  private final Species context;

  private final Map<Species, Integer> removals;

  public RestSolution(String name, Species context,
      Map<Species, Integer> removals) {
    this.name = name;
    this.context = context;
    this.removals = removals;
  }

  /**
   * Compute the set of species of this rest solution and add the calculated
   * rest solution to the given environment.
   */
  public void computeValue(MLEnvironment env) {
    Map<Species, Species> rest = new HashMap<>();
    context.getSubSpeciesStream().forEach(s -> {
      Integer remove = removals.getOrDefault(s, 0);
      if (remove == 0) {
        rest.put(s, s);
      } else {
        if (s.isLeaf()) {
          Species copy = s.copy();
          copy.setAmount(copy.getAmount() - remove);
          if (Double.compare(copy.getAmount(), 0) > 0) {
            rest.put(copy, copy);
          }
        }
      }
    });
    env.setValue(name, rest);
  }

}
