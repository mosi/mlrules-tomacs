/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.util;

import org.jamesii.cmlrules.parser.standard.types.Tuple;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;

/**
 * Lazy initialization is used so that assignments are not evaluated until the
 * variables of the assignment are really needed, e.g., if (a == b) then x else
 * y where x = f(),y=h().
 * 
 * @author Tobias Helms
 *
 */
public class LazyInitialization {

  private final Assignment a;

  private final MLEnvironment env;

  public LazyInitialization(Assignment a, MLEnvironment env) {
    this.a = a;
    this.env = env;
  }

  /**
   * Compute the variable values of the assignment and add the variables to the
   * given environment.
   */
  public void computeValue(MLEnvironment newEnv) {
    Node node = a.getExpression().calc(env);
    if (node instanceof ValueNode<?>
        && ((ValueNode<?>) node).getValue() != null) {
      if (((ValueNode<?>) node).getValue() instanceof Tuple) {
        Tuple tuple = (Tuple) ((ValueNode<?>) node).getValue();
        for (int i = 0; i < tuple.size(); ++i) {
          newEnv.setValue(a.getNames().get(i), tuple.get(i));
        }
      } else {
        if (a.getNames().size() != 1) {
          throw new IllegalArgumentException(String.format(
              "Invalid number %s of names in assignment.", a.getNames().size()));
        }
        newEnv.setValue(a.getNames().get(0), ((ValueNode<?>) node).getValue());
      }
    }
  }

}
