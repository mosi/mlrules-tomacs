package org.jamesii.cmlrules.util.expressions;


public interface SimpleRateExpressions {

  public double calc();
  
}
