package org.jamesii.cmlrules.util;

import java.util.HashMap;
import java.util.Map;

import org.jamesii.core.math.parsetree.variables.IEnvironment;

/**
 * The {@link TmpEnvironment} is a simple version of the {@link MLEnvironment}
 * without a global fixed map of entries so that the retrieval of elements is
 * faster.
 * 
 * @author Tobias Helms
 *
 */
public class TmpEnvironment implements IEnvironment<String> {

  private static final long serialVersionUID = 1L;

  private final Map<String, Object> values = new HashMap<>(4);

  @Override
  public void setValue(String ident, Object value) {
    values.put(ident, value);
  }

  @Override
  public boolean containsIdent(String ident) {
    return values.containsKey(ident);
  }

  @Override
  public void removeValue(String ident) {
    if (values.remove(ident) == null) {
      throw new IllegalArgumentException(String.format(
          "could not remove %s from tmp env with values %s", ident, toString()));
    }
  }

  @Override
  public void removeAll() {
    values.clear();
  }

  @Override
  public Object getValue(String ident) {
    return values.get(ident);
  }

  /**
   * Copy the given environment, add all entries of this temporary environment
   * to the copy and return the copy.
   */
  public MLEnvironment create(MLEnvironment env) {
    MLEnvironment copy = env.copy();
    values.forEach((k, v) -> copy.setValue(k, v));
    return copy;
  }

}
