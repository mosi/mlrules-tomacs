/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.model.standard.species;

import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

/**
 * A species in ML-Rules is the basic entity of the model. It is defined by a
 * type, a list of attributes and its context species. If a species can contain
 * other species, it is a {@link Compartment}. If a species cannot contain other
 * species, it is a {@link LeafSpecies}. Consequently, all species define a
 * species tree and the root of this species tree is always of type
 * {@link SpeciesType#ROOT}.
 * 
 * @author Tobias Helms
 *
 */
public interface Species {

  SpeciesType getType();

  Object getAttribute(int i);

  void setAttribute(int i, Object value);

  Species getContext();

  void setContext(Species context);

  Double getAmount();

  void setAmount(double amount);

  Species copy();

  boolean contains(Species species);

  void add(Species species);

  boolean remove(Species species);

  Stream<Species> getSubSpeciesStream();

  Stream<Species> getCompartmentStream();

  Stream<Species> getContextStream();

  Map<Species, Species> getSubSpecies(SpeciesType type);
  
  String toSimpleString(boolean shortenReals);

  void individualize(Map<Species, Species> species);

  boolean isLeaf();

  Set<SpeciesType> getTypes();

}
