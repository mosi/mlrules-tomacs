/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.model;

import org.jamesii.cmlrules.util.MLEnvironment;

/**
 * All mlrules models must implement the model interface.
 * 
 * @author Tobias Helms
 */
public interface Model {

  /** The keyword used for the random number generator in the environment. */
  static final String RNG = "§rng";

  /**
   * The keyword used for the time (the current simulation time) entry in the
   * environment.
   */
  static final String TIME = "§time";

  /**
   * Return the environment of the model containing constants, functions, the
   * random number generator, etc.
   */
  MLEnvironment getEnv();

}
