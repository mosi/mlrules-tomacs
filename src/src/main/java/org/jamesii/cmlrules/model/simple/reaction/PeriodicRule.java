/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.model.simple.reaction;

import java.util.List;

import org.jamesii.cmlrules.model.standard.rule.Reactant;
import org.jamesii.cmlrules.util.Assignment;
import org.jamesii.core.math.parsetree.Node;

public class PeriodicRule extends Rule {

  private final double period;
  
  public PeriodicRule(double period, List<Reactant> reactants, List<Reactant> products,
      Node rate, List<Assignment> assignments) {
    super(reactants, products, rate, assignments);
    this.period = period;
  }
  
  public double getPeriod() {
    return period;
  }

}
