/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.model.standard.rule;

import java.util.List;

import org.jamesii.cmlrules.util.Assignment;
import org.jamesii.core.math.parsetree.Node;

/**
 * A rule scheme ("A(x) -> B(x) @1") in ML-rules is a pattern used to compute
 * concrete rules ("A(1) -> B(1) @1").
 * 
 * @author Tobias Helms
 *
 */
public class Rule {

  private final List<Reactant> reactants;

  private final Node product;

  private final Node rate;

  private final List<Assignment> assignments;

  public Rule(List<Reactant> reactants, Node product, Node rate,
      List<Assignment> assignments) {
    this.reactants = reactants;
    this.product = product;
    this.rate = rate;
    this.assignments = assignments;
  }

  public String toString() {
    return reactants.toString() + " -> " + product.toString() + "@" + rate
        + " where " + assignments.toString();
  }
  
  public List<Reactant> getReactants() {
    return reactants;
  }
  
  public Node getProduct() {
    return product;
  }
  
  public Node getRate() {
    return rate;
  }
  
  public List<Assignment> getAssignments() {
    return assignments;
  }

}
