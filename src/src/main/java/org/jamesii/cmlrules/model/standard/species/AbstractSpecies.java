/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.model.standard.species;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Basic class for all species of the new ML-Rules formalism. The amount of
 * species containing other species is always one, i.e., they are treated
 * individually. All other species are treated population-based, i.e., the
 * amount can be higher than one.
 * 
 * @author Tobias Helms
 *
 */
public abstract class AbstractSpecies implements Species {

  private double amount;

  private final SpeciesType type;

  private final Object[] attributes;

  private Species context;

  public AbstractSpecies(double amount, SpeciesType type,
      Object[] attributes, Species context) {
    this.amount = amount;
    this.type = type;
    if (attributes.length != type.getAttributes()) {
      throw new IllegalArgumentException(
          String
              .format(
                  "Could not create species %s because of wrong attribute numbers: %s vs. %s",
                  type, attributes.length, type.getAttributes()));
    }
    this.attributes = Arrays.copyOf(attributes, attributes.length);
    this.context = context;
  }

  public SpeciesType getType() {
    return type;
  }

  public Object getAttribute(int i) {
    if (i < 0 || i > attributes.length - 1) {
      throw new IllegalArgumentException(String.format(
          "Could not access attribute %s from species %s", i, toString()));
    }
    return attributes[i];
  }

  public Species getContext() {
    return context;
  }

  public void setContext(Species context) {
    this.context = context;
  }

  public void setAttribute(int i, Object value) {
    this.attributes[i] = value;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }

  public String toStringWithoutAmount() {
    return super.toString();
  }
  
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(amount);
    builder.append(" ");
    builder.append(getType().getName());
    if (getType().getAttributes() > 0) {
      builder.append("(");
      builder.append(getAttribute(0).toString());
      for (int i = 1; i < getType().getAttributes(); ++i) {
        builder.append(",");
        builder.append(getAttribute(i).toString());
      }
      builder.append(")");
    }
    return builder.toString();
  }
  
  public static final Species UNKNOWN = new Species() {
    @Override
    public SpeciesType getType() {
      return SpeciesType.UNKNOWN;
    }

    @Override
    public Object getAttribute(int i) {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public Species getContext() {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public boolean contains(Species species) {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public void add(Species species) {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public boolean remove(Species species) {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public Map<Species, Species> getSubSpecies(SpeciesType type) {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public String toSimpleString(boolean shortenReals) {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public Double getAmount() {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public void setAmount(double amount) {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public Species copy() {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public void setAttribute(int i, Object value) {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public void setContext(Species context) {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public boolean isLeaf() {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public Stream<Species> getSubSpeciesStream() {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public Stream<Species> getCompartmentStream() {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public void individualize(Map<Species, Species> species) {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public Stream<Species> getContextStream() {
      throw new IllegalStateException("Not supported.");
    }

    @Override
    public Set<SpeciesType> getTypes() {
      throw new IllegalStateException("Not supported.");
    }

  };

}
