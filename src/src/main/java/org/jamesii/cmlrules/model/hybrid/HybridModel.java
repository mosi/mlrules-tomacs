package org.jamesii.cmlrules.model.hybrid;

import java.util.List;
import java.util.Map;

import org.jamesii.cmlrules.model.standard.rule.Reactant;
import org.jamesii.cmlrules.model.standard.rule.Rule;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesModel;
import org.jamesii.cmlrules.util.MLEnvironment;

public class HybridModel implements SpeciesModel {

  private final Species species;

  private final List<org.jamesii.cmlrules.model.simple.reaction.Rule> simpleRules;

  private final List<Rule> complexRules;
  
  private final List<Rule> timedRules;

  private final Map<Reactant, Reactant> reactantProducts;

  private final MLEnvironment env;

  public HybridModel(Species species,
      List<org.jamesii.cmlrules.model.simple.reaction.Rule> simpleRules,
      List<Rule> complexRules, Map<Reactant, Reactant> reactantProducts,
      List<Rule> timedRules, MLEnvironment env) {
    this.species = species;
    this.simpleRules = simpleRules;
    this.complexRules = complexRules;
    this.reactantProducts = reactantProducts;
    this.timedRules = timedRules;
    this.env = env;
  }

  @Override
  public String toString() {
    return String.format("%s \n %s \n %s \n %s", species.toString(),
        simpleRules.toString(), complexRules.toString(), env.toString());
  }

  @Override
  public Species getSpecies() {
    return species;
  }

  public List<org.jamesii.cmlrules.model.simple.reaction.Rule> getSimpleRules() {
    return simpleRules;
  }

  public List<Rule> getComplexRules() {
    return complexRules;
  }

  public List<Rule> getTimedRules() {
    return timedRules;
  }
  
  @Override
  public MLEnvironment getEnv() {
    return env;
  }
  
  public Map<Reactant, Reactant> getReactantProducts() {
    return reactantProducts;
  }
}
