/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.model.simple.reaction;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.parser.standard.nodes.SpeciesAmountNode;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.cmlrules.util.NodeHelper;
import org.jamesii.cmlrules.util.expressions.SimpleRateExpressionOneSpecies;
import org.jamesii.cmlrules.util.expressions.SimpleRateExpressionTwoSpecies;
import org.jamesii.cmlrules.util.expressions.SimpleRateExpressions;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.control.IfThenElseNode;
import org.jamesii.core.math.parsetree.math.MultNode;
import org.jamesii.core.math.parsetree.variables.Identifier;

/**
 * A reaction has change vector and a propensity. The original rule of this
 * reaction is also saved. This {@link Reaction} can be used to describe
 * reactions that do not change the structure of the model.
 * 
 * @author Tobias Helms
 *
 */
public class Reaction {

  private final Species context;

  private final Map<Species, Map<Species, Integer>> changeVector;

  private final Map<Species, Integer> changeVectorNonZero;

  private final Node propensity;

  private final Optional<SimpleRateExpressions> simplePropensity;

  private Double calculatedPropensity;

  private final MLEnvironment env;

  private final Rule rule;

  private boolean possible = true;

  @SuppressWarnings("unchecked")
  private Optional<SimpleRateExpressions> createSimpleExpression(Node node,
      MLEnvironment env) {
    if (node instanceof MultNode) {
      MultNode mn = (MultNode) node;
      if (mn.getLeft() instanceof Identifier
          && mn.getRight() instanceof SpeciesAmountNode) {
        SpeciesAmountNode san = (SpeciesAmountNode) mn.getRight();

        Object o = env.getValue(san.getOtherName());
        Map<?, ?> list = (Map<?, ?>) o;
        Species s = (Species) list.entrySet().iterator().next().getKey();

        double c = ((ValueNode<Double>) mn.getLeft().calc(env)).getValue();
        return Optional.of(new SimpleRateExpressionOneSpecies(s, c));
      }
      if (mn.getLeft() instanceof MultNode
          && mn.getRight() instanceof SpeciesAmountNode) {
        MultNode mn2 = (MultNode) mn.getLeft();
        if (mn2.getLeft() instanceof Identifier
            && mn2.getRight() instanceof SpeciesAmountNode) {
          double c = ((ValueNode<Double>) mn2.getLeft().calc(env)).getValue();

          SpeciesAmountNode san = (SpeciesAmountNode) mn.getRight();
          Object o = env.getValue(san.getOtherName());
          Map<?, ?> list = (Map<?, ?>) o;
          Species s1 = (Species) list.entrySet().iterator().next().getKey();

          san = (SpeciesAmountNode) mn2.getRight();
          o = env.getValue(san.getOtherName());
          list = (Map<?, ?>) o;
          Species s2 = (Species) list.entrySet().iterator().next().getKey();
          return Optional.of(new SimpleRateExpressionTwoSpecies(s1, s2, c));
        }
      }
    }
    if (node instanceof IfThenElseNode) {
      IfThenElseNode iten = (IfThenElseNode) node;
      if (((ValueNode<Boolean>) iten.getCondition().calc(env)).getValue()) {
        if (iten.getThenStmt() instanceof MultNode) {
          MultNode mn = (MultNode) iten.getThenStmt();
          if (mn.getLeft() instanceof Identifier
              && mn.getRight() instanceof SpeciesAmountNode) {
            SpeciesAmountNode san = (SpeciesAmountNode) mn.getRight();

            Object o = env.getValue(san.getOtherName());
            Map<?, ?> list = (Map<?, ?>) o;
            Species s = (Species) list.entrySet().iterator().next().getKey();

            double c = ((ValueNode<Double>) mn.getLeft().calc(env)).getValue();
            return Optional.of(new SimpleRateExpressionOneSpecies(s, c));
          }
        }
      } else {
        if (!containsSpeciesAmountNode(iten.getCondition()) && iten.getElseStmt() instanceof ValueNode<?>
            && ((ValueNode<Double>) iten.getElseStmt()).getValue().equals(0D)) {
          possible = false;
        }
      }
    }
    return Optional.empty();
  }

  private boolean containsSpeciesAmountNode(INode node) {
    if (node instanceof SpeciesAmountNode) {
      return true;
    }
    return node.getChildren().stream().anyMatch(c -> containsSpeciesAmountNode(c));
  }
  
  public Reaction(Species context,
      Map<Species, Map<Species, Integer>> changeVector, Node propensity,
      Rule rule, MLEnvironment env) {
    this.context = context;
    this.changeVector = changeVector;
    this.propensity = propensity;
    this.simplePropensity = createSimpleExpression(propensity, env);
    this.rule = rule;
    this.env = env;

    this.changeVectorNonZero = new IdentityHashMap<>();
    changeVector.values().stream().flatMap(e -> e.entrySet().stream())
        .filter(e -> e.getValue() != 0)
        .forEach(e -> changeVectorNonZero.put(e.getKey(), e.getValue()));

    update();
  }

  public Node getPropensity() {
    return propensity;
  }

  public Species getContext() {
    return context;
  }

  public MLEnvironment getEnv() {
    return env;
  }

  public Map<Species, Map<Species, Integer>> getChangeVector() {
    return changeVector;
  }

  public Set<Species> execute() {
    Set<Species> changedContexts = new HashSet<>();
    for (Entry<Species, Map<Species, Integer>> e : changeVector.entrySet()) {
      e.getKey().getContextStream().forEach(c -> changedContexts.add(c));
      e.getValue()
          .entrySet()
          .forEach(
              e2 -> e2.getKey().setAmount(
                  e2.getKey().getAmount() + e2.getValue()));
    }
    return changedContexts;
  }

  /**
   * Return true if the reaction can be executed. It can happen that a reaction
   * is not executable due to violated amount conditions.
   */
  public boolean executable() {
    return changeVector
        .values()
        .stream()
        .flatMap(m -> m.entrySet().stream())
        .allMatch(
            e -> Double.compare(e.getKey().getAmount(), -1 * e.getValue()) >= 0);
  }

  public Double getCalculatedPropensity() {
    return Math.max(0, calculatedPropensity);
  }

  public void update() {
    calculatedPropensity = executable() ? (simplePropensity.isPresent() ? simplePropensity
        .get().calc() : NodeHelper.getDouble(propensity, env))
        : 0D;
  }

  public void updateAlways() {
    calculatedPropensity = (simplePropensity.isPresent() ? simplePropensity
        .get().calc() : NodeHelper.getDouble(propensity, env));
  }

  public Rule getRule() {
    return rule;
  }

  public boolean isPossible() {
    return possible;
  }
  
  public Map<Species, Integer> getChangeVectorNonZero() {
    return changeVectorNonZero;
  }

}
