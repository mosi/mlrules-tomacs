/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.model.standard;

import java.util.List;

import org.jamesii.cmlrules.model.standard.rule.Rule;
import org.jamesii.cmlrules.model.standard.rule.RulesModel;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesModel;
import org.jamesii.cmlrules.util.MLEnvironment;

/**
 * The standard ML-Rules model consists of a species tree, a list of rule
 * schemes, a list of timed rule schemes and an environment. If timed rule
 * schemes implement the {@link PeriodicRule} interface, they will be executed
 * periodically.
 * 
 * @author Tobias Helms
 *
 */
public class StandardModel implements SpeciesModel, RulesModel {

  private final Species species;

  private final List<Rule> rules;

  private final List<Rule> timedRules;

  private final MLEnvironment env;

  public StandardModel(Species species, List<Rule> rules,
      List<Rule> timedRules, MLEnvironment env) {
    this.species = species;
    this.rules = rules;
    this.timedRules = timedRules;
    this.env = env;
  }

  @Override
  public String toString() {
    return String.format("%s \n %s \n %s \n %s", species.toString(),
        rules.toString(), timedRules.toString(), env.toString());
  }

  @Override
  public Species getSpecies() {
    return species;
  }

  @Override
  public List<Rule> getRules() {
    return rules;
  }

  @Override
  public List<Rule> getTimedRules() {
    return timedRules;
  }

  @Override
  public MLEnvironment getEnv() {
    return env;
  }

}
