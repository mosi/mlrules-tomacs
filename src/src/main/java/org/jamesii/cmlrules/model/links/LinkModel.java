package org.jamesii.cmlrules.model.links;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jamesii.cmlrules.model.standard.StandardModel;
import org.jamesii.cmlrules.model.standard.rule.Reactant;
import org.jamesii.cmlrules.model.standard.rule.Rule;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.cmlrules.util.Nu;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.Identifier;

public class LinkModel extends StandardModel {

  /**
   * This map contains for each rule all link variable names that must have a
   * non-free value. For example, "A(x) + B(x) -> ..." (assuming that both
   * attributes are link attributes) has the link variable name "x". "x" cannot
   * be free, because in this case the modeler should have written "free". In
   * contrast, the rule "A(x) + B(y) -> ..." does not have a link variable name,
   * because the value of the link variables do not matter. This distinction is
   * important, because it is possible that link attributes does not matter in
   * rules.
   */
  private final Map<Rule, Set<String>> linkVariables = new HashMap<>();

  private final Map<SpeciesType, Set<Integer>> linkAttributes;

  private final Map<Reactant, Map<Integer, String>> reactantLinks =
      new HashMap<>();

  public LinkModel(Species species, List<Rule> rules, List<Rule> timedRules,
      Map<SpeciesType, Set<Integer>> linkAttributes, MLEnvironment env) {
    super(species, rules, timedRules, env);
    this.linkAttributes = linkAttributes;
    rules.forEach(r -> {
      computeLinkVariables(r, r.getReactants());
      r.getReactants().forEach(reactant -> computeReactantLinks(r, reactant));
    });
  }

  private void computeLinkVariables(Rule rule, List<Reactant> reactants) {
    Map<Reactant, Set<String>> linkMap = new HashMap<>();
    for (Reactant r : reactants) {
      Set<String> linkSet = new HashSet<>();
      for (Integer i : linkAttributes.getOrDefault(r.getType(),
          Collections.emptySet())) {
        Node n = r.getAttributeNodes().get(i);
        if (n instanceof ValueNode<?>) {
          ValueNode<?> vn = (ValueNode<?>) n;
          if (vn.getValue() != Nu.FREE) {
            throw new IllegalArgumentException(String.format(
                "invalid link value %s given in rule %s", vn.getValue(), rule));
          }
        } else if (n instanceof Identifier<?>) {
          Identifier<?> in = (Identifier<?>) n;
          if (in.getIdent() instanceof String) {
            String name = (String) in.getIdent();
            if (linkVariables.getOrDefault(rule, Collections.emptySet())
                .contains(name)) {
              throw new IllegalArgumentException(String.format(
                  "link variable %s cannot be used in more than two reactants",
                  name));
            }
            if (!linkSet.add(name)) {
              throw new IllegalArgumentException(
                  String
                      .format(
                          "link variable %s cannot be used twice within the same reactant",
                          name));
            }
            if (linkMap.values().stream().anyMatch(v -> v.contains(name))) {
              linkVariables.compute(rule,
                  (k, v) -> v == null ? new HashSet<>() : v).add(name);
            }
          }
        }
      }
      linkMap.put(r, linkSet);
      computeLinkVariables(rule, r.getSubReactants());
    }
  }

  @SuppressWarnings("unchecked")
  private void computeReactantLinks(Rule rule, Reactant reactant) {
    for (Integer index : linkAttributes.getOrDefault(reactant.getType(), Collections.emptySet())) {
      if (reactant.getAttributeNodes().get(index) instanceof Identifier<?>) {
        if (linkVariables.getOrDefault(rule, Collections.emptySet()).contains(((Identifier<String>) reactant.getAttributeNodes().get(index)).getIdent())) {
        reactantLinks.computeIfAbsent(reactant, r -> new HashMap<>()).put(
            index,
            (String) ((Identifier<?>) reactant.getAttributeNodes().get(index))
                .getIdent());
        }
      }
    }
    reactant.getSubReactants().forEach(r -> computeReactantLinks(rule, r));
  }

  public Map<SpeciesType, Set<Integer>> getLinkAttributes() {
    return linkAttributes;
  }

  public Map<Rule, Set<String>> getLinkVariableNames() {
    return linkVariables;
  }
  
  public Map<Reactant, Map<Integer, String>> getReactantLinks() {
    return reactantLinks;
  }

}
