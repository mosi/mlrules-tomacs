/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.model.simple;

import java.util.List;
import java.util.Map;

import org.jamesii.cmlrules.model.simple.reaction.Reaction;
import org.jamesii.cmlrules.model.simple.reaction.ReactionsModel;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesModel;
import org.jamesii.cmlrules.util.MLEnvironment;

/**
 * The simple model has a static tree structure, i.e., no compartment species
 * are allowed to change. Further, it is not allowed to switch between infinite
 * and non-infinite rates within one rule.
 * 
 * @author Tobias Helms
 *
 */
public class SimpleModel implements SpeciesModel, ReactionsModel {

  private final Species species;

  private final Map<Species, List<Reaction>> reactions;
  
  private final List<Reaction> infiniteReactions;

  private final Map<Species, List<Reaction>> timedReactions;

  private final MLEnvironment env;

  public SimpleModel(Species species, Map<Species, List<Reaction>> reactions, List<Reaction> infiniteReactions,
      Map<Species, List<Reaction>> timedReactions, MLEnvironment env) {
    this.species = species;
    this.reactions = reactions;
    this.infiniteReactions = infiniteReactions;
    this.timedReactions = timedReactions;
    this.env = env;
  }

  @Override
  public String toString() {
    return species.toString() + "\n" + reactions.toString() + "\n"
        + timedReactions.toString() + "\n" + env.toString();
  }

  @Override
  public MLEnvironment getEnv() {
    return env;
  }

  @Override
  public Species getSpecies() {
    return species;
  }

  @Override
  public Map<Species, List<Reaction>> getReactions() {
    return reactions;
  }

  @Override
  public Map<Species, List<Reaction>> getTimedReactions() {
    return timedReactions;
  }
  
  @Override
  public List<Reaction> getInfiniteReactions() {
    return infiniteReactions;
  }

}
