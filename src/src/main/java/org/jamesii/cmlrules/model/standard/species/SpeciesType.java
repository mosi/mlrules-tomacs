/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.model.standard.species;


/**
 * A type defines a name and a number of attributes for species and
 * compartments.
 * 
 * @author Tobias Helms
 *
 */
public final class SpeciesType {

  private final String name;

  private final int attributes;

  public static final String ROOT_NAME = "$$ROOT$$";
  
  public static final String UNKNOWN_NAME = "$$UNKNOWN$$";

  /**
   * The type of the species which is always the root of the species tree of an
   * ML-Rules model.
   */
  public static final SpeciesType ROOT = new SpeciesType(ROOT_NAME, 0);
  
  public static final SpeciesType UNKNOWN = new SpeciesType(UNKNOWN_NAME, 0);

  public SpeciesType(String name, int attributes) {
    this.name = name;
    this.attributes = attributes;
  }

  public final String getName() {
    return name;
  }

  public final int getAttributes() {
    return attributes;
  }

  @Override
  public String toString() {
    return name + "(" + attributes + ")";
  }

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + attributes;
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	SpeciesType other = (SpeciesType) obj;
	if (attributes != other.attributes)
		return false;
	if (name == null) {
		if (other.name != null)
			return false;
	} else if (!name.equals(other.name))
		return false;
	return true;
}
  
  

}
