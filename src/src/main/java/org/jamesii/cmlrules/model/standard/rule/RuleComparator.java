/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.model.standard.rule;

import java.util.Comparator;

import org.jamesii.core.math.parsetree.ValueNode;

public class RuleComparator implements Comparator<Rule> {

  public static final RuleComparator instance = new RuleComparator();
  
  private RuleComparator() {
  }
  
  @SuppressWarnings("unchecked")
  @Override
  public int compare(Rule arg0, Rule arg1) {
    return Double.compare(((ValueNode<Double>) arg0.getRate()).getValue(),
        ((ValueNode<Double>) arg1.getRate()).getValue());
  }

}
