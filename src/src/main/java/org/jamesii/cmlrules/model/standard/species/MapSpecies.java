/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.model.standard.species;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapSpecies extends AbstractSpecies {

  private final Map<SpeciesType, Map<Species, Species>> subSpecies;

  private final int hash;

  public MapSpecies(double amount, SpeciesType type, Object[] attributes,
      Species context, Map<SpeciesType, Map<Species, Species>> subSpecies) {
    super(amount, type, attributes, context);
    this.subSpecies = subSpecies;
    this.hash = computeHash();
  }

  public MapSpecies(Map<Species, Species> subSpecies, double amount,
      SpeciesType type, Object[] attributes, Species context) {
    super(amount, type, attributes, context);
    this.hash = computeHash();
    this.subSpecies = new HashMap<>();
    subSpecies.keySet().forEach(
        s -> this.subSpecies.computeIfAbsent(s.getType(), t -> new HashMap<>())
            .put(s, s));
  }

  private int computeHash() {
    int result = getType().hashCode();
    for (int i = 0; i < getType().getAttributes(); ++i) {
      result = 31 * result + getAttribute(i).hashCode();
    }
    return result;
  }

  @Override
  public Map<Species, Species> getSubSpecies(SpeciesType type) {
    return Collections.unmodifiableMap(subSpecies.getOrDefault(type,
        Collections.emptyMap()));
  }

  @Override
  public boolean contains(Species species) {
    return subSpecies.getOrDefault(species.getType(), Collections.emptyMap())
        .containsKey(species);
  }

  @Override
  public Stream<Species> getSubSpeciesStream() {
    return subSpecies.values().stream().flatMap(l -> l.keySet().stream());
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(getAmount());
    builder.append(" ");
    builder.append(getType().getName());
    if (getType().getAttributes() > 0) {
      builder.append("(");
      builder.append(getAttribute(0).toString());
      for (int i = 1; i < getType().getAttributes(); ++i) {
        builder.append(",");
        builder.append(getAttribute(i).toString());
      }
      builder.append(")");
    }
    if (!subSpecies.isEmpty()) {
      builder.append("[");
      subSpecies.forEach((k, v) -> {
        v.keySet().forEach(s -> {
          builder.append(s.toString());
          builder.append(",");
        });
      });
      builder.delete(builder.length() - 1, builder.length());
      builder.append("]");
    }
    return builder.toString();
  }

  @Override
  public void add(Species species) {
    subSpecies.computeIfAbsent(species.getType(), k -> new HashMap<>()).put(
        species, species);
  }

  @Override
  public boolean remove(Species species) {
    return subSpecies.getOrDefault(species.getType(), Collections.emptyMap())
        .remove(species) != null;
  }

  @Override
  public String toSimpleString(boolean shortenReals) {
    StringBuilder builder = new StringBuilder();
    builder.append(getType().getName());
    if (getType().getAttributes() > 0) {
      StringJoiner joiner = new StringJoiner(",", "(", ")");
      for (int i = 0; i < getType().getAttributes(); ++i) {
        if (shortenReals && getAttribute(i) instanceof Double) {
          joiner.add("real");
        } else {
          joiner.add(getAttribute(i).toString());
        }
      }
      builder.append(joiner.toString());
    }
    return builder.toString();
  }

  @Override
  public Species copy() {
    Object[] attributes = new Object[getType().getAttributes()];
    for (int i = 0; i < getType().getAttributes(); ++i) {
      attributes[i] = getAttribute(i);
    }
    Map<SpeciesType, Map<Species, Species>> sub = new HashMap<>();
    subSpecies.forEach((k, v) -> {
      sub.put(
          k,
          v.keySet().stream().map(s -> s.copy())
              .collect(Collectors.toMap(s -> s, s -> s)));
    });
    Species result =
        new MapSpecies(getAmount(), getType(), attributes, getContext(), sub);
    sub.values().forEach(v -> v.keySet().forEach(s -> s.setContext(result)));
    return result;
  }

  @Override
  public void individualize(Map<Species, Species> indies) {
    if (isLeaf()) {
      indies.put(this, this);
      return;
    }

    Map<SpeciesType, Map<Species, Species>> individualized = new HashMap<>();
    getSubSpeciesStream().forEach(
        s -> s.individualize(individualized.computeIfAbsent(s.getType(),
            key -> new HashMap<>())));
    subSpecies.clear();
    subSpecies.putAll(individualized);

    for (int i = 0; i < getAmount().intValue(); ++i) {
      Species copy = copy();
      copy.setAmount(1);
      indies.put(copy, copy);
    }
  }

  private void getCompartmentStreamInternal(Stream.Builder<Species> builder) {
    if (!isLeaf()) {
      builder.add(this);
      getSubSpeciesStream().filter(s -> !isLeaf()).forEach(
          s -> ((MapSpecies) s).getCompartmentStreamInternal(builder));
    }
  }

  @Override
  public Stream<Species> getCompartmentStream() {
    Stream.Builder<Species> builder = Stream.builder();
    getCompartmentStreamInternal(builder);
    return builder.build();
  }

  private void getContextStreamInternal(Stream.Builder<Species> builder) {
    builder.add(this);
    if (getContext() != UNKNOWN) {
      ((MapSpecies) getContext()).getContextStreamInternal(builder);
    }
  }
  
  @Override
  public Stream<Species> getContextStream() {
    Stream.Builder<Species> builder = Stream.builder();
    getContextStreamInternal(builder);
    return builder.build();
  }

  @Override
  public Set<SpeciesType> getTypes() {
    return subSpecies.keySet();
  }

  @Override
  public boolean isLeaf() {
    return subSpecies.isEmpty();
  }

  @Override
  public int hashCode() {
    return hash;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o instanceof Species) {
      Species other = (Species) o;
      if (!isLeaf() || !other.isLeaf()) {
        return false;
      }

      if (!getType().equals(other.getType())) {
        return false;
      }

      for (int i = 0; i < getType().getAttributes(); ++i) {
        if (!getAttribute(i).equals(other.getAttribute(i))) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

}
