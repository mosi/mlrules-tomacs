/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.model.simple.reaction;

import java.util.List;

import org.jamesii.cmlrules.model.standard.rule.Reactant;
import org.jamesii.cmlrules.util.Assignment;
import org.jamesii.core.math.parsetree.Node;

/**
 * In contrast to the standard
 * {@link org.jamesii.cmlrules.model.standard.rule.Rule}, the simple
 * {@link Rule} has a list of reactants as products and not an arbitrary
 * expression.
 * 
 * @author Tobias Helms
 *
 */
public class Rule {

  private final List<Reactant> reactants;

  private final List<Reactant> products;

  private final Node rate;

  private final List<Assignment> assignments;

  public Rule(List<Reactant> reactants, List<Reactant> products, Node rate,
      List<Assignment> assignments) {
    this.reactants = reactants;
    this.products = products;
    this.rate = rate;
    this.assignments = assignments;
  }

  public String toString() {
    return reactants.toString() + " -> " + products.toString() + "@" + rate
        + " where " + assignments.toString();
  }

  public List<Reactant> getReactants() {
    return reactants;
  }

  public List<Reactant> getProducts() {
    return products;
  }

  public Node getRate() {
    return rate;
  }

  public List<Assignment> getAssignments() {
    return assignments;
  }

}
