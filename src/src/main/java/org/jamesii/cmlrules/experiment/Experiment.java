/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.experiment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.ode.nonstiff.DormandPrince853Integrator;
import org.jamesii.cmlrules.experiment.stop.StopConditionFactory;
import org.jamesii.cmlrules.model.Model;
import org.jamesii.cmlrules.model.hybrid.HybridModel;
import org.jamesii.cmlrules.model.standard.StandardModel;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.observation.Instrumenter;
import org.jamesii.cmlrules.parser.hybrid.HybridRulesListener;
import org.jamesii.cmlrules.parser.hybrid.HybridSimpleRulesListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesLexer;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ModelContext;
import org.jamesii.cmlrules.parser.standard.functions.PredefinedFunctions;
import org.jamesii.cmlrules.parser.standard.listener.ConstantCrawlerListener;
import org.jamesii.cmlrules.parser.standard.listener.FunctionCrawlerListener;
import org.jamesii.cmlrules.parser.standard.listener.FunctionDefinitionListener;
import org.jamesii.cmlrules.parser.standard.listener.RulesListener;
import org.jamesii.cmlrules.parser.standard.listener.SpeciesTypeCrawlerListener;
import org.jamesii.cmlrules.simulator.Simulator;
import org.jamesii.cmlrules.simulator.hybrid.HybridSimulator;
import org.jamesii.cmlrules.simulator.standard.StandardSimulator;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;

/**
 * This basic experiment class allows to execute replications of ML-Rules
 * simulations in parallel. The model location, model parameters, observers,
 * stopping conditions, the number of replications as well as the number of used
 * cores can be set.
 *
 * @author Tobias Helms
 */
public class Experiment {

  private final String MODEL_LOCATION;

  private final ConcurrentLinkedQueue<Job> JOBS = new ConcurrentLinkedQueue<>();

  protected final Instrumenter INSTRUMENTER;

  private final StopConditionFactory STOP_CONDITION;

  private final ExecutorService executor;

  private final List<SimulationRun> runs = new ArrayList<>();

  public Experiment(String modelLocation, Instrumenter instrumenter,
      StopConditionFactory stopCondition, int cores) {
    this.MODEL_LOCATION = modelLocation;
    this.INSTRUMENTER = instrumenter;
    this.STOP_CONDITION = stopCondition;
    this.executor = Executors.newFixedThreadPool(cores);
  }

  protected Model createModel(Map<String, Object> parameter) throws IOException {
    File file = new File(MODEL_LOCATION);
    if (!file.exists()) {
      throw new IllegalArgumentException(
          String.format("Could not find file %s", MODEL_LOCATION));
    }
    MLRulesLexer lexer = new MLRulesLexer(
        new ANTLRInputStream(FileUtils.readFileToString(file)));
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    MLRulesParser parser = new MLRulesParser(tokens);
    ModelContext context = parser.model();
    if (parser.getNumberOfSyntaxErrors() > 0) {
      throw new IllegalArgumentException(
          String.format("Could not create the model from %s", MODEL_LOCATION));
    }
    ParseTreeWalker walker = new ParseTreeWalker();

    MLEnvironment env = new MLEnvironment();
    env.setGlobalValue(StandardModel.RNG,
        new MersenneTwister(System.currentTimeMillis()));
    PredefinedFunctions.addAll(env);
    env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);

    MLRulesListener functionCrawler = new FunctionCrawlerListener(env);
    walker.walk(functionCrawler, context);
    MLRulesListener speciesCrawler = new SpeciesTypeCrawlerListener(env);
    walker.walk(speciesCrawler, context);
    MLRulesListener functionDefCrawler = new FunctionDefinitionListener(env);
    walker.walk(functionDefCrawler, context);
    MLRulesListener constantsCrawler =
    		new ConstantCrawlerListener(env, parameter);
    walker.walk(constantsCrawler, context);

//    Map<SpeciesType, Set<Integer>> linkAttributes = new HashMap<>();
//    Map<SpeciesType, Set<Integer>> noLinkAttributes = new HashMap<>();
//    MLEnvironment copy = env.completeCopy();
//    LinkWhereVariablesListener variablesChecker = new LinkWhereVariablesListener(
//        linkAttributes, noLinkAttributes, copy);
//    walker.walk(variablesChecker, context);
//
//    LinkSemanticsListener semanticsChecker = new LinkSemanticsListener(
//        linkAttributes, noLinkAttributes, copy,
//        variablesChecker.getRuleVariables(),
//        variablesChecker.getFunctionVariables());
//    walker.walk(semanticsChecker, context);
//
//    RulesListener rulesListener = new LinkRulesListener(env,
//        linkAttributes);
//    walker.walk(rulesListener, context);
//
//    return rulesListener.create();


  RulesListener rulesListener = new RulesListener(env);
  walker.walk(rulesListener, context);
  return rulesListener.create();


//    SimpleRulesListener simpleRulesListener = new SimpleRulesListener(
//    		env);
//    walker.walk(simpleRulesListener, context);
//    return simpleRulesListener.create();

//    HybridSimpleRulesListener hybridListener = new HybridSimpleRulesListener(
//            env);
//        walker.walk(hybridListener, context);
//  HybridRulesListener rulesListener = new HybridRulesListener(
//          env, hybridListener.getRules(), hybridListener.getReactantProducts());
//      walker.walk(rulesListener, context);
//      return rulesListener.create();
  }

  public void addJob(Job job) {
    JOBS.add(job);
  }

  protected Simulator getSimulator(Model model, Job job) {
    return new StandardSimulator((StandardModel) model, INSTRUMENTER.create(job, model),true);
  }
  
  /**
   * Start executing the jobs in the queue. The experiment will not accept
   * new jobs after this method has been called.
   */
  public void execute() {
    while (!JOBS.isEmpty()) {
      Job job = JOBS.poll();
      try {
        Model model = createModel(job.getParameter());
//        Simulator simulator = new LinkSimulator((LinkModel) model, INSTRUMENTER.create(job, model));
//      Simulator simulator = new StandardSimulator((StandardModel) model,
//      INSTRUMENTER.create(job, model));
//        Simulator simulator = new SimpleSimulatorSSA((SimpleModel) model, INSTRUMENTER.create(job, model));
//        Simulator simulator = new HybridSimulator((HybridModel) model,
//                INSTRUMENTER.create(job, model), new DormandPrince853Integrator(1.e-07,
//                        10.0, 1.0, 1.0e-1));
        Simulator simulator = getSimulator(model, job);
        SimulationRun run =
            new SimulationRun(job.getID(), simulator, STOP_CONDITION);
        runs.add(run);
        executor.execute(run);
      } catch (Exception e) {
        e.printStackTrace();
        Logger.getGlobal()
            .warning(String.format("Job %s crashed!", job.getID()));
      }
    }

    // the executor will terminate as soon as all jobs are finished
    try {
      executor.shutdown();
      executor.awaitTermination(10, TimeUnit.DAYS);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public List<SimulationRun> getRuns() {
    return runs;
  }

}
