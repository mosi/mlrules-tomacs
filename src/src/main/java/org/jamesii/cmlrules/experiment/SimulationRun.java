/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.experiment;

import java.util.logging.Logger;

import org.jamesii.cmlrules.experiment.stop.StopCondition;
import org.jamesii.cmlrules.experiment.stop.StopConditionFactory;
import org.jamesii.cmlrules.simulator.Simulator;

/**
 * A simulation run executes one replication of an ML-Rules model with a given
 * stopping conditions.
 * 
 * @author Tobias Helms
 *
 */
public class SimulationRun implements Runnable {

  private final int ID;

  private final StopConditionFactory STOP;

  private final Simulator SIMULATOR;
  
  private long runtime = 0;

  public SimulationRun(int id, Simulator simulator,
      StopConditionFactory stopFactory) {
    this.ID = id;
    this.SIMULATOR = simulator;
    this.STOP = stopFactory;
  }

  @Override
  public void run() {
    Logger.getGlobal().info(String.format("Start simulation %s", ID));
    long time = System.currentTimeMillis();
    StopCondition stopCondition = STOP.create();
    do {
      SIMULATOR.nextStep();
    } while (SIMULATOR.getObserver().stream()
        .flatMap(o -> o.getListener().stream()).anyMatch(l -> l.isActive())
        && !stopCondition.stop(SIMULATOR));
    SIMULATOR.getObserver().stream().flatMap(o -> o.getListener().stream())
        .forEach(l -> l.finish());
    runtime = System.currentTimeMillis() - time;
    Logger.getGlobal().info(
        String.format("Finished simulation %s in %s ms", ID,
            runtime));
  }
  
  public long getRuntime() {
    return runtime;
  }
  
}
