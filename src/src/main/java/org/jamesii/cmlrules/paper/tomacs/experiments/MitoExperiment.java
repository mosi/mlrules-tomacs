package org.jamesii.cmlrules.paper.tomacs.experiments;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.analysis.function.Exp;
import org.jamesii.cmlrules.debug.experiment.ExpInstrumenter;
import org.jamesii.cmlrules.experiment.Experiment;
import org.jamesii.cmlrules.experiment.Job;
import org.jamesii.cmlrules.experiment.stop.SimTimeStopConditionFactory;
import org.jamesii.cmlrules.experiment.stop.StopConditionFactory;
import org.jamesii.cmlrules.model.Model;
import org.jamesii.cmlrules.model.links.LinkModel;
import org.jamesii.cmlrules.model.standard.StandardModel;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.observation.Instrumenter;
import org.jamesii.cmlrules.paper.tomacs.experiments.WntExperiment.Mode;
import org.jamesii.cmlrules.parser.links.listener.typecheck.LinkRulesListener;
import org.jamesii.cmlrules.parser.links.listener.typecheck.LinkSemanticsListener;
import org.jamesii.cmlrules.parser.links.listener.typecheck.LinkWhereVariablesListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesLexer;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ModelContext;
import org.jamesii.cmlrules.parser.standard.functions.PredefinedFunctions;
import org.jamesii.cmlrules.parser.standard.listener.ConstantCrawlerListener;
import org.jamesii.cmlrules.parser.standard.listener.FunctionCrawlerListener;
import org.jamesii.cmlrules.parser.standard.listener.FunctionDefinitionListener;
import org.jamesii.cmlrules.parser.standard.listener.RulesListener;
import org.jamesii.cmlrules.parser.standard.listener.SpeciesTypeCrawlerListener;
import org.jamesii.cmlrules.simulator.Simulator;
import org.jamesii.cmlrules.simulator.links.LinkSimulator;
import org.jamesii.cmlrules.simulator.standard.StandardSimulator;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.cmlrules.util.Pair;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;
import org.jamesii.core.util.misc.Files;

public class MitoExperiment extends Experiment {

  public enum Mode {
    STANDARD_WITH, STANDARD_WITHOUT, STATIC_WITH, STATIC_WITHOUT;
  }

  public MitoExperiment(String modelLocation, Instrumenter instrumenter,
      StopConditionFactory stopCondition, int cores, Mode mode) {
    super(modelLocation, instrumenter, stopCondition, cores);
    this.mode = mode;
  }

  public static int CORES = 1;

  public static int REPS = 20;

  public static double END_TIME = 100;
  
  public static double OBSERVATION_TIME = 1;

  private final Mode mode;

  @Override
  protected Model createModel(Map<String, Object> parameter) throws IOException {
    File file = new File("./models/mitos.mlrj");
    if (!file.exists()) {
      throw new IllegalArgumentException(String.format(
          "Could not find file %s", "./models/mitos.mlrj"));
    }
    MLRulesLexer lexer = new MLRulesLexer(new ANTLRInputStream(
        FileUtils.readFileToString(file)));
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    MLRulesParser parser = new MLRulesParser(tokens);
    ModelContext context = parser.model();
    if (parser.getNumberOfSyntaxErrors() > 0) {
      throw new IllegalArgumentException(String.format(
          "Could not create the model from %s", "./models/mitos.mlrj"));
    }
    ParseTreeWalker walker = new ParseTreeWalker();

    MLEnvironment env = new MLEnvironment();
    env.setGlobalValue(StandardModel.RNG,
        new MersenneTwister(System.currentTimeMillis()));
    PredefinedFunctions.addAll(env);
    env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);

    MLRulesListener functionCrawler = new FunctionCrawlerListener(env);
    walker.walk(functionCrawler, context);
    MLRulesListener speciesCrawler = new SpeciesTypeCrawlerListener(env);
    walker.walk(speciesCrawler, context);
    MLRulesListener functionDefCrawler = new FunctionDefinitionListener(env);
    walker.walk(functionDefCrawler, context);
    MLRulesListener constantsCrawler = new ConstantCrawlerListener(env,
        parameter);
    walker.walk(constantsCrawler, context);

    RulesListener rulesListener;
    switch (mode) {
    case STANDARD_WITH:
    case STANDARD_WITHOUT:
      rulesListener = new RulesListener(env);
      walker.walk(rulesListener, context);
      return rulesListener.create();
    case STATIC_WITH:
    case STATIC_WITHOUT:
      default:
      Map<SpeciesType, Set<Integer>> linkAttributes = new HashMap<>();
      Map<SpeciesType, Set<Integer>> noLinkAttributes = new HashMap<>();
      MLEnvironment copy = env.completeCopy();
      LinkWhereVariablesListener variablesChecker = new LinkWhereVariablesListener(
          linkAttributes, noLinkAttributes, copy);
      walker.walk(variablesChecker, context);
  
      LinkSemanticsListener semanticsChecker = new LinkSemanticsListener(
          linkAttributes, noLinkAttributes, copy,
          variablesChecker.getRuleVariables(),
          variablesChecker.getFunctionVariables());
      walker.walk(semanticsChecker, context);
  
      rulesListener = new LinkRulesListener(env,
          linkAttributes);
      walker.walk(rulesListener, context);
  
      return rulesListener.create();
      
    }

  }
  
  protected Simulator getSimulator(Model model, Job job) {
    switch (mode) {
    case STANDARD_WITH: return new StandardSimulator((StandardModel) model, INSTRUMENTER.create(job, model),true);
    case STANDARD_WITHOUT: return new StandardSimulator((StandardModel) model, INSTRUMENTER.create(job,model),false);
    case STATIC_WITH: return new LinkSimulator((LinkModel) model, INSTRUMENTER.create(job, model),true);
    case STATIC_WITHOUT: return new LinkSimulator((LinkModel) model, INSTRUMENTER.create(job, model),false);
    }
    throw new RuntimeException("Invalid simulator mode");
  }
  
  public static void readVariables(String[] args) throws ParseException {
    Options options = new Options();
    options.addOption(Option.builder("reps").argName("num").hasArg().desc("number of replications (default = 20)").build());
    options.addOption(Option.builder("endTime").argName("num").hasArg().desc("simulation end time (default = 100.0)").build());
    options.addOption(Option.builder("obs").argName("num").hasArg().desc("observation interval (default = 1.0)").build());
    options.addOption(Option.builder("parallel").argName("num").hasArg().desc("number of parallel simulations (default = 1)").build());
    
    
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp( "ml-rules", options );
    
    CommandLineParser parser = new DefaultParser();
    CommandLine cmd = parser.parse(options, args);
    
    if (cmd.hasOption("reps")) {
      REPS = Integer.valueOf(cmd.getOptionValue("reps"));
      if (REPS < 0) {
        throw new IllegalArgumentException("Negative number of replications is not allowed.");
      }
    } else {
      REPS = 20;
    }
    if (cmd.hasOption("endTime")) {
      END_TIME = Double.valueOf(cmd.getOptionValue("endTime"));
      if (Double.compare(END_TIME, 0.0) <= 0) {
        throw new IllegalArgumentException("Simulation end time must be positive.");
      }
    } else {
      END_TIME = 100.0;
    }
    if (cmd.hasOption("obs")) {
      OBSERVATION_TIME = Double.valueOf(cmd.getOptionValue("obs"));
      if (Double.compare(OBSERVATION_TIME,0.0) <= 0 || Double.compare(OBSERVATION_TIME, END_TIME) > 0) {
        throw new IllegalArgumentException("Observation interval must be positive and equal or smaller than the simulation end time.");
      }
    } else {
      OBSERVATION_TIME = 1.0;
    }
    if (cmd.hasOption("parallel")) {
      CORES = Integer.valueOf(cmd.getOptionValue("parallel"));
      if (CORES < 1) {
        throw new IllegalArgumentException("Number of parallel simulations must be positive.");
      }
    } else {
      CORES = 1;
    }
    
  }
  
  private void write(String path, int config, int rep) {
    try {
      FileReader reader = new FileReader("mitoResults/" + path + "/" + config + "/" + rep
          + "---Mito.csv");
      BufferedReader buffer = new BufferedReader(reader);

      FileWriter writer = new FileWriter("mitoResults/" + 
          path + "/result_" + config
              + "_" + rep + "_agg");
      BufferedWriter bufferW = new BufferedWriter(writer);

      buffer.readLine();

      String line;
      double time = 0D;
      int falseFreeCounter = 0;
      int falseLinkCounter = 0;
      int trueFreeCounter = 0;
      int trueLinkCounter = 0;
      bufferW.write("MitoNoTubuleFree,MitoNoTubuleLinked,MitoTubuleFree,MitoTubuleLinked\n");
      boolean active = true;
      while ((line = buffer.readLine()) != null) {
        String[] vals = line.split(",");
        if (Double.valueOf(vals[0]) > time) {
          time = Double.valueOf(vals[0]);

          StringJoiner sj = new StringJoiner(",", "", "\n");
          sj.add(String.valueOf(falseFreeCounter));
          sj.add(String.valueOf(falseLinkCounter));
          sj.add(String.valueOf(trueFreeCounter));
          sj.add(String.valueOf(trueLinkCounter));
          if (active) {
            bufferW.write(sj.toString());
          }

          falseFreeCounter = 0;
          falseLinkCounter = 0;
          trueFreeCounter = 0;
          trueLinkCounter = 0;

          if (Double.compare(time,END_TIME) > 0) {
            active = false;
          }
          
        }
        
        if (Boolean.valueOf(vals[2])) {
          if (vals[3].equals("free")) {
            ++trueFreeCounter;
          } else {
            ++trueLinkCounter;
          }
        } else {
          if (vals[3].equals("free")) {
            ++falseFreeCounter;
          } else {
            ++falseLinkCounter;
          }
        }
        
      }
      
      if (Double.compare(time,END_TIME) <= 0) {
        StringJoiner sj = new StringJoiner(",", "", "\n");
        sj.add(String.valueOf(falseFreeCounter));
        sj.add(String.valueOf(falseLinkCounter));
        sj.add(String.valueOf(trueFreeCounter));
        sj.add(String.valueOf(trueLinkCounter));
        
        bufferW.write(sj.toString());
      }

      bufferW.flush();
      bufferW.close();
      writer.close();

      buffer.close();
      reader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  
  public static void main(String[] args) {

    try {
     readVariables(args);
    }catch (Exception e) {
      e.printStackTrace();
      System.exit(-1);
    }
    
    for (Mode mode : Mode.values()) {
      String path = "";
      switch (mode) {
      case STANDARD_WITH: path = "standardWithDG";
      break;
      case STANDARD_WITHOUT: path = "standardWithoutDG";
      break;
      case STATIC_WITH: path = "linkWithDG";
      break;
      case STATIC_WITHOUT: path = "linkWithoutDG";
      break;
      default:
        throw new RuntimeException("Wrong mode");
      }
      
    try {
      File file = new File("./mitoResults/" + path);
      Files.deleteRecursively(file);
      file.mkdirs();
      FileWriter writer = new FileWriter("./mitoResults/" + path + "/runtimes");
      BufferedWriter buffer = new BufferedWriter(writer);

      boolean first = true;
      for (int i : new int[] { 20, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200}) {
        String modelLocation = "./models/mitos.mlrj";
        StopConditionFactory stop = new SimTimeStopConditionFactory(END_TIME);
        File directory = new File("./mitoResults/" + path + "/" + i + "/");
        directory.mkdirs();

        MitoExperiment exp = new MitoExperiment(modelLocation, new ExpInstrumenter(
            directory, OBSERVATION_TIME), stop, CORES,mode);

        for (int j = 0; j < REPS; ++j) {
          HashMap<String, Object> params = new HashMap<>();
          params.put("mitoNum", i);
          exp.addJob(new Job(j, params));
        }

        exp.execute();

        if (!first) {
          StringJoiner sj = new StringJoiner(",", "", "\n");
          exp.getRuns().forEach(r -> sj.add(String.valueOf(r.getRuntime())));
          buffer.write(sj.toString());
          buffer.flush();
          
          for (int j = 0; j < REPS; ++j) {
            exp.write(path, i, j);
          }
        }
        first = false;
      }

      buffer.flush();
      buffer.close();
      writer.close();
      
    } catch (IOException e) {
      e.printStackTrace();
    }
    }
  }

  
}
