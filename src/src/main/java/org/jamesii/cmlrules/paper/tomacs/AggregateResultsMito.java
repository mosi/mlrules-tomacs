package org.jamesii.cmlrules.paper.tomacs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.jamesii.cmlrules.util.Pair;

public class AggregateResultsMito {

  public static final int CONFIGS = 1;

  public static final int REPS = 20;

  public static final String PATH = "D:/tmp/tomacs/link/simpleNoDG/";

  private static void write(int config, int rep) {
    try {
      FileReader reader = new FileReader(PATH + config + "/" + rep
          + "---Mito.csv");
      BufferedReader buffer = new BufferedReader(reader);

      FileWriter writer = new FileWriter(
          PATH + "result_" + config
              + "_" + rep + "_agg");
      BufferedWriter bufferW = new BufferedWriter(writer);

      buffer.readLine();

      String line;
      double time = 0D;
      int falseFreeCounter = 0;
      int falseLinkCounter = 0;
      int trueFreeCounter = 0;
      int trueLinkCounter = 0;
      while ((line = buffer.readLine()) != null) {
        String[] vals = line.split(",");
        if (Double.valueOf(vals[0]) > time) {
          time = Double.valueOf(vals[0]);

          StringJoiner sj = new StringJoiner(",", "", "\n");
          sj.add(String.valueOf(falseFreeCounter));
          sj.add(String.valueOf(falseLinkCounter));
          sj.add(String.valueOf(trueFreeCounter));
          sj.add(String.valueOf(trueLinkCounter));
          bufferW.write(sj.toString());

          falseFreeCounter = 0;
          falseLinkCounter = 0;
          trueFreeCounter = 0;
          trueLinkCounter = 0;

        }
        
        if (Boolean.valueOf(vals[2])) {
          if (vals[3].equals("free")) {
            ++trueFreeCounter;
          } else {
            ++trueLinkCounter;
          }
        } else {
          if (vals[3].equals("free")) {
            ++falseFreeCounter;
          } else {
            ++falseLinkCounter;
          }
        }
        
      }
      
      StringJoiner sj = new StringJoiner(",", "", "\n");
      sj.add(String.valueOf(falseFreeCounter));
      sj.add(String.valueOf(falseLinkCounter));
      sj.add(String.valueOf(trueFreeCounter));
      sj.add(String.valueOf(trueLinkCounter));
      bufferW.write(sj.toString());

      bufferW.close();
      writer.close();

      buffer.close();
      reader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  /**
   * 0 = Mito(false,free), 1 = Mito(true,free), 2 = Mito(false, link), 3 =
   * Mito(true,link)
   * 
   * @throws IOException
   */
  public static void main(String[] args) throws IOException {
    for (int i : new int[] {20,40,60,80,100,120,140,160,180,200}) {
      for (int j = 0; j < REPS; ++j) {
        write(i,j);
      }
    }

  }

}
