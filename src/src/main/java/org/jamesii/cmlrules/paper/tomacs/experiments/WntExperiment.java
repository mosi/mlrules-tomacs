package org.jamesii.cmlrules.paper.tomacs.experiments;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.jamesii.cmlrules.debug.experiment.ExpInstrumenter;
import org.jamesii.cmlrules.experiment.Experiment;
import org.jamesii.cmlrules.experiment.Job;
import org.jamesii.cmlrules.experiment.stop.SimTimeStopConditionFactory;
import org.jamesii.cmlrules.experiment.stop.StopConditionFactory;
import org.jamesii.cmlrules.model.Model;
import org.jamesii.cmlrules.model.simple.SimpleModel;
import org.jamesii.cmlrules.model.standard.StandardModel;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.observation.Instrumenter;
import org.jamesii.cmlrules.parser.simple.listener.SimpleRulesListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesLexer;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ModelContext;
import org.jamesii.cmlrules.parser.standard.functions.PredefinedFunctions;
import org.jamesii.cmlrules.parser.standard.listener.ConstantCrawlerListener;
import org.jamesii.cmlrules.parser.standard.listener.FunctionCrawlerListener;
import org.jamesii.cmlrules.parser.standard.listener.FunctionDefinitionListener;
import org.jamesii.cmlrules.parser.standard.listener.RulesListener;
import org.jamesii.cmlrules.parser.standard.listener.SpeciesTypeCrawlerListener;
import org.jamesii.cmlrules.simulator.Simulator;
import org.jamesii.cmlrules.simulator.simple.SimpleSimulatorSSA;
import org.jamesii.cmlrules.simulator.standard.StandardSimulator;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.cmlrules.util.Pair;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;
import org.jamesii.core.util.misc.Files;

public class WntExperiment extends Experiment {

  public enum Mode {
    STANDARD_WITH, STANDARD_WITHOUT, STATIC_WITH, STATIC_WITHOUT;
  }

  public WntExperiment(String modelLocation, Instrumenter instrumenter,
      StopConditionFactory stopCondition, int cores, Mode mode) {
    super(modelLocation, instrumenter, stopCondition, cores);
    this.mode = mode;
  }

  public static int CORES = 1;

  public static int REPS = 20;

  public static double END_TIME = 300;
  
  public static double OBSERVATION_TIME = 1;

  private final Mode mode;

  private static List<Pair<Integer,Integer>> getAxins(String path, int config, int rep) {
    List<Pair<Integer,Integer>> result = new ArrayList<>();
    
    try {
    FileReader reader = new FileReader("./wntResults/" + path + "/" + config + "/" + rep + "---Axin.csv");
    BufferedReader buffer = new BufferedReader(reader);
    buffer.readLine();
    
    String line;
    Pair<Integer, Integer> values = new Pair<>();
    double time = 0;
    while ((line = buffer.readLine()) != null) {
      String[] vals = line.split(",");
      if (!Double.valueOf(vals[0]).equals(time)) {
        time = Double.valueOf(vals[0]);
        if (Double.compare(time, END_TIME) > 0) {
          break;
        }
        if (values.fst() == null) {
          values.setFst(0);
        }
        if (values.snd() == null) {
          values.setSnd(0);
        }
        result.add(values);
        values = new Pair<>();
      }
      if (vals[2].startsWith("p")) {
        if (values.fst() != null) {
          if (values.snd() == null) {
            values.setSnd(0);
          }
          result.add(values);
          values = new Pair<>();
        }
        values.setFst(Double.valueOf(vals[1]).intValue());
      } else {
        if (values.snd() != null) {
          if (values.fst() == null) {
            values.setFst(0);
          }
          result.add(values);
          values = new Pair<>();
        }
        values.setSnd(Double.valueOf(vals[1]).intValue());
      }
    }
    if (values.fst() == null) {
      values.setFst(0);
    }
    if (values.snd() == null) {
      values.setSnd(0);
    }
    result.add(values);
    
    buffer.close();
    reader.close();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    return result;
  }
  
  private static  List<Integer> get(String path, int config, int rep, String name) {
    List<Integer> result = new ArrayList<>();
    
    try {
    FileReader reader = new FileReader("./wntResults/" + path + "/" + config + "/" + rep + "---" + name + ".csv");
    BufferedReader buffer = new BufferedReader(reader);
    buffer.readLine();
    
    String line;
    while ((line = buffer.readLine()) != null) {
      result.add(Double.valueOf(line.split(",")[1]).intValue());
    }
    
    buffer.close();
    reader.close();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    return result;
  }
  
  @Override
  protected Model createModel(Map<String, Object> parameter) throws IOException {
    File file = new File("./models/wnt.mlrj");
    if (!file.exists()) {
      throw new IllegalArgumentException(String.format(
          "Could not find file %s", "./models/wnt.mlrj"));
    }
    MLRulesLexer lexer = new MLRulesLexer(new ANTLRInputStream(
        FileUtils.readFileToString(file)));
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    MLRulesParser parser = new MLRulesParser(tokens);
    ModelContext context = parser.model();
    if (parser.getNumberOfSyntaxErrors() > 0) {
      throw new IllegalArgumentException(String.format(
          "Could not create the model from %s", "./models/wnt.mlrj"));
    }
    ParseTreeWalker walker = new ParseTreeWalker();

    MLEnvironment env = new MLEnvironment();
    env.setGlobalValue(StandardModel.RNG,
        new MersenneTwister(System.currentTimeMillis()));
    PredefinedFunctions.addAll(env);
    env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);

    MLRulesListener functionCrawler = new FunctionCrawlerListener(env);
    walker.walk(functionCrawler, context);
    MLRulesListener speciesCrawler = new SpeciesTypeCrawlerListener(env);
    walker.walk(speciesCrawler, context);
    MLRulesListener functionDefCrawler = new FunctionDefinitionListener(env);
    walker.walk(functionDefCrawler, context);
    MLRulesListener constantsCrawler = new ConstantCrawlerListener(env,
        parameter);
    walker.walk(constantsCrawler, context);

    switch (mode) {
    case STANDARD_WITH:
    case STANDARD_WITHOUT:
      RulesListener rulesListener = new RulesListener(env);
      walker.walk(rulesListener, context);
      return rulesListener.create();
    case STATIC_WITH:
    case STATIC_WITHOUT:
      default:
      SimpleRulesListener simpleRulesListener = new SimpleRulesListener(env);
      walker.walk(simpleRulesListener, context);
      return simpleRulesListener.create();
    }

  }

  protected Simulator getSimulator(Model model, Job job) {
    switch (mode) {
    case STANDARD_WITH: return new StandardSimulator((StandardModel) model, INSTRUMENTER.create(job, model),true);
    case STANDARD_WITHOUT: return new StandardSimulator((StandardModel) model, INSTRUMENTER.create(job,model),false);
    case STATIC_WITH: return new SimpleSimulatorSSA((SimpleModel) model, INSTRUMENTER.create(job, model),true);
    case STATIC_WITHOUT: return new SimpleSimulatorSSA((SimpleModel) model, INSTRUMENTER.create(job, model), false);
    }
    throw new RuntimeException("Invalid simulator mode");
  }
  
  public static void readVariables(String[] args) throws ParseException {
    Options options = new Options();
    options.addOption(Option.builder("reps").argName("num").hasArg().desc("number of replications (default = 10)").build());
    options.addOption(Option.builder("endTime").argName("num").hasArg().desc("simulation end time (default = 300.0)").build());
    options.addOption(Option.builder("obs").argName("num").hasArg().desc("observation interval (default = 1.0)").build());
    options.addOption(Option.builder("parallel").argName("num").hasArg().desc("number of parallel simulations (default = 1)").build());
    
    
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp( "ml-rules", options );
    
    CommandLineParser parser = new DefaultParser();
    CommandLine cmd = parser.parse(options, args);
    
    if (cmd.hasOption("reps")) {
      REPS = Integer.valueOf(cmd.getOptionValue("reps"));
      if (REPS < 0) {
        throw new IllegalArgumentException("Negative number of replications is not allowed.");
      }
    } else {
      REPS = 20;
    }
    if (cmd.hasOption("endTime")) {
      END_TIME = Double.valueOf(cmd.getOptionValue("endTime"));
      if (Double.compare(END_TIME, 0.0) <= 0) {
        throw new IllegalArgumentException("Simulation end time must be positive.");
      }
    } else {
      END_TIME = 300.0;
    }
    if (cmd.hasOption("obs")) {
      OBSERVATION_TIME = Double.valueOf(cmd.getOptionValue("obs"));
      if (Double.compare(OBSERVATION_TIME,0.0) <= 0 || Double.compare(OBSERVATION_TIME, END_TIME) > 0) {
        throw new IllegalArgumentException("Observation interval must be positive and equal or smaller than the simulation end time.");
      }
    } else {
      OBSERVATION_TIME = 1.0;
    }
    if (cmd.hasOption("parallel")) {
      CORES = Integer.valueOf(cmd.getOptionValue("parallel"));
      if (CORES < 1) {
        throw new IllegalArgumentException("Number of parallel simulations must be positive.");
      }
    } else {
      CORES = 1;
    }
    
  }
  
  public static void main(String[] args) {

    try {
     readVariables(args);
    }catch (Exception e) {
      e.printStackTrace();
      System.exit(-1);
    }
    
    for (Mode mode : Mode.values()) {
      String path = "";
      switch (mode) {
      case STANDARD_WITH: path = "standardWithDG";
      break;
      case STANDARD_WITHOUT: path = "standardWithoutDG";
      break;
      case STATIC_WITH: path = "staticWithDG";
      break;
      case STATIC_WITHOUT: path = "staticWithoutDG";
      break;
      default:
        throw new RuntimeException("Wrong mode");
      }
      
    try {
      File file = new File("./wntResults/" + path);
      Files.deleteRecursively(file);
      file.mkdirs();
      FileWriter writer = new FileWriter("./wntResults/" + path + "/runtimes");
      BufferedWriter buffer = new BufferedWriter(writer);

      boolean first = true;
      for (int i : new int[] { 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }) {
        String modelLocation = "./models/wnt.mlrj";
        StopConditionFactory stop = new SimTimeStopConditionFactory(END_TIME);
        File directory = new File("./wntResults/" + path + "/" + i + "/");
        directory.mkdirs();

        Experiment exp = new WntExperiment(modelLocation, new ExpInstrumenter(
            directory, OBSERVATION_TIME), stop, CORES,mode);

        for (int j = 0; j < REPS; ++j) {
          HashMap<String, Object> params = new HashMap<>();
          params.put("nCells", i);
          exp.addJob(new Job(j, params));
        }

        exp.execute();

        if (!first) {
          StringJoiner sj = new StringJoiner(",", "", "\n");
          exp.getRuns().forEach(r -> sj.add(String.valueOf(r.getRuntime())));
          buffer.write(sj.toString());
          buffer.flush();
        }
        first = false;
      }

      buffer.flush();
      buffer.close();
      writer.close();
      
      for (int i = 1; i <= 10; ++i) {
        for (int j = 0; j < REPS; ++j) {
          List<Pair<Integer,Integer>> axins = getAxins(path, i,j);
          List<Integer> wnt = get(path,i,j,"Wnt");
          List<Integer> bcat = get(path,i,j,"Bcat");
            
            writer = new FileWriter("./wntResults/" + path + "/result_" + i + "_" + j + "_agg");
            BufferedWriter bufferW = new BufferedWriter(writer);

            bufferW.write("Wnt,AxinP,AxinU,Bcat\n");
            
            
            for (int k = 0; k < bcat.size(); ++k) {
              StringJoiner sj = new StringJoiner(",","","\n");
              sj.add(String.valueOf(wnt.size() <= k ? 0 : wnt.get(k)));
              sj.add(String.valueOf(axins.get(k).fst()));
              sj.add(String.valueOf(axins.get(k).snd()));
              sj.add(String.valueOf(bcat.get(k)));
              bufferW.write(sj.toString());
            }

            bufferW.close();
            writer.close();

        }
      }
      

    } catch (IOException e) {
      e.printStackTrace();
    }
    }
  }

}
