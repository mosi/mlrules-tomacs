package org.jamesii.cmlrules.paper.tomacs.experiments;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.ode.nonstiff.DormandPrince853Integrator;
import org.jamesii.cmlrules.debug.experiment.ExpInstrumenter;
import org.jamesii.cmlrules.experiment.Experiment;
import org.jamesii.cmlrules.experiment.Job;
import org.jamesii.cmlrules.experiment.stop.SimTimeStopConditionFactory;
import org.jamesii.cmlrules.experiment.stop.StopConditionFactory;
import org.jamesii.cmlrules.model.Model;
import org.jamesii.cmlrules.model.hybrid.HybridModel;
import org.jamesii.cmlrules.model.standard.StandardModel;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.observation.Instrumenter;
import org.jamesii.cmlrules.parser.hybrid.HybridRulesListener;
import org.jamesii.cmlrules.parser.hybrid.HybridSimpleRulesListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesLexer;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ModelContext;
import org.jamesii.cmlrules.parser.standard.functions.PredefinedFunctions;
import org.jamesii.cmlrules.parser.standard.listener.ConstantCrawlerListener;
import org.jamesii.cmlrules.parser.standard.listener.FunctionCrawlerListener;
import org.jamesii.cmlrules.parser.standard.listener.FunctionDefinitionListener;
import org.jamesii.cmlrules.parser.standard.listener.SpeciesTypeCrawlerListener;
import org.jamesii.cmlrules.simulator.Simulator;
import org.jamesii.cmlrules.simulator.hybrid.HybridSimulator;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;
import org.jamesii.core.util.misc.Files;

public class DictyExperiment extends Experiment {

  public DictyExperiment(String modelLocation, Instrumenter instrumenter,
      StopConditionFactory stopCondition, int cores) {
    super(modelLocation, instrumenter, stopCondition, cores);
  }

  public static int CORES = 1;

  public static int REPS = 20;

  public static double END_TIME = 5000.0;
  
  public static double OBSERVATION_TIME = 100;

  public static final String RUNTIMES = "./dictyResults/runtimes";

  @Override
  protected Model createModel(Map<String, Object> parameter) throws IOException {
    File file = new File("./models/dicty.mlrj");
    if (!file.exists()) {
      throw new IllegalArgumentException(String.format(
          "Could not find file %s", "./models/dicty.mlrj"));
    }
    MLRulesLexer lexer = new MLRulesLexer(new ANTLRInputStream(
        FileUtils.readFileToString(file)));
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    MLRulesParser parser = new MLRulesParser(tokens);
    ModelContext context = parser.model();
    if (parser.getNumberOfSyntaxErrors() > 0) {
      throw new IllegalArgumentException(String.format(
          "Could not create the model from %s", "./models/dicty.mlrj"));
    }
    ParseTreeWalker walker = new ParseTreeWalker();

    MLEnvironment env = new MLEnvironment();
    env.setGlobalValue(StandardModel.RNG,
        new MersenneTwister(System.currentTimeMillis()));
    PredefinedFunctions.addAll(env);
    env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);

    MLRulesListener functionCrawler = new FunctionCrawlerListener(env);
    walker.walk(functionCrawler, context);
    MLRulesListener speciesCrawler = new SpeciesTypeCrawlerListener(env);
    walker.walk(speciesCrawler, context);
    MLRulesListener functionDefCrawler = new FunctionDefinitionListener(env);
    walker.walk(functionDefCrawler, context);
    MLRulesListener constantsCrawler = new ConstantCrawlerListener(env,
        parameter);
    walker.walk(constantsCrawler, context);

    HybridSimpleRulesListener hybridListener = new HybridSimpleRulesListener(
        env);
    walker.walk(hybridListener, context);
    HybridRulesListener rulesListener = new HybridRulesListener(env,
        hybridListener.getRules(), hybridListener.getReactantProducts());
    walker.walk(rulesListener, context);
    return rulesListener.create();

  }

  protected Simulator getSimulator(Model model, Job job) {
    return new HybridSimulator((HybridModel) model, INSTRUMENTER.create(job,
        model), new DormandPrince853Integrator(1.e-07, 10.0, 1.0, 1.0e-1));
  }

  public static void readVariables(String[] args) throws ParseException {
    Options options = new Options();
    options.addOption(Option.builder("reps").argName("num").hasArg().desc("number of replications (default = 20)").build());
    options.addOption(Option.builder("endTime").argName("num").hasArg().desc("simulation end time (default = 5000.0)").build());
    options.addOption(Option.builder("obs").argName("num").hasArg().desc("observation interval (default = 100.0)").build());
    options.addOption(Option.builder("parallel").argName("num").hasArg().desc("number of parallel simulations (default = 1)").build());
    
    
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp( "ml-rules", options );
    
    CommandLineParser parser = new DefaultParser();
    CommandLine cmd = parser.parse(options, args);
    
    if (cmd.hasOption("reps")) {
      REPS = Integer.valueOf(cmd.getOptionValue("reps"));
      if (REPS < 0) {
        throw new IllegalArgumentException("Negative number of replications is not allowed.");
      }
    } else {
      REPS = 20;
    }
    if (cmd.hasOption("endTime")) {
      END_TIME = Double.valueOf(cmd.getOptionValue("endTime"));
      if (Double.compare(END_TIME, 0.0) <= 0) {
        throw new IllegalArgumentException("Simulation end time must be positive.");
      }
    } else {
      END_TIME = 5000.0;
    }
    if (cmd.hasOption("obs")) {
      OBSERVATION_TIME = Double.valueOf(cmd.getOptionValue("obs"));
      if (Double.compare(OBSERVATION_TIME,0.0) <= 0 || Double.compare(OBSERVATION_TIME, END_TIME) > 0) {
        throw new IllegalArgumentException("Observation interval must be positive and equal or smaller than the simulation end time.");
      }
    } else {
      OBSERVATION_TIME = 100.0;
    }
    if (cmd.hasOption("parallel")) {
      CORES = Integer.valueOf(cmd.getOptionValue("parallel"));
      if (CORES < 1) {
        throw new IllegalArgumentException("Number of parallel simulations must be positive.");
      }
    } else {
      CORES = 1;
    }
    
  }
  
  public static void main(String[] args) {

    try {
      readVariables(args);
     }catch (Exception e) {
       e.printStackTrace();
       System.exit(-1);
     }
    
    try {
      File f = new File("./dictyResults/");
      Files.deleteRecursively(f);
      f.mkdirs();

      FileWriter writer = new FileWriter(RUNTIMES);
      BufferedWriter buffer = new BufferedWriter(writer);

      boolean first = true;
      for (int i : new int[] { 1, 1, 2, 3, 4, 5, 6, 7, 8 }) {
        String modelLocation = "./models/dicty.mlrj";
        StopConditionFactory stop = new SimTimeStopConditionFactory(END_TIME);
        File directory = new File("./dictyResults/" + i + "/");
        directory.mkdirs();

        Experiment exp = new DictyExperiment(modelLocation, new ExpInstrumenter(
            directory, OBSERVATION_TIME), stop, CORES);

        for (int j = 0; j < REPS; ++j) {
          HashMap<String, Object> params = new HashMap<>();
          if (i == 1) {
            params.put("xmax", 5.0);
            params.put("ymax", 10.0);
          } else if (i == 2) {
            params.put("xmax", 10.);
            params.put("ymax", 10.);
          } else if (i == 3) {
            params.put("xmax", 15.);
            params.put("ymax", 10.);
          } else if (i == 4) {
            params.put("xmax", 20.);
            params.put("ymax", 10.);
          } else if (i == 5) {
            params.put("xmax", 25.);
            params.put("ymax", 10.);
          } else if (i == 6) {
            params.put("xmax", 15.);
            params.put("ymax", 20.);
          } else if (i == 7) {
            params.put("xmax", 14.);
            params.put("ymax", 25.);
          } else if (i == 8) {
            params.put("xmax", 20.);
            params.put("ymax", 20.);
          }

          exp.addJob(new Job(j, params));
        }

        exp.execute();

        if (!first) {
          StringJoiner sj = new StringJoiner(",", "", "\n");
          exp.getRuns().forEach(r -> sj.add(String.valueOf(r.getRuntime())));
          buffer.write(sj.toString());
          buffer.flush();
        }
        first = false;
      }

      buffer.flush();
      buffer.close();
      writer.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
