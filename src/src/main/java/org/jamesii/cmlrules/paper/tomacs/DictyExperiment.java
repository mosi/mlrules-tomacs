package org.jamesii.cmlrules.paper.tomacs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.StringJoiner;

import org.jamesii.cmlrules.debug.experiment.ExpInstrumenter;
import org.jamesii.cmlrules.experiment.Experiment;
import org.jamesii.cmlrules.experiment.Job;
import org.jamesii.cmlrules.experiment.stop.SimTimeStopConditionFactory;
import org.jamesii.cmlrules.experiment.stop.StopConditionFactory;

public class DictyExperiment {

  public static final int CORES = 10;
  
  public static final int REPS = 1;
  
  public static final double END_TIME = 1.0;
  
  public static final String RUNTIMES = "F:/th162/tomacs/dictyShow/runtimes";
  
  public static void main(String[] args) {

    try {
    FileWriter writer = new FileWriter(RUNTIMES);
    BufferedWriter buffer = new BufferedWriter(writer);
    
    boolean first = true;
    for (int i : new int[] {1,1,2,3,4,5,6,7,8}) {
      String modelLocation = "./models/Dicty.mlrj";
      StopConditionFactory stop = new SimTimeStopConditionFactory(END_TIME);
      File directory = new File("./dictyResults/" + i + "/");
      directory.mkdirs();
      
      Experiment exp = new Experiment(modelLocation, new ExpInstrumenter(
          directory, 0.1), stop, CORES);

      for (int j = 0; j < REPS; ++j) {
    	HashMap<String, Object> params = new HashMap<>();
    	if (i == 1) {
    	params.put("xmax",5.0);
    	params.put("ymax", 10.0);
    	} else if (i == 2) {
        params.put("xmax", 10.);
        params.put("ymax", 10.);    		
    	} else if (i == 3) {
            params.put("xmax", 15.);
            params.put("ymax", 10.);      		
    	} else if (i == 4) {
            params.put("xmax", 20.);
            params.put("ymax", 10.);  
    	} else if (i == 5) {
            params.put("xmax", 25.);
            params.put("ymax", 10.);  
    	} else if (i == 6) {
            params.put("xmax", 15.);
            params.put("ymax", 20.);  
    	} else if (i == 7) {
            params.put("xmax", 14.);
            params.put("ymax", 25.);  
    	} else if (i == 8) {
            params.put("xmax", 20.);
            params.put("ymax", 20.);  
    	}      
    	
        exp.addJob(new Job(j, params));
      }

      exp.execute();
      
      if (!first) {
	      StringJoiner sj = new StringJoiner(",","","\n");
	      exp.getRuns().forEach(r -> sj.add(String.valueOf(r.getRuntime())));
	      buffer.write(sj.toString());
	      buffer.flush();
      }
      first = false;
    }
    
    buffer.flush();
    buffer.close();
    writer.close();
    
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
