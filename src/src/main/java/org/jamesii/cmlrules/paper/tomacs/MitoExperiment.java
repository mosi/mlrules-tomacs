package org.jamesii.cmlrules.paper.tomacs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.StringJoiner;

import org.jamesii.cmlrules.debug.experiment.ExpInstrumenter;
import org.jamesii.cmlrules.experiment.Experiment;
import org.jamesii.cmlrules.experiment.Job;
import org.jamesii.cmlrules.experiment.stop.SimTimeStopConditionFactory;
import org.jamesii.cmlrules.experiment.stop.StopConditionFactory;

public class MitoExperiment {

  public static final int CORES = 10;
  
  public static final int REPS = 20;
  
  public static final double END_TIME = 100;
  
  public static final String RUNTIMES = "F:/th162/tomacs/link/simpleNoDG/runtimes";
  
  public static void main(String[] args) {

    try {
    FileWriter writer = new FileWriter(RUNTIMES);
    BufferedWriter buffer = new BufferedWriter(writer);
    
    boolean first = true;
    for (int i : new int[] {20,20,40,60,80,100,120,140,160,180,200}) {
      String modelLocation = "./tomacs/mitoNew.mlrj";
      StopConditionFactory stop = new SimTimeStopConditionFactory(END_TIME);
      File directory = new File("F:/th162/tomacs/link/simpleNoDG/" + i + "/");
      directory.mkdirs();
      
      Experiment exp = new Experiment(modelLocation, new ExpInstrumenter(
          directory, 1.0), stop, CORES);

      for (int j = 0; j < REPS; ++j) {
    	HashMap<String, Object> params = new HashMap<>();
    	params.put("mitoNum", i);
        exp.addJob(new Job(j, params));
      }

      exp.execute();
      
      if (!first) {
	      StringJoiner sj = new StringJoiner(",","","\n");
	      exp.getRuns().forEach(r -> sj.add(String.valueOf(r.getRuntime())));
	      buffer.write(sj.toString());
	      buffer.flush();
      }
      first = false;
    }
    
    buffer.flush();
    buffer.close();
    writer.close();
    
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
