package org.jamesii.cmlrules.paper.tomacs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.StringJoiner;

import org.jamesii.cmlrules.debug.experiment.ExpInstrumenter;
import org.jamesii.cmlrules.experiment.Experiment;
import org.jamesii.cmlrules.experiment.Job;
import org.jamesii.cmlrules.experiment.stop.SimTimeStopConditionFactory;
import org.jamesii.cmlrules.experiment.stop.StopConditionFactory;

public class SimpleExperiment {

  public static final int CORES = 10;
  
  public static final int REPS = 20;
  
  public static final double END_TIME = 300;
  
  public static final String RUNTIMES = "F:/th162/tomacs/wnt/standardNoDG/runtimes";
  
  public static void main(String[] args) {

    try {
    FileWriter writer = new FileWriter(RUNTIMES);
    BufferedWriter buffer = new BufferedWriter(writer);
    
    boolean first = true;
    for (int i : new int[] {1,1,2,3,4,5,6,7,8,9,10}) {
      String modelLocation = "./tomacs/wnt.mlrj";
      StopConditionFactory stop = new SimTimeStopConditionFactory(END_TIME);
      File directory = new File("F:/th162/tomacs/wnt/standardNoDG/" + i + "/");
      directory.mkdirs();
      
      Experiment exp = new Experiment(modelLocation, new ExpInstrumenter(
          directory, 1.0), stop, CORES);

      for (int j = 0; j < REPS; ++j) {
    	HashMap<String, Object> params = new HashMap<>();
    	params.put("nCells", i);
        exp.addJob(new Job(j, params));
      }

      exp.execute();
      
      if (!first) {
	      StringJoiner sj = new StringJoiner(",","","\n");
	      exp.getRuns().forEach(r -> sj.add(String.valueOf(r.getRuntime())));
	      buffer.write(sj.toString());
	      buffer.flush();
      }
      first = false;
    }
    
    buffer.flush();
    buffer.close();
    writer.close();
    
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
