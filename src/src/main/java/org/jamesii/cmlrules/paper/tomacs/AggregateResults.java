package org.jamesii.cmlrules.paper.tomacs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.jamesii.cmlrules.util.Pair;

public class AggregateResults {

  public static final int CONFIGS = 10;
  
  public static final int REPS = 20;
  
  public static String PATH = "D:/tmp/tomacs/wnt/standardNoDG/";
  
  private static List<Pair<Integer,Integer>> getAxins(int config, int rep) {
    List<Pair<Integer,Integer>> result = new ArrayList<>();
    
    try {
    FileReader reader = new FileReader(PATH + config + "/" + rep + "---Axin.csv");
    BufferedReader buffer = new BufferedReader(reader);
    buffer.readLine();
    
    String line;
    Pair<Integer, Integer> values = new Pair<>();
    double time = 0;
    while ((line = buffer.readLine()) != null) {
      String[] vals = line.split(",");
      if (!Double.valueOf(vals[0]).equals(time)) {
        time = Double.valueOf(vals[0]);
        if (values.fst() == null) {
          values.setFst(0);
        }
        if (values.snd() == null) {
          values.setSnd(0);
        }
        result.add(values);
        values = new Pair<>();
      }
      if (vals[2].startsWith("p")) {
        if (values.fst() != null) {
          if (values.snd() == null) {
            values.setSnd(0);
          }
          result.add(values);
          values = new Pair<>();
        }
        values.setFst(Double.valueOf(vals[1]).intValue());
      } else {
        if (values.snd() != null) {
          if (values.fst() == null) {
            values.setFst(0);
          }
          result.add(values);
          values = new Pair<>();
        }
        values.setSnd(Double.valueOf(vals[1]).intValue());
      }
    }
    if (values.fst() == null) {
      values.setFst(0);
    }
    if (values.snd() == null) {
      values.setSnd(0);
    }
    result.add(values);
    
    buffer.close();
    reader.close();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    return result;
  }
  
  private static  List<Integer> get(int config, int rep, String name) {
    List<Integer> result = new ArrayList<>();
    
    try {
    FileReader reader = new FileReader(PATH + config + "/" + rep + "---" + name + ".csv");
    BufferedReader buffer = new BufferedReader(reader);
    buffer.readLine();
    
    String line;
    while ((line = buffer.readLine()) != null) {
      result.add(Double.valueOf(line.split(",")[1]).intValue());
    }
    
    buffer.close();
    reader.close();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    return result;
  }
    
 
  /**
   * WNT = 1 Axin(u) = 2 Axin(p) = 3 BcatCell = 4 BcatNuc = 5
   * @throws IOException 
   */
  public static void main(String[] args) throws IOException {
    for (int i = 1; i <= CONFIGS; ++i) {
      for (int j = 0; j < REPS; ++j) {
        List<Pair<Integer,Integer>> axins = getAxins(i,j);
        List<Integer> wnt = get(i,j,"Wnt");
        List<Integer> bcat = get(i,j,"Bcat");
          
          FileWriter writer = new FileWriter(PATH + "result_" + i + "_" + j + "_agg");
          BufferedWriter bufferW = new BufferedWriter(writer);

          for (int k = 0; k < 301; ++k) {
            StringJoiner sj = new StringJoiner(",","","\n");
            sj.add(String.valueOf(wnt.size() <= k ? 0 : wnt.get(k)));
            sj.add(String.valueOf(axins.get(k).fst()));
            sj.add(String.valueOf(axins.get(k).snd()));
            sj.add(String.valueOf(bcat.get(k)));
            bufferW.write(sj.toString());
          }

          bufferW.close();
          writer.close();

      }
    }

  }

  
}
