/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.simulator.simple;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;

import org.jamesii.cmlrules.model.simple.SimpleModel;
import org.jamesii.cmlrules.model.simple.reaction.PeriodicRule;
import org.jamesii.cmlrules.model.simple.reaction.Reaction;
import org.jamesii.cmlrules.model.standard.StandardModel;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.observation.Observer;
import org.jamesii.cmlrules.simulator.Simulator;
import org.jamesii.cmlrules.util.DoubleNumber;
import org.jamesii.cmlrules.util.Pair;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.random.distributions.ExponentialDistribution;
import org.jamesii.core.math.random.generators.IRandom;

/**
 * 
 * @author Tobias Helms
 *
 */
public class SimpleSimulatorSSA extends Simulator {

  private double rateSum = 0D;

  private final ExponentialDistribution expDist;

  private final SimpleModel model;

  private final Map<Reaction, Map<Species, Set<Reaction>>> dependencies = new HashMap<>();

  private final Map<Species, Double> contextRates = new HashMap<>();

  private final boolean useDependencyGraph;

  public SimpleSimulatorSSA(SimpleModel model, Set<Observer> observer,
      boolean useDependencyGraph) {
    super(model, observer);
    this.model = model;
    this.useDependencyGraph = useDependencyGraph;
    expDist = new ExponentialDistribution((IRandom) model.getEnv().getValue(
        StandardModel.RNG));
    model.getReactions().entrySet().stream().forEach(e -> {
      final DoubleNumber rates = new DoubleNumber(0D);
      e.getValue().forEach(r -> rates.addAndGet(r.getCalculatedPropensity()));
      rateSum += rates.get();
      contextRates.put(e.getKey(), rates.get());
    });

    getObserver().forEach(o -> o.update(this));
    createDependencyGraph();
  }

  /**
   * Put the species of the given reaction to the species reactions map (if not
   * already present) and add the given reaction to the corresponding reaction
   * lists.
   */
  private void addSpeciesReactions(Reaction r,
      Map<Species, Set<Reaction>> speciesReactions) {
    r.getChangeVector()
        .values()
        .stream()
        .flatMap(m -> m.entrySet().stream()/* .filter(e -> e.getValue() != 0) */
        .map(e -> e.getKey()))
        .forEach(
            s -> speciesReactions.computeIfAbsent((Species) s,
                k -> new HashSet<>()).add(r));
  }

  private void addReactionDependencies(Reaction r,
      Map<Species, Set<Reaction>> speciesReactions) {
    r.getChangeVector()
        .values()
        .stream()
        .flatMap(m -> m.keySet().stream())
        .forEach(
            s -> {
              Map<Species, Set<Reaction>> reactions = dependencies
                  .computeIfAbsent(r, reaction -> new HashMap<>());
              speciesReactions.getOrDefault(s, Collections.emptySet()).forEach(
                  reaction -> reactions.computeIfAbsent(reaction.getContext(),
                      rr -> new HashSet<>()).add(reaction));
            });
  }

  private void createDependencyGraph() {
    Map<Species, Set<Reaction>> speciesReactions = new IdentityHashMap<>();
    model.getReactions().values().stream().flatMap(l -> l.stream())
        .forEach(r -> addSpeciesReactions(r, speciesReactions));
    model.getTimedReactions().values().stream().flatMap(l -> l.stream())
        .forEach(r -> addSpeciesReactions(r, speciesReactions));
    model.getReactions().values().stream().flatMap(l -> l.stream())
        .forEach(r -> addReactionDependencies(r, speciesReactions));
    model.getTimedReactions().values().stream().flatMap(l -> l.stream())
        .forEach(r -> addReactionDependencies(r, speciesReactions));
  }

  private void updatePropensitiesLoop(Species species,
      Collection<Reaction> reactions) {
    double change = 0D;
    for (Reaction r : reactions) {
      change -= r.getCalculatedPropensity();
      r.update();
      change += r.getCalculatedPropensity();
    }
    rateSum += change;
    contextRates.put(species, contextRates.get(species) + change);
  }

  private void updatePropensities(Reaction reaction) {
    Map<Species, Set<Reaction>> reactions = dependencies.getOrDefault(reaction,
        Collections.emptyMap());
    if (useDependencyGraph) {
      for (Entry<Species, Set<Reaction>> e : reactions.entrySet()) {
        updatePropensitiesLoop(e.getKey(), e.getValue());
      }
    } else {
      for (Entry<Species, List<Reaction>> e : model.getReactions().entrySet()) {
        updatePropensitiesLoop(e.getKey(), e.getValue());
      }
    }

  }

  private Pair<Optional<Reaction>, Double> select() {
    Optional<Reaction> reaction = model
        .getInfiniteReactions()
        .stream()
        .filter(
            r -> Double.isInfinite(r.getCalculatedPropensity())
                && r.executable()).findAny();
    if (reaction.isPresent()) {
      return new Pair<>(reaction, 0D);
    }
    double pivot = ((IRandom) getModel().getEnv().getValue(StandardModel.RNG))
        .nextDouble() * rateSum;

    final DoubleNumber sum = new DoubleNumber(0D);
    Optional<Entry<Species, Double>> context = contextRates.entrySet().stream()
        .filter(value -> sum.addAndGet(value.getValue()) > pivot).findFirst();
    if (!context.isPresent()) {
      return new Pair<>(Optional.empty(), 0D);
    }
    final DoubleNumber sum2 = new DoubleNumber(sum.get()
        - context.get().getValue());
    List<Reaction> reactions = model.getReactions().get(context.get().getKey());
    return new Pair<>(reactions.stream()
        .filter(r -> sum2.addAndGet(r.getCalculatedPropensity()) > pivot)
        .findFirst(), rateSum);
  }

  private Optional<Reaction> selectTimedReaction(double min) {
    List<Reaction> reactions = new ArrayList<>();
    for (Iterator<Entry<Species, List<Reaction>>> i = model.getTimedReactions()
        .entrySet().iterator(); i.hasNext();) {
      Entry<Species, List<Reaction>> e = i.next();
      List<Reaction> newReactions = new ArrayList<>();
      for (Iterator<Reaction> j = e.getValue().iterator(); j.hasNext();) {
        Reaction r = j.next();
        if (r.getCalculatedPropensity().equals(min)) {
          if (r.executable()) {
            reactions.add(r);
          }
          if (r.getRule() instanceof PeriodicRule) {
            newReactions.add(r);
          }
          j.remove();
        }
      }
      for (Reaction nr : newReactions) {
        Reaction newReaction = new Reaction(nr.getContext(),
            nr.getChangeVector(), new ValueNode<Double>(min
                + ((PeriodicRule) nr.getRule()).getPeriod()), nr.getRule(),
            nr.getEnv());
        newReaction.updateAlways();
        e.getValue().add(newReaction);
      }
      if (e.getValue().isEmpty()) {
        i.remove();
      }
    }

    if (!reactions.isEmpty()) {
      return Optional.of(reactions.get(((IRandom) getModel().getEnv().getValue(
          StandardModel.RNG)).nextInt(reactions.size())));
    } else {
      return Optional.empty();
    }
  }

  /**
   * Return the minimum time stamp of all scheduled reactions.
   */
  private OptionalDouble getMinimumTimedReactionTime() {
    return model
        .getTimedReactions()
        .values()
        .stream()
        .flatMapToDouble(
            list -> list.stream().mapToDouble(r -> r.getCalculatedPropensity()))
        .min();
  }

  private Optional<Reaction> checkTimedReactions(Optional<Reaction> reaction) {
    if (!model.getTimedReactions().isEmpty()) {
      OptionalDouble nextTimed = getMinimumTimedReactionTime();
      if (nextTimed.isPresent() && getNextTime() > nextTimed.getAsDouble()) {
        Optional<Reaction> timedReaction = selectTimedReaction(nextTimed
            .getAsDouble());
        if (timedReaction.isPresent()) {
          setNextTime(nextTimed.getAsDouble());
          return timedReaction;
        }
      }
    }
    return reaction;
  }

  @Override
  public void nextStep() {
    Pair<Optional<Reaction>, Double> selected = select();
    Optional<Reaction> reaction = selected.fst();
    if (!reaction.isPresent()) {
      setNextTime(getObserver().stream().map(o -> o.nextObservationPoint())
          .mapToDouble(p -> p.orElse(Double.POSITIVE_INFINITY)).min()
          .orElse(Double.POSITIVE_INFINITY));
    } else if (Double.compare(selected.snd(), 0D) != 0) {
      setNextTime(getCurrentTime()
          + expDist.getRandomNumber(1.0 / selected.snd()));
    }
    reaction = checkTimedReactions(reaction);
    getObserver().forEach(o -> o.update(this));
    if (reaction.isPresent()) {
      reaction.get().execute();
      updatePropensities(reaction.get());
      getModel().getEnv().setGlobalValue(StandardModel.TIME, getNextTime());
    } else if (model.getTimedReactions().isEmpty()) {
      setNextTime(Double.POSITIVE_INFINITY);
    }
    setSteps(getSteps() + 1);
    setCurrentTime(getNextTime());
  }
}
