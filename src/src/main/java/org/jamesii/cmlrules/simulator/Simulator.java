/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.simulator;

import java.util.Set;

import org.jamesii.cmlrules.model.Model;
import org.jamesii.cmlrules.observation.Observer;

public abstract class Simulator {

  private final Model model;

  private double currentTime = 0D;

  private double nextTime = 0D;

  private int steps = 0;

  private final Set<Observer> observer;

  public Simulator(Model model, Set<Observer> observer) {
    this.model = model;
    this.observer = observer;
  }

  public Set<Observer> getObserver() {
    return observer;
  }

  protected void setCurrentTime(double currentTime) {
    this.currentTime = currentTime;
  }

  protected void setNextTime(double nextTime) {
    this.nextTime = nextTime;
  }

  protected void setSteps(int steps) {
    this.steps = steps;
  }

  public abstract void nextStep();
  
  public Model getModel() {
    return model;
  }

  public int getSteps() {
    return steps;
  }

  public double getCurrentTime() {
    return currentTime;
  }

  public double getNextTime() {
    return nextTime;
  }

}
