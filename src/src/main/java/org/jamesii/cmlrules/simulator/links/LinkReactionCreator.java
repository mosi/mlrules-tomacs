/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.simulator.links;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jamesii.cmlrules.model.links.LinkModel;
import org.jamesii.cmlrules.model.standard.rule.Reactant;
import org.jamesii.cmlrules.model.standard.rule.Rule;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.simulator.standard.ContextMatchings;
import org.jamesii.cmlrules.simulator.standard.Matching;
import org.jamesii.cmlrules.simulator.standard.Reaction;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.cmlrules.util.NodeHelper;
import org.jamesii.cmlrules.util.Nu;
import org.jamesii.cmlrules.util.RestSolution;
import org.jamesii.cmlrules.util.TmpEnvironment;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.core.math.parsetree.variables.Identifier;

public class LinkReactionCreator {

  private Map<Species, Map<SpeciesType, Set<Species>>> changedSpecies = null;

  private final LinkModel model;

  private final Map<Nu, Map<Species, Species>> linkedSpecies;

  protected static class Tree {

    private final Matching current;

    private final Tree parent;

    private final Map<Species, Integer> consumption;

    private final Map<String, Nu> nus;

    private void getContexts(List<Matching> result) {
      if (parent != null) {
        result.add(current);
        parent.getContexts(result);
      }
    }

    public Tree(Matching current, Tree parent, Map<String, Nu> nus) {
      this.current = current;
      this.parent = parent;
      this.nus = nus;
      if (parent != null) {
        this.nus.putAll(parent.getNus());
      }
      consumption =
          new HashMap<>(parent != null ? parent.getConsumption()
              : Collections.emptyMap());
      if (current != null) {
        consumption.compute(current.getSpecies(), (k, v) -> (v == null ? 0 : v)
            + current.getRemoval());
      }
    }

    public Matching getCurrent() {
      return current;
    }

    public List<Matching> create() {
      List<Matching> result = new ArrayList<>();
      getContexts(result);
      return result;
    }

    public Map<Species, Integer> getConsumption() {
      return consumption;
    }

    public Map<String, Nu> getNus() {
      return nus;
    }

  }

  public LinkReactionCreator(LinkModel model,
      Map<Nu, Map<Species, Species>> linkedSpecies) {
    this.model = model;
    this.linkedSpecies = linkedSpecies;
  }

  private List<Object> calcAttributeValues(Reactant reactant, MLEnvironment env) {
    try {
      List<Object> values = new ArrayList<>(reactant.getType().getAttributes());
      for (int i = 0; i < reactant.getType().getAttributes(); ++i) {
        if (reactant.getAttributeNodes().get(i) instanceof Identifier<?>) {
          @SuppressWarnings("unchecked")
          String var =
              ((Identifier<String>) reactant.getAttributeNodes().get(i))
                  .getIdent();
          Object o = env.getValue(var);
          if (o != null) {
            values.add(o);
          } else {
            values.add(reactant.getAttributeNodes().get(i));
          }
        } else {
          Node node = reactant.getAttributeNodes().get(i).calc(env);
          if (node instanceof ValueNode<?>) {
            values.add(((ValueNode<?>) node).getValue());
          }
        }
      }
      return values;
    } catch (Exception e) {
      Logger.getGlobal().info(
          String.format("Could not compute attribute values of %s", reactant));
    }
    return Collections.emptyList();
  }

  /**
   * Check the amount condition of the given reactant species.
   */
  protected boolean validAmount(Species species, Tree matching,
      Reactant reactant, MLEnvironment env) {
    return Double.compare(
        species.getAmount(),
        matching.getConsumption().getOrDefault(species, 0)
            + NodeHelper.getDouble(reactant.getAmount(), env)) >= 0;
  }

  private boolean checkAttributes(Species species, Reactant reactant,
      List<Object> reactantValues, MLEnvironment env) {
    for (int i = 0; i < species.getType().getAttributes(); ++i) {
      if (reactantValues.get(i) instanceof Identifier<?>) {
        continue;
      }
      if (species.getAttribute(i) == Nu.FREE
          && reactant.getAttributeNodes().get(i) instanceof Identifier<?>) {
        return false;
      }
      if (!species.getAttribute(i).equals(reactantValues.get(i))) {
        return false;
      }
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  private boolean checkGuard(Reactant reactant, IEnvironment<String> env) {
    if (reactant.getGuard().isPresent()) {
      Node result = reactant.getGuard().get().calc(env);
      if (!(result instanceof ValueNode<?>
          && ((ValueNode<?>) result).getValue() instanceof Boolean && (((ValueNode<Boolean>) result)
            .getValue()))) {
        return false;
      }
    }
    return true;
  }

  /**
   * Return true if the species type and the attributes of the given species
   * match the type and the attributes of the given reactant. Sub reactants are
   * not checked in this method.
   */
  private Optional<MLEnvironment> match(Species species, Reactant reactant,
      List<Object> reactantValues, Tree matching, MLEnvironment env,
      boolean newEnvironment) {
    if (!checkAttributes(species, reactant, reactantValues, env)) {
      return Optional.empty();
    }
    if (!validAmount(species, matching, reactant, env)) {
      return Optional.empty();
    }

    IEnvironment<String> tmp = newEnvironment ? new TmpEnvironment() : env;
    for (int i = 0; i < species.getType().getAttributes(); ++i) {
      if (reactantValues.get(i) instanceof Identifier<?>) {
        @SuppressWarnings("unchecked")
        String var =
            ((Identifier<String>) reactant.getAttributeNodes().get(i))
                .getIdent();
        tmp.setValue(var, species.getAttribute(i));
      }
    }
    if (!checkGuard(reactant, tmp)) {
      return Optional.empty();
    }
    return Optional.of(newEnvironment ? ((TmpEnvironment) tmp).create(env)
        : env);
  }

  private void addMatching(Stream.Builder<Matching> builder, Species species,
      Reactant reactant, Tree matchings, List<Object> attributeValues,
      MLEnvironment env, boolean newEnvironment) {
    Optional<MLEnvironment> oe =
        match(species, reactant, attributeValues, matchings, env,
            newEnvironment);
    if (oe.isPresent()) {
      MLEnvironment e = oe.get();
      if (!reactant.getBoundTo().startsWith("$")) {
        Map<Species, Species> map = new HashMap<>();
        map.put(species, species);
        e.setValue(reactant.getBoundTo(), map);
      }
      if (!reactant.getSubReactants().isEmpty()) {
        Stream.Builder<ContextMatchings> builder2 = Stream.builder();
        Map<Species, Map<SpeciesType, Set<Species>>> changed = null;
        if (changedSpecies != null) {
          changed = changedSpecies;
          changedSpecies = null;
        }
        createContextMatchings(builder2, species, reactant.getSubReactants(), e);
        if (changed != null) {
          changedSpecies = changed;
        }

        builder2.build().forEach(
            cm -> {
              if (!reactant.getRest().startsWith("$")) {
                cm.getEnv().setValue(
                    reactant.getRest(),
                    new RestSolution(reactant.getRest(), cm.getContext(), cm
                        .getRemovals()));
              }
              builder.add(new Matching(reactant, species, cm.getMatchings(), cm
                  .getEnv()));
            });
      } else {
        if (!reactant.getRest().startsWith("$")) {
          e.setValue(reactant.getRest(), new RestSolution(reactant.getRest(),
              species, Collections.emptyMap()));
        }
        builder
            .add(new Matching(reactant, species, Collections.emptyList(), e));
      }
    }
  }

  private Stream<Matching> createMatchings(Set<Species> candidates,
      Reactant reactant, Tree matchings, MLEnvironment env,
      boolean mustBeNonChanged, Species context, boolean newEnvironment) {
    Stream.Builder<Matching> builder = Stream.builder();
    List<Object> attributeValues = calcAttributeValues(reactant, env);
    candidates.forEach(s -> {
      if (!mustBeNonChanged
          || !changedSpecies.get(context)
              .getOrDefault(s.getType(), Collections.emptySet()).contains(s)) {
        addMatching(builder, s, reactant, matchings, attributeValues, env,
            newEnvironment);
      }
    });
    return builder.build();
  }

  private Map<String, Nu> computeLinkValues(Map<Integer, String> linkVariables,
      Species species) {
    Map<String, Nu> result = new HashMap<>();
    linkVariables
        .entrySet()
        .stream()
        .filter(e -> species.getAttribute(e.getKey()) != Nu.FREE)
        .forEach(
            e -> result.put(e.getValue(), (Nu) species.getAttribute(e.getKey())));
    return result;
  }

  private void createContextMatchingsHelper(
      Stream.Builder<ContextMatchings> builder, Species context,
      List<Reactant> reactants, int i, Tree matchings, MLEnvironment env,
      int iteration) {
    if (i == reactants.size()) {
      try {
        builder.add(new ContextMatchings(context, matchings.create(), env));
      } catch (Exception e) {
        Logger.getGlobal().info("Invalid context matching: " + e.getMessage());
      }
    } else {
      Reactant reactant = reactants.get(i);
      SpeciesType t = reactant.getType();

      Map<Integer, String> linkVariables =
          model.getReactantLinks().getOrDefault(reactant,
              Collections.emptyMap());
      List<Nu> nus = new ArrayList<>();
      linkVariables.values().forEach(v -> {
        if (matchings.getNus().containsKey(v)) {
          nus.add(matchings.getNus().get(v));
        }
      });

      Set<Species> linked = new HashSet<>();
      nus.forEach(n -> linked.addAll(linkedSpecies
          .getOrDefault(n, Collections.emptyMap()).keySet().stream()
          .filter(s -> s.getType() == t).collect(Collectors.toList())));

      Set<Species> candidates =
          (changedSpecies != null && i == 0) ? changedSpecies.getOrDefault(context,
              Collections.emptyMap()).getOrDefault(t, Collections.emptySet())
              : (nus.isEmpty() ? context.getSubSpecies(t).keySet() : linked);

      createMatchings(candidates, reactant, matchings, env,
          i >= reactants.size() - iteration, context,
          candidates.size() > 1 || i == 0).forEach(
          m -> createContextMatchingsHelper(
              builder,
              context,
              reactants,
              i + 1,
              new Tree(m, matchings, computeLinkValues(linkVariables,
                  m.getSpecies())), m.getEnv(), iteration));
    }
  }

  private void createContextMatchings(Stream.Builder<ContextMatchings> builder,
      Species context, List<Reactant> reactants, MLEnvironment env) {
    for (int i = 0; i < reactants.size(); ++i) {
      if ((changedSpecies == null || changedSpecies
          .getOrDefault(context, Collections.emptyMap()).keySet()
          .contains(reactants.get(0).getType()))
          && reactants.stream().allMatch(
              t -> context.getTypes().contains(t.getType()))) {
        createContextMatchingsHelper(builder, context, reactants, 0, new Tree(
            null, null, Collections.emptyMap()), env, i);
      }
      if (changedSpecies == null) {
        break;
      }
      reactants.add(reactants.remove(0));
    }
  }

  private void createReactions(Species context, Rule rule,
      List<Reaction> reactions, Map<Species, List<Reaction>> speciesReactions,
      List<Reaction> infiniteReactions, MLEnvironment env) {
    Stream.Builder<ContextMatchings> builder = Stream.builder();
    createContextMatchings(builder, context,
        new ArrayList<>(rule.getReactants()), env);
    builder.build().forEach(
        cm -> {
          Reaction r = new Reaction(cm, rule);
          if (Double.isInfinite(r.getRate())) {
            infiniteReactions.add(r);
          } else if (Double.compare(r.getRate(), 0D) != 0) {
            reactions.add(r);
            r.getContextMatchings()
                .getMatchings()
                .forEach(
                    m -> speciesReactions.computeIfAbsent(m.getSpecies(),
                        k -> new ArrayList<>()).add(r));
          }
        });
  }

  public void createReactions(Species species,
      Map<Species, Map<SpeciesType, Set<Species>>> changedSpecies,
      List<Rule> rules, Reactions reactions, List<Reaction> infiniteReactions,
      MLEnvironment env) {
    this.changedSpecies = changedSpecies;
    List<Reaction> speciesReactions =
        reactions.getContextReactions().computeIfAbsent(species,
            s -> new ArrayList<>());
    rules.forEach(r -> createReactions(species, r, speciesReactions,
        reactions.getSpeciesReactions(), infiniteReactions, env));
    this.changedSpecies = null;
  }

  public void createInitialReactions(Species species, List<Rule> rules,
      Reactions reactions, List<Reaction> infiniteReactions, MLEnvironment env) {
    if (!species.isLeaf()) {
      createReactions(species, null, rules, reactions, infiniteReactions, env);
      species.getSubSpeciesStream().forEach(
          s -> createInitialReactions(s, rules, reactions, infiniteReactions,
              env));
    }
  }

}
