/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.simulator.simple;

import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.jamesii.cmlrules.model.simple.SimpleModel;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.observation.Observer;
import org.jamesii.cmlrules.simulator.Simulator;

public class ODESimulator extends Simulator implements
    FirstOrderDifferentialEquations {

  private final SimpleModel model;

  private final double minimalStep;

  private final double[] state;

  private final Map<Species, Integer> speciesIndex = new IdentityHashMap<>();

  private final FirstOrderIntegrator integrator;

  public ODESimulator(SimpleModel model, Set<Observer> observer,
      double minimalStep, FirstOrderIntegrator integrator) {
    super(model, observer);
    this.model = model;
    this.minimalStep = minimalStep;
    this.integrator = integrator;
    collect(model.getSpecies(), new AtomicInteger(0));
    state = new double[speciesIndex.size()];
    speciesIndex.forEach((k, v) -> state[v] = k.getAmount());
  }

  private void collect(Species compartment, AtomicInteger counter) {
    compartment.getSubSpeciesStream().forEach(s -> {
      if (s.isLeaf()) {
        speciesIndex.put(s, counter.getAndIncrement());
      } else {
        collect(s, counter);
      }
    });
  }

  @Override
  public int getDimension() {
    return speciesIndex.size();
  }

  public void nextStep() {
    getObserver().forEach(o -> o.update(this));

    double min = Double.POSITIVE_INFINITY;
    for (Observer observer : getObserver()) {
      Optional<Double> next = observer.nextObservationPoint();
      if (!next.isPresent()) {
        min = getCurrentTime() + minimalStep;
        break;
      }
      min = Math.min(next.get(), min);
    }
    
    setNextTime(min);
    setSteps(getSteps() + 1);

    integrator.integrate(this, getCurrentTime(), state, getNextTime(), state);
    speciesIndex.forEach((k, v) -> k.setAmount(state[v]));
    model.getReactions().values().forEach(v -> v.forEach(r -> r.update()));
    setCurrentTime(getNextTime());
  }

  @Override
  public void computeDerivatives(double t, double[] y, double[] yDot) {
    speciesIndex.forEach((k, v) -> k.setAmount(y[v]));
    for (int i = 0; i < yDot.length; ++i) {
      yDot[i] = 0;
    }
    model
        .getReactions()
        .values()
        .forEach(
            v -> v.forEach(r -> {
              r.updateAlways();
              r.getChangeVector()
                  .values()
                  .forEach(
                      l -> l.forEach((k, v2) -> yDot[speciesIndex.get(k)] +=
                          (double) v2 * r.getCalculatedPropensity()));
            }));
  }

}
