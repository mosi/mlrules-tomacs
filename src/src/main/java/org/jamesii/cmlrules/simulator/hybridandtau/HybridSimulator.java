package org.jamesii.cmlrules.simulator.hybridandtau;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.jamesii.cmlrules.model.hybrid.HybridModel;
import org.jamesii.cmlrules.model.standard.StandardModel;
import org.jamesii.cmlrules.model.standard.rule.PeriodicRule;
import org.jamesii.cmlrules.model.standard.rule.Rule;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.observation.Observer;
import org.jamesii.cmlrules.simulator.Simulator;
import org.jamesii.cmlrules.simulator.simple.SimpleReactionCreator;
import org.jamesii.cmlrules.simulator.standard.ChangedSpecies;
import org.jamesii.cmlrules.simulator.standard.Reaction;
import org.jamesii.cmlrules.simulator.standard.ReactionCreator;
import org.jamesii.cmlrules.util.DoubleNumber;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.random.distributions.ExponentialDistribution;
import org.jamesii.core.math.random.generators.IRandom;

public class HybridSimulator extends Simulator implements
    FirstOrderDifferentialEquations {

  private final ExponentialDistribution expDist;

  private org.apache.commons.math3.distribution.ExponentialDistribution expDist2;

  private final ReactionCreator complexCreator = new ReactionCreator();

  private final SimpleReactionCreator simpleCreator = new SimpleReactionCreator();

  private final List<Reaction> infiniteReactions = new ArrayList<>();

  private final Map<Species, List<Reaction>> complexReactions = new HashMap<>();

  private final Map<Species, List<org.jamesii.cmlrules.model.simple.reaction.Reaction>> simpleReactions = new HashMap<>();
  
  private final Map<Species, List<org.jamesii.cmlrules.model.simple.reaction.Reaction>> tauReactions = new HashMap<>();

  private final Map<Species, Double> complexRates = new HashMap<>();

  private final HybridModel model;

  private double[] state;

  private final Map<Species, Integer> speciesIndex = new IdentityHashMap<>();

  private final FirstOrderIntegrator integrator;

  private double nextComplexReaction = Double.POSITIVE_INFINITY;

  private PriorityQueue<Integer> freePositions = new PriorityQueue<>();

  private double quantil = 0.03;

  private double oldSum = 0;

  private final List<Rule> timedRules;

  public HybridSimulator(HybridModel model, Set<Observer> observer,
      FirstOrderIntegrator integrator) {
    super(model, observer);
    this.timedRules = new ArrayList<>(model.getTimedRules());
    this.model = model;
    this.integrator = integrator;

    simpleReactions.clear();
    simpleCreator.createInitialReactions(model.getSpecies(),
        model.getSimpleRules(), simpleReactions, new ArrayList<>(),
        model.getEnv(), model.getReactantProducts());
    collect(model.getSpecies(), new AtomicInteger(0));
    updateSimpleReactions();

    expDist = new ExponentialDistribution((IRandom) model.getEnv().getValue(
        StandardModel.RNG));

    complexCreator.createInitialReactions(model.getSpecies(),
        model.getComplexRules(), complexReactions, infiniteReactions,
        model.getEnv(),true);

    for (Iterator<Entry<Species, List<Reaction>>> iterator = complexReactions
        .entrySet().iterator(); iterator.hasNext();) {
      Entry<Species, List<Reaction>> e = iterator.next();
      if (e.getValue().isEmpty()) {
        iterator.remove();
      }
    }

    complexReactions
        .entrySet()
        .stream()
        .forEach(
            e -> {
              if (!e.getValue().isEmpty()) {
                complexRates.put(e.getKey(),
                    e.getValue().stream().mapToDouble(r -> r.getRate()).sum());
              }
              ;
            });
    getObserver().forEach(o -> o.update(this));
  }

  private void updateNextComplexReactionTime() {
    double sum = 0D;
    for (Double d : complexRates.values()) {
      sum += d;
    }
    if (Double.compare(sum, 0) > 0) {
    	nextComplexReaction = getCurrentTime() + expDist.getRandomNumber(1.0 / sum);
    } else {
    	nextComplexReaction = Double.POSITIVE_INFINITY;
    }
    expDist2 = new org.apache.commons.math3.distribution.ExponentialDistribution(
        null, 1.0 / (Double.compare(sum, 0D) == 0 ? 0.0001 : sum));

    double relation = 1 - (Double.compare(sum, oldSum) == 0 ? 1
        : (sum / oldSum));
    if (relation > -0.03 && relation < 0.03) {
      quantil = Math.min(quantil * 2.0, 1);
    } else {
      quantil = Math.max(quantil / 2., 0.001);
    }

    oldSum = sum;
  }

  private void updateSimpleReactions() {
    if (!freePositions.isEmpty()) {
      PriorityQueue<Entry<Species, Integer>> pq = new PriorityQueue<Entry<Species, Integer>>(
          freePositions.size(), new Comparator<Entry<Species, Integer>>() {
            @Override
            public int compare(Entry<Species, Integer> e1,
                Entry<Species, Integer> e2) {
              return e1.getValue().compareTo(e2.getValue());
            }
          });
      for (Entry<Species, Integer> e : speciesIndex.entrySet()) {
        pq.add(e);
        if (pq.size() > freePositions.size()) {
          pq.remove();
        }
      }
      for (Entry<Species, Integer> e : pq) {
        if (freePositions.element() >= speciesIndex.size()) {
          break;
        }
        speciesIndex.put(e.getKey(), freePositions.remove());
      }
      freePositions.clear();
    }

    if (state == null || state.length != speciesIndex.size()) {
      state = new double[speciesIndex.size()];
    }
    for (Entry<Species, Integer> e : speciesIndex.entrySet()) {
      state[e.getValue()] = e.getKey().getAmount();
    }
    // speciesIndex.forEach((k, v) -> state[v] = k.getAmount());
  }

  private void collect(Species compartment, AtomicInteger counter) {
    compartment.getSubSpeciesStream().forEach(s -> {
      if (s.isLeaf()) {
        speciesIndex.put(s, counter.getAndIncrement());
      } else {
        collect(s, counter);
      }
    });
  }

  private Optional<Reaction> select(Map<Species, List<Reaction>> reactions,
      Map<Species, Double> rates) {
    if (!infiniteReactions.isEmpty()) {
      return Optional.of(infiniteReactions.get(((IRandom) getModel().getEnv()
          .getValue(StandardModel.RNG)).nextInt(infiniteReactions.size())));
    }
    double rateSum = rates.values().stream().reduce(0.0, Double::sum);
    double pivot = ((IRandom) getModel().getEnv().getValue(StandardModel.RNG))
        .nextDouble() * rateSum;

    DoubleNumber sum = new DoubleNumber(0D);
    return reactions.values().stream().flatMap(l -> l.stream())
        .filter(r -> sum.addAndGet(r.getRate()) > pivot).findFirst();
  }

  private boolean removeReaction(Reaction r, List<Species> removedSpecies,
      Map<SpeciesType, Set<Species>> changedSpecies) {
    return r
        .getContextMatchings()
        .getMatchings()
        .stream()
        .anyMatch(
            m -> removedSpecies.contains(m.getSpecies())
                || changedSpecies.getOrDefault(m.getSpecies().getType(),
                    Collections.emptySet()).contains(m.getSpecies()));
  }

  private void removeSubReactions(ChangedSpecies changed) {
    for (Species s : changed.getRemovedSpecies()) {
      s.getSubSpeciesStream().filter(sub -> sub.isLeaf()).forEach(sub -> {
        Integer i = speciesIndex.remove(sub);
        if (i != null) {
          freePositions.add(i);
        }
      });
      s.getCompartmentStream().forEach(sub -> {
        complexReactions.remove(sub);
        simpleReactions.remove(sub);
        complexRates.remove(sub);
        Iterator<Reaction> i = infiniteReactions.iterator();
        while (i.hasNext()) {
          Reaction r = i.next();
          if (r.getContextMatchings().getContext() == s) {
            i.remove();
          }
        }
      });
    }
  }

  private void removeReactions(List<Species> removedSpecies,
      Map<SpeciesType, Set<Species>> changedSpecies, Species s) {
    Iterator<Reaction> i;
    if (complexReactions.containsKey(s)) {
      i = complexReactions.get(s).iterator();
      while (i.hasNext()) {
        Reaction r = i.next();
        if (removeReaction(r, removedSpecies, changedSpecies)) {
          i.remove();
        }
      }
      i = infiniteReactions.iterator();
      while (i.hasNext()) {
        Reaction r = i.next();
        if (removeReaction(r, removedSpecies, changedSpecies)) {
          i.remove();
        }
      }
    }

    simpleReactions.getOrDefault(s, Collections.emptyList()).clear();
  }

  private void updateChangedSpecies(ChangedSpecies changed, Reaction selected) {
    Set<Species> current = new HashSet<>();
    selected
        .getContextMatchings()
        .getContext()
        .getContextStream()
        .forEach(
            s -> {
              Map<SpeciesType, Set<Species>> changedSpecies;
              if (s == selected.getContextMatchings().getContext()) {
                changedSpecies = changed.getAllChangedSpecies();
              } else {
                changedSpecies = new HashMap<>();
                changedSpecies
                    .put(current.iterator().next().getType(), current);
              }
              removeReactions(changed.getRemovedSpecies(), changedSpecies, s);
              complexRates.remove(s);
              complexCreator.createReactions(s, changedSpecies,
                  ((HybridModel) getModel()).getComplexRules(),
                  complexReactions, infiniteReactions, getModel().getEnv(),true);
              for (Iterator<Entry<Species, List<Reaction>>> iterator = complexReactions
                  .entrySet().iterator(); iterator.hasNext();) {
                Entry<Species, List<Reaction>> e = iterator.next();
                if (e.getValue().isEmpty()) {
                  iterator.remove();
                }
              }
              simpleCreator.createReactions(s, null, model.getSimpleRules(),
                  simpleReactions, Collections.emptyList(), model.getEnv(),
                  model.getReactantProducts());
              if (complexReactions.containsKey(s)) {
                complexRates.put(s, complexReactions.get(s).stream()
                    .mapToDouble(r -> r.getRate()).sum());
              }
              current.clear();
              current.add(s);
            });
  }

  private void addReactionsToAddedSpecies(ChangedSpecies changed) {
    changed.getAddedSpecies().forEach(
        s -> s.getCompartmentStream().forEach(
            sub -> {
              complexCreator.createReactions(sub, null,
                  ((HybridModel) getModel()).getComplexRules(),
                  complexReactions, infiniteReactions, getModel().getEnv(),true);
              simpleCreator.createReactions(sub, null, model.getSimpleRules(),
                  simpleReactions, Collections.emptyList(), model.getEnv(),
                  model.getReactantProducts());
              if (complexReactions.containsKey(sub)) {
                complexRates.put(sub, complexReactions.get(sub).stream()
                    .mapToDouble(r -> r.getRate()).sum());
              }
            }));
  }

  private void addSpeciesIndexValues(Species addedSpecies) {
    if (addedSpecies.isLeaf()) {
      int pos = freePositions.isEmpty() ? speciesIndex.size() : freePositions
          .remove();
      speciesIndex.put(addedSpecies, pos);
    } else {
      addedSpecies.getSubSpeciesStream().forEach(s -> addSpeciesIndexValues(s));
    }
  }

  protected void updateReactions(ChangedSpecies changed, Reaction selected) {
    removeSubReactions(changed);
    updateChangedSpecies(changed, selected);
    addReactionsToAddedSpecies(changed);
    changed.getAddedSpecies().forEach(as -> addSpeciesIndexValues(as));
  }

  @SuppressWarnings("unchecked")
  private Optional<Reaction> selectTimedReaction() {
    Map<Species, List<Reaction>> timedReactions = new HashMap<>();
    Map<Species, Double> timedRates = new HashMap<>();
    double time = ((ValueNode<Double>) timedRules.get(0).getRate()).getValue();
    Iterator<Rule> iterator = timedRules.iterator();
    List<Rule> rules = new ArrayList<>();
    List<PeriodicRule> newTimedRules = new ArrayList<>();
    while (iterator.hasNext()) {
      Rule next = iterator.next();
      if (((ValueNode<Double>) next.getRate()).getValue() > time) {
        break;
      }
      iterator.remove();
      if (next instanceof PeriodicRule) {
        newTimedRules.add((PeriodicRule) next);
      }
      rules.add(next);
    }

    newTimedRules.forEach(r -> timedRules.add(new PeriodicRule(r.getPeriod(), r
        .getReactants(), r.getProduct(), new ValueNode<Double>(time
        + r.getPeriod()), r.getAssignments())));

    complexCreator.createInitialReactions(
        ((HybridModel) getModel()).getSpecies(), rules, timedReactions,
        new ArrayList<>(), getModel().getEnv(),true);
    timedReactions
        .entrySet()
        .stream()
        .forEach(
            e -> timedRates.put(e.getKey(),
                e.getValue().stream().mapToDouble(r -> r.getRate()).sum()));
    return select(timedReactions, timedRates);
  }

  @SuppressWarnings("unchecked")
  private Optional<Reaction> checkTimedReactions(Optional<Reaction> reaction) {
    if (!timedRules.isEmpty()) {
      double time = ((ValueNode<Double>) timedRules.get(0).getRate())
          .getValue();
      if (Double.compare(getNextTime(), time) == 0) {
        Optional<Reaction> timedReaction = selectTimedReaction();
        if (timedReaction.isPresent()) {
          setNextTime(time);
          return timedReaction;
        }
      }
    }
    return reaction;
  }

  private void executeSSAStep() {
    Optional<Reaction> selected = select(complexReactions, complexRates);
    Optional<Reaction> reaction = selected;
    reaction = checkTimedReactions(reaction);
    if (reaction.isPresent()) {
      ChangedSpecies changed = reaction.get().execute();

      getModel().getEnv().setGlobalValue(StandardModel.TIME, getNextTime());
      updateReactions(changed, reaction.get());

    } else if (timedRules.isEmpty()) {
      setNextTime(Double.POSITIVE_INFINITY);
    }
  }

  private void updateComplexReactionPropensities(Species context,
      List<Reaction> reactions) {
    double change = 0D;
    for (Reaction r : reactions) {
      r.updateRate();
      change += r.getRate();
    }
    complexRates.put(context, Math.max(0D, change));
  }

  @Override
  public void nextStep() {
    getObserver().forEach(o -> o.update(this));
    double min = Double.POSITIVE_INFINITY;
    for (Observer observer : getObserver()) {
      Optional<Double> next = observer.nextObservationPoint();
      min = Math.min(next.orElse(min), min);
    }

    updateNextComplexReactionTime();
    double min1 = Math.min(min,
        getCurrentTime() + expDist2.inverseCumulativeProbability(quantil));
    min = Math.min(min1, nextComplexReaction);

    boolean timed = false;
    if (!timedRules.isEmpty()) {
      @SuppressWarnings("unchecked")
      double time = ((ValueNode<Double>) timedRules.get(0).getRate())
          .getValue();
      if (Double.compare(min, time) >= 0) {
        min = time;
        timed = true;
      }
    }

    setNextTime(min);

    if (Double.isFinite(min)) {
      try {
        integrator.integrate(this, getCurrentTime(), state, getNextTime(),
            state);
      } catch (NumberIsTooSmallException e) {
        Logger
            .getGlobal()
            .info(
                String
                    .format(
                        "skip continuous step due to too small time step from %s to %s",
                        getCurrentTime(), getNextTime()));
      }
      for (Entry<Species, Integer> e : speciesIndex.entrySet()) {
        e.getKey().setAmount(Math.max(0, state[e.getValue()]));
      }
    }

    if (timed || Double.compare(min1, nextComplexReaction) > 0) {
      executeSSAStep();
      updateSimpleReactions();
    }
    for (Entry<Species, List<Reaction>> e : complexReactions.entrySet()) {
      updateComplexReactionPropensities(e.getKey(), e.getValue());
    }

    setCurrentTime(getNextTime());
    setSteps(getSteps() + 1);
  }

  private void updateSpeciesAndYDot(Species s, int index, double[] y,
      double[] yDot) {
    s.setAmount(Math.max(0, y[index]));
    yDot[index] = 0;
  }

  private void addProp(Species s, double[] yDot, int change, double prop) {
    yDot[speciesIndex.get(s)] += change * prop;
  }

  @Override
  public void computeDerivatives(double t, double[] y, double[] yDot) {
    for (Entry<Species, Integer> e : speciesIndex.entrySet()) {
      updateSpeciesAndYDot(e.getKey(), e.getValue(), y, yDot);
    }

    for (List<org.jamesii.cmlrules.model.simple.reaction.Reaction> reactions : simpleReactions
        .values()) {
      for (org.jamesii.cmlrules.model.simple.reaction.Reaction r : reactions) {
        r.updateAlways();
        for (Entry<Species, Integer> e : r.getChangeVectorNonZero().entrySet()) {
          addProp(e.getKey(), yDot, e.getValue(), r.getCalculatedPropensity());
        }
      }
    }
  }

  @Override
  public int getDimension() {
    return speciesIndex.size();
  }

}
