/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.simulator.standard;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;

public class ChangedSpecies {

  private final List<Species> addedSpecies;

  private final List<Species> removedSpecies;

  private final Set<Species> changedSpecies;

  public ChangedSpecies(Set<Species> changedSpecies,
      List<Species> addedSpecies, List<Species> removedSpecies) {
    this.changedSpecies = changedSpecies;
    this.addedSpecies = addedSpecies;
    this.removedSpecies = removedSpecies;
  }

  public List<Species> getAddedSpecies() {
    return addedSpecies;
  }

  public List<Species> getRemovedSpecies() {
    return removedSpecies;
  }

  public Set<Species> getChangedSpecies() {
    return changedSpecies;
  }

  public Map<SpeciesType, Set<Species>> getAllChangedSpecies() {
    Map<SpeciesType, Set<Species>> result = new HashMap<>();
    addedSpecies.forEach(s -> result.computeIfAbsent(s.getType(),
        k -> new HashSet<>()).add(s));
    changedSpecies.forEach(s -> result.computeIfAbsent(s.getType(),
        k -> new HashSet<>()).add(s));
    return result;
  }

}
