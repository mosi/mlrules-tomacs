package org.jamesii.cmlrules.simulator.links;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.jamesii.cmlrules.model.links.LinkModel;
import org.jamesii.cmlrules.model.standard.StandardModel;
import org.jamesii.cmlrules.model.standard.rule.PeriodicRule;
import org.jamesii.cmlrules.model.standard.rule.Rule;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.observation.Observer;
import org.jamesii.cmlrules.simulator.Simulator;
import org.jamesii.cmlrules.simulator.standard.ChangedSpecies;
import org.jamesii.cmlrules.simulator.standard.Reaction;
import org.jamesii.cmlrules.util.Nu;
import org.jamesii.cmlrules.util.Pair;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.random.distributions.ExponentialDistribution;
import org.jamesii.core.math.random.generators.IRandom;

public class LinkSimulator extends Simulator {

  private final Map<Nu, Map<Species, Species>> linkedSpecies = new HashMap<>();

  private final Reactions reactions = new Reactions();

  private final List<Reaction> infiniteReactions = new ArrayList<>();

  private final Map<Species, Double> rates = new HashMap<>();

  private final ExponentialDistribution expDist;

  private final List<Rule> timedRules;

  private final LinkReactionCreator creator;
  
  private final boolean useDependencyGraph;

  protected static class DoubleNumber {

    private double number;

    public DoubleNumber(double number) {
      this.number = number;
    }

    public double addAndGet(double other) {
      number += other;
      return number;
    }

    public void set(double number) {
      this.number = number;
    }

    public double get() {
      return number;
    }

  }

  public LinkSimulator(LinkModel model, Set<Observer> observer, boolean useDependencyGraph) {
    super(model, observer);
    this.useDependencyGraph = useDependencyGraph;
    timedRules = new ArrayList<>(model.getTimedRules());
    expDist = new ExponentialDistribution((IRandom) model.getEnv().getValue(
        StandardModel.RNG));
    creator = new LinkReactionCreator(model, linkedSpecies);
    creator.createInitialReactions(model.getSpecies(), model.getRules(),
        reactions, infiniteReactions, model.getEnv());
    reactions
        .getContextReactions()
        .entrySet()
        .stream()
        .forEach(
            e -> rates.put(e.getKey(),
                e.getValue().stream().mapToDouble(r -> r.getRate()).sum()));
    getObserver().forEach(o -> o.update(this));
  }

  private void removeLinks(Species removed) {
    for (int i = 0; i < removed.getType().getAttributes(); ++i) {
      if (removed.getAttribute(i) instanceof Nu) {
        linkedSpecies.getOrDefault(removed.getAttribute(i),
            Collections.emptyMap()).remove(removed);
        if (linkedSpecies.getOrDefault(removed.getAttribute(i),
            Collections.emptyMap()).isEmpty()) {
          linkedSpecies.remove(removed.getAttribute(i));
        }
      }
    }
    removed.getSubSpeciesStream().forEach(sub -> removeLinks(sub));
  }

  private void addLinks(Species added) {
    for (int i = 0; i < added.getType().getAttributes(); ++i) {
      if (added.getAttribute(i) instanceof Nu
          && added.getAttribute(i) != Nu.FREE) {
        linkedSpecies.compute((Nu) added.getAttribute(i),
            (k, v) -> v == null ? new IdentityHashMap<>() : v)
            .put(added, added);
      }
    }
    added.getSubSpeciesStream().forEach(sub -> addLinks(sub));
  }

  protected void updateReactions(ChangedSpecies changed, Reaction selected) {
    changed.getRemovedSpecies().forEach(r -> removeLinks(r));
    changed.getAddedSpecies().forEach(a -> addLinks(a));
    
    if (useDependencyGraph) {
    removeSubReactions(changed);
    updateChangedSpecies(changed, selected);
    addReactionsToAddedSpecies(changed);
    } else {
    
    rates.clear();
    reactions.getContextReactions().clear();
    reactions.getSpeciesReactions().clear();
    creator.createInitialReactions(((LinkModel) getModel()).getSpecies(), ((LinkModel) getModel()).getRules(),
            reactions, infiniteReactions, ((LinkModel) getModel()).getEnv());
        reactions
            .getContextReactions()
            .entrySet()
            .stream()
            .forEach(
                e -> rates.put(e.getKey(),
                    e.getValue().stream().mapToDouble(r -> r.getRate()).sum()));
    }
  }

  private void removeReaction(Species s) {
    List<Reaction> r = reactions.getSpeciesReactions().remove(s);
    if (r != null) {
      r.forEach(rr -> {
        List<Reaction> lr = reactions.getContextReactions().getOrDefault(
            rr.getContextMatchings().getContext(), Collections.emptyList());
        lr.remove(rr);
        rr.getContextMatchings()
            .getMatchings()
            .forEach(
                m -> {
                  List<Reaction> lsr = reactions.getSpeciesReactions()
                      .getOrDefault(m.getSpecies(), Collections.emptyList());
                  lsr.remove(rr);
                  if (lsr.isEmpty()) {
                    reactions.getSpeciesReactions().remove(m.getSpecies());
                  }
                });

        if (lr.isEmpty()) {
          reactions.getContextReactions().remove(
              rr.getContextMatchings().getContext());
        }
      });
    }
  }

  private void removeReactions(List<Species> removedSpecies,
      Map<Species, Map<SpeciesType, Set<Species>>> changedSpecies) {
    removedSpecies.forEach(rs -> {
      removeReaction(rs);
    });
    changedSpecies.values().forEach(
        ms -> ms.values().forEach(ls -> ls.forEach(s -> removeReaction(s))));
    Iterator<Reaction> i = infiniteReactions.iterator();
    while (i.hasNext()) {
      Reaction r = i.next();
      if (r
          .getContextMatchings()
          .getMatchings()
          .stream()
          .anyMatch(
              m -> removedSpecies.contains(m.getSpecies())
                  || changedSpecies
                      .getOrDefault(r.getContextMatchings().getContext(),
                          Collections.emptyMap())
                      .getOrDefault(m.getSpecies().getType(),
                          Collections.emptySet()).contains(m.getSpecies()))) {
        i.remove();
      }
    }
  }

  private void removeSubReactions(ChangedSpecies changed) {
    changed.getRemovedSpecies().forEach(
        s -> s.getCompartmentStream().forEach(
            sub -> {
              reactions.getContextReactions().remove(sub);
              if (s != sub) {
                reactions.getSpeciesReactions().remove(sub);
              }
              sub.getSubSpeciesStream()
                  .filter(subLeaf -> subLeaf.isLeaf())
                  .forEach(
                      subLeaf -> reactions.getSpeciesReactions()
                          .remove(subLeaf));
              rates.remove(sub);
              Iterator<Reaction> i = infiniteReactions.iterator();
              while (i.hasNext()) {
                Reaction r = i.next();
                if (r.getContextMatchings().getContext() == s) {
                  i.remove();
                }
              }
            }));
  }

  private void updateChangedSpecies(ChangedSpecies changed, Reaction selected) {
    Set<Species> current = new HashSet<>();
    Map<Species, Map<SpeciesType, Set<Species>>> changedSpecies = new HashMap<>();
    selected.getContextMatchings().getContext().getContextStream()
        .forEach(s -> {
          if (s == selected.getContextMatchings().getContext()) {
            changedSpecies.put(s, changed.getAllChangedSpecies());
          } else {
            Map<SpeciesType, Set<Species>> c = new HashMap<>();
            c.put(current.iterator().next().getType(), new HashSet<>(current));
            changedSpecies.put(s, c);
          }
          current.clear();
          current.add(s);
        });

    removeReactions(changed.getRemovedSpecies(), changedSpecies);
    current.clear();
    selected
        .getContextMatchings()
        .getContext()
        .getContextStream()
        .forEach(
            s -> {
              Map<SpeciesType, Set<Species>> cs;
              if (s == selected.getContextMatchings().getContext()) {
                cs = changed.getAllChangedSpecies();
              } else {
                cs = new HashMap<>();
                cs.put(current.iterator().next().getType(), current);
              }
              rates.remove(s);
              creator.createReactions(s, changedSpecies,
                  ((StandardModel) getModel()).getRules(), reactions,
                  infiniteReactions, getModel().getEnv());
              rates.put(s, reactions.getContextReactions().get(s).stream()
                  .mapToDouble(r -> r.getRate()).sum());
              current.clear();
              current.add(s);
            });
  }

  private void addReactionsToAddedSpecies(ChangedSpecies changed) {
    changed.getAddedSpecies().forEach(
        s -> s.getCompartmentStream().forEach(
            sub -> {
              creator.createReactions(sub, null,
                  ((StandardModel) getModel()).getRules(), reactions,
                  infiniteReactions, getModel().getEnv());
              rates.put(sub, reactions.getContextReactions().get(sub).stream()
                  .mapToDouble(r -> r.getRate()).sum());
            }));
  }

  private Pair<Optional<Reaction>, Double> select(Reactions reactions,
      Map<Species, Double> rates) {
    if (!infiniteReactions.isEmpty()) {
      return new Pair<>(Optional.of(infiniteReactions.get(((IRandom) getModel()
          .getEnv().getValue(StandardModel.RNG)).nextInt(infiniteReactions
          .size()))), 0D);
    }
    double rateSum = rates.values().stream().reduce(0.0, Double::sum);
    double pivot = ((IRandom) getModel().getEnv().getValue(StandardModel.RNG))
        .nextDouble() * rateSum;

    DoubleNumber sum = new DoubleNumber(0D);
    return new Pair<>(reactions.getContextReactions().values().stream()
        .flatMap(l -> l.stream())
        .filter(r -> sum.addAndGet(r.getRate()) > pivot).findFirst(), rateSum);
  }

  @SuppressWarnings("unchecked")
  private Optional<Reaction> selectedTimedReaction() {
    Map<Species, List<Reaction>> timedReactions = new HashMap<>();
    Map<Species, Double> timedRates = new HashMap<>();
    double time = ((ValueNode<Double>) timedRules.get(0).getRate()).getValue();
    Iterator<Rule> iterator = timedRules.iterator();
    List<Rule> rules = new ArrayList<>();
    List<PeriodicRule> newTimedRules = new ArrayList<>();
    while (iterator.hasNext()) {
      Rule next = iterator.next();
      if (((ValueNode<Double>) next.getRate()).getValue() > time) {
        break;
      }
      iterator.remove();
      if (next instanceof PeriodicRule) {
        newTimedRules.add((PeriodicRule) next);
      }
      rules.add(next);
    }

    newTimedRules.forEach(r -> timedRules.add(new PeriodicRule(r.getPeriod(), r
        .getReactants(), r.getProduct(), new ValueNode<Double>(time
        + r.getPeriod()), r.getAssignments())));

    // creator.createInitialReactions(((StandardModel) getModel()).getSpecies(),
    // rules, timedReactions, new ArrayList<>(), getModel().getEnv());
    // timedReactions
    // .entrySet()
    // .stream()
    // .forEach(
    // e -> timedRates.put(e.getKey(),
    // e.getValue().stream().mapToDouble(r -> r.getRate()).sum()));
    // return select(timedReactions, timedRates).fst();
    return null;
  }

  @SuppressWarnings("unchecked")
  private Optional<Reaction> checkTimedReactions(Optional<Reaction> reaction) {
    if (!timedRules.isEmpty()) {
      double time = ((ValueNode<Double>) timedRules.get(0).getRate())
          .getValue();
      if (getNextTime() > time) {
        Optional<Reaction> timedReaction = selectedTimedReaction();
        if (timedReaction.isPresent()) {
          setNextTime(time);
        }
        return timedReaction;
      }
    }
    return reaction;
  }

  @Override
  public void nextStep() {
    Pair<Optional<Reaction>, Double> selected = select(reactions, rates);
    Optional<Reaction> reaction = selected.fst();
    if (!reaction.isPresent()) {
      setNextTime(Double.POSITIVE_INFINITY);
    } else if (Double.compare(selected.snd(), 0D) != 0) {
      setNextTime(getCurrentTime()
          + expDist.getRandomNumber(1.0 / selected.snd()));
    }
    reaction = checkTimedReactions(reaction);
    getObserver().forEach(o -> o.update(this));
    if (reaction.isPresent()) {
      ChangedSpecies changed = reaction.get().execute();
      getModel().getEnv().setGlobalValue(StandardModel.TIME, getNextTime());
      updateReactions(changed, reaction.get());
    } else if (timedRules.isEmpty()) {
      setNextTime(Double.POSITIVE_INFINITY);
    }
    setSteps(getSteps() + 1);
    setCurrentTime(getNextTime());
  }
}
