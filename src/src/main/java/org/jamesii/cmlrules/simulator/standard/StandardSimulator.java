/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.simulator.standard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.jamesii.cmlrules.model.standard.StandardModel;
import org.jamesii.cmlrules.model.standard.rule.PeriodicRule;
import org.jamesii.cmlrules.model.standard.rule.Rule;
import org.jamesii.cmlrules.model.standard.rule.RuleComparator;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.observation.Observer;
import org.jamesii.cmlrules.simulator.Simulator;
import org.jamesii.cmlrules.util.DoubleNumber;
import org.jamesii.cmlrules.util.Pair;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.random.distributions.ExponentialDistribution;
import org.jamesii.core.math.random.generators.IRandom;

public class StandardSimulator extends Simulator {

  private final Map<Species, List<Reaction>> reactions = new HashMap<>();

  private final List<Reaction> infiniteReactions = new ArrayList<>();

  private final Map<Species, Double> rates = new HashMap<>();

  private final ExponentialDistribution expDist;

  private final List<Rule> timedRules;

  private final ReactionCreator creator = new ReactionCreator();

  private final boolean useDependencyGraph;

  public StandardSimulator(StandardModel model, Set<Observer> observer,
      boolean useDependencyGraph) {
    super(model, observer);
    this.useDependencyGraph = useDependencyGraph;
    timedRules = new ArrayList<>(model.getTimedRules());
    expDist = new ExponentialDistribution((IRandom) model.getEnv().getValue(
        StandardModel.RNG));

    creator.createInitialReactions(model.getSpecies(), model.getRules(),
        reactions, infiniteReactions, model.getEnv(),false);
    reactions
        .entrySet()
        .stream()
        .forEach(
            e -> rates.put(e.getKey(),
                e.getValue().stream().mapToDouble(r -> r.getRate()).sum()));
    getObserver().forEach(o -> o.update(this));
  }

  private boolean removeReaction(Reaction r, List<Species> removedSpecies,
      Map<SpeciesType, Set<Species>> changedSpecies) {
    return r
        .getContextMatchings()
        .getMatchings()
        .stream()
        .anyMatch(
            m -> removedSpecies.contains(m.getSpecies())
                || changedSpecies.getOrDefault(m.getSpecies().getType(),
                    Collections.emptySet()).contains(m.getSpecies()));
  }

  private void removeReactions(List<Species> removedSpecies,
      Map<SpeciesType, Set<Species>> changedSpecies, Species s) {
    Iterator<Reaction> i;
    if (reactions.containsKey(s)) {
      i = reactions.get(s).iterator();
      while (i.hasNext()) {
        Reaction r = i.next();
        if (removeReaction(r, removedSpecies, changedSpecies)) {
          i.remove();
        }
      }
      i = infiniteReactions.iterator();
      while (i.hasNext()) {
        Reaction r = i.next();
        if (removeReaction(r, removedSpecies, changedSpecies)) {
          i.remove();
        }
      }
    }
  }

  private void removeSubReactions(ChangedSpecies changed) {
    changed.getRemovedSpecies().forEach(
        s -> s.getCompartmentStream().forEach(sub -> {
          reactions.remove(sub);
          rates.remove(sub);
          Iterator<Reaction> i = infiniteReactions.iterator();
          while (i.hasNext()) {
            Reaction r = i.next();
            if (r.getContextMatchings().getContext() == s) {
              i.remove();
            }
          }
        }));
  }

  private void updateChangedSpecies(ChangedSpecies changed, Reaction selected) {
    Set<Species> current = new HashSet<>();
    selected
        .getContextMatchings()
        .getContext()
        .getContextStream()
        .forEach(
            s -> {
              Map<SpeciesType, Set<Species>> changedSpecies;
              if (s == selected.getContextMatchings().getContext()) {
                changedSpecies = changed.getAllChangedSpecies();
              } else {
                changedSpecies = new HashMap<>();
                changedSpecies
                    .put(current.iterator().next().getType(), current);
              }
              removeReactions(changed.getRemovedSpecies(), changedSpecies, s);
              rates.remove(s);
              creator.createReactions(s, changedSpecies,
                  ((StandardModel) getModel()).getRules(), reactions,
                  infiniteReactions, getModel().getEnv(),false);
              if (reactions.containsKey(s)) {
                rates.put(s,
                    reactions.get(s).stream().mapToDouble(r -> r.getRate())
                        .sum());
              }
              current.clear();
              current.add(s);
            });
  }

  private void addReactionsToAddedSpecies(ChangedSpecies changed) {
    changed.getAddedSpecies().forEach(
        s -> s.getCompartmentStream().forEach(
            sub -> {
              creator.createReactions(sub, null,
                  ((StandardModel) getModel()).getRules(), reactions,
                  infiniteReactions, getModel().getEnv(),false);
              if (reactions.containsKey(sub)) {
                rates.put(sub,
                    reactions.get(sub).stream().mapToDouble(r -> r.getRate())
                        .sum());
              }
            }));
  }

  protected void updateReactions(ChangedSpecies changed, Reaction selected) {
    removeSubReactions(changed);
    updateChangedSpecies(changed, selected);
    addReactionsToAddedSpecies(changed);
  }

  private Pair<Optional<Reaction>, Double> select(
      Map<Species, List<Reaction>> reactions, Map<Species, Double> rates) {
    if (!infiniteReactions.isEmpty()) {
      return new Pair<>(Optional.of(infiniteReactions.get(((IRandom) getModel()
          .getEnv().getValue(StandardModel.RNG)).nextInt(infiniteReactions
          .size()))), 0D);
    }
    double rateSum = rates.values().stream().reduce(0.0, Double::sum);
    double pivot = ((IRandom) getModel().getEnv().getValue(StandardModel.RNG))
        .nextDouble() * rateSum;

    DoubleNumber sum = new DoubleNumber(0D);
    return new Pair<>(reactions.values().stream().flatMap(l -> l.stream())
        .filter(r -> sum.addAndGet(r.getRate()) > pivot).findFirst(), rateSum);
  }

  @SuppressWarnings("unchecked")
  private Optional<Reaction> selectTimedReaction() {
    Map<Species, List<Reaction>> timedReactions = new HashMap<>();
    Map<Species, Double> timedRates = new HashMap<>();
    double time = ((ValueNode<Double>) timedRules.get(0).getRate()).getValue();
    Iterator<Rule> iterator = timedRules.iterator();
    List<Rule> rules = new ArrayList<>();
    List<PeriodicRule> newTimedRules = new ArrayList<>();
    while (iterator.hasNext()) {
      Rule next = iterator.next();
      if (((ValueNode<Double>) next.getRate()).getValue() > time) {
        break;
      }
      iterator.remove();
      if (next instanceof PeriodicRule) {
        newTimedRules.add((PeriodicRule) next);
      }
      rules.add(next);
    }

    newTimedRules.forEach(r -> timedRules.add(new PeriodicRule(r.getPeriod(), r
        .getReactants(), r.getProduct(), new ValueNode<Double>(time
        + r.getPeriod()), r.getAssignments())));
    timedRules.sort(RuleComparator.instance);

    creator.createInitialReactions(((StandardModel) getModel()).getSpecies(),
        rules, timedReactions, new ArrayList<>(), getModel().getEnv(),false);
    timedReactions
        .entrySet()
        .stream()
        .forEach(
            e -> timedRates.put(e.getKey(),
                e.getValue().stream().mapToDouble(r -> r.getRate()).sum()));
    return select(timedReactions, timedRates).fst();
  }

  @SuppressWarnings("unchecked")
  private Optional<Reaction> checkTimedReactions(Optional<Reaction> reaction) {
    if (!timedRules.isEmpty()) {
      double time = ((ValueNode<Double>) timedRules.get(0).getRate())
          .getValue();
      if (getNextTime() > time) {
        Optional<Reaction> timedReaction = selectTimedReaction();
        if (timedReaction.isPresent()) {
          setNextTime(time);
          return timedReaction;
        }
      }
    }
    return reaction;
  }

  @Override
  public void nextStep() {
    Pair<Optional<Reaction>, Double> selected = select(reactions, rates);
    Optional<Reaction> reaction = selected.fst();
    if (!reaction.isPresent()) {
      setNextTime(getObserver().stream().map(o -> o.nextObservationPoint())
          .mapToDouble(p -> p.orElse(Double.POSITIVE_INFINITY)).min()
          .orElse(Double.POSITIVE_INFINITY));
    } else if (Double.compare(selected.snd(), 0D) != 0) {
      setNextTime(getCurrentTime()
          + expDist.getRandomNumber(1.0 / selected.snd()));
    }
    reaction = checkTimedReactions(reaction);
    getObserver().forEach(o -> o.update(this));
    if (reaction.isPresent()) {
      ChangedSpecies changed = reaction.get().execute();

      getModel().getEnv().setGlobalValue(StandardModel.TIME, getNextTime());

      if (useDependencyGraph) {
        updateReactions(changed, reaction.get());
      } else {
        rates.clear();
        reactions.clear();
        creator.createInitialReactions(
            ((StandardModel) getModel()).getSpecies(),
            ((StandardModel) getModel()).getRules(), reactions,
            infiniteReactions, getModel().getEnv(),false);
        reactions
            .entrySet()
            .stream()
            .forEach(
                e -> rates.put(e.getKey(),
                    e.getValue().stream().mapToDouble(r -> r.getRate()).sum()));
      }

    } else if (timedRules.isEmpty()) {
      setNextTime(Double.POSITIVE_INFINITY);
    }
    setSteps(getSteps() + 1);
    setCurrentTime(getNextTime());
  }
}
