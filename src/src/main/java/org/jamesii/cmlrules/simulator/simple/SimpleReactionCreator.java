/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.simulator.simple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.jamesii.cmlrules.model.simple.reaction.Reaction;
import org.jamesii.cmlrules.model.simple.reaction.Rule;
import org.jamesii.cmlrules.model.standard.rule.Reactant;
import org.jamesii.cmlrules.model.standard.species.MapSpecies;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.parser.standard.nodes.SpeciesAmountNode;
import org.jamesii.cmlrules.simulator.standard.AbstractReactionCreator;
import org.jamesii.cmlrules.simulator.standard.ContextMatchings;
import org.jamesii.cmlrules.simulator.standard.Matching;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.cmlrules.util.NodeHelper;
import org.jamesii.cmlrules.util.expressions.SimpleRateExpressionOneSpecies;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.control.IfThenElseNode;
import org.jamesii.core.math.parsetree.math.MultNode;
import org.jamesii.core.math.parsetree.variables.Identifier;

/**
 * The {@link SimpleReactionCreator} ignores amount conditions of rules, i.e.,
 * the amount of a species is not considered when checking the validity of this
 * species for a reactant.
 * 
 * @author Tobias Helms
 *
 */
public class SimpleReactionCreator extends AbstractReactionCreator {

  @Override
  protected boolean validAmount(Species species, Tree matching,
      Reactant reactant, MLEnvironment env) {
    return true;
  }

  private void checkMatching(Matching matching, Node rateNode, double rate,
      Map<Species, Map<Species, Integer>> changeVector,
      Map<Reactant, Reactant> reactantProducts, MLEnvironment env) {
    if (!matching.getReactant().isCompartment()) {
      changeVector.computeIfAbsent(matching.getSpecies().getContext(),
          s -> new HashMap<>()).compute(matching.getSpecies(),
          (k, v) -> (v == null ? 0 : v) - matching.getRemoval());
    } else {
      matching.getSubReactants().forEach(
          m -> checkMatching(m, rateNode, rate, changeVector, reactantProducts, env));
      reactantProducts
          .get(matching.getReactant())
          .getSubReactants()
          .forEach(
              r -> checkProducts(matching.getSpecies(), r, rateNode, rate, changeVector,
                  env));
    }
  }

  @SuppressWarnings("unchecked")
  private void checkProducts(Species context, Reactant reactant, Node rateNode, double rate,
      Map<Species, Map<Species, Integer>> changeVector, MLEnvironment env) {
    if (!reactant.isCompartment()) {
      Object[] attributes = new Object[reactant.getType().getAttributes()];
      for (int i = 0; i < attributes.length; ++i) {
        Node tmp = reactant.getAttributeNodes().get(i).calc(env);
        if (tmp instanceof ValueNode<?>) {
          attributes[i] = ((ValueNode<?>) tmp).getValue();
        } else {
          throw new IllegalArgumentException(String.format(
              "could not compute the attribute value of a species %s",
              reactant.getType()));
        }
      }
      Species species = new MapSpecies(new HashMap<>(), NodeHelper.getInt(
          reactant.getAmount(), env), reactant.getType(), attributes, context);
      if (context.getSubSpecies(species.getType()).get(species) == null) {
        boolean possible = true;
        if (rateNode instanceof IfThenElseNode) {
          IfThenElseNode iten = (IfThenElseNode) rateNode;
          if (!((ValueNode<Boolean>) iten.getCondition().calc(env)).getValue()) {
            if (iten.getElseStmt() instanceof ValueNode<?>
                && ((ValueNode<Double>) iten.getElseStmt()).getValue().equals(0D)) {
              possible = false;
            }
          }
        }
        if (possible) {
          throw new IllegalArgumentException(
              String
                  .format(
                      "the species %s is not available, i.e., all species must be added to the initial solution, even species with amount 0",
                      species));
        }
      } else {
        changeVector.computeIfAbsent(context, c -> new HashMap<>()).compute(
            context.getSubSpecies(species.getType()).get(species),
            (k, v) -> (v == null ? 0 : v) + species.getAmount().intValue());
      }
    }
  }

  private Map<Species, Map<Species, Integer>> computeChangeVector(
      ContextMatchings cm, Rule rule, double rate,
      Map<Reactant, Reactant> reactantProducts) {
    Map<Species, Map<Species, Integer>> result = new HashMap<>();
    cm.getMatchings().forEach(
        m -> checkMatching(m, rule.getRate(), rate, result, reactantProducts, cm.getEnv()));
    rule.getProducts().forEach(
        p -> checkProducts(cm.getContext(), p, rule.getRate(), rate, result, cm.getEnv()));
    return result;
  }

  private void createReactions(Species context,
      Map<SpeciesType, Set<Species>> changedSpecies, Rule rule,
      List<Reaction> reactions, List<Reaction> infiniteReactions,
      MLEnvironment env, Map<Reactant, Reactant> reactantProducts) {
    Stream.Builder<ContextMatchings> builder = Stream.builder();
    createContextMatchings(builder, context, changedSpecies,
        rule.getReactants(), env);
    builder.build().forEach(
        cm -> {
          double rate = NodeHelper.getDouble(rule.getRate(), cm.getEnv());
          Reaction r = new Reaction(context, computeChangeVector(cm, rule,
              rate, reactantProducts), rule.getRate(), rule, cm.getEnv());
          if (r.isPossible()) {
            if (Double.isInfinite(rate)) {
              infiniteReactions.add(r);
            } else {
              reactions.add(r);
            }
          }
        });
  }

  public void createReactions(Species species,
      Map<SpeciesType, Set<Species>> changedSpecies, List<Rule> rules,
      Map<Species, List<Reaction>> reactions, List<Reaction> infiniteReactions,
      MLEnvironment env, Map<Reactant, Reactant> reactantProducts) {
    List<Reaction> speciesReactions = reactions.computeIfAbsent(species,
        s -> new ArrayList<>());
    rules.forEach(r -> createReactions(species, changedSpecies, r,
        speciesReactions, infiniteReactions, env, reactantProducts));
    if (speciesReactions.isEmpty()) {
      reactions.remove(species);
    }
  }

  public void createInitialReactions(Species species, List<Rule> rules,
      Map<Species, List<Reaction>> reactions, List<Reaction> infiniteReactions,
      MLEnvironment env, Map<Reactant, Reactant> reactantProducts) {
    if (!species.isLeaf()) {
      createReactions(species, null, rules, reactions, infiniteReactions, env,
          reactantProducts);
      species.getSubSpeciesStream().forEach(
          s -> createInitialReactions(s, rules, reactions, infiniteReactions,
              env, reactantProducts));
    }
  }

}
