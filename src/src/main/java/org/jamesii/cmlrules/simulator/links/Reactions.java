package org.jamesii.cmlrules.simulator.links;

import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.simulator.standard.Reaction;

public class Reactions {

  /**
   * All reactions possible directly within the context species given as key.
   */
  private final Map<Species, List<Reaction>> contextReactions = new HashMap<>();

  /**
   * For each species all reactions this species is a top level reactant.
   */
  private final Map<Species, List<Reaction>> speciesReactions = new IdentityHashMap<>();

  public Map<Species, List<Reaction>> getContextReactions() {
    return contextReactions;
  }
  
  public Map<Species, List<Reaction>> getSpeciesReactions() {
    return speciesReactions;
  }
  
}
