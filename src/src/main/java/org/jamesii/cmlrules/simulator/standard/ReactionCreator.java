/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.simulator.standard;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import org.jamesii.cmlrules.model.standard.rule.Rule;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.util.MLEnvironment;

public class ReactionCreator extends AbstractReactionCreator {

  private void createReactions(Species context,
      Map<SpeciesType, Set<Species>> changedSpecies, Rule rule,
      List<Reaction> reactions, List<Reaction> infiniteReactions,
      MLEnvironment env, boolean keepZeroPropensityReactions) {
    Stream.Builder<ContextMatchings> builder = Stream.builder();
    createContextMatchings(builder, context, changedSpecies,
        rule.getReactants(), env);
    builder.build().forEach(cm -> {
      Reaction r = new Reaction(cm, rule);
      if (Double.isInfinite(r.getRate())) {
        infiniteReactions.add(r);
      } else if (keepZeroPropensityReactions || r.getRate() > 0) {
//      } else {
        reactions.add(r);
      }
    });
  }
  
  public void createReactions(Species species,
      Map<SpeciesType, Set<Species>> changedSpecies, List<Rule> rules,
      Map<Species, List<Reaction>> reactions, List<Reaction> infiniteReactions,
      MLEnvironment env, boolean keepZeroPropensityReactions) {
    List<Reaction> speciesReactions =
        reactions.computeIfAbsent(species, s -> new ArrayList<>());
    rules.forEach(r -> createReactions(species, changedSpecies, r,
        speciesReactions, infiniteReactions, env, keepZeroPropensityReactions));
    if (speciesReactions.isEmpty()) {
      reactions.remove(species);
    }
  }

  public void createInitialReactions(Species species, List<Rule> rules,
      Map<Species, List<Reaction>> reactions, List<Reaction> infiniteReactions,
      MLEnvironment env, boolean keepZeroPropensityReactions) {
    if (!species.isLeaf()) {
      createReactions(species, null, rules, reactions,
          infiniteReactions, env, keepZeroPropensityReactions);
      species.getSubSpeciesStream().forEach(
          s -> createInitialReactions(s, rules, reactions,
              infiniteReactions, env, keepZeroPropensityReactions));
    }
  }

}
