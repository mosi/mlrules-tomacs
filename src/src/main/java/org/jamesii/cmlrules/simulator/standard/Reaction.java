/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.simulator.standard;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jamesii.cmlrules.model.standard.rule.Rule;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.util.Assignment;
import org.jamesii.cmlrules.util.LazyInitialization;
import org.jamesii.cmlrules.util.NodeHelper;
import org.jamesii.core.math.parsetree.ValueNode;

public class Reaction {

  private final ContextMatchings matchings;

  private final Rule rule;

  private double rate;
  
  public Reaction(ContextMatchings matchings, Rule rule) {
    this.matchings = matchings;
    this.rule = rule;
    
    for (Assignment assign : rule.getAssignments()) {
      assign.getNames().forEach(
          n -> matchings.getEnv().setValue(n,
              new LazyInitialization(assign, matchings.getEnv())));
    }

    this.rate = NodeHelper.getDouble(rule.getRate(), matchings.getEnv());
  }

  public ContextMatchings getContextMatchings() {
    return matchings;
  }

  public Rule getRule() {
    return rule;
  }

  public double getRate() {
    return rate;
  }
  
  public void updateRate() {
    rate = NodeHelper.getDouble(rule.getRate(), matchings.getEnv());
  }

  public ChangedSpecies execute() {
    Species context = matchings.getContext();
    @SuppressWarnings("unchecked")
    Map<Species, Species> products =
        ((ValueNode<Map<Species, Species>>) rule.getProduct().calc(
            matchings.getEnv())).getValue();
    products.keySet().forEach(p -> p.setContext(context));

    List<Species> removedSpecies = new ArrayList<>();
    Set<Species> changedSpecies = new HashSet<>();
    matchings
        .getMatchings()
        .forEach(
            m -> {
              if (m.getSpecies().isLeaf()) {
                m.getSpecies().setAmount(
                    m.getSpecies().getAmount()
                        - NodeHelper.getDouble(m.getReactant().getAmount(),
                            m.getEnv()));
                if (Double.compare(m.getSpecies().getAmount(), 0D) <= 0) {
                  context.remove(m.getSpecies());
                  removedSpecies.add(m.getSpecies());
                } else if (!changedSpecies.contains(m.getSpecies())) {
                  changedSpecies.add(m.getSpecies());
                }
              } else {
                if (!context.remove(m.getSpecies())) {
                  throw new IllegalStateException(
                      String
                          .format(
                              "could not remove species %s from context %s while executing %s",
                              m.getSpecies(), context, rule));
                }
                removedSpecies.add(m.getSpecies());
              }
            });

    List<Species> addedSpecies = new ArrayList<>();
    products.keySet().forEach(p -> {
      if (p.isLeaf()) {
        Species original = context.getSubSpecies(p.getType()).get(p);
        if (original == null) {
          context.add(p);
          addedSpecies.add(p);
        } else {
          original.setAmount(original.getAmount() + p.getAmount());
          if (!changedSpecies.contains(original)) {
            changedSpecies.add(original);
          }
        }
      } else {
        context.add(p);
        addedSpecies.add(p);
      }
    });

    return new ChangedSpecies(changedSpecies, addedSpecies, removedSpecies);
  }
  
}
