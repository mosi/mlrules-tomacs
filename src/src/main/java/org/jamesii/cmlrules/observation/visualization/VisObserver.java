/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.observation.visualization;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesModel;
import org.jamesii.cmlrules.model.standard.species.SpeciesType;
import org.jamesii.cmlrules.observation.Observer;
import org.jamesii.cmlrules.simulator.Simulator;

/**
 * 
 * This observer saves all information of the simulator for each step.
 * 
 * @author Tobias Helms
 *
 */
public class VisObserver extends Observer {

  private int counter = 0;

  private final double interval;

  private Map<String, Double> species = new HashMap<>();

  private double time;

  private final double endTime;

  private final boolean withAttributes;

  private final boolean withHierarchy;

  private final boolean shortenReals;

  public VisObserver(SpeciesModel model, double endTime, double interval,
      boolean withAttributes, boolean withHierarchy, boolean shortenReals) {
    super();
    this.endTime = endTime;
    this.withAttributes = withAttributes;
    this.withHierarchy = withHierarchy;
    this.shortenReals = shortenReals;
    this.interval = interval;
  }

  private String createKey(Species s, Species parent) {
    return withAttributes ? s.toSimpleString(shortenReals) : s.getType()
        .getName();
  }

  private void createNames(String upwardKey, Species parent) {
    parent
        .getSubSpeciesStream()
        .forEach(
            s -> {
              String key = (withHierarchy ? upwardKey
                  + (s.getType() != SpeciesType.ROOT ? (parent.getType() == SpeciesType.ROOT ? ""
                      : "/")
                      : "")
                  : "")
                  + (s.getType() != SpeciesType.ROOT ? createKey(s, parent)
                      : "");
              if (!key.isEmpty()) {
                species.put(key, s.getAmount() + species.getOrDefault(key, 0D));
              }
              if (!s.isLeaf()) {
                createNames(key, s);
              }
            });
  }

  @Override
  public void update(Simulator simulator) {
    while ((Double.compare(simulator.getCurrentTime(), counter * interval) == 0 || Double
        .compare(simulator.getNextTime(), counter * interval) > 0)
        && Double.compare(counter * interval, endTime) <= 0) {
      createNames("", ((SpeciesModel) simulator.getModel()).getSpecies());
      time = counter * interval;
      notifyListener();
      ++counter;
      species.clear();
      Logger.getGlobal().info(
          "steps " + simulator.getSteps() + ", time "
              + simulator.getCurrentTime() + ", state "
              + ((SpeciesModel) simulator.getModel()).getSpecies().toString());
      if (Double.isInfinite(simulator.getNextTime())) {
        break;
      }
    }
  }

  public Map<String, Double> getSpecies() {
    return species;
  }

  public double getTime() {
    return time;
  }

  @Override
  public Optional<Double> nextObservationPoint() {
    double result = counter * interval;
    return result > endTime ? Optional.empty() : Optional.of(result);
  }

}
