/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.observation;

import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.util.Pair;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Searches the given model state for entities of the given species and collects
 * the values of the n-th attribute of these entities in a list of values. The
 * list is then fed to a callback. <br> Currently, only attributes of type
 * double are supported.
 */
public class AttributeListener implements Listener {

  private final String species;

  private final int attribute;

  private final Consumer<Pair<Double, List<Double>>> callback;

  public AttributeListener(String species, int attribute,
      Consumer<Pair<Double, List<Double>>> callback) {
    this.species = species;
    this.attribute = attribute;
    this.callback = callback;
  }

  public void notify(Observer o) {

    if (o instanceof TimeTriggeredObserver) {

      TimeTriggeredObserver observer = (TimeTriggeredObserver) o;

      Species root = observer.getSpecies();

      List<Double> result = new LinkedList<>();

      handleSpecies(root, species, attribute, result);

      callback.accept(new Pair<>(observer.getTime(), result));
    }
  }

  private void handleSpecies(Species root, String species, int attribute,
      List<Double> result) {

    if (root.getType().getName().equals(species)) {
      if (root.getAttribute(attribute) instanceof Double) {
        // handle population based species
        for (int i = 0; i < root.getAmount(); i++) {
          result.add((Double) root.getAttribute(attribute));
        }
      }
    }

    root.getSubSpeciesStream()
        .forEach(sub -> handleSpecies(sub, species, attribute, result));

  }

  public boolean isActive() {
    return true;
  }

  public void finish() {

  }
}
