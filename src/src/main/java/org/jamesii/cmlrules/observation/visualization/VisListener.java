/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.observation.visualization;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.WindowConstants;

import org.jamesii.cmlrules.observation.Listener;
import org.jamesii.cmlrules.observation.Observer;
import org.jamesii.cmlrules.observation.visualization.chart.MLRulesChartFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class VisListener implements Listener {

  private final XYSeriesCollection dataset = new XYSeriesCollection();

  private final Map<String, XYSeries> entries = new TreeMap<>();

  private final JFreeChart chart = ChartFactory.createXYLineChart("Trajectory",
      "Time", "Species Amount", dataset, PlotOrientation.VERTICAL, true, false,
      false);

  private final MLRulesChartFrame frame;
  
  private AtomicBoolean isActive;
  
  public VisListener(File directory) {
    frame = new MLRulesChartFrame("Demo", chart, entries, directory);
    frame.pack();
    frame.setVisible(true);
    isActive = new AtomicBoolean(true);
    frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
    frame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        isActive.set(false);
      }
    });
    
  }
  
  @Override
  public void notify(Observer observer) {
    if (observer instanceof VisObserver) {
      VisObserver allObserver = (VisObserver) observer;

      allObserver.getSpecies().forEach((k, v) -> {
        if (!entries.containsKey(k)) {
          XYSeries series = new XYSeries(k);
          dataset.addSeries(series);
          entries.put(k, series);
        }
      });
      
      entries.forEach((k, v) -> {
          v.add(allObserver.getTime(), v.isEmpty() ? 0 : v.getDataItem(v.getItemCount()-1).getY(),true);
        v.add(allObserver.getTime(),
            allObserver.getSpecies().getOrDefault(k, 0.0),true);
      });
      
      return;
    }

    throw new IllegalArgumentException(
        "The AllListener can only handle AllObserver observer!");
  }

  @Override
  public boolean isActive() {
    return isActive.get();
  }

  @Override
  public void finish() {
    entries.forEach((e,v) -> v.fireSeriesChanged());    
  }

}
