/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.observation;

import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.model.standard.species.SpeciesModel;
import org.jamesii.cmlrules.simulator.Simulator;

/**
 * An observer that is triggered for specific observation time points.
 */
public abstract class TimeTriggeredObserver extends Observer {

  private Species root = null;

  private Double time = 0D;

  @Override public void update(Simulator simulator) {

    while (simulationTimeExtendsNextObservationPoint(simulator)) {
      time = nextObservationPoint().get();
      increaseObservationPoint();
      root = ((SpeciesModel) simulator.getModel()).getSpecies();
      notifyListener();
      root = null;
      if (Double.isInfinite(simulator.getNextTime())) {
        break;
      }
    }

  }

  public Species getSpecies() {
    return root;
  }

  /**
   * @return the simulation time of the current/last observation
   */
  public Double getTime() {
    return time;
  }

  /**
   * @return true if an observation should by triggered before the simulation
   * continues
   */
  private boolean simulationTimeExtendsNextObservationPoint(
      Simulator simulator) {

    if (!nextObservationPoint().isPresent()) {
      return false;
    }

    double nextTimeToObserve = nextObservationPoint().get();

    return simulator.getCurrentTime() == nextTimeToObserve
        || simulator.getNextTime() > nextTimeToObserve;
  }

  /**
   * Signalling that an observation was done for the current next observation
   * point and the next next one should be activated
   */
  protected abstract void increaseObservationPoint();

}
