/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.observation.visualization.chart;

import java.io.File;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;

/**
 * Like {@link ChartFrame}, but uses {@link MLRulesChartPanel} that allows to
 * save results as CSV instead of {@link ChartPanel} that only allows to save
 * results as PNG.
 * 
 * @author Tobias Helms
 *
 */
public class MLRulesChartFrame extends JFrame {

  private static final long serialVersionUID = 1L;

  private ChartPanel chartPanel;

  public MLRulesChartFrame(String title, JFreeChart chart,
      Map<String, XYSeries> entries, File directory) {
    this(title, chart, entries, false, directory);
  }

  public MLRulesChartFrame(String title, JFreeChart chart,
      Map<String, XYSeries> entries, boolean scrollPane, File directory) {
    super(title);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    this.chartPanel = new MLRulesChartPanel(chart, entries, directory);
    if (scrollPane) {
      setContentPane(new JScrollPane(this.chartPanel));
    } else {
      setContentPane(this.chartPanel);
    }
  }

  public ChartPanel getChartPanel() {
    return this.chartPanel;
  }

}
