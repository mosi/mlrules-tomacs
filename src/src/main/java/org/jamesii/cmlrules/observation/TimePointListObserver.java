/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.observation;

import java.util.List;
import java.util.Optional;

/**
 * Observes at each of a number of given time points.
 */
public class TimePointListObserver extends TimeTriggeredObserver {

  private final List<Double> observationTimes;

  private int observationTimeIndex = 0;

  public TimePointListObserver(List<Double> observationTimes) {
    this.observationTimes = observationTimes;
  }

  @Override protected void increaseObservationPoint() {
    observationTimeIndex++;
  }

  @Override public Optional<Double> nextObservationPoint() {
    if (observationTimeIndex < observationTimes.size())
      return Optional.of(observationTimes.get(observationTimeIndex));
    else
      return Optional.empty();
  }
}
