/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.observation;

import org.jamesii.cmlrules.model.standard.species.SpeciesModel;

import java.util.Optional;

public class IntervalObserver extends TimeTriggeredObserver {

  private int counter = 0;

  private final double interval;

  public IntervalObserver(SpeciesModel model, double interval) {
    this.interval = interval;
  }

  @Override public Optional<Double> nextObservationPoint() {
    return Optional.of(counter * interval);
  }

  @Override protected void increaseObservationPoint() {
    counter++;
  }
}
