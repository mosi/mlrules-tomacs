package org.jamesii.cmlrules.observation;

import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.util.Pair;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

/**
 * Created by Tom
 */
public class SpeciesCountListener implements Listener {

  private final String species;

  private final Consumer<Pair<Double, AtomicReference<Double>>> callback;

  public SpeciesCountListener(String species,
      Consumer<Pair<Double, AtomicReference<Double>>> callback) {
    this.species = species;
    this.callback = callback;
  }

  @Override public void notify(Observer o) {

    if (o instanceof TimeTriggeredObserver) {

      TimeTriggeredObserver observer = (TimeTriggeredObserver) o;

      Species root = observer.getSpecies();

      AtomicReference<Double> result = new AtomicReference<>(0.0);

      handleSpecies(root, species, result);

      callback.accept(new Pair<>(observer.getTime(), result));
    }
  }

  private void handleSpecies(Species root, String species,
      AtomicReference<Double> result) {

    if (root.getType().getName().equals(species)) {
      result.set(result.get() + root.getAmount());
    }

    root.getSubSpeciesStream()
        .forEach(sub -> handleSpecies(sub, species, result));

  }

  @Override public boolean isActive() {
    return true;
  }

  @Override public void finish() {

  }
}
