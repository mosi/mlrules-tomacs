/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.cmlrules.observation;

import java.util.Set;

import org.jamesii.cmlrules.experiment.Job;
import org.jamesii.cmlrules.model.Model;

/**
 * An instrumenter creates a list of observer for an ML-Rules simulation and
 * attaches listener to the observer.
 * 
 * @author Tobias Helms
 *
 */
public interface Instrumenter {

  public Set<Observer> create(Job job, Model model);
  
}
