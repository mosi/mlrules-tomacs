// initial species counts
nbetacyt: 12989;
nbetanuc: 5282;
nAxin:    252;
nAxinP:   219;
nWnt:     1000;
nCells:   2; // number of cells

// reaction rate coefficients
kbetasyn:     600;
kWdeg:        0.27;
kApA_act:     20;
kApA:         0.03;
kAAp:         0.03;
kApdeg:       4.48E-3;
kAdeg:        4.48E-3;
kbetadeg_act: 2.1E-4;
kbetadeg:     1.13E-4;
kbetain:      0.0549;
kbetaout:     0.135;
kAsyn:        4E-4;

// species definitions (number of attributes)
Cell(2); // cell cycle phase / cytosolic compartment volume
Nuc(1);  // compartment volume
Wnt(0);
Axin(1); // phosphorylation state
Bcat(0);


// initial solution
>>INIT[
	(nWnt) Wnt + 
	nCells Cell('G1',1)[(nbetacyt) Bcat + 
	                    nAxin Axin('u') + 
	                    nAxinP Axin('p') + 
	                    Nuc(1)[(nbetanuc) Bcat]
	                   ]
];


// (1) Wnt degradation
Wnt:w -> @ kWdeg*#w;

// (2) activated AxinP dephosphorylation (prepared for dynamic compartment volume)
Wnt:w + Cell(phase,vol)[Axin('p'):a + s?]:split -> Wnt + Cell(phase,vol)[Axin('u') + s?] @ #split*((kApA_act*#w*#a)/vol);

// (3) basal AxinP dephosphorylation
Axin('p'):a -> Axin('u') @ kApA*#a;

// (4) Axin phosphorylation
Axin('u'):a -> Axin('p') @ kAAp*#a;

// (5) AxinP degradation
Axin('p'):a -> @ kApdeg*#a;

// (6) Axin degradation
Axin('u'):a -> @ kAdeg*#a;

// (7) activated beta-catenin degradation (prepared for dynamic compartment volume)
Cell(phase,vol)[Axin('p'):a + Bcat:b + s?]:split -> Cell(phase,vol)[Axin('p') + s?] @ #split*((kbetadeg_act*#a*#b)/vol);

// (8) beta-catenin synthesis
Cell(phase,vol)[s?]:split -> Cell(phase,vol)[Bcat + s?] @ #split*kbetasyn;

// (9) basal beta-catenin degradation
Bcat:b -> @ kbetadeg*#b;

// (10) beta-catenin shuttling into the nucleus
Bcat:b + Nuc(vol)[s?] -> Nuc(vol)[Bcat + s?] @ kbetain*#b;

// (11) beta-catenin shuttling out of the nucleus
Nuc(vol)[Bcat:b + s?] -> Bcat + Nuc(vol)[s?] @ kbetaout*#b;

// (12) Axin synthesis
Nuc(vol)[Bcat:b + s?] -> Nuc(vol)[Bcat + s?] + Axin('u') @ kAsyn*#b;

