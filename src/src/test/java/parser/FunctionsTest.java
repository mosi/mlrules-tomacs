package parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.io.FileUtils;
import org.jamesii.cmlrules.model.standard.StandardModel;
import org.jamesii.cmlrules.model.standard.species.Species;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesLexer;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesListener;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser;
import org.jamesii.cmlrules.parser.standard.antlr.MLRulesParser.ModelContext;
import org.jamesii.cmlrules.parser.standard.functions.PredefinedFunctions;
import org.jamesii.cmlrules.parser.standard.listener.ConstantCrawlerListener;
import org.jamesii.cmlrules.parser.standard.listener.FunctionCrawlerListener;
import org.jamesii.cmlrules.parser.standard.listener.FunctionDefinitionListener;
import org.jamesii.cmlrules.parser.standard.listener.RulesListener;
import org.jamesii.cmlrules.parser.standard.listener.SpeciesTypeCrawlerListener;
import org.jamesii.cmlrules.parser.standard.types.Tuple;
import org.jamesii.cmlrules.util.MLEnvironment;
import org.jamesii.cmlrules.util.Nu;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;
import org.junit.Test;

public class FunctionsTest {

  private MLEnvironment parse(String file) throws IOException {
    String model = FileUtils.readFileToString(new File(file));
    MLRulesLexer lexer = new MLRulesLexer(new ANTLRInputStream(model));
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    MLRulesParser parser = new MLRulesParser(tokens);
    ModelContext context = parser.model();
    ParseTreeWalker walker = new ParseTreeWalker();

    MLEnvironment env = new MLEnvironment();
    env.setGlobalValue(StandardModel.RNG,
        new MersenneTwister(System.currentTimeMillis()));
    PredefinedFunctions.addAll(env);
    MLRulesListener functionCrawler = new FunctionCrawlerListener(env);
    walker.walk(functionCrawler, context);
    MLRulesListener speciesCrawler = new SpeciesTypeCrawlerListener(env);
    walker.walk(speciesCrawler, context);
    FunctionDefinitionListener functionDefCrawler =
        new FunctionDefinitionListener(env);
    walker.walk(functionDefCrawler, context);

    if (!functionDefCrawler.getData().isEmpty()) {
      throw new IllegalStateException("Data not empty:"
          + functionDefCrawler.getData().toString());
    }

    ConstantCrawlerListener constantsCrawler = new ConstantCrawlerListener(env, new HashMap<>());
    walker.walk(constantsCrawler, context);

    if (!constantsCrawler.getData().isEmpty()) {
      throw new IllegalStateException("Data not empty: "
          + constantsCrawler.getData().toString());
    }

    RulesListener rulesListener = new RulesListener(env);
    walker.walk(rulesListener, context);

    return env;
  }

  @Test
  public void parseExpressionsWithoutSpecies() {
    try {
      MLEnvironment env = parse("./correct/expressionsWithoutSpecies.mlrj");
      assertEquals(env.getValue("a1"), 3.0);
      assertEquals(env.getValue("a2"), 3.4);
      assertEquals(env.getValue("a3"), 1.5e+10 + 1.5E+10 + 5E+20);
      assertEquals(env.getValue("a4"), 6.4);
      assertEquals(env.getValue("a5"), "test");
      assertEquals(env.getValue("a6"), true);
      assertEquals(env.getValue("a7"), -3.0);
      assertEquals(env.getValue("a8"), 3 + 3 * (4 + 4) / 3 * 2 + 6.4);
      List<Double> values = Arrays.asList(1.0, 2D, 3D, 4D, 5., 6., 7., 8., 9., 10.);
      for (int i = 0; i < values.size(); ++i) {
        assertEquals(((Tuple) env.getValue("a9")).get(i), values.get(i));
      }
      assertEquals(((Tuple) env.getValue("a10")).size(), 0);
      assertEquals(env.getValue("a11"), 1.0);
      assertEquals(env.getValue("a15"), 1.0);
      assertEquals(env.getValue("a16"), 120.0);
      assertEquals(env.getValue("a17"), true);
      assertEquals(env.getValue("a18"), 5.0);
      assertEquals(env.getValue("a19"), 100);
      assertEquals(env.getValue("a20"), 1.0);
      assertEquals(env.getValue("a21"), 0.0);
      assertEquals(env.getValue("a22"), 5.0);
      assertEquals(env.getValue("a23"), 2.0);
    } catch (IOException e) {
      fail(e.toString());
    }
  }

  @SuppressWarnings("unchecked")
  @Test
  public void parseExpressionsWithSpecies() {
    try {
      MLEnvironment env = parse("./correct/expressionsWithSpecies.mlrj");
      assertEquals(env.getValue("a5"), 3.0);
      assertEquals(env.getValue("a6"), "A1");
      assertEquals(env.getValue("a9"), 492);
      assertEquals(env.getValue("a18"), 10);
      Species s =
          ((Map<Species, Species>) env.getValue("a19")).keySet().iterator()
              .next();
      assertTrue(s.getAttribute(0) instanceof Nu);
      assertEquals(env.getValue("a20"), true);

    } catch (IOException e) {
      fail(e.toString());
    }
  }

}
