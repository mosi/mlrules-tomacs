package util.quick;

import org.apache.commons.math3.random.MersenneTwister;

public class Quick {

public static void main(String[] args) {

  MersenneTwister apache = new MersenneTwister(1);
  org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister jamesii = new org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister(1);
  MTRandom mt = new MTRandom(1);
  MersenneTwisterFast fast = new MersenneTwisterFast(1);
  
  int l = 10000000;
  boolean flag = false;
  
  for (int j = 0; j < 10; ++j) {
  long time1 = System.currentTimeMillis();
  for (int i = 0; i < l; ++i) {
    if (flag) {
    System.out.println(apache.nextInt());
    } else {
    apache.nextDouble();
    }
    
  }
  time1 = System.currentTimeMillis() - time1;
  
  long time2 = System.currentTimeMillis();
  for (int i = 0; i < l; ++i) {
    if (flag) {
    System.out.println(jamesii.nextInt());
    } else {
    jamesii.nextDouble();
    }
  }
  time2 = System.currentTimeMillis() - time2;
  
  long time3 = System.currentTimeMillis();
  for (int i = 0; i < l; ++i) {
    if (flag) {
    System.out.println(mt.nextInt());
    } else {
    mt.nextDouble();
    }
  }
  time3 = System.currentTimeMillis() - time3;
  
  long time4 = System.currentTimeMillis();
  for (int i = 0; i < l; ++i) {
    if (flag) {
    System.out.println(fast.nextInt());
    } else {
    fast.nextDouble();
    }
  }
  time4 = System.currentTimeMillis() - time4;
  
  
  System.out.println(time1 + " : " + time2 + " : " + time3 + " : " + time4);
  }
  
}

  
}