package util;

import java.util.Random;

public class Motion {

  public static int replications = 10;

  public static int steps = 100000;
  public static int hits = 0;

  public static int numA = 1;
  public static int numB = 1;
  public static int numC = 1;

  public static int[] x1;
  public static int[] y1;
  public static int[] x2;
  public static int[] y2;
  public static int[] x3;
  public static int[] y3;

  public static int min = 0;
  public static int max = 2;

  public static Random rng = new Random();

  private static int move(int pos) {
    int newPos = pos;
    newPos = pos + rng.nextInt(3) - 1;
    if (newPos < min) {
      newPos = max - 1;
    } else if (newPos >= max) {
      newPos = min;
    }
    return newPos;
  }

  public static void step() {
    // System.out.println(String.format("(%s,%s),(%s,%s)",x1,y1,x2,y2));

    for (int i = 0; i < numA; ++i) {
      x1[i] = move(x1[i]);
      y1[i] = move(y1[i]);
    }
    for (int i = 0; i < numB; ++i) {
      x2[i] = move(x2[i]);
      y2[i] = move(y2[i]);
    }
    for (int i = 0; i < numC; ++i) {
      x3[i] = move(x3[i]);
      y3[i] = move(y3[i]);
    }

    for (int i = 0; i < numA; ++i) {
      for (int j = 0; j < numB; ++j) {
        if (x1[i] == x2[j] && y1[i] == y2[j] && x1[i] == x3[i] && y1[i] == y3[i]) {
          ++hits;
        }
      }
    }
  }

  public static void main(String[] args) {
    for (int k = 0; k < replications; ++k) {
      x1 = new int[numA];
      y1 = new int[numA];
      x2 = new int[numB];
      y2 = new int[numB];
      x3 = new int[numC];
      y3 = new int[numC];
      for (int i = 0; i < numA; ++i) {
        x1[i] = rng.nextInt(max);
        y1[i] = rng.nextInt(max);
      }
      for (int i = 0; i < numB; ++i) {
        x2[i] = rng.nextInt(max);
        y2[i] = rng.nextInt(max);
      }
      for (int i = 0; i < numC; ++i) {
        x3[i] = rng.nextInt(max);
        y3[i] = rng.nextInt(max);
      }

      for (int i = 0; i < steps; ++i) {
        step();
      }
    }

    System.out.println(String.format("Expected: %s, Observed: %s",(double)numA*numB*numC*steps/Math.pow((max*max),2),(double) hits / replications));
  }

}
