package util;

import java.io.IOException;

public class BitShift {
  
  public static void main(String[] args) throws IOException {
    System.in.read();
    for (int i = 0; i < 100; ++i) {
      if ((i & (i - 1)) == 0) {
        System.out.println(i);
      }
    }
  }
  
}
