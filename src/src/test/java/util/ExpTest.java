package util;

import org.apache.commons.math3.distribution.ExponentialDistribution;

public class ExpTest {

  public static void main(String[] args) {
    
    double sum = 0D;
    long start = System.currentTimeMillis();
    for (int i = 0; i < 1000000; ++i) {
      ExponentialDistribution dist = new ExponentialDistribution(1.0/(10.0 + i));    
      sum += dist.inverseCumulativeProbability(0.03);
    }
    System.out.println(System.currentTimeMillis() - start);
    System.out.println(sum);
    
  }
  
}
