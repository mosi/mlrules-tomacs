package util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.Random;
import java.util.TreeSet;

public class RandomTest {

  public static int NUM = 50000;
  
  public static void execute(int k) throws InterruptedException, IOException {
    TreeSet<Point> data = new TreeSet<Point>(new Comparator<Point>() {
      @Override
      public int compare(Point a, Point b) {
        int c = Double.compare(a.x, b.x);
        // confusing, but needed to save same values within the treeset
        return c == 0 ? c + 1 : c;
      }
    });
    Random ran = new Random();
    
    long start = System.currentTimeMillis();
    for (int i = 0; i < NUM; ++i){
      data.add(new Point(ran.nextDouble()));
    }
    FileWriter writer = new FileWriter("./" + k);
    BufferedWriter buffer = new BufferedWriter(writer);
    for (Point p : data) {
      buffer.write(p.x + "\n");
    }
    buffer.flush();
    buffer.close();
    writer.close();
    System.out.println(System.currentTimeMillis() - start);
  }
  
  public static void main(String[] args) throws InterruptedException, IOException {
    System.in.read();
    for (int i = 0; i < 20; ++i) {
      execute(i);
    }
  }
  
}
