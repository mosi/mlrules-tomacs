package util;

public class Point {
  public double x;
  
  public Point(double x) {
    this.x = x;
  }
  
  @Override
  public String toString() {
    return String.valueOf(x);
  }
  
}
