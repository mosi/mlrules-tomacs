\documentclass[]{article}

%%%%%%%%%%%%%%%%%%%
% Packages/Macros %
%%%%%%%%%%%%%%%%%%%
\usepackage{amssymb,latexsym,amsmath}     % Standard packages
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
%\usepackage{ngerman}
\usepackage{graphicx}
\usepackage{xspace}
\usepackage{url}
\usepackage{listings}
\usepackage{xcolor}


%%%%%%%%%%%
% Margins %
%%%%%%%%%%%
\addtolength{\textwidth}{1.0in}
\addtolength{\textheight}{1.00in}
\addtolength{\evensidemargin}{-0.75in}
\addtolength{\oddsidemargin}{-0.75in}
\addtolength{\topmargin}{-.50in}

%% Man kann auch selber Befehle definieren
\newcommand{\james}{\textsc{JAMES II}\xspace}
\newcommand{\latex}{\texttt{LaTeX}\xspace}

\definecolor{lg}{gray}{0.9}
\lstset{basicstyle=\footnotesize\ttfamily,commentstyle=\color{gray},numberstyle=\footnotesize\ttfamily,numbers=left, backgroundcolor=\color{lg}, morecomment=[l]{//}}

%% Ab hier geht das eigentliche Dokument los
\begin{document}

\title{ML-Rules Demo Tool 2 - User Manual}
\date{}
\maketitle

\begin{abstract}
This manual is intended to help users working with the ML-Rules demo tool 2 that extends the existing ML-Rules demo tool with various features. Besides explaining the concrete syntax, many simple examples illustrate how to use ML-Rules for modeling diverse biochemical and multi-level systems. The demo tool 2 provides a concrete implementation of ML-Rules including a text-based model editor, simulator, a simple line chart visualization, and simulation data output in a CSV format.
\end{abstract}

%\tableofcontents

\section{Getting Started}

\subsection{System Requirements}

The ML-Rules 2 demo tool is written in Java 8 and should therefore run on every major operating system for which the Java Runtime Environment (Version 8 or higher) is available.

\subsection{Running the Tool}

If not yet done, unzip the ml-rules2.zip archive to a directory of your choice. To run the tool, in most cases it is sufficient to double-click the run.jar file. If this does not work, type and enter the following command into a terminal console: \texttt{java -jar run.jar}.

\subsection{The Main Window}

After starting the tool, the main window including the model editor appears (see Figure 1). Model description files can be opened and saved. The ``examples'' directory contains several (simple and complex) example models. The textual editor includes syntax highlighting and simple syntax and semantics checks. Inconsistencies will generate according messages within the logger. The size relation of the editor and logger can be changed by moving the bar between them. At the bottom of the main window buttons exist to load and save model files, and to configure and start the next simulation. 

\begin{figure}
\centering
\includegraphics[width=\linewidth]{./mainwindow.PNG}
\caption{The main window of the ML-Rules 2 demo tool.}
\label{fig:editor}
\end{figure}

\subsection{Differences to ML-Rules 1}

Besides the extensions, the most important difference of the new ML-Rules compared to the previous version is that species used as compartments are not treated in a population-based manner any more.
Thus, the amount of compartmental reactants is always $1$:
\begin{lstlisting}
C[A:a + s?]:c -> C[B + s?] @k * #a * #c;// #c is always 1, because only a 
					// compartmental C containing at least an
					// A matches the reactant
C[s?]:c -> C[A + s?] @ k * #c;  // #c is not necessarily only 1, because also C
				// without any content, i.e., currently no
				// compartmental C match the reactant
\end{lstlisting}

\section{Elements of a Model Description}

A model description consists of five different kinds of elements that have to be described in the following order:
\begin{enumerate}
	\item Parameters (optional)
	\item Functions (optional)
	\item Species definitions
	\item Initial solution
	\item Rule schemata
\end{enumerate}
At any point in the model description, comments can be inserted by using \texttt{//} or \texttt{/* ... */}.

\subsection{Types}

Values and variables can have various types in ML-Rules 2. Types are used only explicitly for signatures of functions (see Section \ref{sec:functions}). In all other cases, the types of values are inferred automatically. The following types are available in ML-Rules 2:

\begin{itemize}
	\item \textbf{num} \\ Numerical values, e.g., \texttt{1, 3.4, 2,63e-4}.
	\item \textbf{bool} \\ Boolean values, i.e., \texttt{true} or \texttt{false}.
	\item \textbf{string} \\ Text values, e.g., \texttt{'hello', 'abc123'}.
	\item \textbf{species} \\ A single Species. Species always have a name starting with a capital letter, followed by parentheses containing the attributes of the species and brackets containing potential subspecies. The amount of a single species does not have to be 1, but any number is allowed. Species can be nested, i.e., species can contain other species. Therefore, square brackets indicate that a species encloses further species.
\begin{lstlisting}
1 A()	// a simple species A without attributes and an amount = 1
A	// the same as 1 A(), i.e., 1 can be omitted and 
	// empty parentheses can be ommitted
(2+2) C(1,true)	// four species of type C with two attributes and 
		// the values 1 and true (expressions representing the amount
		// of a species must be written in parentheses)
Cell[10 A + 10 B] // a species of type Cell with amount = 1, 
		  // no attributes and two sub species, 
		  // i.e., the cell contains 10 A species and 10 B species
Cell[Nuc[10 A]] // a species of type Cell containing a species of type Nuc
		// containing 10 species of type A
\end{lstlisting}
	\item \textbf{sol} \\ A set of species.
	\begin{lstlisting}
1 A() + 1 C(1, true)[10 B] // a set of two species, the first one a simple A
			   // with amount = 1, the second one a species of 
			   // type C with two parameters and 10 B sub species
	\end{lstlisting}
	\item \textbf{link}
	A special type that can be used to represent bindings between species. Links can be generated with the function \texttt{nu()} (see Section \ref{sec:predefined}). Each call of this function generates a unique value which will never be generated again.
	\item \textbf{tuple} \\ A tuple of values. The types of the values do not have to be equal. If a variable $x$ is a tuple, the individual values cannot be accessed directly, but a function must be written to deal with a tuple. Tuples are defined with angle brackets:
		\begin{lstlisting}
<'test'> // a tuple containing only the value 'test'
<'test',2.0,false> // a tuple containing tree values
		\end{lstlisting}
\end{itemize}

\subsection{Expressions}

It is possible to calculate with values, e.g., arithmetic operations, boolean operations, and conditional operations are possible.
Further, it is possible to call predefined functions and self-defined functions.
Finally, species can be combined to a solution by adding them:

\begin{lstlisting}
2+3-2.1*(1+2) // simple arithmetics
true || false && (true || false) // boolean arithmetics
if (2 + 2 < 3) then 5 else 0 // conditions
norm(unif(0,1),1) // function calls
A('on',true) + 10 B('off') // species arithmetics
\end{lstlisting}
However, expressions can be arbitrarily nested, i.e., very complex expressions are possible, e.g.:

\begin{lstlisting}
2 + (if 1 < f(3) then 2 else 5) - g(1 + 2, h(true && i(1))) + amount(10 A(1,2,3))
\end{lstlisting}

\subsection{Parameters}

At the beginning of a model description, an optional list of constant model parameters can be specified.
Each parameter consists of a name starting with a minor letter followed by a colon and its value.
Finally, a semicolon marks the end of the parameter definition.
A parameter can be referenced from any element in the model description.
It is allowed to use expressions to define parameters.
Some valid examples are:
\begin{lstlisting}
k1: 1.0;
k2: 10 + 10.5;
k3: false || true;
k4: if (k3) then k1 else k2;
\end{lstlisting}

\subsection{Species Definitions}

Species definitions specify the type of model entities that are defined by their name and their arity, i.e., the number of attributes.
The arity of each species is defined by a non-negative integer value within a pair of parentheses following its name.
For example, a species \texttt{A} with one attribute and a species \texttt{B} without attributes are specified as follows: 
\begin{lstlisting}
A(1);
B();
\end{lstlisting}
Note: Attributes do not have names.

\subsection{Initial Solution}

The initial model state is defined by the initial solution \texttt{>>INIT[...]}.
Distinct species are separated by a \texttt{+} symbol and the amount of a species can be specified by an integer number put in front.
The following example specifies an initial solution comprising two species of type \texttt{A} (one with attribute 'u' and one with 'p'), each of them with an amount of $1000$, a third species of the non-attributed \texttt{B} type with an initial amount of 200 and a fourth species with is the result of the function call \texttt{f(100)} (assuming that \texttt{f} is a function with one numerical parameter that returns a species). The amount of the fifth species is determined by an expression.
When using an expression to determine the amount of a species, it must always be put within parentheses.

\begin{lstlisting}
>>INIT[
    1000 A('u') + 
    1000 A('p') +
    200 B + 
    f(100) + // functions returning species or solutions are allowed
    (3 + g(true)) C // expressions used to calculate the amount
		    // of species must be written in parentheses
];
\end{lstlisting}

\subsection{Functions}
\label{sec:functions}

The opportunity to define functions is the main extension of ML-Rules 2 compared to the old version.
The manner to define functions is inspired from the programming language \texttt{Haskell}.

\subsubsection{Function Definitions}

To define a function, initially a function signature must be defined that describes the name of the function, the parameter types and the result type:
\begin{itemize}
	\item \texttt{f :: num -> num} \\ The function \texttt{f} has a numerical parameter and returns a numerical value.
	\item \texttt{g :: num -> string -> bool} \\ The function \texttt{g} has two parameters (firstly a numerical parameter and secondly a text parameter) and returns a boolean value.
	\item \texttt{h :: link} \\ The function \texttt{h} has no parameters and returns a \textbf{link} value.
\end{itemize}
Besides these simple signatures, it is also possible to define functions as parameters:
\begin{itemize}
	\item \texttt{f :: (num -> num) -> num} \\ The function \texttt{f} has one parameter (that must be a function which has a numerical parameter and returns a numerical parameter) and returns a numerical value.
	\item \texttt{g :: (string -> bool) -> (num -> string) -> species} \\ The function \texttt{g} has two parameters, both are functions with one parameter (the first one has a text parameter and returns a boolean value, the second one has a numerical parameter and returns a text value). It returns a species.
\end{itemize}
After defining the signature of a function, several function definitions can be defined.
A function definition starts with the name of the function, followed by parameter names or pattern, an equals sign and an expression.
Parameter pattern can be used to save nested conditions.
Function definitions are analyzed from top to bottom and the first definition matching the given arguments is executed.
For simple types (\textbf{num},\textbf{string},\textbf{bool}), it is possible to directly write values instead of variables.
For example, the faculty function could look like this:
\begin{lstlisting}
faculty :: num -> num;
faculty 0 = 1 // this definition is executed if the argument is 0
faculty x = x * faculty(x-1); // otherwise this definition is executed
\end{lstlisting}
For \textbf{tuple}, \textbf{species}, and \textbf{sol}, particular pattern exist:
\begin{lstlisting}
f :: tuple -> num;
f <'test'> = 1; // this definition is executed if the argument is a tuple
	        // containing only the value 'test'
f <x> = 2; // this definition is executed with all other tuples containing 
	   // exactly one value
f <1,x> = x; // this definition is executed if the argument is a tuple
	     // containing two values and the first value is 1
f x = 0; // otherwise this definition is executed 
	     
// assumption: both species types A and B have two attributes
g :: species -> num;
g A(x,y) = 1; // this definition is executed if the argument is a species
	      // of type A with an arbitrary attribute values
g B(1,x) = 0; // this definition is executed if the argument is a specices
	      // of type B and the first attribute value is 1
g x = -1; // otherwise this definition is called

h :: sol -> num;
h [] = 0; // this definition is executed if the given solution is empty
h x + xs = 1; // this definition is executed if the given solution contains at
	      // least one element - x represents one of the elements of this
	      // solution and xs represents the rest solution without x
\end{lstlisting}
By defining functions, it is possible to get complex information about a solution:
\begin{lstlisting}
// calculate the number (not the amount!) of the different species of a solution
count1 :: sol -> num;
count1 [] = 0;
count1 x + xs = 1 + count1(xs);

// calculate the total amount of the given solution
count2 :: sol -> num;
count2 [] = 0;
count2 x + xs = amount(x) + count2(xs);

// calculate the total amount of all species of all levels of the given solution
count3 :: sol -> num;
count3 [] = 0;
count3 x + xs = amount(x) + amount(x) * count3(sub(x)) + count3(xs);

// return true if the given species contains a species of type A
has1 :: sol -> bool;
has1 [] = false;
has1 x + xs = if (name(x) == 'A') then true else has1(xs);

// a more general version of the previous function
has2 :: sol -> string -> bool;
has2 [] n = false;
has2 x + xs n = if (name(x) == n) then true else has2(xs);

// an even more general version of the previous function
has3 :: sol -> (species -> bool) -> bool;
has3 [] f = false;
has3 x + xs f = if (f(x)) then true else has3(xs,f);
\end{lstlisting}
Many more examples can be found within the example models.
\textbf{Note:} If none of the parameter pattern match the given arguments, an exception is thrown and the simulation is stopped.
Besides these options, it is also possible to define local variables within a function definition by using the keyword \textbf{where}:
\begin{lstlisting}
// an alternative version of count3
count3 :: sol -> num;
count3 [] = 0;
count3 x + xs = y + y * count3(sub(x)) + count3(xs) where y = amount(x);
\end{lstlisting}
By using local variables, it is possible to define even more complex functions:
\begin{lstlisting}
// create a copy of the given species with the given amount
new :: species -> num -> species;
new x n = (n)(name(x))(att(x))[sub(x)]; // the amount, name, attributes, and sub
					// species of the result species
					// are determined by functions

// split the given solution into two equally divided parts
split :: sol -> tuple;
split [] = <[],[]>;
split x + xs = <new(x,l) + restL, new(x,r) + restR> 
	where l = toInt(amount(x)/2.0), // half amount and cut decimal places
	      r = amount(x) - l, 
	      <restL,restR> = split(xs);

\end{lstlisting}

\subsubsection{Predefined Functions}
\label{sec:predefined}

Many useful functions are already integrated into ML-Rules 2, so that they do not have to be implemented again. The following functions are available:

\begin{itemize}

	\item \texttt{unif(a,b)}
		\begin{itemize}
		\item signature: \texttt{unif :: num -> num -> num}
		\item description: return a random number sampled from a uniform distribution from \texttt{a} (inclusive)  \texttt{b} (exclusive)
		\end{itemize}
	\item \texttt{norm(a,b)}
		\begin{itemize}
			\item signature: \texttt{norm :: num -> num -> num}
			\item description: return a random number sampled from a normal distribution with mean \texttt{a} and standard deviation \texttt{b}
		\end{itemize}
	\item \texttt{toInt(a)}
		\begin{itemize}
			\item signature: \texttt{toInt :: num -> num}
			\item description: remove the decimal places of the given numerical value and return the computed integer (Note: the number is not rounded, the decimal places are only omitted!) 
		\end{itemize}
	\item \texttt{infinity()}
		\begin{itemize}
			\item signature: \texttt{infinity :: num}
			\item description: return infinity
		\end{itemize}
	\item \texttt{simtime()}
		\begin{itemize}
			\item signature: \texttt{simtime :: num}
			\item description: return the current simulation time
			\item help: it is \textbf{not} recommended to use this method within the reactants or rates (because the reactants and rates are evaluated when the rule is instantiated; only the products are evaluated when the according reaction fires)
		\end{itemize}
	\item \texttt{nu()}
		\begin{itemize}
			\item signature: \texttt{nu :: link}
			\item description: return a new freshly generated unique \textbf{link} value
		\end{itemize}		
	\item \texttt{name(s)}
		\begin{itemize}
			\item signature: \texttt{name :: species -> string}
			\item description: return the name of the given species
		\end{itemize}
	\item \texttt{amount(s)}
		\begin{itemize}
			\item signature: \texttt{amount:: species -> num}
			\item description: return the amount of the given species
		\end{itemize}
	\item \texttt{\#s}
		\begin{itemize}
			\item signature: \texttt{\# :: species -> num}
			\item description: a syntactically more simple variant of the \texttt{amount} function --- it is the \textbf{only} function which is called without parentheses 
		\end{itemize}
	\item \texttt{att(s)}
		\begin{itemize}
			\item signature: \texttt{att :: species -> tuple}
			\item description: return the attributes of the given species as a tuple
		\end{itemize}
	\item \texttt{sub(s)}
		\begin{itemize}
			\item signature: \texttt{sub :: species -> sol}
			\item description: return the sub species of the given species
		\end{itemize}
	\item \texttt{count(s,n)}
		\begin{itemize}
			\item signature: \texttt{count :: sol -> string -> num}
			\item description: count the number of a specific species type without considering sub species and attributes
		\end{itemize}
	\item \texttt{dist(x1,y1,x2,y2)}
		\begin{itemize}
			\item signature: \texttt{count :: sol -> num -> num -> num -> num -> num}
			\item description: compute the distance of the two points (x1,y1) and (x2,y2)
		\end{itemize}
	\item \texttt{filter(s,n)}
		\begin{itemize}
			\item signature: \texttt{count :: sol -> string -> sol}
			\item description: filter all species of a specific species type from the given solution without considering sub species and attributes
		\end{itemize}
	\item \texttt{sin(angle)}
		\begin{itemize}
			\item signature: \texttt{count :: num -> num}
			\item description: return the sine value of the given angle
		\end{itemize}
	\item \texttt{cos(angle)}
		\begin{itemize}
			\item signature: \texttt{count :: num -> num}
			\item description: return the cosine value of the given angle
		\end{itemize}
	\item \texttt{log(a)}
		\begin{itemize}
			\item signature: \texttt{log :: num -> num}
			\item description: return the natural logarithm of the given number
		\end{itemize}
	
\end{itemize}

\subsection{Rule Schemata}

Rules (or rule schemata) define the dynamics of a model. A rule (schemata) consists of a multiset of reactants, a multiset of products, a stochastic rate, and optional local variable definitions.
The first two parts are separated by \texttt{->}, the rate follows the \texttt{@} symbol, and the optional variables follow the \textbf{where} keyword:
\begin{lstlisting}
reactants -> products @ rate where variables;
\end{lstlisting}

\section{Non-hierarchical Systems and Rules (Basics)}

\subsection{Simple (biochemical) reactions}

A simple biochemical reaction typically follows the law of mass action where the amount of reactant species determines the speed of the reaction, i.e., the rate of the reaction rule.
By assigning an identifier to each reactant, the matched species can be accessed for specifying the rate (but also for defining the products, e.g., by assigning certain attributes).
For example, the following rule describes a degradation reaction of \texttt{B} with a reaction rate constant \texttt{k}:
\begin{lstlisting}
B:b -> @ k * #b;
\end{lstlisting}
Degradation rules for an attributed species look as follows:
\begin{lstlisting}
A('u'):a -> @ k * #a;
A('p'):a -> @ k * #a;
\end{lstlisting}
Instead of specifying each potential state of \texttt{A} explicitly, one can specify a schematic rule with a variable rather than a defined attribute value:
\begin{lstlisting}
A(x):a -> @ k * #a;
\end{lstlisting}
Applied to a solution that comprises both species \texttt{A('u')} and \texttt{A('p')} would then lead to two rule instantiations that are equivalent to the two explicit rules above.
Missing in the above degradation examples, the state of a species can be modified by simply putting a product species to the right-hand side of the rule.
An auto-phosphorylation reaction of species \texttt{A} may look as follows:
\begin{lstlisting}
A('u'):a -> A('p') @ k * #a;
\end{lstlisting}
The previous rules describe first order reactions only, i.e., reactions that depend on the amount of a single species.
Bimolecular reactions can be specified in a similar way.
The only difference is that multiple reactants (equally to multiple products) are separated by a \texttt{+}:
\begin{lstlisting}
A('p'):a + B:b -> ApB @ k * #a * #b;
A('u'):a1 + A('p'):a2 -> A('p') + A('p') @ k * #a1 + #a2;
\end{lstlisting}
The latter reaction rule in line 2 can be also specified in a more compact manner by using a stoichiometric factor for describing the products:
\begin{lstlisting}
A('u'):a1 + A('p'):a2 -> 2 A('p') @ k * #a1 * #a2;
\end{lstlisting}
Stochiometric factors can also be used to describe unimolecular reactions with identical reactant species:
\begin{lstlisting}
2 B:b -> BB @ k * #b * (#b - 1);
\end{lstlisting}

\subsection{Alternative Kinetics}

The previous example reactions all follow the law of mass action.
However, ML-Rules allows to specify arbitrary stochastic rates in a flexible manner so that alternative rate kinetics like Michaelis-Menten or Hill-type kinetics can be described easily.
For example, an enzymatic reaction:

can be either described by three detailed mass action rules or in an approximated way by assuming a quasi-steady state of the very fast binding/unbinding events between enzyme \texttt{E} and substrate \texttt{S}:
\begin{lstlisting}
// mass action kinetics
E:e + S:s -> ES @ k1 * #e * #s;
ES:c -> E + S @ k2 * #c;
ES:c -> E + P @ k3 * #c;

// Michaelis-Menten kinetics
E:e + S:s -> E + P @ (k3 * #e * #s) / (KM + #s);
\end{lstlisting}

\subsection{Reaction Constraints}

Besides specifying prerequisites for firing by defined attributes, rules can be further constrained in a flexible manner.
For example, one can use the \texttt{if-then-else} conditional expression for constraining a rule to only fire if a certain threshold amount \texttt{T} of a reactant species \texttt{A('p')} is exceeded:
\begin{lstlisting}
A('p'):a -> @ if (#a > T) then k * #a else 0;
\end{lstlisting}
Nested conditions are also possible just as conditional constraints to specify attributes of reactants and products.
For example, the following rule describes switching between the two states of \texttt{A}, i.e., the assigned attribute of the product depends on the attribute of the matched reactant species:
\begin{lstlisting}
A(x):a -> A(if (x == 'p') then 'u' else 'p') @ k * #a;
\end{lstlisting}

\subsection{Complexation via Unique Bonds}

Links between molecules or other entities can be used to reduce model complexity and to preserve states of attributed species when modeling binding reactions.
By using the \texttt{nu()} function, a unique value can be created and assigned to each binding partner:
\begin{lstlisting}
A(x,0):a + B(0):b -> A(x,l) + B(l) @ k * #a * #b where l = nu();
\end{lstlisting}
Please notice, both species need additional binding site attributes and the value \texttt{0} is assumed to indicate the unbound state in this example.
For the backward reaction (dissociation) one only needs to match reactants that share the same unique value (and is not \texttt{0}, i.e., unbound):
\begin{lstlisting}
A(x,y) + B(y) -> A(x,0) + B(0) @ if (y != 0) then k else 0;
// amounts are omitted since the must be 1 anyway
\end{lstlisting}
Please note that the simulation performance may be slowed down by modeling complexes with unique links as the number of species may increase dramatically and therefore the time needed for matching reactants may also increase.
Creation and assignment of unique values can also be used to individualize certain species, e.g., to observe state changes of an individual entity over time.

\subsection{Preserving (rest) solutions}

When applying rules to nested species, i.e., species which contain a sub-solution, one typically might want to preserve the entire sub-solution without specifying its defined content.
Therefore, a variable with the suffix \texttt{?} can be specified and re-inserted into the product.
For example, the following rule describes an abstract process of cell growth (increasing an attribute value of a \texttt{Cell}) by which the whole content of the cell remains untouched:
\begin{lstlisting}
Cell(v)[sol?]:c -> Cell(v+1)[sol?] @ k * #c;
\end{lstlisting}
Similarly, when certain species of a sub-solution lie in the focus of a rule schemata, one might want to preserve the rest of the solution, i.e., the whole sub-solution minus explicitly defined reactant species:
\begin{lstlisting}
Nucleus[B:b + sol?]:n -> Nucleus[sol?] @ k * #b;
\end{lstlisting}
Besides defined species types, there always exist the special species type \texttt{\$\$ROOT\$\$} which is always the top level species containing the whole model solution.
It can be used to guarantee that a reaction is executed at the top level or to change the whole solution by using functions (see Section \ref{sec:pf}).
\begin{lstlisting}
$$ROOT$$[A:a + sol?] -> $$ROOT$$[sol?] @ k * #a;
\end{lstlisting}

\subsection{Migration}

Rules can be defined for describing migration of species, i.e., entry into or exit from a nested species.
For example, a particle \texttt{P} enters a \texttt{Cell} by crossing the membrane:
\begin{lstlisting}
P:p + Cell[sol?]:c -> Cell[P + sol?] @ k * #p * #c;
\end{lstlisting}
Endocytosis can be modeled by creation of an \texttt{Endosome} that encloses the entering particle \texttt{P}:
\begin{lstlisting}
P:p + Cell[sol?]:c -> Cell[Endosome[P] + sol?] @ k * #p * #c;
\end{lstlisting}
Also, entire solutions may migrate.
For example, during exocytosis, a \texttt{Vesicle} fuses with the cell membrane and thereby releases its content to the extracellular solution:
\begin{lstlisting}
Cell[Vesicle[sv?]:v + sC?]:c -> Cell[sC?] + sv? @ k * #v * #c;
\end{lstlisting}

\subsection{Product Functions}
\label{sec:pf}

Besides specifying product species directly, it is also possible to call functions producing solutions.
By using functions, it is possible to describe complex reactions, e.g., remove all species of type \texttt{A} within a cell:
\begin{lstlisting}
// remove all A species from the given species
filter :: sol -> sol;
filter [] = [];
filter x + xs = (if (name(x) == 'A') then [] else x) + filter(xs);

Cell[sol?]:c -> Cell[filter(sol?)] @ k * #c; 
\end{lstlisting}
Further, by using the \texttt{\$\$ROOT\$\$} species and functions, it is possible to change the complete solution, e.g., adding a \texttt{Virus} randomly to ten percent of \texttt{Cell} species:
\begin{lstlisting}
// add a virus to a cell with probability 0.1
addVirus :: species -> num -> sol;
addVirus x 0.0 = [];
addVirus x n = s + addVirus(x, n-1)
	where s = if ((name(x) == 'Cell') && (unif(0,1) < 0.1)) 
			then 1(name(x))(att(x))[sub(x) + Virus] 
			else 1(name(x))(att(x))[sub(x)];

addAll :: sol -> sol;
addAll [] = [];
addAll x + xs = addVirus(x, amount(x)) + addAll(xs);

$$ROOT$$[sol?] -> $$ROOT$$[addAll(sol?)] @ k; 
\end{lstlisting}

\subsection{Timed Reactions}

To execute reactions at a specific time of the simulation, it is possible to define the exact time of a reaction with \texttt{@EXACT}:
\begin{lstlisting}
// add the viruses exactly at time point 10
$$ROOT$$[sol?] -> $$ROOT$$[addAll(sol?)] @EXACT 10; 
\end{lstlisting}
Exactly \textbf{one} timed reaction is only fired during one specific time point, i.e., when defining two rules which should fire exactly at time point $x$, only one of them will be executed.
This is done to avoid infinite reactions during such a time point.

To periodically execute reactions at specific times of the simulation, the period can be defined with \texttt{@EACH}:
\begin{lstlisting}
// add the viruses exactly at time point 10, 20, 30, 40, 50, ...
$$ROOT$$[sol?] -> $$ROOT$$[addAll(sol?)] @EACH 10; 
\end{lstlisting}
Analogous to the one-time reactions defined with \texttt{@EXACT}, at each time point, at most one timed reactions is executed.

\section{Editor}

The editor provides several simulation and configuration options (see Figure \ref{fig:editor}):
\begin{itemize}
	\item \texttt{Load}: load a model file to the editor
	\item \texttt{Save}: save the current model to a file
	\item \texttt{End Time}: set the simulation end time, must be greater than 0
	\item \texttt{Interval}: set the observation interval of the simulation, i.e., after which time interval the current state of the model is observed and, e.g., added to a plot
	\item \texttt{Visualization}: show a visualization of the simulation
	\item \texttt{Attributes}: consider attributes of species for the visualization, e.g., if selected \texttt{A(true)} and \texttt{A(false)} are distinguished, otherwise their amounts is summed and only \texttt{A} is shown in the visualization
	\item \texttt{Hierarchies}: analog to attributes, but with the hierarchies of species
	\item \texttt{Reals}: when selected, real valued attributes of species are considered in the visualization, otherwise they are replaced by the term \texttt{real} --- not considering real valued attributes is useful to reduce the number of distinguished species in the visualization
	\item \texttt{Save Directly}: by using this option, results of the simulation are directly saved to the chosen directory
	\item \texttt{Start Simulation}: start one simulation run
\end{itemize}
Further, the text size and the theme of the editor can be changed by right-clicking inside the editor area.

\section{Misc}

In the following some often appearing issues or problems and possible solutions are enlisted:
\begin{itemize}
\item Performance: The usage of complex functions can dramatically reduce the performance of the simulation, i.e., use functions only when needed.
\item Performance: The simulation can be slow a) if few compartments contain many species or b) if many compartments species exist. Thus, try to avoid compartment species with high amounts.
\item When defining the amount of a species with an expression, this expression must be written in parentheses:
\begin{lstlisting}
(10 + 10) A // 20 species of type A
10 + 10 A // this would not compile
\end{lstlisting}
\item The \texttt{\$\$ROOT\$\$} species itself cannot be changed or removed, i.e., it can only be used within rules of the following form: 
\begin{lstlisting}
$$ROOT$$[...] -> $$ROOT$$[...] @...
\end{lstlisting}
\item Individual boolean expressions must always be written in parentheses:
\begin{lstlisting}
(10 > 5) // ok
(a < b) // ok
((a < b) || (c < d)) // ok
(a < b || c < d) // not ok
\end{lstlisting}
\item The function name \texttt{add} is not allowed.
\end{itemize}

\end{document}