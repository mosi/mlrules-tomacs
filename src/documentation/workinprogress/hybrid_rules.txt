This version of the ML-Rules 2 Sandbox uses an experimental hybrid simulator to execute simulations.
The basic idea of the hybrid simulator is do split all rules into 'simple' and 'complex' rules.
Simple rules are computed continuously, complex rules are computed stochastically.

Generally speaking, simple rules do not change the structure of the current solution and they do not change attributes of compartment species:

A:a -> B @k*#a;
Cell[A:a + s?] -> Cell[B + s?] @k*#a; //amount of a matching Cell is always 1, because it must be a compartmental Cell containing at least one A
CAMPe(x,y):a -> CAMPe(x,y+1) @if (y<ymax) then k*#a else 0; // CAMPe is not allowed to be a compartmental species at any time of the simulation!

Computationally, by using the hybrid simulator, the matching of species becomes less important.
Instead, an efficient updating of propensities of simple rules becomes crucial. We implemented an efficient updating for the following types of rates:

1. constant * amount of species (e.g., k1 * #a, k2 * #b)
2. constant * amount of species * amount of species (e.g., k1 * #a * #b)
3. if condition then constant * amount of species else 0 (e.g., if (x > 3) then k1 * #a else 0) -> the condition is not allowed to depend on amounts of species or solution properties!!!

The order of the factors is important! So, the following rate expression is NOT calculated efficiently:

1. amount of species * constant (e.g., #a * k1)

Even worse, conditional rates with conditions depending on the amount of a species procude incorrect results:

1. if (#a > 10) then k * #a else 0 -> NOT WORKING!!!

We work on more stable and more general solutions for these optimizations...